<div class="section_1">
     
	  <div class="sub_section">
      <div class="main_menu">
        <div class="navbar navbar-inverse navbar-fixed-top">
          <div class="navbar-inner">
          <span class="menn">  Menu </span>
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <div class="nav-collapse collapse">
              <ul class="nav">
               <li><a href="<?php echo base_url();?>products/new_products">New </a></li>
                <li><a href="<?php echo base_url();?>products/featured_products" class="active">Featured </a></li>
               
                 <?php
			  foreach($this->product_model->getCategoryData() as $key=>$val)
			  {
				 $link = $this->uri->segment(3); 
			  ?>
               <li > <a href="<?php echo base_url();?>products/categories/<?php echo $val['id'];?>" <?php if($link==$val['id']){ echo 'class="active"';}?>> <?php echo ucfirst(strtolower($val['category_name']));?></a></li>	
			  <?php  }?>
              </ul>
            </div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="head_1">
        <span class="left_hd">Featured</span>
      
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
      <ul class="list_prodcs">
       <?php
	   if($this->session->userdata("consumer_id")!=""){
		$userid=$this->session->userdata("consumer_id");}else{$userid=0;}	 
		if(!empty($resultset)){
		$i=1;
		foreach($resultset as $key=>$val)
		{ 
		//debug($val);die;
		 $datacart = $this->product_model->check_product($val['product_id']);
		
	    $price = $this->product_model->getDefaultPrice($resultset[$key]['product_id']);
		$productdefaultimage = $this->product_model->get_default_image($val['product_id']);
		 $product_default=(isset($productdefaultimage) && $productdefaultimage!="")?$productdefaultimage:$this->config->item("productdefaultimage"); ?>
        <li> 
         <!--  show remove from cart --> 
         <?php if(in_array($val['product_id'],$datacart))
		 { 
		 
		 $cart_product_id = array_keys($datacart,$val['product_id']);
		 ?>
           <div class="main_prod">
               <div class="main_prod_1">
                   <div class="left_prod"> 
                       <div class="left_prod_1">
                      <?php echo strlen($val['product_name']) > 16 ? substr($val['product_name'], 0,16)." ..." : $val['product_name']; ?></div>
                       <div class="left_prod_2">$<?php echo $price;?> </div>
                   </div>
                   <div class="ryt_prod"><a href="javascript:void(0)" onclick="show(<?php echo  $i;?>)"><img src="<?php echo base_url();?>images/green-cart-icon.png" width="57" height="66" alt="pic" /></a> </div>
                  <div class="clear"></div>
                </div>
               <div class="main_prod_2"><img src="<?php echo $this->config->item("productimageurl_new").$product_default;?>" height="274" width="100%" alt="pic" />
               
                <div class="clear"></div>
                <a href="<?php echo base_url();?>products/product_details/<?php echo $val['product_id'];?>" class="hover_div"> <span>
                <span class="text_hover">
                <?php echo $val['short_description'];?>
                </span> 
              </span></a>
               </div>
               
             <?php  $num=$this->product_model->checktofav($val['product_id'],$userid);
                
                if($num==1){?>
                <div class="main_prod_3"> 
                <div class="fav_1_new" id="fav_<?php echo $val['product_id'];?>">  <a href="javascript:void(0);" onclick="remove_product_from_fav(<?php echo $val['product_id'];?>,<?php echo $userid;?>)">Favorite</a></div>
                <div class="fav_2"><a href="#">Tie</a></div>
                <div class="clear"></div>
                </div>
                
                <?php }else {?>
                <div class="main_prod_3"> 
                <div class="fav_1" id="fav_<?php echo $val['product_id'];?>">  <a href="javascript:void(0);" onclick="add_product_to_fav(<?php echo $val['product_id'];?>,<?php echo $userid;?>)">Favorite</a></div>
                <div class="fav_2"><a href="#">Tie</a></div>
                <div class="clear"></div>
                </div>
                <?php  }?> 
               
              <div class="clear"></div>
          </div>
          
            <div class="main_prod_2_2" id="shimg<?php echo  $i;?>">
                    <div class="head_av">
                     <span class="av_11">Available in </span>
                     
                     <span class="av_12"> <a href="javascript:void(0)" onclick="show_hide(<?php echo  $i;?>)" ><img src="<?php echo base_url();?>images/cross1.png" height="23" width="22" alt="pic" /> </a> </span>
                     <div class="clear"></div>
                    </div>
                  <div class="clear"></div>
                      <ul class="list_attr2">
                     <!--  <li> <a href="#">Shop another</a> </li>-->
                       <li> <a href="<?php echo base_url();?>carts/delete_from_cart/<?php echo $cart_product_id[0];?>">Remove from cart</a> </li>
                                          
                    </ul>
                    <div class="clear"></div>

                </div>    
          
         <?php   }
           else
           { ?>    
       <!-- if product is  in cart then show attribute and add to cart functionality -->
         <div class="main_prod">
               <div class="main_prod_1">
                   <div class="left_prod"> 
                        <div class="left_prod_1">
                    <?php echo strlen($val['product_name']) > 16 ? substr($val['product_name'], 0,16)." ..." : $val['product_name']; ?></div>
                       <div class="left_prod_2">$<?php echo $price;?> </div>
                   </div>
                  
                    <div class="ryt_prod"><a href="javascript:void(0)" onclick="show(<?php echo  $i;?>)"><img src="<?php echo base_url();?>images/cart_image.png" width="57" height="66"  alt="pic" /> </a> </div>
                  <div class="clear"></div>
                </div>
               <div class="main_prod_2"> <img src="<?php echo $this->config->item("productimageurl_new").$product_default;?>" height="274" width="100%" alt="pic" />
               
                <div class="clear"></div>
                 
                     <a href="<?php echo base_url();?>products/product_details/<?php echo $val['product_id'];?>" class="hover_div">
                    <span>
                <span class="text_hover">
              <?php echo $val['short_description'];?>
                </span> 
              </span></a>
               </div>
               
             <?php  $num=$this->product_model->checktofav($val['product_id'],$userid);
                
                if($num==1){?>
                <div class="main_prod_3"> 
                <div class="fav_1_new" id="fav_<?php echo $val['product_id'];?>">  <a href="javascript:void(0);" onclick="remove_product_from_fav(<?php echo $val['product_id'];?>,<?php echo $userid;?>)">Favorite</a></div>
                <div class="fav_2"><a href="#">Tie</a></div>
                <div class="clear"></div>
                </div>
                
                <?php }else {?>
                <div class="main_prod_3"> 
                <div class="fav_1" id="fav_<?php echo $val['product_id'];?>">  <a href="javascript:void(0);" onclick="add_product_to_fav(<?php echo $val['product_id'];?>,<?php echo $userid;?>)">Favorite</a></div>
                <div class="fav_2"><a href="#">Tie</a></div>
                <div class="clear"></div>
                </div>
                <?php  }?> 
              <div class="clear"></div>
          </div>
          
          <div class="main_prod_2_2" id="shimg<?php echo  $i;?>">
                    <div class="head_av">
                     <span class="av_11">Available in </span>
                     
                   <span class="av_12"> <a href="javascript:void(0)" onclick="show_hide(<?php echo  $i;?>)" ><img src="<?php echo base_url();?>images/cross1.png" height="23" width="22" alt="pic" /> </a> </span>
                     <div class="clear"></div>
                    </div>
                   <?php $k=1; $att_name= $this->product_model->getProductAttName($val["product_id"]);
			   foreach($att_name as $att_key=>$att_val){?>
          
				   <ul class="list_attr cart<?php echo $k;?>"> 
			       <?php $array= $this->product_model->getProductAttValue($val["product_id"],$att_val['id']); 
				   $att_val = array_unique($array);
				   $j=1;
				  foreach($att_val as $val_key=>$val_val){
				?>
                  <li id="cartattr<?php echo $j;?>" value="<?php echo $val["product_id"];?>"> <a href="javascript:void(0);"> <?php echo ucfirst($val_val);?> </a></li>
                    <?php $j++;} ?>
					 </ul>
					 <div class="clear"></div>
					 <form action="<?php echo base_url();?>carts/add_to_cart" method="post">
					<input type="hidden"  value="<?php echo $val["product_id"];?>" id="product_id<?php echo $i; ?>" name="productid"/>
                <input type="hidden" name="storeid" value="<?php echo $this->product_model->getStoreOfProduct($val["product_id"]); ?>">
					
				<?php $k++;}?>  
                    <div class="bottom_cart">
                    
                   <input type="hidden" name="quantity" value="1" >
                    <input type="hidden" name="variationid" value="" class="variationid<?php echo $val["product_id"];?>">
                      <input type="submit" class="add_crt" value="add to cart" onclick="javascript:return check_product_attributes(<?php echo $val["product_id"];?>);"/>
                      </form>
                    </div>
                  <div class="clear"></div>
                </div>   
             
         
                
                
         <?php   }?>
          
         <!-- else show to remove functionality -->
          <div class="clear"></div>
       </li>
       <?php $i++; } }?>
      </ul>
      <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>

    <div class="clear"></div>