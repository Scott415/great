<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class emailer extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('email_model');
	}
	
	/***********************************************Email function starts **********************************************************/	
	
	public function index()
	{
		$this->send_email();	
	}
	
	public function send_emails()
	{
		
	}
	
	public function send_email()
	{
		$data["resultset"]=$this->session->flashdata("tempdata");
		$data["master_title"]="Send email";   // Please enter the title of page......
		$data["master_body"]="send_email";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);  // Loading theme
	}
	
	public function send_email_to_recipients()
	{
		$err=0;
		$arr["stores"]=$this->input->post("stores");
		$arr["advertisers"]=$this->input->post("advertisers");
		$arr["users"]=$this->input->post("users");
		$arr["subject"]=$this->input->post("subject");
		$arr["message"]=$this->input->post("message");
		$this->session->set_flashdata("tempdata",$arr);		
		if($this->validations->validate_recipients($arr))
		{
			if($arr["stores"]!="")
			{
				$arr["mailto"]="store";
				$this->email_model->sendEmailToAllStores($arr);
			}	
			if($arr["advertisers"]!="")
			{
				$arr["mailto"]="advertisers";
				$this->email_model->sendEmailToAllAdvertisers($arr);
			}	
			if($arr["users"]!="")
			{
				$arr["mailto"]="users";
				$this->email_model->sendEmailToAllUsers($arr);
			}				
		}
		else
		{
			$err=1;	
		}
		
		if($err==0)
		{
			$this->session->set_flashdata("successmsg","Mail sent successfully");
			$this->session->set_flashdata("tempdata","");
			redirect(base_url()."emailer/send_email");	
		}
		else
		{			
			redirect(base_url()."emailer/send_email");	
		}
		
	}
	
	public function email_history()
	{
		$data["item"]="Email history";
		$data["resultset"]=$this->email_model->getEmailHistory($_GET);
		$data["master_title"]="Email history";   // Please enter the title of page......
		$data["master_body"]="email_history";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);  // Loading theme	
	}
	
	public function view_email()
	{
		$emailid=$this->uri->segment(3);
		$data["resultset"]=$this->email_model->getIndividualEmailHistory($emailid);
		$data["master_title"]="View email";   // Please enter the title of page......
		$data["master_body"]="view_email";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);  // Loading theme		
	}
	
	/***********************************************Email function ends **************************************************************/	
	
}