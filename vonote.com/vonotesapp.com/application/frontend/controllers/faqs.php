<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class faqs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("faq_model");
	}
	public function index()
	{
		$this->show_faqs();
	}
	
	public function show_faqs()
	{
		$data["item"]="UGK";
		$data["faqdata"]=$this->faq_model->getFaqData(array("type"=>"activated"));
		$data["master_title"]="UnderGroundKlothing-FAQ";  
		$data["master_body"]="show_faqs";
		$this->load->layout('mainlayout',$data); 
	}
	
}
