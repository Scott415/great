<?php 
/* -----------------------------------------------------------------------------------------
   IdiotMinds - http://idiotminds.com
   -----------------------------------------------------------------------------------------
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//include the facebook.php from libraries directory
require_once APPPATH.'libraries/facebook/facebook.php';

class fbci extends CI_Controller {

   public function __construct(){
	    parent::__construct();
	    $this->load->library('session');  //Load the Session 
		$this->config->load('facebook'); //Load the facebook.php file which is located in config directory
		$this->load->model('user_model');
		$this->load->helper('url');
    }
	public function index()
	{
	  $this->load->view('main'); //load the main.php file for view
	}
	
	function logout(){
		$base_url=$this->config->item('base_url'); //Read the baseurl from the config.php file
		$this->session->sess_destroy();  //session destroy
		header('Location: '.$base_url);  //redirect to the home page
		
	}
	function fblogin(){
		 $base_url=$this->config->item('base_url'); //Read the baseurl from the config.php file
		
		//get the Facebook appId and app secret from facebook.php which located in config directory for the creating the object for Facebook class
    	$facebook = new Facebook(array(
		'appId'		=>  $this->config->item('appID'), 
		'secret'	=> $this->config->item('appSecret'),
		));
		
		$user = $facebook->getUser(); // Get the facebook user id 
		if($user){
			try{
				$user_profile = $facebook->api('/me');  //Get the facebook user profile data
				$params = array('next' => $base_url.'fbci/logout');
				$ses_user=array('User'=>$user_profile,
				   'logout' =>$facebook->getLogoutUrl($params)   //generating the logout url for facebook 
				);
			//	header("location: ".$base_url."user/my_desk");
					$arr['fbid']=$user_profile['id'];
					$arr['first_name']=$user_profile['first_name'];
					$arr['last_name']=$user_profile['last_name'];
					$arr['email']=$user_profile['email'];
					$arr['user_status']=$user_profile['verified'];
					$email = $user_profile['email'];
					$fbid = $user_profile['id'];
					//echo $this->common->checkUserFacebookLogin($user_profile['email'],$user_profile['id']); die;
					if($this->common->checkUserFacebookLogin($user_profile['email'],$user_profile['id'])!=0){
						$this->session->set_userdata("user_id",$this->user_model->get_user_data_fb($user_profile["email"]));
						$this->session->set_userdata("user_email",$user_profile["email"]);
						$this->session->set_userdata("user_status",'1');
						//header("location: ".base_url."user/my_desk");
						redirect(base_url()."user/my_desk");

				}
					else{
						if($this->user_model->add_user($arr)){
							if($this->common->checkUserFacebookLogin($email,$fbid)){
								$this->session->set_userdata("user_id",$this->user_model->get_user_data_fb($user_profile["email"]));
								$this->session->set_userdata("user_email",$user_profile["email"]);
								$this->session->set_userdata("user_status",'1');
								//header("location: ".$base_url."user/my_desk");
								redirect(base_url()."user/fb_new_password");
							}
						}
						}
			}catch(FacebookApiException $e){
				error_log($e);
				$user = NULL;
			}		
		}
		else{
			header("location: ".$base_url."home/login");
		}
		//$this->load->view('main');
		
	}
}
/* End of file fbci.php */
/* Location: ./application/controllers/fbci.php */