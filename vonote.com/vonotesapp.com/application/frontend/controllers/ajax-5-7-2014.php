<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ajax extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('page_model');
		$this->load->model('user_model');
		$this->load->model('email_model');	
		$this->load->model('login_model');	
		$this->load->helper('url');				
	}

	public function index(){
		$this->common->check_user_login();
		//$this->load->view('home');
		$this->ajax();
	}
	public function ajax(){
		$pagename='signup_content';
		$data["item"]="Signup";
		$data["master_title"]="Signup | ". $this->config->item('sitename'); 
		$data['resultset']=$this->common->getcontentpagedata($pagename);
		$data["userdata"]=$this->session->userdata("tempdata",$data);
		$data["master_body"]="signup";
		$this->load->theme('home_layout',$data);
	}
	//for seach for file
	public function search_file(){
		$data['search_txt']=$this->uri->segment('3');
		$data["user_id"] = $this->session->userdata("user_id");
		//$pagename='search';
		$data["item"]="Search";
		$data["master_title"]="Search | ". $this->config->item('sitename'); 
		$data['resultset']=$this->user_model->getsearchpagedata($data);
		//$data["userdata"]=$this->session->userdata("tempdata",$data);
	//	$data["master_body"]="search";
	//	$data['sidebar_content']='user/left_bar';
	//	$this->load->theme('home_layout',$data);
	}
	
//for search for user/friend	
	public function find_friend(){
		$data['search_txt']=$this->uri->segment('3');
		$data["user_id"] = $this->session->userdata("user_id");
		//$pagename='search';
		$data["item"]="Search";
		$data["master_title"]="Search | ". $this->config->item('sitename'); 
		$data['resultset']=$this->user_model->get_search_user_data($data);
		//$data["userdata"]=$this->session->userdata("tempdata",$data);
	//	$data["master_body"]="search";
	//	$data['sidebar_content']='user/left_bar';
	//	$this->load->theme('home_layout',$data);
	}
	
//for add friend
	public function add_friend(){ 
		$arr['reciver_id']=$this->uri->segment('3');
		$arr["sender_id"] = $this->session->userdata("user_id");
		$data["item"]="Add Friend";
		$data["master_title"]="Add Friend | ". $this->config->item('sitename'); 
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		$where = array("reciver_id" => $arr['reciver_id'], "sender_id" => $arr["sender_id"],  "status" => '0', "archive" => '0' );
		//for chek friend request already sent or not
		$num_req = $this->common->chk_request('friend_requests', $where); 
		if($num_req == 0){
			$result=$this->user_model->send_friend_request($arr);
			/*if($result){
				
			}else{
				$this->session->set_flashdata("errormsg","There is error updating content to database. Please contact database admin");	
			}*/

		}
	}
	
//for confirm friend request
	public function confirm_frnd_req(){
		$arr['sender_id']=$this->uri->segment('3');
		$arr["reciver_id"] = $this->session->userdata("user_id");
		$data["item"]="Confirm Friend";
		$data["master_title"]="Confirm Friend | ". $this->config->item('sitename'); 
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		$where = array("reciver_id" => $arr['reciver_id'], "sender_id" => $arr["sender_id"],  "status" => '0', "archive" => '0' );
		//for chek friend request already sent or not
		 $num_req = $this->common->chk_request('friend_requests', $where);
		//echo $this->db->last_query($num_req); 
		
		if($num_req == 1){ 
			$result=$this->user_model->confirm_frnd_req_db($arr);
		}
	}
//for confirm friend request
	public function ignore_frnd_req(){
		$arr['sender_id']=$this->uri->segment('3');
		$arr["reciver_id"] = $this->session->userdata("user_id");
		$data["item"]="Ignore Friend";
		$data["master_title"]="Ignore Friend | ". $this->config->item('sitename'); 
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		$where = array("reciver_id" => $arr['reciver_id'], "sender_id" => $arr["sender_id"],  "status" => '0', "archive" => '0' );
		//for chek friend request already sent or not
		 $num_req = $this->common->chk_request('friend_requests', $where);
		//echo $this->db->last_query($num_req); 
		
		if($num_req == 1){ 
			$result=$this->user_model->ignore_frnd_req_db($arr);
		}
	}
	
//for message in details
public function my_conversation(){
 	$arr['conversation_id']=$this->uri->segment('3');
	$timestamp = $this->uri->segment('4');
	$timestamp_m = $this->uri->segment('5');
	$arr["user_id"] = $this->session->userdata("user_id");
	$data["item"]="My Conversation";
	$data["master_title"]="My Conversation | ". $this->config->item('sitename'); 
	$data['resultset']=$this->user_model->my_conversation_data($arr);
}

//for message reply
public function reply_message(){
 	$arr['conversation_id']=$this->uri->segment('3');
	$arr['sender_id']=$this->uri->segment('4');
	$arr['reciver_id']=$this->uri->segment('5');
	$arr['message']=$this->uri->segment('6');
	
	$data["item"]="My Conversation";
	$data["master_title"]="My Conversation | ". $this->config->item('sitename'); 
	$data['resultset']=$this->user_model->my_reply_data($arr);
}

	
//for notification
	public function notification(){
		$arr["reciver_id"] = $this->session->userdata("user_id");
		$result=$this->user_model->total_no_of_notifi($arr);
	}
//for notification_menu
	public function notification_menu(){
		$arr["reciver_id"] = $this->session->userdata("user_id");
		$result=$this->user_model->notification_menu_db($arr);
	}
//for update notification status
	public function update_noti_status_ajax(){
		$id = $this->uri->segment('3');
		$result=$this->user_model->update_noti_status_db($id);
	}

//for message in details
public function my_conversation_d(){ 
 	$arr['conversation_id']=$this->uri->segment('3');
	$arr['m_reciver_id'] = $this->uri->segment('5');
	$arr['timestamp'] = $this->uri->segment('4');
  //$arr['timestamp_m'] = $this->uri->segment('5');
	$arr["user_id"] = $this->session->userdata("user_id");
	$data["item"]="My Conversation";
	$data["master_title"]="My Conversation | ". $this->config->item('sitename'); 
	$data['resultset']=$this->user_model->my_conversation_data_d($arr);
}
public function my_conversation_more(){ 
 	$arr['conversation_id']=$this->uri->segment('3');
	$arr['timestamp'] = $this->uri->segment('4');
	$arr["user_id"] = $this->session->userdata("user_id");
	$data["item"]="My Conversation";
	$data["master_title"]="My Conversation | ". $this->config->item('sitename'); 
	$data['resultset']=$this->user_model->my_conversation_data_more_d($arr);
}

//for rename folder on mydesk page
public function rename_folder(){
 	$arr['new_name']=trim(urldecode($this->uri->segment('3')));
	$arr['id'] = $this->uri->segment('4');
	$arr["user_id"] = $this->session->userdata("user_id");
	if($arr['new_name'] <> ''){
		$this->db->select('*');
		$this->db->from('user_folders');
		$this->db->where(array('name'=>$arr['new_name'], 'relation'=>$arr['user_id'], 'id <>'=>$arr['id'], "archive <>"=>"1"));
		$query=$this->db->get();
	  // echo $this->db->last_query();
		$resultset1=$query->num_rows();
		if($resultset1 <> '0' || $resultset1 <> 0){ 
			$message[1] = '';
			$message[2] = 'Error: Folder name already exists, Please try another name';
		}else{
			$resultset=$this->user_model->old_foldername($arr);
			if($resultset <> $arr['new_name']){
				$oldname = './userdata/'.$arr["user_id"].'/'.$resultset;
				$newname = './userdata/'.$arr["user_id"].'/'.$arr["new_name"];
				if($this->user_model->rename_folder_db($arr)){
					rename($oldname, urldecode($newname));
					$message[1] = 'Success: Folder rename successfully';
					$message[2] = '';
				}
			}else{
					$message[1] = '';
					$message[2] = '';
			}
		}
		$message[0] = 'success';
		/*$message[3] = $arr["user_id"];*/
		ksort($message);
		$message_arr = implode('|::|', $message);
		echo $message_arr;
	}
	
}

//for rename file on my upload page under a folder
public function rename_file(){
 	$arr['new_name1']=trim(urldecode($this->uri->segment('3')));
	//$n = $arr['new_name1'];
	$arr['id'] = $this->uri->segment('4');
	$arr['folder_name'] = $this->uri->segment('5');
	$textext = trim(urldecode($this->uri->segment('6')));
	$fname = $arr['new_name1'].$textext;
	$arr["user_id"] = $this->session->userdata("user_id");
	if($arr['new_name1'] <> ''){
		$this->db->select('*');
		$this->db->from('user_files');
		$this->db->where(array('userfile'=>$fname, 'user_id'=>$arr['user_id'], 'sub_folder'=>$arr['folder_name'], 'id <>'=>$arr['id'], "archive <>"=>"1"));
		$query=$this->db->get();
	  //echo $this->db->last_query();
		$resultset1=$query->num_rows();
		if($resultset1 <> '0' || $resultset1 <> 0){ 
			$message[1] = '';
			$message[2] = 'Error: Filename already exists, Please try another name';
		}else{
			    $resultset=$this->user_model->old_filename($arr);
				/*$ext1 = explode('.', $resultset); //echo $ext2 = end($ext1);
				$fnew_name = $arr['new_name1'].'.'.$ext2;*/
				if($resultset <> $fname){
					$arr['new_name'] = $fname;
					$oldname = './userdata/'.$arr["user_id"].'/'.$arr['folder_name'].'/'.$resultset;
					$newname = './userdata/'.$arr["user_id"].'/'.$arr['folder_name'].'/'.$arr['new_name'];
					if($this->user_model->rename_file_db($arr)){
						rename($oldname, urldecode($arr['new_name']));
						$message[1] = 'Success: File rename successfully';
						$message[2] = '';
					}
				}else{
						$message[1] = '';
						$message[2] = '';
				}
		}
		$message[0] = 'success';
		$message[3] = $arr["user_id"];
		ksort($message);
		$message_arr = implode('|::|', $message);
		echo $message_arr;
	}
	
}

	//for copy folder file to database
	public function copy_file_folder_to_database(){ 
		$arr["relation"]=$this->session->userdata("user_id");;
		$arr["copyfile"]=$this->uri->segment(3);
		$arr["path"]=$this->uri->segment(4);
		$arr["name"]=$this->uri->segment(5);
		$arr["privacy"]=$this->uri->segment(6);
		$this->session->set_flashdata("tempdata",$arr);	
		$pp=$this->user_model->copy_file_validation($arr);
		 if($pp == 0){
			if($this->user_model->copy_file_under_folder_db($arr)){
				$path = './userdata/'.$arr["relation"].'/'.$arr["name"].'/'.$arr["copyfile"];
				copy($arr["path"],$path);
				$message[1] = '<div id="dev_s_msg" style="color:#191">Success : Copy a file succesfully</div>';
				$message[2] = "success";
			}	
			else{
				$message[1] = '<div id="dev_e_msg" style="color:#911">Error : There is error copy file to data base . Please contact database admin or try later!</div>';
				$message[2] = "error";
			}
    	}
		else{ 
			$message[1] = '<div id="dev_e_msg" style="color:#911">Error : File already exit in this foilder please select another folder!</div>';
			$message[2] = "error";
		}
		
		$message[0] = 'success';
		//$message[3] = $arr["user_id"];
		ksort($message);
		$message_arr = implode('|::|', $message);
		echo $message_arr;

		
	}


}

