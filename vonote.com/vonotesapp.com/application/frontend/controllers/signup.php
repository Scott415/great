<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class signup extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('page_model');
		$this->load->model('user_model');
		$this->load->model('email_model');	
		$this->load->model('login_model');	
		$this->load->helper('url');				
	}

	public function index(){
		$this->common->check_user_login();
		//$this->load->view('home');
		$this->signup();
	}
	public function signup(){
		$pagename='signup_content';
		$data["item"]="Signup";
		$data["master_title"]="Signup | ". $this->config->item('sitename'); 
		$data['resultset']=$this->common->getcontentpagedata($pagename);
		$data["userdata"]=$this->session->userdata("tempdata",$data);
		$data["master_body"]="signup";
		$this->load->theme('home_layout',$data);
	}
	public function add_user_to_database(){
		$arr["email"]= mysql_real_escape_string($this->input->post("email"));
		//$arr["username"] = mysql_real_escape_string($this->input->post("username"));
		$arr["password"]= mysql_real_escape_string($this->input->post("password"));
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		if($this->validations->validate_userdata($arr)){
			
			$result = $this->user_model->add_user($arr);
			//echo $this->db->last_query();
			$last_id = $this->db->insert_id();
			$emailarr["to"] = $arr["email"];
			$emailarr["subject"] = "Sign Up Verification Notification";
				
			$emailarr["message"]= "<p>Welcome to ".$this->config->item('sitename')."</p>
            <p>An account has just been created for ".$emailarr["to"]." </p>
            <p>To activate your account and log in, click here: </p>
            <p><a href=".base_url()."signup/verify_email/?id=".$last_id.">".base_url()."signup/verify_email/?id=".$last_id."</a></p> <br /> <br />
            Thank you, <br />
           	<p>The ".$this->config->item('sitename')." Team</p>";
			$this->email_model->sendIndividualEmail($emailarr); 
			
			//echo $result;die;
			if($result){
			//echo	$path = base_url().'userdata/'.$last_id; 
			$path = './userdata/'.$last_id;
				if(!is_dir($path)){ //create the folder if it's not already exists
				  //directory_map($path);
				  mkdir($path,0755,true);
				} 
					$this->session->set_flashdata("successmsg","Account created successfully");
					$this->session->unset_userdata('tempdata');
					redirect(base_url()."signup");
					//$this->session->set_flashdata;
			}	
			else{
				$this->session->set_flashdata("errormsg","There is error updating content to database. Please contact database admin");	
			}
			//redirect(base_url()."signup/");
		}else{
			//echo "validations fail";
			redirect(base_url()."signup");
		}
		/*if($result){
				$this->session->set_flashdata("successmsg","Account created successfully");
				redirect(base_url()."signup/");
		}	
		else{
			$this->session->set_flashdata("errormsg","There is error updating content to database. Please contact database admin");	
		}*/
	}
	public function verify_email()
	{
		$user_id = $this->input->get("id");//$loginId;
		 $varify = $this->login_model->user_email_verify($user_id); 
	   if($varify)
		{
			
			$this->session->set_flashdata("successmsg","Your email has been successfully verified");
			$result = $this->login_model->getUserData($user_id);
			$login['user_email'] = $result['email'];
			$login['user_status'] = $result['status'];
			$this->session->set_flashdata("tempdata",$login);
			
			redirect(base_url()."home/login");
		}
	}
	
	
}

