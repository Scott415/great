<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller {
	public function __construct(){
		parent::__construct();
		//$this->load->helper('url');
		$this->load->model('page_model');
		$this->load->model('email_model');
		$this->load->helper('cookie');	
	}

	public function index(){
		//$this->load->view('home');
		if(isset($_COOKIE['login'])){
			redirect(base_url()."user/my_desk");
			$this->common->check_user_login();
		}
		else{
			$this->login();
		}
		//$this->login_check_user_login();
		//$this->config->item['sitename'];
		
	}
	public function home(){
		//$this->common->login_check_user_login();
		//echo $config['sitename'];
		$data["item"]="Vo Notes";
		$data["master_title"]= "Home | ". $this->config->item('sitename'); 
		$data['resultset']=$this->common->gethomepagedata();
		$data["master_body"]="home";
		$this->load->theme('home_layout',$data);
		
	}
	
	//for login page
	public function login(){ 
		if(isset($_COOKIE['login']) || $this->session->userdata("user_status")==1){
			redirect(base_url()."user/my_desk");
			$this->common->check_user_login();
		}
		else{
			$pagename='login_content';
			$data["item"]="Login";
			$data["master_title"]="Login | ". $this->config->item('sitename'); 
			$data['resultset']=$this->common->getcontentpagedata($pagename);
			$data["userdata"]=$this->session->userdata("tempdata",$data);
			$data["master_body"]="login";
			$this->load->theme('home_layout',$data);
		}
		
	}
	 // check user login
	 
	public function login_check_user_login(){ 
		 if($_COOKIE['login'] <> ''){
			 $login["email"] = $_COOKIE['login'];
		 }else{
		 	$login["email"] = trim($this->input->post("email"));
		 }
		 $login1 = trim($this->input->post("email"));	
		 $password = trim($this->input->post("password"));
		 //$login["password"] = sha1($password);
		 $login["password"] = $password;
		 $rember = $this->input->post("rember");
		
		
		$this->session->set_flashdata("tempdata",$login);
		if($this->common->authenticateUserLogin($login)){
			$this->session->userdata("user_status");
				if($this->session->userdata("user_status")==1){
					if($rember==1){
						$login['user_status']=1;
						$cookie = array(
						'name'   => 'login',
						'value'  =>  $login1,
						'expire' => '31536000' // 1 year
					);
						$this->input->set_cookie($cookie);
						//echo $this->input->cookie('login');
						//$_COOKIE['login'];
						redirect(base_url()."user/my_desk");
						$this->session->unset_userdata('tempdata');
					}
					else{
						redirect(base_url()."user/my_desk");
					}
				}
				
		}
		else{
			redirect(base_url()."home/login");	 // redirect if user is not login 
		}
	}	
	public function forget_password(){  
		$this->common->check_user_login();
		$data["item"]="Forgot Password";
		$data["master_title"]="Forgot Password | ". $this->config->item('sitename'); 
		//$data['resultset']=$this->common->getcontentpagedata($pagename);
		$data["userdata"]=$this->session->userdata("tempdata",$data);
		$data["master_body"]="forget_password";
		$this->load->theme('home_layout',$data);
		
	}
	public function forget_password_db(){ 
	 	$data['email'] = mysql_real_escape_string($this->input->post("email"));	
		$data["userdata"]=$this->session->set_userdata("tempdata",$data);
		if($this->validations->validate_forgot_password($data['email'])){ 
			$user_id = $this->common->getuser_id($data['email']);
			$emailarr["to"] = $data['email'];
			$emailarr["subject"] = "Password recovery request -> ".$this->config->item('sitename');
			$emailarr["message"]="Dear ". ucwords($user_id['user_name'])." <br>
As per your forgot password request, Your Login information as below: <br>
Email : ".$user_id['email']."<br>
Password : ".$user_id['password']."<br>
Best Regards <br>
".$this->config->item('sitename');
				// print_r($emailarr);
				$result = $this->email_model->UserPasswordsendIndividualEmail($emailarr);
			//$result = $this->email_model->send_forgot_password_email($arr); 
			if($result){
				$err=0;
				$this->session->set_flashdata("successmsg", "Password recovery email sent successfully, please check your email");	
				$this->session->set_flashdata("tempdata");
				$this->session->unset_userdata('tempdata');
				redirect(base_url()."home/forget_password");

			}	
			else{
				$err=1;
				$this->session->set_flashdata("errormsg", "Email Sending unsuccessful");
			}
		}
		else{
			//echo "validations fail";
			redirect(base_url()."home/forget_password");
		}
	}
	
	
	
	
	
}
