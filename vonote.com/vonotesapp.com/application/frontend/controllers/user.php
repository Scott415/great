<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class user extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("user_model");
	$this->load->library('Datatables');
        $this->load->library('table');
        $this->load->database();
                
                	
		
	}

	public function index(){
	$this->load->view('home');
	$this->my_desk();
    
       }

	//for left bar
	public function left_bar(){
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Wall | ".$this->config->item('sitename');   // Please enter the title of page......
		$this->load->theme("head",$data);
		$this->load->view("user/left_bar",$data);
	}

	//for load view for my profile/my desk page
	public function my_desk(){
		$this->common->check_user_acess();
		//echo "dadas".file_logo("php");
		$this->load->helper('calendar');
		$arr["user_id"] = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]="My Desk | ". $this->config->item('sitename'); 
		$data['resultset']=$this->user_model->my_desk_files_from_db($arr);
		$data['userdata']=$this->user_model->user_profile_data($arr);
		$data["master_body"]="my_desk";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	
//for update user profile page
	public function update_profile(){
		$this->common->check_user_acess();
		$arr["user_id"] = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]="Update Profile | ". $this->config->item('sitename'); 
		$data['resultset']=$this->user_model->user_profile_data($arr);
		$data["master_body"]="update_profile";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	
	public function update_profile_db(){
		$arr["user_id"] = $this->session->userdata("user_id");
		$arr["first_name"]= mysql_real_escape_string($this->input->post("first_name"));
		$arr["last_name"]= mysql_real_escape_string($this->input->post("last_name"));
		$arr["education"]= mysql_real_escape_string($this->input->post("education"));
		$arr["worked"]= mysql_real_escape_string($this->input->post("worked"));
		$arr["about_me"]= mysql_real_escape_string($this->input->post("about_me"));
		$arr["image"]=$_FILES["userfile"]["name"];
		if($arr["image"] != ""){
			$arr["image"]=time().".".$this->common->get_extension($_FILES["userfile"]["name"]);
		}
		else{
			$arr["image"]=$this->input->post("image");	
		}
		$this->session->set_flashdata("tempdata",$arr);	
		if($this->validations->validate_user_profile_data($arr)){
			if($this->user_model->add_edit_user($arr)){
				//$last_id = $this->db->insert_id();
				if($arr["image"]!=$this->input->post("image")){
					$config['upload_path'] = './blogimages/';
					$config['allowed_types'] = '*';
					$config['file_name']=$arr["image"];
					$this->upload->initialize($config);
					if($this->upload->do_upload()){
						$err=0;
					}		
					else{
						//echo $this->upload->display_errors();die;
						$this->session->set_flashdata("successmsg","There is some error uploading the files to server. Please contact server admin");		
					}		
				}
					$this->session->set_flashdata("successmsg","Profile updated succesfully");
					$err=2; // for blog updated succesfully
					redirect(base_url()."user/update_profile");	
			}	
			else{
				$this->session->set_flashdata("errormsg","There is error updating profile to database . Please contact Admin");
				$err=1;
			}
		}
		else{
			$err=1;
			redirect(base_url()."user/update_profile");		
		}
		
	}
	
	
	//for load view for Create Folder
	public function create_folder(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]="Create Folder | ". $this->config->item('sitename'); 
		$data["master_body"]="create_folder";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	//for create floder pop up
	public function popup(){
		$data['relation']=$this->uri->segment('3');
		$data['name']=$this->uri->segment('4');
		$data["item"]="Vo Notes";
		$data["master_title"]= "Create Folder | ".$this->config->item('sitename');   // Please enter the title of page......
		//$data['blogresult']=$this->blog_model->getIndividualBlog($blogid);
		$data["userdata"]=$this->session->userdata("tempdata",$data);
		$this->load->theme("head",$data);
		$this->load->view("user/popup",$data);
	}
	//create folder 
	public function create_folder_to_database(){
		$arr["name"]= mysql_real_escape_string($this->input->post("folder_name"));
		//$arr["privacy"]= mysql_real_escape_string($this->input->post("privacy"));
		$arr["privacy"]= 1; /// default 1 for private folder
		$arr["relation"]= $this->session->userdata("user_id");
		
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		if($this->validations->validate_create_folder_data($arr)){
			$result = $this->user_model->create_folder($arr);
			if($result){
				$path = './userdata/'.$arr["relation"].'/'.$arr["name"];
				if(!is_dir($path)){ //create the folder if it's not already exists
				  mkdir($path,0755,true);
				} 
				$err=0;
				$this->session->set_flashdata("successmsg","Folder created successfully");
				$this->session->unset_userdata('tempdata');
				
				
			}else{
				 $this->session->set_flashdata("errormsg","There is error updating content to database. Please contact database admin");	
				 redirect(base_url()."user/popup/".$arr["relation"]."/".$arr["name"]);
			}
			redirect(base_url()."user/popup/".$arr["relation"]."/".$arr["name"]."/2");
		}else{

			$this->session->set_flashdata("errormsg1","Error : Use of special character “@#$^&*()_!” is not allowed");
			redirect(base_url()."user/popup/");
		}
	}
	
	
//for delete a folder
	public function delete_a_folder(){
		$data['page_name']=$this->uri->segment('2'); 
		$data['user_id']=$this->uri->segment('3');
				$page_name=$this->uri->segment('4');
			echo $data['folder_name'] =  str_replace('%20'," ",$page_name);  
		$data['file_id']=$this->uri->segment('5');
		if($this->validations->validate_delete_folder($data)){ 
			if($this->user_model->delete_user_uploaded_folder($data)){ 
			    $this->user_model->delete_user_uploaded_file_f($data);
				$path = base_url().'userdata/'.$data["user_id"].'/'.$data["folder_name"]; 
				delete_files($path, true); 
				rmdir($path);
				$err=0;
				$this->session->set_flashdata("successmsg","File removed succesfully");
			}else{
				$this->session->set_flashdata("errormsg","There is error removing file to data base . Please contact database admin or try later");
				$err=1;
				
			}
		}else{ //echo "hjkghjk";
		}
		 redirect(base_url()."user/my_desk");
	}
	
	//for upload file to floder pop up
	public function popup1(){ 
		$data['relation']=$this->uri->segment('3');
		$data['name']=$this->uri->segment('4');
		$data["item"]="Vo Notes";
		$data["master_title"]= "Create Folder | ".$this->config->item('sitename');   // Please enter the title of page......
		//$data['blogresult']=$this->blog_model->getIndividualBlog($blogid);
		$data["userdata"]=$this->session->userdata("tempdata",$data);
		$this->load->theme("head",$data);
		$this->load->view("user/popup1",$data);
	}
	//for upload folder file to database
	public function upload_file_folder_to_database(){
		$page_name =  $this->uri->segment(2);
		$arr["relation"]=$this->input->post("relation");
		$arr["name"]=$this->input->post("name");
		$arr["privacy"]=$this->input->post("privacy");
		$arr["userfile"]=$_FILES["userfile"]["name"];
		$arr["file_size"]=(($_FILES["userfile"]["size"])/1048576);
		if($arr["userfile"] != ""){
			$arr["userfile"]=time().".".$this->common->get_extension($_FILES["userfile"]["name"]);
		}
		else{
			$this->session->set_flashdata("errormsg","Please upload file");	
			//$arr["userfile"]=$this->input->post("userfile");	
		}
		$this->session->set_flashdata("tempdata",$arr);	
		 $relationa; echo $namea;echo $pagenamea; 
		if($this->validations->validate_upload_file_folder_data($arr)){
			if($this->user_model->upload_file_under_folder_db($arr)){
			//	$last_id = $this->db->insert_id();
				
				if($arr["userfile"]!=$this->input->post("userfile")){
					$path = './userdata/'.$arr["relation"].'/'.$arr["name"].'/';
					$config['upload_path'] = $path;
					$config['allowed_types'] = '*';
					$config['file_name']=$arr["userfile"];
					$this->upload->initialize($config);
					if($this->upload->do_upload()){
						$err=0;
					}		
					else{
						//echo $this->upload->display_errors();die;
						$this->session->set_flashdata("successmsg","There is some error uploading the files to server. Please contact server admin");		
					}		
				}
					$this->session->set_flashdata("successmsg","File uploaded succesfully");
					$err=0;      // for blog added succesfully
					//redirect(base_url()."user/my_desk");
			}	
			else{
				$this->session->set_flashdata("errormsg","There is error uploading file to data base . Please contact database admin");
				$err=1;
			}
			 redirect(base_url()."user/popup1/".$arr["relation"]."/".$arr["name"]."/".$page_name."/2");	
		}
		else{
			$err=1;
			 redirect(base_url()."user/popup1/".$arr["relation"]."/".$arr["name"]."/".$page_name);		
		}
		 redirect(base_url()."user/popup1/".$arr["relation"]."/".$arr["name"]."/".$page_name);
	}
	//for copy file to floder pop up
	public function popup_copyfile(){ 
		$data['relation']=$this->uri->segment('3');
		$data['name']=$this->uri->segment('4');
		$data["item"]="Vo Notes";
		$data["master_title"]= "Copy File | ".$this->config->item('sitename');   // Please enter the title of page......
		//$data['blogresult']=$this->blog_model->getIndividualBlog($blogid);
		$data["userdata"]=$this->session->userdata("tempdata",$data);
		
		$this->load->theme("head",$data);
		$this->load->view("user/popup_copyfile",$data);
	}
	
//for delete a file
	public function delete_a_file(){ 
		$data['user_id']=$this->uri->segment('3');
		$data['folder_name']=$this->uri->segment('4');
		$data['file_id']=$this->uri->segment('5');
		if($this->validations->validate_delete_file($data)){ 
			if($this->user_model->delete_user_uploaded_file($data)){ 
				$err=0;
				$this->session->set_flashdata("successmsg","File removed succesfully");
			}else{
				$this->session->set_flashdata("errormsg","There is error removing file to data base . Please contact database admin or try later");
				$err=1;
				
			}
		}else{ //echo "hjkghjk";
		}
		 redirect(base_url()."user/my_upload_files/".$data["user_id"].'/'.$data["folder_name"]);
	}
	//for delete a file
	public function delete_a_friend(){ 
	//error_reporting(true);
	$this->common->check_user_acess();
	$this->load->model('User_model');

		$data['friend_id']=$this->uri->segment('3');
		$data['user_id'] = $this->session->userdata("user_id");
		//print_r($data);

		echo $this->user_model->delete_a_friend($data);

		/*if($this->user_model->delete_friend($data)){ 
				$err=0;
				$this->session->set_flashdata("successmsg","File removed succesfully");
			}else{
				$this->session->set_flashdata("errormsg","There is error removing file to data base . Please contact database admin or try later");
				 $err=1;
				
			} 
		*/
		redirect(base_url()."user/my_friends/");
	
	}
	
	
//for create file pop up
	public function file_popup(){
		$data['relation']=$this->uri->segment('3');
		$data['name']=$this->uri->segment('4');
		$data["item"]="Vo Notes";
		$data["master_title"]= "Create File | ".$this->config->item('sitename');   // Please enter the title of page......
		//$data['blogresult']=$this->blog_model->getIndividualBlog($blogid);
		$data["userdata"]=$this->session->userdata("tempdata",$data);
		$this->load->theme("head",$data);
		$this->load->view("user/file_popup",$data);
		
	}
	


public function file_pop(){

   //echo $ii = $_POST['name'];
      //echo $tt = $this->input->post('name');
		$data['relation']=$this->uri->segment('3');
		$data['name']=$this->uri->segment('4');
		$data['filea']=$this->uri->segment('5');
		$data["item"]="Vo Notesjhbdfhdb";
		$data["master_title"]= "Create File | ".$this->config->item('sitename');   // Please enter the title of page......
    	$filename = base_url()."userdata/".$data['relationa']."/".$data['namea']."/".$data['filea'];
		
		//if ( file_exists($filename) ) {

		if ( ($fh = fopen($filename, 'r')) !== false ) {

		$headers = fread($fh, 0xA00);

		# 1 = (ord(n)*1) ; Document has from 0 to 255 characters
		$n1 = ( ord($headers[0x21C]) - 1 );

		# 1 = ((ord(n)-8)*256) ; Document has from 256 to 63743 characters
		$n2 = ( ( ord($headers[0x21D]) - 8 ) * 256 );

		# 1 = ((ord(n)*256)*256) ; Document has from 63744 to 16775423 characters
		$n3 = ( ( ord($headers[0x21E]) * 256 ) * 256 );

		# (((ord(n)*256)*256)*256) ; Document has from 16775424 to 4294965504 characters
		$n4 = ( ( ( ord($headers[0x21F]) * 256 ) * 256 ) * 256 );

		# Total length of text in the document
		$textLength = ($n1 + $n2 + $n3 + $n4);

		$extracted_plaintext = fread($fh, $textLength);

		# if you want the plain text with no formatting, do this
		//echo $extracted_plaintext;

		# if you want to see your paragraphs in a web page, do this
		$data['content']= nl2br($extracted_plaintext);
		}
		if(empty($data['content'])) :
		//echo file_get_contents($filename);
		$data['content'] = file_get_contents($filename);
						//echo $doc_data;
		endif;

		//$data['blogresult']=$this->blog_model->getIndividualBlog($blogid);
		//$data['file']=get_file_details($data['filea']);
		$data["userdatas"]=$this->session->userdata("tempdata",$data);
		$data["master_body"]="file_pop";
		//$this->load->theme("head",$data);
		//$this->load->view("user/file_pop",$data);
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}



//for create  file to database
	public function created_file_folder_to_database(){
		$page_name =  $this->uri->segment(2);
		$arr["relation"]=$this->input->post("relation");
		$arr["name"]=$this->input->post("name");
		$arr["file_content"]=$this->input->post("file_content");
		$arr["folder_name"]=$this->input->post("folder_name");
		$arr["privacy"]=$this->input->post("privacy");
		$arr["userfile"] = time().$arr["name"].'.doc';
		//$arr["userfile"]=$_FILES["userfile"]["name"];
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		if($this->validations->validate_create_file_folder_data($arr)){
		  $arr["file_content"] = str_replace('"', "'", $arr["file_content"]);
		  $content = "<html><body>".$arr["file_content"]."</body></html>";
		  $fp = fopen('./userdata/'.$arr["relation"].'/'.$arr["folder_name"].'/'.$arr["userfile"],"w+");
		  fwrite($fp,$content);
		  fclose($fp);	
		  $total=$this->user_model->user_limit();
		$path=$_SERVER['DOCUMENT_ROOT']."/userdata/".$this->session->userdata("user_id");;
		$disk_used = ($this->common->foldersize($path))/1048576;
			if($disk_used > $total){
			$this->session->set_flashdata("errormsg","Error : You have no space, Please delete some files ");
			$err=1;	
		}
		else {
			if($this->user_model->create_file_under_folder_db($arr)){
			
					$this->session->set_flashdata("successmsg","File created successfully!");
					$err=0;      // for blog added succesfully
					$this->session->unset_userdata('tempdata');
					//redirect(base_url()."user/my_desk");
			}	
		
			else{
				$this->session->set_flashdata("errormsg","There is error uploading file to data base . Please contact database admin");
				$err=1;
			}
			}
			redirect(base_url()."user/file_popup/".$arr["relation"]."/".$arr["folder_name"]."/".$page_name."/2");	
		}
		else{
			$err=1;
			 redirect(base_url()."user/file_popup/".$arr["relation"]."/".$arr["folder_name"]."/".$page_name);		
		}
		 redirect(base_url()."user/file_popup/".$arr["relation"]."/".$arr["folder_name"]."/".$page_name);
	}
	


//for update  file to database
	public function update_file_folder_to_database(){
		$page_name =  $this->uri->segment(2);
		
		$arr["relation"]=$this->input->post("user_id");
		//$arr["name"]=$this->input->post("file_name");
		$arr["file_content"]=$this->input->post("file_content");
		$folder_name=$this->input->post("folder_name");
		$arr["folder_name"] =  str_replace('%20'," ",$folder_name);   
		$arr["privacy"]=$this->input->post("privacy");
		$arr["userfile"] = $this->input->post("file_name");
		$file=explode(".",$arr["userfile"]);
		$arr["userfile"]=$file[0].".doc";
		//$this->session->unset_userdata('tempdata');
		$arr["friendly_name"] = $this->input->post("friendly_name");
		//$arr["userfile"]=$_FILES["userfile"]["name"];
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		  $arr["file_content"] = str_replace('"', "'", $arr["file_content"]);
		  //print_r($arr);
		  $content = "<html><body>".$arr["file_content"]."</body></html>";
		 //$strings = './userdata/'.$arr["relation"].'/'.$arr["folder_name"].'/'.$arr["userfile"],"w+";
		  $fp = fopen('./userdata/'.$arr["relation"].'/'.$arr["folder_name"].'/'.$arr["userfile"],"w+");
			if(fwrite($fp,$content)):
			echo "wrong";
		  fclose($fp);
		  
			else :
			echo "heeheh";
			endif;		  
			if($this->user_model->update_file_under_folder_db($arr)){
					$this->session->set_flashdata("successmsg","File Updated succesfully");
					$err=0;      // for blog added succesfully
					$this->session->unset_userdata('tempdata');
					redirect(base_url()."user/my_upload_files/".$arr["relation"]."/".$arr["folder_name"]);
			}	
			else{
				$this->session->set_flashdata("errormsg","There is error updating file to data base . Please contact database admin");
				$err=1;
			} 
			redirect(base_url()."user/file_pop/".$arr["relation"]."/".$arr["folder_name"]."/".$arr["userfile"]."/2");	
		
	
		 redirect(base_url()."user/file_pop/".$arr["relation"]."/".$arr["folder_name"]."/".$arr["userfile"]);
	}
	
	



//for update created file 
	public function update_my_file(){
		$this->common->check_user_acess();
		$arr['userfile']=$this->uri->segment('4');
		$arr['sub_folder']=$this->uri->segment('3');
		$arr["user_id"] = $this->session->userdata("user_id");
		//$arr["privacy"] = $this->session->userdata("privacy");
		
		//$data['name']=$this->uri->segment('4');
		$data["item"]="Update File | Vo Notes";
		$data["master_title"]="Update File | ". $this->config->item('sitename'); 
		// for .txt file content
		$file = './userdata/'.$arr["user_id"]."/".$arr['sub_folder'].'/'.$arr['userfile'];
		$data['filedata'] = read_file($file);
		$data['resultset']=$this->user_model->get_file_data_db($arr);
		$data["master_body"]="update_my_file";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
		 fclose($fp);	
	}


//for update  file to database
	public function edit_file_folder_to_database(){
		$arr["userfile"]=$this->input->post("userfile");
		$arr["sub_folder"]=$this->input->post("sub_folder");
		$arr["file_content"]=$this->input->post("file_content");
		$arr["user_id"]=$this->input->post("user_id");
		$arr["privacy"]=$this->input->post("privacy");
		//$arr["userfile"] = $arr["name"].'.txt';
		//$arr["userfile"]=$_FILES["userfile"]["name"];
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		$path = base_url().'user/update_my_file/'.$arr["sub_folder"].'/'.$arr["userfile"];
		if($this->validations->validate_update_file_folder_data($arr)){
		  
		  $content = $arr["file_content"];
		  write_file('./userdata/'.$arr["user_id"].'/'.$arr["sub_folder"].'/'.$arr["userfile"], $arr["file_content"]);
		  /*$fp = fopen('./userdata/'.$arr["user_id"].'/'.$arr["sub_folder"].'/'.$arr["userfile"].".txt","wb");
		  fwrite($fp,$content);
		  fclose($fp);	*/
			if($this->user_model->update_file_under_folder_db($arr)){
					$this->session->set_flashdata("successmsg","File Updated succesfully");
					$err=0;      // for blog added succesfully
					$this->session->unset_userdata('tempdata');
					//redirect(base_url()."user/my_desk");
			}	
			else{
				$this->session->set_flashdata("errormsg","There is error uploading file to data base . Please contact database admin");
				$err=1;
			}
		}
		else{
			$err=1;
			 redirect($path);		
		}
		 redirect($path);
	}	
	
	//for view file pop up
	public function view_file_popup(){
		$this->common->check_user_acess();
		$arr['namea']=$this->uri->segment('4');
		$arr['relationa']=$this->uri->segment('3');
		$arr['filea']=$this->uri->segment('5');
		
		$arr["item"]="View File | Vo Notes";
		$arr1["master_title"]="View File | ". $this->config->item('sitename'); 
		// for .txt file content
	//	$file = './userdata/'.$arr["user_id"]."/".$arr['sub_folder'].'/'.$arr['userfile'];
		//$data['filedata'] = read_file($file);
		$data['userdata']=$this->session->set_userdata("tempdata",$arr);
		//print_r($data['userdata']);
		//$data['resultset']=$this->session->set_userdata("khattra",$data);;
		$data["master_body"]="view_file_popup";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
		
		
		/*//$this->load->library('session');
		$this->session->unset_userdata('khattra');
		$arr1['relationa']=$this->uri->segment('3');
		$arr1['namea']=$this->uri->segment('4');
		$arr1['filea']=$this->uri->segment('5');
		$this->session->set_userdata("khattra",$arr1);
		$this->load->theme("head",$data);
	//	$this->load->view("user/view_file_popup",$data);
	    $this->load->theme('home_layout',$data);*/

	}
	
	
	/*public function my_costom_folder(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Costom Folder | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="my_costom_folder";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}*/
// list of recent files uploaded by user	
	public function my_recent_files(){
		$arr["user_id"] = $this->session->userdata("user_id");
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Recent Files | ".$this->config->item('sitename');   //Please enter the title of page......
		$data['resultset']=$this->user_model->my_recent_files_from_db($arr);
		$data["master_body"]="my_recent_files";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}

// for upload user files	
	public function my_upload(){
		$data['relation']=$this->uri->segment('3');
		$data['name']=urldecode($this->uri->segment('4'));
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Recent Files | ".$this->config->item('sitename');   //Please enter the title of page......
		$data['resultset']=$this->user_model->my_uploaded_file_under_folder_from_db($data); 
		$data["master_body"]="my_upload_files";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);

		/*$data["item"]="Vo Notes";
		$data["master_title"]= "My Recent Files | ".$this->config->item('sitename');   //Please enter the title of page......
		$data["master_body"]="my_upload";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);*/
	}
// for upload user files	
	public function my_upload_files(){
		$data['relation']=$this->uri->segment('3');
		$data['name']= urldecode($this->uri->segment('4'));
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Recent Files | ".$this->config->item('sitename');   //Please enter the title of page......
		$data['resultset']=$this->user_model->my_uploaded_file_under_folder_from_db($data); 
		$data["master_body"]="my_upload_files";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}	

//for my friend list
	public function my_friends(){
		$this->common->check_user_acess();
		$arr["user_id"] = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Friends | ".$this->config->item('sitename');   // Please enter the title of page......
		$data['resultset']=$this->user_model->my_friend_list($arr); //echo $this->db->last_query();
		$data["master_body"]="my_friends";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	
//for my conversation
	public function my_messages(){
		$this->common->check_user_acess();
		$conversation_id=$this->uri->segment('3');
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Messages | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="my_messages";
		$data['resultset']=$this->user_model->my_conversation($conversation_id);
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
//for message reply
	public function reply_message(){
		$arr['conversation_id']=$this->input->post('conversation_id');
		$arr['sender_id']=$this->input->post('user_id');
		$arr['reciver_id']=$this->input->post('reciver_id');
		$arr['message']= trim($this->input->post('message'));
		$arr["image"]=$_FILES["userfile"]["name"];
		if($arr["image"] != ""){
			$arr["image"]=time().".".$this->common->get_extension($_FILES["userfile"]["name"]);
		}
		else{
			$arr["image"]=$this->input->post("image");	
		}
		if($arr['message'] <> ''){
			$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
			if($this->validations->validate_send_message($arr)){ 
				if($this->user_model->my_reply_data($arr)){	
					if($arr["image"]!=$this->input->post("image")){
					$config['upload_path'] = './send_files/';
					$config['allowed_types'] = '*';
					$config['file_name']=$arr["image"];
					$this->upload->initialize($config);
					if($this->upload->do_upload()){
						$err=0;
					}		
					else{
						//echo $this->upload->display_errors();die;
						$this->session->set_flashdata("successmsg","There is some error uploading the files to server. Please contact server admin");		
					}	
				}
					$this->session->set_flashdata("successmsg","Message Sent Successfully");
					$err=0;      // for blog added succesfully
					$this->session->unset_userdata('tempdata');
				}
				else{
					$this->session->set_flashdata("errormsg","There is error sending message. Please contact Admin");
					$err=1;
				}
			}
    	}
		else{
			$this->session->set_flashdata("errormsg","Error : Please enter message in test box<br>");
			$err=1;
		}

		redirect(base_url()."user/my_messages/".$arr["conversation_id"]."/".$arr['reciver_id']);
	}
	
	//for send message 
	public function send_message(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "Send Messages | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="send_message";
		$data['sidebar_content']='user/left_bar';
		$data["userdata"]=$this->session->userdata("tempdata",$data);
		$this->load->theme('home_layout',$data);
		
	}

//for send message
	public function send_message_db(){
		$this->common->check_user_acess();
		$arr["sender_id"]=$this->input->post("sender_id");
		$arr["reciver_id"]=$this->input->post("reciver_id");
		$arr["message"]=trim($this->input->post("message"));
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		$arr["image"]=$_FILES["userfile"]["name"];
		if($arr["image"] != ""){
			$arr["image"]=time().".".$this->common->get_extension($_FILES["userfile"]["name"]);
		}
		else{
			$arr["image"]=$this->input->post("image");	
		}
		if($this->validations->validate_send_message($arr)){ 
			if($this->user_model->send_message_to_db($arr)){
				if($arr["image"]!=$this->input->post("image")){
					$config['upload_path'] = './send_files/';
					$config['allowed_types'] = '*';
					$config['file_name']=$arr["image"];
					$this->upload->initialize($config);
					if($this->upload->do_upload()){
						$err=0;
					}		
					else{
						//echo $this->upload->display_errors();die;
						$this->session->set_flashdata("successmsg","There is some error uploading the files to server. Please contact server admin");		
					}	
				}
	
				$this->session->set_flashdata("successmsg","Message Sent Successfully");
					$err=0;      // for blog added succesfully
					$this->session->unset_userdata('tempdata');
			}
			else{
				$this->session->set_flashdata("errormsg","There is error sending message. Please contact Admin");
				$err=1;
			}
		}
		/*else{
			$this->session->set_flashdata("errormsg","There is error sending message. Please contact Admin");
			$err=1;
		}*/
		redirect(base_url()."user/send_message/".$arr["reciver_id"]);
	}
		
//for user inbox
	public function inbox(){
		$this->common->check_user_acess();
		$user_id = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Messages | ".$this->config->item('sitename');   // Please enter the title of page......
		$data['resultset']=$this->user_model->my_inbox($user_id);
		$data["master_body"]="inbox";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	
//for delete message
	public function delete_a_message(){  
		 $data['meesage']=$this->uri->segment('3'); 
		 $data['conversation_id']=$this->uri->segment('4');
		 $data['reciver_id']=$this->uri->segment('5');
//print_r($data); die;
		//if($this->validations->validate_delete_message($data)){ //echo "hjkghjk";
			if($this->user_model->delete_user_message($data)){  
				$err=0;
				$this->session->set_flashdata("successmsg","Message removed succesfully");
			}else{
				$this->session->set_flashdata("errormsg","There is error removing message to data base . Please contact admin or try later");
				$err=1;
				
			}
		/*}else{ //echo "hjkghjk";
		}*/
		 redirect(base_url()."user/my_messages/".$data['conversation_id'].'/'.$data['reciver_id']);
	}

	
	
	public function my_settings(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Settings | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="my_settings";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}




function display_info(){
		$this->load->model('user_model');
		
		$data['results'] = $this->user_model->get_info();
		
		$this->load->view('user/view_info', $data);
	}

	function search_name(){
		if(isset($_GET['name'])){
			$name=$_GET['name'];
		}
		$this->load->model('user_model');
		
		$data['results'] = $this->user_model->searchbyname($name);
		
		$this->load->view('view_info', $data);
	}






	//for update/change password
	public function reset_password(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data['userdata']=$this->session->userdata('tempdata');
		$data["master_title"]= "Reset Password | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="reset_password";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	//reset/update password in database
	public function update_password_to_db(){
		//$this->common->check_user_acess();
		$arr["id"] = mysql_real_escape_string($this->input->post("login_user_id"));
		$arr["old_password"] = mysql_real_escape_string($this->input->post("old_password"));
		$arr["new_password"] = mysql_real_escape_string(trim($this->input->post("new_password")));
		$arr["confirm_password"] = mysql_real_escape_string(trim($this->input->post("confirm_password")));
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		//print_r();
		if($this->validations->update_password($arr)){
			if($this->user_model->changeUserPassword($arr)){
				$this->session->set_flashdata("successmsg","Your password has been successfully updated.");
				$this->session->set_flashdata("tempdata");
				$err=0;
				$this->session->unset_userdata('tempdata');
			}	
			else{
				$err=1;
				$this->session->set_flashdata("errormsg","Database error occurred, please contact admin.");
			}
			
		}
		redirect(base_url()."user/reset_password");
	}
	
	//for after login first time with facebook set password
	public function fb_new_password(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "Set Password | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="fb_new_password";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	//for after login first time with facebook set password in database
	public function fb_new_password_to_db(){
		//$this->common->check_user_acess();
		$arr["id"] = mysql_real_escape_string($this->session->userdata("user_id"));
		$arr["new_password"] = mysql_real_escape_string(trim($this->input->post("new_password")));
		$arr["confirm_password"] = mysql_real_escape_string(trim($this->input->post("confirm_password")));
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		if($this->validations->set_password_fb($arr)){
			if($this->user_model->changeUserPassword_fb($arr)){
				$this->session->set_flashdata("successmsg","Your password has been successfully updated.");
				$this->session->set_flashdata("tempdata");
				$err=0;
			}	
			else{
				$err=1;
				$this->session->set_flashdata("errormsg","Database error occurred, please contact admin.");
			}
			
		}
		redirect(base_url()."user/fb_new_password");
	}


	public function notification(){
		$this->common->check_user_acess();
		$arr['reciver_id'] = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]= "Notification | ".$this->config->item('sitename');   // Please enter the title of page......
		$data['resultset']=$this->user_model->notifications_page_db($arr);
                // $data["reciver_id"] = $this->session->userdata("user_id");
                // $result = $this->user_model->total_no_of_notifi($arr);
 
		$data["master_body"]="notification";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	


//for notification
 /* public function notification() {
        $arr["reciver_id"] = $this->session->userdata("user_id");
        $result = $this->user_model->total_no_of_notifi($arr);
    }
*/
//for notification_menu
    public function notification_menu() {
        $arr["reciver_id"] = $this->session->userdata("user_id");
        $result = $this->user_model->notification_menu_db($arr);
    }

//for update notification status
    public function update_noti_status_ajax() {
        $id = $this->uri->segment('3');
        $result = $this->user_model->update_noti_status_db($id);
    }


	//for my wall
	public function my_bulletin_board(){
		$this->common->check_user_acess();
		$this->load->helper('calendar');
		$arr["user_id"] = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Bulletin Board | ".$this->config->item('sitename');   // Please enter the title of page......
		$data['resultset']=$this->user_model->my_wall_files_from_db($arr);
		$data['userdata']=$this->user_model->user_profile_data($arr);
		$data["master_body"]="my_wall";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	//for user wall
	public function user_wall(){
		$this->common->check_user_acess();
		$arr['user_id']=$this->uri->segment('3');
		$data["item"]="Vo Notes";
		$data["master_title"]= "User Bulletin Board | ".$this->config->item('sitename');   // Please enter the title of page......
		$data['resultset']=$this->user_model->my_wall_files_from_db($arr);
		$data['userdata']=$this->user_model->user_profile_data($arr);
		$data["master_body"]="user_wall";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	
	//file data for auto suggestion
	public function file_data(){
		$arr["user_id"] = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]= "file data | ".$this->config->item('sitename');   // Please enter the title of page......
		$this->load->theme("head",$data);
		$data['resultset']=$this->user_model->file_data_db($arr);
		$data["master_body"]="file_data";
	//	$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout1',$data);
	}
	

       function datatable()
             {
                 $this->datatables->select('userfile,sub_folder,id')
                //$this->datatables->select('userfile,sub_folder,id')
                 ->from('user_files');
                 $where1 = array('privacy =' => 0);
                //array("id" =>$arr['user_id'],'name= ' => kiki);
	         $this->datatables->where($where1);
                 echo $this->datatables->generate();
    }


public function search(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "Search | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="search";
                $this->load->model("user_model");
                $tmpl = array ( 'table_open'  => '<table id="big_table" border="1" cellpadding="0" cellspacing="0" class="">' );
                $this->table->set_template($tmpl); 
                $this->table->set_heading('File Name','Folder Name', 'Id');
                //$this->table->set_heading('File Name','Folder Name', 'Id');
                 //$this->load->view('user/search');		
                $data['sidebar_content']='user/left_bar';
                $this->load->theme('home_layout',$data);
                //set table id in table open tag
	}

        public function saview(){

              $saa = $_POST['hre'];
		
                $query = $this->db->query("SELECT * FROM `user_files` WHERE `id` = '$saa';");
                foreach ($query->result('User') as $row)
                {
                 $sa1 = $row->user_id;
                 $sa2 = $row->sub_folder;
                 $sa3 = $row->userfile;
 //echo "<a href='javascript:;' class='fancybox12'><img height='24' src='http://vonotesapp.com/images/move_icon.png' title='Move File'></a>
 echo "<a href='http://vonotesapp.com/user/view_file_popup/$sa1/$sa2/$sa3'><img height='24' src='http://vonotesapp.com/images/69.png' title='View File'></a>";
                } 
               
             
      }



function move_filename() {

            echo $filee = $_POST['name'];
           $arrr1 = $this->input->post("relation");
             $arrr5 = $this->input->post("name");
            // echo $arrr2 = $this->input->post("sub_folder");
             $arrr2 = $this->input->post("folder_name");
             $arrr3 = $this->input->post("id"); 
            $query = $this->db->query("SELECT * FROM `user_files` WHERE `name` = '$arrr5'"); 
           
             if ($query->num_rows() == 1)
                {
                $row = $query->row();
                $id_ll = $row->id;
                }
               
                 $arrr4 = $id_ll;


             
                      $query = $this->db->query("SELECT * FROM `user_files` WHERE `id` = '$arrr4';");
                      foreach ($query->result('User') as $row)
                           {   $id = $row->id;
                               $sa12 = $row->user_id;
                               $folder_name= $row->folder_name;
                               $sa22 = $row->sub_folder;
                               $sa32 = $row->userfile;
                               $name= $row->name;
                               $ext= $row->ext;
                               $privacy= $row->privacy;
                               $sort= $row->sort;
                               $status= $row->status;
                               $archive= $row->archive;
                               $time= $row->time;
                               

                           }
             
	 	   $fullPath = './userdata/'.$sa12.'/'.$sa22.'/'.$sa32; 
                   $fullPath;
                   //move_uploaded_file($tmp_name, "$uploads_dir/$name");
                   $fileName = $sa32;
                   $fileTmpLoc = './userdata/'.$sa12.'/'.$sa22.'/'.$sa32;
                   $pathAndName = "./userdata/$arrr1/$arrr2/".$fileName;
                   $moveResult = copy($fileTmpLoc, $pathAndName);

          if($moveResult == true) {


                                
 $a1 = $this->db->query("SELECT `name` FROM `user_files` WHERE `id` = '$id'");
 foreach ($a1->result('User') as $row1)
                           {
 $name = $row1->userfile;

						   }

$ssql ="INSERT INTO `user_files`(`user_id`, `folder_name`, `sub_folder`, `userfile`, `name`, `ext`, `privacy`, `sort`, `status`, `archive`, `time`) VALUES ('$arrr1','$arrr1','$arrr2','$sa32','$arrr5','$ext','$privacy','$sort','$status','$archive','$time')";

	$this->db->query($ssql);

echo $this->db->affected_rows(); 					 


            //$query = $this->db->query("UPDATE `user_files` SET `user_id`='$arrr1',`folder_name`='$arrr1',`sub_folder`='$arrr2' WHERE `id`='$arrr4'");
              echo "File has been Copy from " .$sa22. " to" . $arrr2;
              } else {
                       echo "ERROR: File not moved correctly";
                     }
          //$query = $this->db->query("UPDATE `user_files` SET `user_id`='$arrr1',`folder_name`='$arrr1',`sub_folder`='$arrr2' WHERE `id`='$arrr4'");
}



       function datatable1()
             {
                 $this->datatables->select('first_name,last_name,id')
                 ->from('users');
                
	        //$this->datatables->where($where);
                echo $this->datatables->generate();
    }


    public function saview12(){
                $saa = $_POST['hreff'];
		
                $query = $this->db->query("SELECT * FROM `users` WHERE `id` = '$saa';");
                foreach ($query->result('User') as $row)
                {
                   $sa1 = $row->id;
                   $sa2 = $row->first_name;
                   $sa3 = $row->last_name;
 //echo "<a href='javascript:;' class='fancybox12'><img height='24' src='http://vonotesapp.com/images/move_icon.png' title='Move File'></a>


 echo "<a href='http://vonotesapp.com/user/user_wall/$sa1/'><img height='24' src='http://vonotesapp.com/images/69.png' title='View File'></a>";
                } 
               
             
      }



//for search friend
	public function find_friend(){
	error_reporting(true);
	$this->common->check_user_acess();
	
		 $tmpl = array ( 'table_open'  => '<table id="keywords" border="1" cellpadding="0" cellspacing="0" class="">' );
	 $this->table->set_template($tmpl); 
	 $this->table->set_heading('First Name','Last Name','Education');
	if($this->input->post('limit')):
	$data['content']=$this->user_model->get_all_user_data_limit($this->input->post('limit'));
	//-- Content Rows
		foreach ($data['content'] as $row)
    {
        if(empty($row['first_name'])):
		$row['first_name']=" ";
		endif;
		 if(empty($row['last_name'])):
		$row['last_name']=" ";
		endif;
	
		 if(empty($row['education'])):
		$row['education']=" ";
		endif;
        $this->table->add_row(anchor(base_url()."user/user_wall/$row[id]", $row['first_name']), anchor(base_url()."user/user_wall/$row[id]",$row['last_name']) ,anchor(base_url()."user/user_wall/$row[id]", $row['education'])); 
    }

	 echo $this->table->generate(); 
			else :
				$data["item"]="Vo Notes";
		$data["master_title"]= "Search New Friend | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="find_friend";
			$data['content']=$this->user_model->get_all_user_data();
				foreach ($data['content'] as $row)
    {
	 if(empty($row['first_name'])):
		$row['first_name']=" ";
		endif;
		 if(empty($row['last_name'])):
		$row['last_name']=" ";
		endif;
	 if(empty($row['education'])):
		$row['education']=" ";
		endif;
        $this->table->add_row(anchor(base_url()."user/user_wall/$row[id]", $row['first_name']), anchor(base_url()."user/user_wall/$row[id]",$row['last_name']) ,anchor(base_url()."user/user_wall/$row[id]", $row['education']));
    }
			$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	endif;
		
	}


//for download message sentfile
	  public function download_sent_file(){
		$data['sub_folder']=$this->uri->segment('3');
		$fullPath = './send_files/'.$data['sub_folder'];
		  // Must be fresh start
		  if( headers_sent() )
			die('Headers Sent');
		  // Required for some browsers
		  if(ini_get('zlib.output_compression'))
			ini_set('zlib.output_compression', 'Off');
		  // File Exists?
		  if(file_exists($fullPath) ){
			// Parse Info / Get Extension
			$fsize = filesize($fullPath);
			$path_parts = pathinfo($fullPath);
			$ext = strtolower($path_parts["extension"]);
		
			  // Determine Content Type
			  switch ($ext) {
			  case "pdf": $ctype="application/pdf"; break;
			  case "exe": $ctype="application/octet-stream"; break;
			  case "zip": $ctype="application/zip"; break;
			  case "doc": $ctype="application/msword"; break;
			  case "xls": $ctype="application/vnd.ms-excel"; break;
			  case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
			  case "gif": $ctype="image/gif"; break;
			  case "png": $ctype="image/png"; break;
			  case "jpeg": $ctype="image/jpeg"; break;
			  case "jpg": $ctype="image/jpg"; break;
			  case "txt": $ctype="application/txt"; break;
			  default: $ctype="application/force-download";
			}
		
			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false); // required for certain browsers
			header("Content-Type: $ctype");
			header("Content-Disposition: attachment; filename=\"".basename($fullPath)."\";" );
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ".$fsize);
			ob_clean();
			flush();
			readfile( $fullPath );
		
		  } else
			die('File Not Found');
		
		}
	
//for download file
	public function download_file(){
		$arr["user_id"] = $this->session->userdata("user_id");
                //$this->uri->segment('3');;
		$data['sub_folder']=$this->uri->segment('3');
		$data['file_name']=$this->uri->segment('4');
	 	$fullPath = './userdata/'.$arr["user_id"].'/'.$data['sub_folder'].'/'.$data['file_name']; 
 
		echo $fullPath;
		  // Must be fresh start
		  if( headers_sent() ){
			die('Headers Sent');
		  }
		  // Required for some browsers
		  if(ini_get('zlib.output_compression')){
			ini_set('zlib.output_compression', 'Off');
		  }
		  // File Exists?
		  if(file_exists($fullPath) ){
			// Parse Info / Get Extension
			$fsize = filesize($fullPath);
			$path_parts = pathinfo($fullPath);
			$ext = strtolower($path_parts["extension"]);
		
			// Determine Content Type
			switch ($ext) {
			  case "pdf": $ctype="application/pdf"; break;
			  case "exe": $ctype="application/octet-stream"; break;
			  case "zip": $ctype="application/zip"; break;
			  case "doc": $ctype="application/msword"; break;
			  case "xls": $ctype="application/vnd.ms-excel"; break;
			  case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
			  case "gif": $ctype="image/gif"; break;
			  case "png": $ctype="image/png"; break;
			  case "jpeg": $ctype="image/jpeg"; break;
			  case "jpg": $ctype="image/jpg"; break;
			  case "txt": $ctype="application/txt"; break;
			  default: $ctype="application/force-download";
			}
		
			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false); // required for certain browsers
			header("Content-Type: $ctype");
			header("Content-Disposition: attachment; filename=\"".basename($fullPath)."\";" );
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ".$fsize);
			ob_clean();
			flush();
			echo readfile( $fullPath );
		
		  } else{
			die('File Not Found');
		  }
		
		}
//for view notes on my wall 
	public function view_notes(){
		$this->common->check_user_acess();
		$arr['userfile']=$this->uri->segment('4');
		$arr['sub_folder']=$this->uri->segment('3');
		$arr["user_id"] = $this->session->userdata("user_id");
		//$arr["privacy"] = $this->session->userdata("privacy");
		
		//$data['name']=$this->uri->segment('4');
		$data["item"]="Update File | Vo Notes";
		$data["master_title"]="Update File | ". $this->config->item('sitename'); 
		// for .txt file content
		$file = './userdata/'.$arr["user_id"]."/".$arr['sub_folder'].'/'.$arr['userfile'];
		$data['filedata'] = read_file($file);
		$data['resultset']=$this->user_model->get_file_data_db($arr);
		$data["master_body"]="view_notes";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
		 fclose($fp);	
	}


	
	

//for logout 
	public function logout(){
		 $this->session->sess_destroy();
		 delete_cookie("login");
		redirect(base_url());
	}
	
	//for user to my desk redirection
	public function user(){ //echo "sdfsdfsdf";
	  redirect(base_url());
	}

      
//for add friend
    public function add_friend() {
        $arr['reciver_id'] = $this->uri->segment('3');
        $arr["sender_id"] = $this->session->userdata("user_id");
        $data["item"] = "Add Friend";
        $data["master_title"] = "Add Friend | " . $this->config->item('sitename');
        $data["userdata"] = $this->session->set_userdata("tempdata", $arr);
        $where = array("reciver_id" => $arr['reciver_id'], "sender_id" => $arr["sender_id"], "status" => '0', "archive" => '0');
        //for chek friend request already sent or not
        $num_req = $this->common->chk_request('friend_requests', $where);
        if ($num_req == 0) {
            $result = $this->user_model->send_friend_request($arr);
            /* if($result){

              }else{
              $this->session->set_flashdata("errormsg","There is error updating content to database. Please contact database admin");
              } */
        }
    }

//for confirm friend request
    public function confirm_frnd_req() {
        $arr['sender_id'] = $this->uri->segment('3');
        $arr["reciver_id"] = $this->session->userdata("user_id");
        $data["item"] = "Confirm Friend";
        $data["master_title"] = "Confirm Friend | " . $this->config->item('sitename');
        $data["userdata"] = $this->session->set_userdata("tempdata", $arr);
        $where = array("reciver_id" => $arr['reciver_id'], "sender_id" => $arr["sender_id"], "status" => '0', "archive" => '0');
        //for chek friend request already sent or not
        $num_req = $this->common->chk_request('friend_requests', $where);
        echo $this->db->last_query($num_req); 

        if ($num_req == 1) {
            $result = $this->user_model->confirm_frnd_req_db($arr);
        }
    }



public function search_file(){
		$data['search_txt']=$this->uri->segment('3');
		$data["user_id"] = $this->session->userdata("user_id");
		//$pagename='search';
		$data["item"]="Search";
		$data["master_title"]="Search | ". $this->config->item('sitename'); 
		$data['resultset']=$this->user_model->getsearchpagedata($data);
		//$data["userdata"]=$this->session->userdata("tempdata",$data);
	//	$data["master_body"]="search";
	//	$data['sidebar_content']='user/left_bar';
	//	$this->load->theme('home_layout',$data);
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}