<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Subscriber extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->database();
    }
    function index()
    {

        //set table id in table open tag
        $tmpl = array ( 'table_open'  => '<div id="big_table" width="730"  border="1" cellpadding="2" cellspacing="1" class="">' );
        $this->table->set_template($tmpl); 
        
        $this->table->set_heading('File Name','Folder Name');

       $this->load->view('subscriber_view');
    }
    //function to handle callbacks
    function datatable()
    {
        $this->datatables->select('id,userfile,name')
        ->unset_column('id')
        ->from('user_files');
        
        echo $this->datatables->generate();
    }
}