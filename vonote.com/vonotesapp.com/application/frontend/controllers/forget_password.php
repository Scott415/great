<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class forget_password extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('email_model');
		$this->load->model('login_model');
		$this->load->model('store_model');
		
	}
	
	public function stores()
	{
		$data["item"]="Forget password";
		$data["master_title"]="Password Recovery";   // Please enter the title of page......
		$data["master_body"]="stores";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->layout('mainlayout',$data);  // Loading theme		
	}
	
	public function forgot_password()
	{
		if($this->session->userdata['consumer_id'] <> ''){
			redirect(base_url().'logins/users');	
		}
		else{
		$data["item"]="Forget password";
		$data["master_title"]="Password Recovery";   // Please enter the title of page......
		$data["master_body"]="forgot_password";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout_common',$data);  // Loading theme	
		}
	}
	public function forget_user_password()
	{
		$forget["email"]=$this->input->post("useremail");
		if($forget["email"]=="")
		{
			$this->session->set_flashdata("errormsg","Please enter your email address." );
		  	redirect(base_url()."forget_password/forgot_password");	
		}
		else if(!$this->common->validate_email($forget["email"]))
		{
			$this->session->set_flashdata("errormsg","Please enter a valid email address." );
		  	redirect(base_url()."forget_password/forgot_password");	
		}
		
		else
		{
			 $user_id=$this->common->getuser_id($forget["email"]);
			 
			$this->session->set_userdata("resultset",$forget["email"]);
			if($user_id["id"]=="")
			{
				$this->session->set_flashdata("errormsg","This email did not match our records");
				redirect(base_url()."forget_password/forgot_password");	
			}
			else
			{
				
				$link=base_url()."forget_password/update_password/?uid=".$user_id["id"]."&time=".time();	
				$emailarr["to"] =$user_id["email"];
				$emailarr["subject"] = "Password reset request";
				$sercharray = array('#ownme#','#link#');
				$replacearray = array($user_id["user_name"],$link);
				//$emailarr["message"] = str_replace($sercharray,$replacearray,$this->config->item('forget_password_email'));
				//$emailarr["message"] = "Dear ".$user_id["user_name"].",\n<br/>To login into the Social Tyer please verify your login on clicking the link below:\n\n<br/> ".base_url()."forget_password/update_password/?uid=".$user_id["id"]."&type=".base64_encode($user_id["type"])."&time=".time().".\n\n<br/><br/>Thanks Socail Tyer Support Team.";
				
			$emailarr["message"]	= "<p>Hi ".$user_id['user_name']."</p>
			
			<p>You may use the link below to change your password, and to continue using SocialTyer through your account:</p>
			
			<p>".base_url()."forget_password/update_password/?uid=".$user_id["id"]."&type=".base64_encode($user_id["type"])."&time=".time()."</p>
			
			<p>If you did not request a password change to your account, please ignore this message.</p>
			
			<p>Best Wishes,</p>
			
			<p>The SocialTyer Team</p>";
				
				
			if($this->email_model->sendIndividualEmail($emailarr))
				{
					$this->session->set_flashdata("successmsg","An email has been sent to you to reset your password.");
				}
				$this->session->set_flashdata("successmsg","An email has been sent to you to reset your password.");
			}
			redirect(base_url()."forget_password/forgot_password");	
		}
	}
	
	public function update_password()
	{
		$data["user_id"]=$this->input->get("uid");	
		$data["type"]=base64_decode($this->input->get("type"));	
		$data["time"]=$this->input->get("time");	
		
		if((time()-$time)<=3600)
		{
			$data["item"]="Expired link";
			$data["master_title"]="Expired link";   // Please enter the title of page......
			$data["master_body"]="expired_link";  //  Please use view name in this field please do not include '.php' for including view name
			$this->load->theme('mainlayout_common',$data);  // Loading theme		
		}
		else
		{
			$data["item"]="Update your password";
			$data["master_title"]="Update your password";   // Please enter the title of page......
			$data["master_body"]="update_user_password";  //  Please use view name in this field please do not include '.php' for including view name
			$this->load->theme('mainlayout_common',$data);  // Loading theme		
		}
	}
	public function update_new_password()
	{
		$data["type"]=$this->input->post("type");
		$data["user_id"]=$this->input->post("user_id");	
		$data["time"]=$this->input->post("time");
		$data["password"]=$this->input->post("password");
		$data["confirmpassword"]=$this->input->post("confirmpassword");	
		
		if($this->validations->update_password($data))
		{
			if($data["password"]==$data["confirmpassword"])
			{
				if($this->common->update_new_password($data))
				{
					$this->session->set_flashdata("successmsg","Your password has been successfully updated.");
					
						redirect(base_url());	
					
				}	
			}
			else
			{
				$this->session->set_flashdata("errormsg","Both passwords should match");
				redirect(base_url()."forget_password/update_password/?uid=".$data["user_id"]."&type=".base64_encode($data["type"])."&time=".$data["time"]);	
			}
		}
		else
		{
				redirect(base_url()."forget_password/update_password/?uid=".$data["user_id"]."&type=".base64_encode($data["type"])."&time=".$data["time"]);
		}
	}
}

?>