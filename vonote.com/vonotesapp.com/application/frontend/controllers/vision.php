<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class vision extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->load->helper('url');
		$this->load->model('page_model');		
	}

	public function index()
	{
		//$this->load->view('home');
		$this->vision();
	}
	public function vision(){
		$pagename = "vision";
		$data["item"]="Vision";
		$data["master_title"]="Vision | ". $this->config->item('sitename'); 
		$data["resultset"]=$this->common->getcontentpagedata($pagename);
		$data["master_body"]="vision";
		$this->load->theme('home_layout',$data);
	}
	
}
