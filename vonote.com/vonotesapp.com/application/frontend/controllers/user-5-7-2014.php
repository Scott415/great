<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("user_model");	
		
	}

	public function index(){
		$this->load->view('home');
		$this->my_desk();
		
	}
	//for left bar
	public function left_bar(){
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Wall | ".$this->config->item('sitename');   // Please enter the title of page......
		$this->load->theme("head",$data);
		$this->load->view("user/left_bar",$data);
	}
	//for load view for my profile/my desk page
	public function my_desk(){
		$this->common->check_user_acess();
		//echo "dadas".file_logo("php");
		$arr["user_id"] = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]="My Desk | ". $this->config->item('sitename'); 
		$data['resultset']=$this->user_model->my_desk_files_from_db($arr);
		$data['userdata']=$this->user_model->user_profile_data($arr);
		$data["master_body"]="my_desk";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	
//for update user profile page
	public function update_profile(){
		$this->common->check_user_acess();
		$arr["user_id"] = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]="Update Profile | ". $this->config->item('sitename'); 
		$data['resultset']=$this->user_model->user_profile_data($arr);
		$data["master_body"]="update_profile";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	
	public function update_profile_db(){
		$arr["user_id"] = $this->session->userdata("user_id");
		$arr["first_name"]= mysql_real_escape_string($this->input->post("first_name"));
		$arr["last_name"]= mysql_real_escape_string($this->input->post("last_name"));
		$arr["education"]= mysql_real_escape_string($this->input->post("education"));
		$arr["worked"]= mysql_real_escape_string($this->input->post("worked"));
		$arr["about_me"]= mysql_real_escape_string($this->input->post("about_me"));
		$arr["image"]=$_FILES["userfile"]["name"];
		if($arr["image"] != ""){
			$arr["image"]=time().".".$this->common->get_extension($_FILES["userfile"]["name"]);
		}
		else{
			$arr["image"]=$this->input->post("image");	
		}
		$this->session->set_flashdata("tempdata",$arr);	
		if($this->validations->validate_user_profile_data($arr)){
			if($this->user_model->add_edit_user($arr)){
				//$last_id = $this->db->insert_id();
				if($arr["image"]!=$this->input->post("image")){
					$config['upload_path'] = './blogimages/';
					$config['allowed_types'] = '*';
					$config['file_name']=$arr["image"];
					$this->upload->initialize($config);
					if($this->upload->do_upload()){
						$err=0;
					}		
					else{
						//echo $this->upload->display_errors();die;
						$this->session->set_flashdata("successmsg","There is some error uploading the files to server. Please contact server admin");		
					}		
				}
					$this->session->set_flashdata("successmsg","Profile updated succesfully");
					$err=2; // for blog updated succesfully
					redirect(base_url()."user/update_profile");	
			}	
			else{
				$this->session->set_flashdata("errormsg","There is error updating profile to database . Please contact Admin");
				$err=1;
			}
		}
		else{
			$err=1;
			redirect(base_url()."user/update_profile");		
		}
		
	}
	
	
	//for load view for Create Folder
	public function create_folder(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]="Create Folder | ". $this->config->item('sitename'); 
		$data["master_body"]="create_folder";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	//for create floder pop up
	public function popup(){
		$data['relation']=$this->uri->segment('3');
		$data['name']=$this->uri->segment('4');
		$data["item"]="Vo Notes";
		$data["master_title"]= "Create Folder | ".$this->config->item('sitename');   // Please enter the title of page......
		//$data['blogresult']=$this->blog_model->getIndividualBlog($blogid);
		$data["userdata"]=$this->session->userdata("tempdata",$data);
		$this->load->theme("head",$data);
		$this->load->view("user/popup",$data);
	}
	//create folder 
	public function create_folder_to_database(){
		$arr["name"]= mysql_real_escape_string($this->input->post("name"));
		//$arr["privacy"]= mysql_real_escape_string($this->input->post("privacy"));
		$arr["privacy"]= 1; /// default 1 for private folder
		$arr["relation"]= $this->session->userdata("user_id");
		
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		if($this->validations->validate_create_folder_data($arr)){
			$result = $this->user_model->create_folder($arr);
			if($result){
				$path = './userdata/'.$arr["relation"].'/'.$arr["name"];
				if(!is_dir($path)){ //create the folder if it's not already exists
				  mkdir($path,0755,true);
				} 
				$err=0;
				$this->session->set_flashdata("successmsg","Folder created successfully");
				$this->session->unset_userdata('tempdata');
				
				
			}else{
				 $this->session->set_flashdata("errormsg","There is error updating content to database. Please contact database admin");	
				 redirect(base_url()."user/popup/".$arr["relation"]."/".$arr["name"]);
			}
			redirect(base_url()."user/popup/".$arr["relation"]."/".$arr["name"]."/2");
		}else{
			redirect(base_url()."user/popup/".$arr["relation"]."/".$arr["name"]);
		}
	}
	
	
//for delete a folder
	public function delete_a_folder(){
		$data['page_name']=$this->uri->segment('2'); 
		$data['user_id']=$this->uri->segment('3');
		$data['folder_name']=$this->uri->segment('4');
		$data['file_id']=$this->uri->segment('5');
		if($this->validations->validate_delete_folder($data)){ 
			if($this->user_model->delete_user_uploaded_folder($data)){ 
			    $this->user_model->delete_user_uploaded_file_f($data);
				$path = './userdata/'.$data["user_id"].'/'.$data["folder_name"]; 
				delete_files($path, true); 
				rmdir($path);
				$err=0;
				$this->session->set_flashdata("successmsg","File removed succesfully");
			}else{
				$this->session->set_flashdata("errormsg","There is error removing file to data base . Please contact database admin or try later");
				$err=1;
				
			}
		}else{ //echo "hjkghjk";
		}
		 redirect(base_url()."user/my_desk");
	}
	
	//for upload file to floder pop up
	public function popup1(){ 
		$data['relation']=$this->uri->segment('3');
		$data['name']=$this->uri->segment('4');
		$data["item"]="Vo Notes";
		$data["master_title"]= "Create Folder | ".$this->config->item('sitename');   // Please enter the title of page......
		//$data['blogresult']=$this->blog_model->getIndividualBlog($blogid);
		$data["userdata"]=$this->session->userdata("tempdata",$data);
		$this->load->theme("head",$data);
		$this->load->view("user/popup1",$data);
	}
	//for upload folder file to database
	public function upload_file_folder_to_database(){
		$page_name =  $this->uri->segment(2);
		$arr["relation"]=$this->input->post("relation");
		$arr["name"]=$this->input->post("name");
		$arr["privacy"]=$this->input->post("privacy");
		$arr["userfile"]=$_FILES["userfile"]["name"];
		if($arr["userfile"] != ""){
			$arr["userfile"]=time().".".$this->common->get_extension($_FILES["userfile"]["name"]);
		}
		else{
			$this->session->set_flashdata("errormsg","Please upload file");	
			//$arr["userfile"]=$this->input->post("userfile");	
		}
		$this->session->set_flashdata("tempdata",$arr);	
		 $relationa; echo $namea;echo $pagenamea; 
		if($this->validations->validate_upload_file_folder_data($arr)){
			if($this->user_model->upload_file_under_folder_db($arr)){
			//	$last_id = $this->db->insert_id();
				
				if($arr["userfile"]!=$this->input->post("userfile")){
					$path = './userdata/'.$arr["relation"].'/'.$arr["name"].'/';
					$config['upload_path'] = $path;
					$config['allowed_types'] = '*';
					$config['file_name']=$arr["userfile"];
					$this->upload->initialize($config);
					if($this->upload->do_upload()){
						$err=0;
					}		
					else{
						//echo $this->upload->display_errors();die;
						$this->session->set_flashdata("successmsg","There is some error uploading the files to server. Please contact server admin");		
					}		
				}
					$this->session->set_flashdata("successmsg","File uploaded succesfully");
					$err=0;      // for blog added succesfully
					//redirect(base_url()."user/my_desk");
			}	
			else{
				$this->session->set_flashdata("errormsg","There is error uploading file to data base . Please contact database admin");
				$err=1;
			}
			 redirect(base_url()."user/popup1/".$arr["relation"]."/".$arr["name"]."/".$page_name."/2");	
		}
		else{
			$err=1;
			 redirect(base_url()."user/popup1/".$arr["relation"]."/".$arr["name"]."/".$page_name);		
		}
		 redirect(base_url()."user/popup1/".$arr["relation"]."/".$arr["name"]."/".$page_name);
	}
	//for copy file to floder pop up
	public function popup_copyfile(){ 
		$data['relation']=$this->uri->segment('3');
		$data['name']=$this->uri->segment('4');
		$data["item"]="Vo Notes";
		$data["master_title"]= "Copy File | ".$this->config->item('sitename');   // Please enter the title of page......
		//$data['blogresult']=$this->blog_model->getIndividualBlog($blogid);
		$data["userdata"]=$this->session->userdata("tempdata",$data);
		
		$this->load->theme("head",$data);
		$this->load->view("user/popup_copyfile",$data);
	}
	
//for delete a file
	public function delete_a_file(){ 
		$data['user_id']=$this->uri->segment('3');
		$data['folder_name']=$this->uri->segment('4');
		$data['file_id']=$this->uri->segment('5');
		if($this->validations->validate_delete_file($data)){ 
			if($this->user_model->delete_user_uploaded_file($data)){ 
				$err=0;
				$this->session->set_flashdata("successmsg","File removed succesfully");
			}else{
				$this->session->set_flashdata("errormsg","There is error removing file to data base . Please contact database admin or try later");
				$err=1;
				
			}
		}else{ //echo "hjkghjk";
		}
		 redirect(base_url()."user/my_upload_files/".$data["user_id"].'/'.$data["folder_name"]);
	}
	
//for create file pop up
	public function file_popup(){
		$data['relation']=$this->uri->segment('3');
		$data['name']=$this->uri->segment('4');
		$data["item"]="Vo Notes";
		$data["master_title"]= "Create File | ".$this->config->item('sitename');   // Please enter the title of page......
		//$data['blogresult']=$this->blog_model->getIndividualBlog($blogid);
		$data["userdata"]=$this->session->userdata("tempdata",$data);
		$this->load->theme("head",$data);
		$this->load->view("user/file_popup",$data);
	}
	
//for create  file to database
	public function created_file_folder_to_database(){
		$page_name =  $this->uri->segment(2);
		$arr["relation"]=$this->input->post("relation");
		$arr["name"]=$this->input->post("name");
		$arr["file_content"]=$this->input->post("file_content");
		$arr["folder_name"]=$this->input->post("folder_name");
		$arr["privacy"]=$this->input->post("privacy");
		$arr["userfile"] = $arr["name"].'.txt';
		//$arr["userfile"]=$_FILES["userfile"]["name"];
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		if($this->validations->validate_create_file_folder_data($arr)){
		  
		  $content = $arr["file_content"];
		  $fp = fopen('./userdata/'.$arr["relation"].'/'.$arr["folder_name"].'/'.$arr["name"].".txt","wb");
		  fwrite($fp,$content);
		  fclose($fp);	
			if($this->user_model->create_file_under_folder_db($arr)){
					$this->session->set_flashdata("successmsg","File Created succesfully");
					$err=0;      // for blog added succesfully
					$this->session->unset_userdata('tempdata');
					//redirect(base_url()."user/my_desk");
			}	
			else{
				$this->session->set_flashdata("errormsg","There is error uploading file to data base . Please contact database admin");
				$err=1;
			}
			redirect(base_url()."user/file_popup/".$arr["relation"]."/".$arr["folder_name"]."/".$page_name."/2");	
		}
		else{
			$err=1;
			 redirect(base_url()."user/file_popup/".$arr["relation"]."/".$arr["folder_name"]."/".$page_name);		
		}
		 redirect(base_url()."user/file_popup/".$arr["relation"]."/".$arr["folder_name"]."/".$page_name);
	}
	
//for update created file 
	public function update_my_file(){
		$this->common->check_user_acess();
		$arr['userfile']=$this->uri->segment('4');
		$arr['sub_folder']=$this->uri->segment('3');
		$arr["user_id"] = $this->session->userdata("user_id");
		//$arr["privacy"] = $this->session->userdata("privacy");
		
		//$data['name']=$this->uri->segment('4');
		$data["item"]="Update File | Vo Notes";
		$data["master_title"]="Update File | ". $this->config->item('sitename'); 
		// for .txt file content
		$file = './userdata/'.$arr["user_id"]."/".$arr['sub_folder'].'/'.$arr['userfile'];
		$data['filedata'] = read_file($file);
		$data['resultset']=$this->user_model->get_file_data_db($arr);
		$data["master_body"]="update_my_file";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
		 fclose($fp);	
	}


//for update  file to database
	public function edit_file_folder_to_database(){
		$arr["userfile"]=$this->input->post("userfile");
		$arr["sub_folder"]=$this->input->post("sub_folder");
		$arr["file_content"]=$this->input->post("file_content");
		$arr["user_id"]=$this->input->post("user_id");
		$arr["privacy"]=$this->input->post("privacy");
		//$arr["userfile"] = $arr["name"].'.txt';
		//$arr["userfile"]=$_FILES["userfile"]["name"];
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		$path = base_url().'user/update_my_file/'.$arr["sub_folder"].'/'.$arr["userfile"];
		if($this->validations->validate_update_file_folder_data($arr)){
		  
		  $content = $arr["file_content"];
		  write_file('./userdata/'.$arr["user_id"].'/'.$arr["sub_folder"].'/'.$arr["userfile"], $arr["file_content"]);
		  /*$fp = fopen('./userdata/'.$arr["user_id"].'/'.$arr["sub_folder"].'/'.$arr["userfile"].".txt","wb");
		  fwrite($fp,$content);
		  fclose($fp);	*/
			if($this->user_model->update_file_under_folder_db($arr)){
					$this->session->set_flashdata("successmsg","File Updated succesfully");
					$err=0;      // for blog added succesfully
					$this->session->unset_userdata('tempdata');
					//redirect(base_url()."user/my_desk");
			}	
			else{
				$this->session->set_flashdata("errormsg","There is error uploading file to data base . Please contact database admin");
				$err=1;
			}
		}
		else{
			$err=1;
			 redirect($path);		
		}
		 redirect($path);
	}	
	
	//for view file pop up
	public function view_file_popup(){
		$this->common->check_user_acess();
		$arr['namea']=$this->uri->segment('4');
		$arr['relationa']=$this->uri->segment('3');
		$arr['filea']=$this->uri->segment('5');
		
		$arr["item"]="View File | Vo Notes";
		$arr1["master_title"]="View File | ". $this->config->item('sitename'); 
		// for .txt file content
	//	$file = './userdata/'.$arr["user_id"]."/".$arr['sub_folder'].'/'.$arr['userfile'];
		//$data['filedata'] = read_file($file);
		$data['userdata']=$this->session->set_userdata("tempdata",$arr);
		//print_r($data['userdata']);
		//$data['resultset']=$this->session->set_userdata("khattra",$data);;
		$data["master_body"]="view_file_popup";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
		
		
		/*//$this->load->library('session');
		$this->session->unset_userdata('khattra');
		$arr1['relationa']=$this->uri->segment('3');
		$arr1['namea']=$this->uri->segment('4');
		$arr1['filea']=$this->uri->segment('5');
		$this->session->set_userdata("khattra",$arr1);
		$this->load->theme("head",$data);
	//	$this->load->view("user/view_file_popup",$data);
	    $this->load->theme('home_layout',$data);*/

	}
	
	
	/*public function my_costom_folder(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Costom Folder | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="my_costom_folder";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}*/
// list of recent files uploaded by user	
	public function my_recent_files(){
		$arr["user_id"] = $this->session->userdata("user_id");
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Recent Files | ".$this->config->item('sitename');   //Please enter the title of page......
		$data['resultset']=$this->user_model->my_recent_files_from_db($arr);
		$data["master_body"]="my_recent_files";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}

// for upload user files	
	public function my_upload(){
		$data['relation']=$this->uri->segment('3');
		$data['name']=urldecode($this->uri->segment('4'));
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Recent Files | ".$this->config->item('sitename');   //Please enter the title of page......
		$data['resultset']=$this->user_model->my_uploaded_file_under_folder_from_db($data); 
		$data["master_body"]="my_upload_files";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);

		/*$data["item"]="Vo Notes";
		$data["master_title"]= "My Recent Files | ".$this->config->item('sitename');   //Please enter the title of page......
		$data["master_body"]="my_upload";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);*/
	}
// for upload user files	
	public function my_upload_files(){
		$data['relation']=$this->uri->segment('3');
		$data['name']= urldecode($this->uri->segment('4'));
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Recent Files | ".$this->config->item('sitename');   //Please enter the title of page......
		$data['resultset']=$this->user_model->my_uploaded_file_under_folder_from_db($data); 
		$data["master_body"]="my_upload_files";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}	
//for my friend list
	public function my_friends(){
		$this->common->check_user_acess();
		$arr["user_id"] = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Friends | ".$this->config->item('sitename');   // Please enter the title of page......
		$data['resultset']=$this->user_model->my_friend_list($arr); //echo $this->db->last_query();
		$data["master_body"]="my_friends";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
//for my conversation
	public function my_messages(){
		$this->common->check_user_acess();
		$conversation_id=$this->uri->segment('3');
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Messages | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="my_messages";
		$data['resultset']=$this->user_model->my_conversation($conversation_id);
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
//for message reply
	public function reply_message(){
		$arr['conversation_id']=$this->input->post('conversation_id');
		$arr['sender_id']=$this->input->post('user_id');
		$arr['reciver_id']=$this->input->post('reciver_id');
		$arr['message']= trim($this->input->post('message'));
		$arr["image"]=$_FILES["userfile"]["name"];
		if($arr["image"] != ""){
			$arr["image"]=time().".".$this->common->get_extension($_FILES["userfile"]["name"]);
		}
		else{
			$arr["image"]=$this->input->post("image");	
		}
		if($arr['message'] <> ''){
			$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
			if($this->validations->validate_send_message($arr)){ 
				if($this->user_model->my_reply_data($arr)){	
					if($arr["image"]!=$this->input->post("image")){
					$config['upload_path'] = './send_files/';
					$config['allowed_types'] = '*';
					$config['file_name']=$arr["image"];
					$this->upload->initialize($config);
					if($this->upload->do_upload()){
						$err=0;
					}		
					else{
						//echo $this->upload->display_errors();die;
						$this->session->set_flashdata("successmsg","There is some error uploading the files to server. Please contact server admin");		
					}	
				}
					$this->session->set_flashdata("successmsg","Message Sent Successfully");
					$err=0;      // for blog added succesfully
					$this->session->unset_userdata('tempdata');
				}
				else{
					$this->session->set_flashdata("errormsg","There is error sending message. Please contact Admin");
					$err=1;
				}
			}
    	}
		else{
			$this->session->set_flashdata("errormsg","Error : Please enter message in test box<br>");
			$err=1;
		}

		redirect(base_url()."user/my_messages/".$arr["conversation_id"]."/".$arr['reciver_id']);
	}
	
	//for send message 
	public function send_message(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "Send Messages | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="send_message";
		$data['sidebar_content']='user/left_bar';
		$data["userdata"]=$this->session->userdata("tempdata",$data);
		$this->load->theme('home_layout',$data);
		
	}

//for send message
	public function send_message_db(){
		$this->common->check_user_acess();
		$arr["sender_id"]=$this->input->post("sender_id");
		$arr["reciver_id"]=$this->input->post("reciver_id");
		$arr["message"]=trim($this->input->post("message"));
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		$arr["image"]=$_FILES["userfile"]["name"];
		if($arr["image"] != ""){
			$arr["image"]=time().".".$this->common->get_extension($_FILES["userfile"]["name"]);
		}
		else{
			$arr["image"]=$this->input->post("image");	
		}
		if($this->validations->validate_send_message($arr)){ 
			if($this->user_model->send_message_to_db($arr)){
				if($arr["image"]!=$this->input->post("image")){
					$config['upload_path'] = './send_files/';
					$config['allowed_types'] = '*';
					$config['file_name']=$arr["image"];
					$this->upload->initialize($config);
					if($this->upload->do_upload()){
						$err=0;
					}		
					else{
						//echo $this->upload->display_errors();die;
						$this->session->set_flashdata("successmsg","There is some error uploading the files to server. Please contact server admin");		
					}	
				}
	
				$this->session->set_flashdata("successmsg","Message Sent Successfully");
					$err=0;      // for blog added succesfully
					$this->session->unset_userdata('tempdata');
			}
			else{
				$this->session->set_flashdata("errormsg","There is error sending message. Please contact Admin");
				$err=1;
			}
		}
		/*else{
			$this->session->set_flashdata("errormsg","There is error sending message. Please contact Admin");
			$err=1;
		}*/
		redirect(base_url()."user/send_message/".$arr["reciver_id"]);
	}
		
//for user inbox
	public function inbox(){
		$this->common->check_user_acess();
		$user_id = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Messages | ".$this->config->item('sitename');   // Please enter the title of page......
		$data['resultset']=$this->user_model->my_inbox($user_id);
		$data["master_body"]="inbox";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	
//for delete message
	public function delete_a_message(){  
		 $data['meesage']=$this->uri->segment('3'); 
		 $data['conversation_id']=$this->uri->segment('4');
		 $data['reciver_id']=$this->uri->segment('5');
//print_r($data); die;
		//if($this->validations->validate_delete_message($data)){ //echo "hjkghjk";
			if($this->user_model->delete_user_message($data)){  
				$err=0;
				$this->session->set_flashdata("successmsg","Message removed succesfully");
			}else{
				$this->session->set_flashdata("errormsg","There is error removing message to data base . Please contact admin or try later");
				$err=1;
				
			}
		/*}else{ //echo "hjkghjk";
		}*/
		 redirect(base_url()."user/my_messages/".$data['conversation_id'].'/'.$data['reciver_id']);
	}

	
	
	public function my_settings(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Settings | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="my_settings";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	public function search(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "Search | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="search";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	//for update/change password
	public function reset_password(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data['userdata']=$this->session->userdata('tempdata');
		$data["master_title"]= "Reset Password | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="reset_password";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	//reset/update password in database
	public function update_password_to_db(){
		//$this->common->check_user_acess();
		$arr["id"] = mysql_real_escape_string($this->input->post("login_user_id"));
		$arr["old_password"] = mysql_real_escape_string($this->input->post("old_password"));
		$arr["new_password"] = mysql_real_escape_string(trim($this->input->post("new_password")));
		$arr["confirm_password"] = mysql_real_escape_string(trim($this->input->post("confirm_password")));
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		//print_r();
		if($this->validations->update_password($arr)){
			if($this->user_model->changeUserPassword($arr)){
				$this->session->set_flashdata("successmsg","Your password has been successfully updated.");
				$this->session->set_flashdata("tempdata");
				$err=0;
				$this->session->unset_userdata('tempdata');
			}	
			else{
				$err=1;
				$this->session->set_flashdata("errormsg","Database error occurred, please contact admin.");
			}
			
		}
		redirect(base_url()."user/reset_password");
	}
	
	//for after login first time with facebook set password
	public function fb_new_password(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "Set Password | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="fb_new_password";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	//for after login first time with facebook set password in database
	public function fb_new_password_to_db(){
		//$this->common->check_user_acess();
		$arr["id"] = mysql_real_escape_string($this->session->userdata("user_id"));
		$arr["new_password"] = mysql_real_escape_string(trim($this->input->post("new_password")));
		$arr["confirm_password"] = mysql_real_escape_string(trim($this->input->post("confirm_password")));
		$data["userdata"]=$this->session->set_userdata("tempdata",$arr);
		if($this->validations->set_password_fb($arr)){
			if($this->user_model->changeUserPassword_fb($arr)){
				$this->session->set_flashdata("successmsg","Your password has been successfully updated.");
				$this->session->set_flashdata("tempdata");
				$err=0;
			}	
			else{
				$err=1;
				$this->session->set_flashdata("errormsg","Database error occurred, please contact admin.");
			}
			
		}
		redirect(base_url()."user/fb_new_password");
	}


	public function notification(){
		$this->common->check_user_acess();
		$arr['reciver_id'] = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]= "Notification | ".$this->config->item('sitename');   // Please enter the title of page......
		$data['resultset']=$this->user_model->notifications_page_db($arr);
		$data["master_body"]="notification";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	
	//for my wall
	public function my_bulletin_board(){
		$this->common->check_user_acess();
		$arr["user_id"] = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]= "My Bulletin Board | ".$this->config->item('sitename');   // Please enter the title of page......
		$data['resultset']=$this->user_model->my_wall_files_from_db($arr);
		$data['userdata']=$this->user_model->user_profile_data($arr);
		$data["master_body"]="my_wall";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	//for user wall
	public function user_wall(){
		$this->common->check_user_acess();
		$arr['user_id']=$this->uri->segment('3');
		$data["item"]="Vo Notes";
		$data["master_title"]= "User Bulletin Board | ".$this->config->item('sitename');   // Please enter the title of page......
		$data['resultset']=$this->user_model->my_wall_files_from_db($arr);
		$data['userdata']=$this->user_model->user_profile_data($arr);
		$data["master_body"]="user_wall";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	
	//file data for auto suggestion
	public function file_data(){
		$arr["user_id"] = $this->session->userdata("user_id");
		$data["item"]="Vo Notes";
		$data["master_title"]= "file data | ".$this->config->item('sitename');   // Please enter the title of page......
		$this->load->theme("head",$data);
		$data['resultset']=$this->user_model->file_data_db($arr);
		$data["master_body"]="file_data";
	//	$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout1',$data);
	}
	
	//for search friend
	public function find_friend(){
		$this->common->check_user_acess();
		$data["item"]="Vo Notes";
		$data["master_title"]= "Search New Friend | ".$this->config->item('sitename');   // Please enter the title of page......
		$data["master_body"]="find_friend";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
	}
	
	//for download message sentfile
	public function download_sent_file(){
		$data['sub_folder']=$this->uri->segment('3');
		$fullPath = './send_files/'.$data['sub_folder'];
		  // Must be fresh start
		  if( headers_sent() )
			die('Headers Sent');
		  // Required for some browsers
		  if(ini_get('zlib.output_compression'))
			ini_set('zlib.output_compression', 'Off');
		  // File Exists?
		  if(file_exists($fullPath) ){
			// Parse Info / Get Extension
			$fsize = filesize($fullPath);
			$path_parts = pathinfo($fullPath);
			$ext = strtolower($path_parts["extension"]);
		
			// Determine Content Type
			switch ($ext) {
			  case "pdf": $ctype="application/pdf"; break;
			  case "exe": $ctype="application/octet-stream"; break;
			  case "zip": $ctype="application/zip"; break;
			  case "doc": $ctype="application/msword"; break;
			  case "xls": $ctype="application/vnd.ms-excel"; break;
			  case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
			  case "gif": $ctype="image/gif"; break;
			  case "png": $ctype="image/png"; break;
			  case "jpeg": $ctype="image/jpeg"; break;
			  case "jpg": $ctype="image/jpg"; break;
			  case "txt": $ctype="application/txt"; break;
			  default: $ctype="application/force-download";
			}
		
			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false); // required for certain browsers
			header("Content-Type: $ctype");
			header("Content-Disposition: attachment; filename=\"".basename($fullPath)."\";" );
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ".$fsize);
			ob_clean();
			flush();
			readfile( $fullPath );
		
		  } else
			die('File Not Found');
		
		}
	
	//for download file
	public function download_file(){
		$arr["user_id"] = $this->session->userdata("user_id");//$this->uri->segment('3');;
		$data['sub_folder']=$this->uri->segment('3');
		$data['file_name']=$this->uri->segment('4');
	 	$fullPath = './userdata/'.$arr["user_id"].'/'.$data['sub_folder'].'/'.$data['file_name']; 
		//echo $fullPath;
		  // Must be fresh start
		  if( headers_sent() ){
			die('Headers Sent');
		  }
		  // Required for some browsers
		  if(ini_get('zlib.output_compression')){
			ini_set('zlib.output_compression', 'Off');
		  }
		  // File Exists?
		  if(file_exists($fullPath) ){
			// Parse Info / Get Extension
			$fsize = filesize($fullPath);
			$path_parts = pathinfo($fullPath);
			$ext = strtolower($path_parts["extension"]);
		
			// Determine Content Type
			switch ($ext) {
			  case "pdf": $ctype="application/pdf"; break;
			  case "exe": $ctype="application/octet-stream"; break;
			  case "zip": $ctype="application/zip"; break;
			  case "doc": $ctype="application/msword"; break;
			  case "xls": $ctype="application/vnd.ms-excel"; break;
			  case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
			  case "gif": $ctype="image/gif"; break;
			  case "png": $ctype="image/png"; break;
			  case "jpeg": $ctype="image/jpeg"; break;
			  case "jpg": $ctype="image/jpg"; break;
			  case "txt": $ctype="application/txt"; break;
			  default: $ctype="application/force-download";
			}
		
			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false); // required for certain browsers
			header("Content-Type: $ctype");
			header("Content-Disposition: attachment; filename=\"".basename($fullPath)."\";" );
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ".$fsize);
			ob_clean();
			flush();
			readfile( $fullPath );
		
		  } else{
			die('File Not Found');
		  }
		
		}
//for view notes on my wall 
	public function view_notes(){
		$this->common->check_user_acess();
		$arr['userfile']=$this->uri->segment('4');
		$arr['sub_folder']=$this->uri->segment('3');
		$arr["user_id"] = $this->session->userdata("user_id");
		//$arr["privacy"] = $this->session->userdata("privacy");
		
		//$data['name']=$this->uri->segment('4');
		$data["item"]="Update File | Vo Notes";
		$data["master_title"]="Update File | ". $this->config->item('sitename'); 
		// for .txt file content
		$file = './userdata/'.$arr["user_id"]."/".$arr['sub_folder'].'/'.$arr['userfile'];
		$data['filedata'] = read_file($file);
		$data['resultset']=$this->user_model->get_file_data_db($arr);
		$data["master_body"]="view_notes";
		$data['sidebar_content']='user/left_bar';
		$this->load->theme('home_layout',$data);
		 fclose($fp);	
	}


	
	
	//for logout 
	public function logout(){
		 $this->session->sess_destroy();
		 delete_cookie("login");
		redirect(base_url());
	}
	
	//for user to my desk redirection
	public function user(){ //echo "sdfsdfsdf";
	  redirect(base_url());
	}
	
}

