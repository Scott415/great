<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class uploads extends CI_Controller{
	public function _construct(){
		parent :: __construct();	

		$this->load->library('upload');
		$this->load->model("user_model");			
	}
	public function post_file(){	
		$f_relation =$this->uri->segment('3');
		$f_name =urldecode($this->uri->segment('4'));
		$allowed_ext = array('jpg','jpeg','png','gif');
		//$config['upload_path'] = $this->config->item('upload_path');
		$config['upload_path'] = './userdata/'.$f_relation.'/'.$f_name.'';
		$config['allowed_types'] =$this->config->item('allowed_types');
		$ext_n = end(explode('.', $_FILES['userfile']['name']));
		$video_name=time().'.'.$ext_n;
        $config['file_name'] = $video_name;
		
		
		$this->upload->initialize($config);
		if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
			$this->exit_status('Error! Wrong HTTP method!');
		}
		
		 
		
			if($this->upload->do_upload()){
				// $ext_n = end(explode('.', $arr['file_name']));
				$arr['file_name']=$config['file_name'];
				$arr=$this->upload->data();
				$this->load->database();
				mysql_query("insert into user_files(userfile,ext,user_id,folder_name,sub_folder, status,time ) values('".$arr['file_name']."', '".$ext_n."', '".$f_relation."', '".$f_relation."', '".$f_name."', '1', '".time()."')");
				$this->give_status($arr['file_name'],'File was uploaded successfuly!');
			}
			else{
				$this->exit_status($this->upload->display_errors());
			}
		// Helper functions
	}

	public function exit_status($str){
		echo json_encode(array('status'=>$str));
		exit;
	}

	public function get_extension($file_name){
		$ext = explode('.', $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}

	public function give_status($filename,$str){
		echo json_encode(array('filename'=>$filename,'status'=>$str));
		exit;
	}

}



?>