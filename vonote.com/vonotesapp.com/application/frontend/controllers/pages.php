<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pages extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('page_model');
		$this->load->model('email_model');
	}
	
	public function index()
	{
		//$this->load->view('home');
		$this->terms_of_use();
		$this->privacy_policy();
		$this->about_us();
		
	}

	//for terms and conditions
	public function terms_of_use(){
		$pagename = "terms";
		$data["item"]="Vo Notes";
		$data["master_title"]="Terms Of Service | ". $this->config->item('sitename'); 
		//$data['active'] = $pagedata;
		$data["resultset"]=$this->page_model->getPageData($pagename);	
		$data['master_body'] = 'terms_of_use';
		$this->load->theme("home_layout",$data);	
	}
	
	//for  privacy_policy
	public function privacy_policy(){
		$pagename = "privacy_policy";
		$data['item'] = 'Privacy Policy';
		//$data['active'] = $pagedata;
		$data["resultset"]=$this->page_model->getPageData($pagename);	
		$data['master_title'] = 'Privacy Policy | '. $this->config->item('sitename');
		$data['master_body'] = 'privacy_policy';
		$this->load->theme("home_layout",$data);	
	}
	//for about company
	public function about_us()
	{
		$pagename = "about_us";
		$data['item'] = 'About Us';	
		//$data['active'] = $pagedata;
		$data["resultset"]=$this->page_model->getPageData($pagename);	
		$data['master_title'] = 'About Us | '. $this->config->item('sitename');
		$data['master_body'] = 'about_us';
		$this->load->theme("home_layout",$data);	
	}
	//for careers
	public function careers()
	{ 
		$pagename = "careers";
		$data['item'] = 'Careers';	
		//$data['active'] = $pagedata;
		$data["resultset"]=$this->page_model->getPageData($pagename);
		$data['master_title'] = 'Careers | '. $this->config->item('sitename');
		$data['master_body'] = 'careers';
		$this->load->theme("home_layout",$data);	
	}
	//for Security page
	public function security()
	{
		$pagename = "security_page";
		$data['item'] = 'Security';	
		//$pagedata='security';
		//$data["contactdata"]=$this->session->flashdata("tempdata");
		$data["resultset"]=$this->common->getcontentpagedata($pagename);
		$data['master_title'] = 'Security | '. $this->config->item('sitename');
		$data['master_body'] = 'security';
		$this->load->theme("home_layout",$data);	
	}
	//for contact us page
	public function contact_us(){
		$data['item'] = 'Contact Us';	
		//$pagedata='contact_us';
		$data["contactdata"]=$this->session->flashdata("tempdata");
		//$data['about_us'] = $this->page_model->getPageData($pagedata);
		$data['master_title'] = 'Contact Us | '. $this->config->item('sitename');
		$data['master_body'] = 'contact_us';
		$this->load->theme("home_layout",$data);	
	}
	
	//for contact us form
	public function add_contact_to_admin(){ //echo "sdfsdfsdf"; die;
		$arr["first_name"]=$this->input->post("first_name");
		$arr["last_name"]=$this->input->post("last_name");
		$arr["email"]=$this->input->post("email");
		$arr["phone"]=$this->input->post("phone");
		$arr["message"]=$this->input->post("message");
		$data["contactdata"]=$this->session->set_flashdata("tempdata",$arr);
		if($this->validations->validate_contact_details($arr)){
			//$arr["to"]='vonotesmail@gmail.com';  
			$arr["to"]="vonotesmail@gmail.com";
			//$arr["to"]="gurpreetkhattra@slinfy.com";
			$arr["subject"] = "You have receive a contact us request from ".$this->config->item('sitename')."!";
			//$arr["to"]= $adminemail;  
			$arr["message1"] = "<p>Hi Admin</p>
				<p>You have receive a contact us request ".$this->config->item('sitename')."!</p> 
				<p>Name : ".$arr["first_name"].' '.$arr["last_name"]."</p>
				<p>Email : ".$arr["email"]."</p>
				<p>Phone No : ".$arr["phone"]." </p>
				<p>Question : ".nl2br($arr["message"])."</p>
				<p>Best Wishes,</p>
				<p>The ".$this->config->item('sitename')." Team</p>";
				
				$result = $this->email_model->send_contact_email($arr); 
			if($result){
				$err=0;
				$this->session->set_flashdata("successmsg", "Your enquiry sent successfully, Our team contact you very soon");	
				$this->session->set_flashdata("tempdata");
				redirect(base_url()."pages/contact_us");
				$this->session->unset_userdata('tempdata');
			}	
			else{
				$err=1;
				$this->session->set_flashdata("errormsg", "Enquiry Sending unsuccessful");
			}
		}	
		
		redirect(base_url()."pages/contact_us");
	}
	
}
