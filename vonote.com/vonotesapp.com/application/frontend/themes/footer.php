<div class="footer">
    <div class="container">
      <div class="footer_inner">
        <div class="one_fifth1">
          <div class="of_hdg"> FOLLOW </div>
          <ul class="social_li">
            <li><a target="_blank" title="Follow us on Facebook" href="https://www.facebook.com/"><img src="<?php echo base_url();?>images/facebook.png" /></a></li>
            <li><a target="_blank" title="Follow us on Google Plus" href="https://plus.google.com‎‎"><img src="<?php echo base_url();?>images/g+.png" /></a></li>
            <li><a target="_blank" title="Follow us on Twitter" href="https://twitter.com/"><img src="<?php echo base_url();?>images/twitter.png" /></a></li>
            <li><a target="_blank" title="Follow us on Youtube" href="https://www.youtube.com/"><img src="<?php echo base_url();?>images/you_tube.png" /></a></li>
          </ul>
        </div>
        <div class="clear"></div>
      </div>
      <div class="footer_divider"></div>
      <div class="sub_footer">
        <div class="lft"> &copy; Copyright <?php echo date('Y', time());  echo ' '. $this->config->item('sitename'); ?> . All rights reserved. </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>