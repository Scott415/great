<div class="header">
    <div class="container">
      <div class="logo"> <a href="<?php echo base_url();?>home"><img src="<?php echo base_url();?>images/logo.jpg" /></a> </div>
      <div class="navigation">
        <ul class="nav">
          <li><a href="<?php echo base_url();?>" class="<?php $pagename=$this->uri->segment(1); if($pagename=="" || $pagename=="home" ){ echo 'active'; }?>">Home</a></li>
         <!-- <li><a href="<?php echo base_url();?>product">PRODUCTS</a></li>-->
          <li><a href="<?php echo base_url();?>vision" class="<?php $pagename=$this->uri->segment(1); if($pagename=="vision" ){ echo 'active'; }?>" >Vision</a></li>
          <!--<li> <a href="#">MARKET</a></li>-->
          <li><a href="<?php echo base_url();?>blogs" class="<?php $pagename=$this->uri->segment(1); if($pagename=="blogs" ){ echo 'active'; }?>" >BLOGS</a></li>
          <div class="clear"></div>
        </ul>
      </div>
      <div class="login_sign">
      <?php if($this->session->userdata("user_status")==1 || isset($_COOKIE['login'])){ ?>
        <ul>
         <li id=""><a href="javascript:void();"><div id="noti_dev"><img onclick="notification_menu()" src="<?php echo base_url();?>images/create-folder image8.png" /></div></a>
            <div class="noti_wrap">
              <div class="upper_strip">
                <div class="n_aroow"><img src="<?php echo base_url();?>images/notification_arrow.png" /></div>
              </div>
              <div class="noti_tag"> Notification </div>
                <div id="noti_menu_dev">
                    <img src="<?php echo base_url();?>images/ajax-loader.gif" style="display:none; margin-left:115px" id="ajax_loder"  />
                </div>
             
            </div>
          </li>
          <li>|</li>
          <li><a href="<?php echo base_url();?>user/my_desk" class="<?php $pagename=$this->uri->segment(2); if($pagename=="my_desk" ){ echo 'active'; }?>" >My Desk</a></li>
          <li>|</li>
          <li><a href="<?php echo base_url();?>user/logout">Logout</a></li>
        </ul>
       <?php } else{ ?>
        <ul>
          <li><a href="<?php echo base_url();?>signup">Sign Up</a></li>
          <li>|</li>
          <li><a href="<?php echo base_url();?>home/login">Log In</a></li>
        </ul>
       <?php } ?>
      </div>
      <div class="clear"></div>
    </div>
  </div>