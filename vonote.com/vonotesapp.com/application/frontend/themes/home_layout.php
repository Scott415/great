<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $master_title; ?></title>
<?php include("head.php");?>    
</head>
<?php
foreach($this->_ci_view_paths as $key=>$val)
{ $view_path=$key;	
}
?>
<?php $controllername=$this->router->class;?>

<body>
<div id="wrapper"> 
  <!--- Header Start --->
    <?php include("header.php"); ?>
  <!--- Header end  --->
    <?php if(isset($master_body) && $master_body!=""){?>
	 <?php include($view_path.$controllername."/".$master_body.".php"); ?>
     <?php } ?>
  
  <!-- Footer Start -->
    <?php include ("footer.php"); ?>
  <!-- Footer End --> 
  
</div>
</body>
</html>