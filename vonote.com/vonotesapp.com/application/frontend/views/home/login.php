<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/validationEngine_signup.jquery.css" />
<div class="content">
	<div class="container">
       <div class="login_inner">
         <div  class="login_in">
           <div class="welcome">Welcome Back</div>
             <div class="divider_full_lo"></div>
             <div class="login_content">
               <div class="login_content_lft">
                 <div class="login_txt">
                 <?php echo $resultset['banner_content']; ?>
                 </div>
                 <div class="icon_text">
                    <div class="log_icon"><img src="<?php echo base_url();?>ckeditorimages/<?php echo $resultset['image0']; ?>"/></div>
                    <div class="log_text">
                        <div class="tx"><?php echo $resultset['title1']; ?></div>
                        <div class="log_tx"><?php echo $resultset['content1']; ?></div>
                    </div>
                 </div>
                 <div class="icon_text">
                    <div class="log_icon"><img src="<?php echo base_url();?>ckeditorimages/<?php echo $resultset['image1']; ?>"/></div>
                    <div class="log_text">
                        <div class="tx"><?php echo $resultset['title2']; ?></div>
                        <div class="log_tx"><?php echo $resultset['content2']; ?> 
</div>
                    </div>
                 </div>
                 <div class="icon_text">
                    <div class="log_icon"><img src="<?php echo base_url();?>ckeditorimages/<?php echo $resultset['image2']; ?>"/></div>
                    <div class="log_text">
                        <div class="tx"><?php echo $resultset['title3']; ?></div>
                        <div class="log_tx"><?php echo $resultset['content3']; ?></div>
                    </div>
                 </div>
                 <div class="icon_text">
                    <div class="log_icon"><img src="<?php echo base_url();?>ckeditorimages/<?php echo $resultset['image3']; ?>"/></div>
                    <div class="log_text">
                        <div class="tx"><?php echo $resultset['title4']; ?></div>
                        <div class="log_tx"><?php echo $resultset['content4']; ?></div>
                    </div>
                 </div>
               </div>
               <div class="login_content_rght">
                  <div class="sign_create">
                    <div class="sign">Sign In</div>
                     <a href="<?php echo base_url();?>signup"><div class="create">Create An Account</div></a>
                  </div>
 			   <div style="margin-top: 20px; width:300px; position:absolute;" id="message"> <font color='red'><?php echo $this->session->flashdata('errormsg'); ?></font> <font color='green'><?php echo $this->session->flashdata('successmsg'); ?></font> <br class="clear" /></div>
               <div class="clear" ></div>
                  <form method="post" action="<?php echo base_url();?>home/login_check_user_login" id="dev_login" >
                  <div class="email_log">
                    <div class="email">Email Id :</div>
                    <input type="text" class="email_inp validate[required,custom[email]]" name="email" value="<?php echo $userdata['email']; ?>"/>
                  </div>
                  <div class="email_log">
                    <div class="email">Password :</div>
                    <input type="password" class="email_inp validate[required]" name="password"/>
                  </div>
                  <div class="clear"></div>
                  <div class="inp_rem">
                  <input type="checkbox" class="ch" value="1" name="rember"/><div class="remem">Remember me</div>
                  </div>
                  <div class="clear"></div>
                  <input type="submit" value="Sign In"  class="log_sub"/>
                  <div class="log_reset"><a id="facebook" href="javascript:void(0);"><img src="<?php echo base_url();?>images/fb-login.png"/></a></div>
                   <div class="log_reset"><a href="<?php echo base_url(); ?>home/forget_password" >Forgot password?</a></div>
               </form>
             </div>
             <div class="clear"></div>
         </div>
       </div>
</div>
</div>
<!--------content ends----->

</div>
<script type="text/javascript">
  window.fbAsyncInit = function() {
	  //Initiallize the facebook using the facebook javascript sdk
	  <?php $this->session->set_userdata("fb_request","consumer"); ?>
     FB.init({ 
       appId:'<?php $this->config->load('facebook'); echo $this->config->item('appID');?>', // App ID
	   cookie:true, // enable cookies to allow the server to access the session
       status:true, // check login status
	   xfbml:true, // parse XFBML
	   oauth : true //enable Oauth 
     });
   };
   //Read the baseurl from the config.php file
   (function(d){
           var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement('script'); js.id = id; js.async = true;
           js.src = "//connect.facebook.net/en_US/all.js";
           ref.parentNode.insertBefore(js, ref);
         }(document));
	//Onclick for fb login
	
 $('#facebook').click(function(e) {
    FB.login(function(response) {
	  if(response.authResponse) {
		  parent.location ='<?php echo base_url(); ?>fbci/fblogin'; //redirect uri after closing the facebook popup
	  }
 },{scope: 'email,read_stream,publish_stream,user_birthday,user_location,user_work_history,user_hometown,user_photos'}); //permissions for facebook
});
   </script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		// binds form submission and fields to the validation engine
		jQuery("#dev_login").validationEngine();
	});
</script>
