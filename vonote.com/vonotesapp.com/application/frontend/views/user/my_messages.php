 <?php $user_id = $this->session->userdata("user_id"); 
	   $conversation_id=$this->uri->segment('3'); 
	   $reciver_id=$this->uri->segment('4'); 
?>
  <!-- Content Start -->
  <div class="content">
    <div class="container">
      <div class="create_folder_wrap">
         <?php echo $this->load->view($sidebar_content);?>
        <div class="cf_content">
          <div class="heading4"> My Messages </div>
          <div class="pdf_wrap remove_mar">
           

            <div class="my_msg_txt">
            <form method="post" action="<?php echo base_url(); ?>user/reply_message" enctype="multipart/form-data" >
              <textarea rows="4" cols="50" placeholder="Write a reply..." id="message_id" name="message"></textarea>
              
              <div style="float:right"><input type="submit" value="Send" class="send"/>
              
              <img src="<?php echo base_url(); ?>images/ajax-loader.gif" id="ajax_loader"  style="display:none;" />
              </div>
              <input type="hidden" value="<?php echo $conversation_id; ?>" name="conversation_id" id="m_conversation"  />
              <input type="hidden" value="<?php echo $user_id; ?>" name="user_id" id="m_sender_id"  />
              <input type="hidden" value="<?php echo $reciver_id; ?>" name="reciver_id" id="m_reciver_id"  />
              <input class="attach_file" type="file" name="userfile" />
           </form>
            </div>
            <ul class="grouplist">
            	<li>
                  <div class="message_wrap">
                    <div style="width:100%; margin-bottom:20px; position:absolute;" id="message"> <font color='red'><?php echo $this->session->flashdata('errormsg'); ?></font> <font color='green'><?php echo $this->session->flashdata('successmsg'); ?></font> <br class="clear" />
               </div>                
            </div>
                  <div class="clear"></div>
				 </li>   
              </ul>         
            <div id="devresults"></div>
               <ul id="postlist" class="grouplist" >
              </ul>
              <div id="more" style=" margin-top:25px; margin-left:247px; "><a class="send" href="#"  id="ajax_loading_image">View more...</a></div>
                 <div class="clear"></div>

          <!--<img style="margin-left:250px" src="<?php echo base_url(); ?>images/ajax-loader.gif" id="ajax_loader1"  style="display:none;" />-->
          </div>
        </div>
        <!-- cf content end -->
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <!-- Content End --> 
<script type="text/javascript" >
//for conversation details
	/*function my_message(){
		$('#ajax_loader1').show();
		var conversation_id = $('#m_conversation').val(); //alert(conversation_id);
		$.ajax({
			type: "POST",
			url: 'http://vonotesapp.com/ajax/my_conversation/'+conversation_id,
			success: function(rep){
				//alert(rep);
				response_array_t = rep.split('|::|');
				if(response_array_t[0] == 'success'){
					$('#devresults').html(response_array_t[1]); //alert(response_array_t[1]);
					$('#devresults').show();
					$('#ajax_loader1').hide();
				}
				else{
					alert("Error connecting to Remote Server");
				}
			}
		});
		$('#loading').hide();
	}*/
	
 
var ajax_finish=1;
function my_message(u){ 	 
	var conversation_id = $('#m_conversation').val(); 
	var timestamp = $('ul#postlist li:first').attr('id');
	//var url11	 = 'http://vonotesapp.com/ajax/my_conversation_d/'+conversation_id+'/'+timestamp; alert(url11);
	//	var url	 = 'http://vonotesapp.com/ajax/my_conversation_d/'+conversation_id+'/'+timestamp; alert(url);
	if(ajax_finish == 1){
		ajax_finish = 0;
		$.ajax({
			type: "GET",
			data: '',
			url: 'http://vonotesapp.com/ajax/my_conversation_d/'+conversation_id+'/'+timestamp,
			//url: "ajax.php?do=update&type=posts&group_id="+group+"&timestamp="+timestamp+"&u="+u,
			success: function(rep){
			//alert(rep);
				response_array_t = rep.split('|::|');
				// add more div
				if(response_array_t[0] == 'Success'){
					$('ul#postlist').prepend(response_array_t[1]);
//					$(".popupp").fancybox({});
					$('.post_li').slideDown("slow");
					$('.post_li').removeClass("post_li");

					if(response_array_t[2] == 'g'){ //alert("hi");
						$('#more').show();
					}
					else{
						$('#more').hide();
					}
				}
				else{
					
				}
			}
		});
		ajax_finish = 1;
	}
	$('#posting_status').hide();
	return false;
}

$(document).ready(function () {
	$('#more').click(function() {
		$('#ajax_loading_image').show();
		var conversation_id = $('#m_conversation').val(); 
		var timestamp = $('ul#postlist li:last').attr('id');
		$.ajax({
			type: "GET",
			data: '',
			url: 'http://vonotesapp.com/ajax/my_conversation_more/'+conversation_id+'/'+timestamp,
			//url: "ajax.php?do=update&type=posts_next&group_id="+group+"&timestamp="+timestamp,
			success: function(rep){
				response_array_t = rep.split('|::|');
				//alert(rep);
				// add more div
				if(response_array_t[0] == 'Success'){
					$('ul#postlist').append(response_array_t[1]);
					$('.post_li').slideDown('slow');
					$('.post_li').removeClass("post_li");
					if(response_array_t[2] == 'g'){  //alert(response_array_t[2]);
						$('#more').show(); 
					}
					else{
						//$('#more').show(); 
						$('#more').hide();
					}
				}
				else{
				}
			}
		});
		//$('#ajax_loading_image').hide();
		return false;
	})

});

  $(document).ready(function(){									
	 /* window.setInterval(function(){
		  //update_groupdata();
		  my_message('u');
	  }, 15000);*/
	  //update_groupdata();
	  my_message();
  });

/*$(document).ready(function() {
   my_message();
});
*/
</script>
