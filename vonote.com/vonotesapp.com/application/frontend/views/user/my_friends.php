 <?php $user_id = $this->session->userdata("user_id"); ?>

  <!-- Content Start -->
  <div class="content">
    <div class="container">
      <div class="create_folder_wrap">
         <?php echo $this->load->view($sidebar_content);?>
        <div class="cf_content">
          <div class="heading4"> My Friends </div>
			    <div style=" margin-bottom:20px;margin-left: 20px; position:absolute;" id="message"> <font color='red'><?php echo $this->session->flashdata('errormsg'); ?></font> <font color='green'><?php echo $this->session->flashdata('successmsg'); ?></font> <br class="clear" />
          </div>
			 
          <div class="pdf_wrap remove_mar">
			
          <?php  if(!empty($resultset)){

   //  print_r($resultset);

			  	foreach($resultset as $k => $v){
					if($v['friend_id'] == $user_id){
						$frnd['user_id'] = $v['user_id'];
					}else{
						$frnd['user_id'] = $v['friend_id'];
					}
					// get friend info
					$resultset1 = $this->user_model->user_profile_data($frnd);
					if($resultset1['image'] <> ''){
						$profile_pic = base_url().'blogimages/'.$resultset1['image'];
					}else{
						$profile_pic = base_url().'images/my-profile image3.png';
					}
					if($resultset1['first_name'] <> ''){
						$uname = ucwords($resultset1['first_name']).' '.ucwords($resultset1['last_name']);
					}
					else{
						$uname = $resultset1['email'];
					}
					
			  
		  ?>
            <div class="my_friends_dev">
              <div class="mf_left"><a href="<?php echo base_url(); ?>user/user_wall/<?php echo $resultset1['id']; ?>" > <img src="<?php echo $profile_pic;?>" width="50" /></a> </div>
              <div class="mf_right">
                <div class="mf_hdg_1"> <?php echo $uname; ?> </div>
                <div class="mf_hdg_2"> <span class="sky"><?php if($resultset1['worked'] <> ''){ echo 'Works at '.$resultset1['worked']; } ?></span> </div>
              </div>
				  <div class="dmf_right delet_icons">
				  <a href="<?php echo base_url(); ?>user/delete_a_friend/<?php echo $resultset1['id']; ?>">Delete </a> 
				  </div>
              <div class="clear"></div>
            </div>
          <?php } } else{ ?>
            <div class="my_friends">
              <div class="mf_right">
                <div class="mf_hdg_1"> No friend found, search you friend <a href="<?php echo base_url(); ?>user/find_friend" > click here </a>  </div>
              </div>
              <div class="clear"></div>
            </div>
           <?php } ?>
          </div>
        </div>
        <!-- cf content end -->
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <!-- Content End --> 
