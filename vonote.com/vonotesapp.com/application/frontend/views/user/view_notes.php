<!-- Content Start -->

<div class="content">
  <div class="container">
    <div class="create_folder_wrap"> <?php echo $this->load->view($sidebar_content);?>
      <div class="cf_content">
        <!--<div class="heading4"> My Recent Files </div>-->
        <div class="pdf_wrap remove_mar">
          <?php if($this->session->flashdata('successmsg')){ ?>
          <script type="text/javascript">
    setTimeout('ourRedirect()',3000)
    function ourRedirect(){
		parent.$.fancybox.close();  //can use jQuery instead of $
		window.parent.location ='<?php	echo base_url()."user/my_desk/"; ?>';
    }
</script>
          <?php	}  //print_r($userdata); ?>
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/notes_styles.css" />
          <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Courgette" />
          <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
            <input type="hidden" value="<?php echo $this->session->userdata("user_id"); ?>" name="user_id"  />
            <input type="hidden" value="<?php echo $this->uri->segment('4'); ?>" name="userfile"  />
            <input type="hidden" value="<?php echo $this->uri->segment('3'); ?>" name="sub_folder"  />
            <div id="pad" style="margin-left:140px;">
              <h2>Note</h2>
              <textarea rows="20" id="note" disabled="disabled" name="file_content"><?php echo $filedata; ?></textarea>
            </div>

          <!-- JavaScript includes. --> 
          <script src="<?php	echo base_url(); ?>assets/js/notes_script.js"></script>
          <div class="clear"></div>
        </div>
      </div>
      <!-- cf content end -->
      <div class="clear"></div>
    </div>
  </div>
</div>
<!-- Content End --> 
