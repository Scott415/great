 <?php $user_id = $this->session->userdata("user_id"); 
	   $conversation_id=$this->uri->segment('3'); 
	   $reciver_id=$this->uri->segment('4'); 
?>
  <!-- Content Start -->
  <div class="content">
    <div class="container">
      <div class="create_folder_wrap">
         <?php echo $this->load->view($sidebar_content);?>
        <div class="cf_content">
          <div class="heading4"> My Messages </div>
          <div class="pdf_wrap remove_mar">
            <div class="my_msg_txt">
            
            <form method="post" action="<?php echo base_url(); ?>user/reply_message" >
              <textarea rows="4" cols="50" placeholder="Write a reply..." id="message_id" name="message"></textarea>
              <div style="float:right"><input type="submit" value="Send" class="send"/>
              <img src="<?php echo base_url(); ?>images/ajax-loader.gif" id="ajax_loader2"  style="display:none;" />
              </div>
              <input type="hidden" value="<?php echo $conversation_id; ?>" name="conversation_id" id="m_conversation"  />
              <input type="hidden" value="<?php echo $user_id; ?>" name="user_id" id="m_sender_id"  />
              <input type="hidden" value="<?php echo $reciver_id; ?>" name="reciver_id" id="m_reciver_id"  />
           </form>
            </div>
            <div style=" margin-bottom:20px; position:absolute;" id="message"> <font color='red'><?php echo $this->session->flashdata('errormsg'); ?></font> <font color='green'><?php echo $this->session->flashdata('successmsg'); ?></font> <br class="clear" />
          </div>
             <img style="margin-left:250px; display:none;" src="<?php echo base_url(); ?>images/ajax-loader.gif" id="ajax_loader1"  />

            <div id="devresults"></div>
               <ul id="postlist" class="grouplist">
              </ul>        

              <div id="more" class="moreload" style="margin-left: 0px; margin-top: 0px; width:100%;">
                  <div style="width:80px; text-align:center; margin:0px auto; padding-top:6px; font-size:12px; color:#000;">
                      <img style="margin-left:250px; display:none;" src="<?php echo base_url(); ?>images/ajax-loader.gif" id="ajax_loader3" /> &nbsp; More...
                  </div>
              </div>
              <div class="clear"></div>
          </div>
        </div>
        <!-- cf content end -->
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <!-- Content End --> 
  <script  type="text/javascript">
var ajax_finish=1;
function update_wallposts(u){ 
	//var neighbours = $('#neighbours').val();
	var timestamp = $('ul#postlist li:first').attr('id');
	
	if(ajax_finish == 1){
		ajax_finish = 0;
		var form = $('#form_wallpost1');
		$.ajax({
			type: "GET",
			data: '',
			url: "ajax.php?do=update&type=wallpost&timestamp="+timestamp+"&u="+u,
			success: function(rep){
					//alert("hi");
				response_array_t = rep.split('|::|'); //alert(rep);
				// add more div
				if(response_array_t[0] == 'Success'){
					$('ul#postlist').prepend(response_array_t[1]);
					$('.post_li').slideDown("slow");
					$('.post_li').removeClass("post_li");

					if(response_array_t[2] == 'g'){//alert("hi");
						$('#more').show();
					}
					else{
						$('#more').hide();
					}
				}
				else{
					
				}
			}
		});
		ajax_finish = 1;
	}
	$('#posting_status').hide();
	return false;
}

$(document).ready(function () {
	$('#form_wallpost1').submit(function() {
		$('#posting_status').show();
		var form = $(this);
		$.ajax({
			type: "POST",
			data: form.serialize(),
			url: "ajax.php?do=addwallpost&type=wallpost",
			success: function(rep){
					//alert(rep);
				response_array_t = rep.split('|::|');
				// add more div
				if(response_array_t[1] == 'g'){
					$('textarea[name="post"]').val("");
					setTimeout('delacc12();', 1000);								
				}
				else{
					$('.error1').html(response_array_t[1]);
					$('.error1').show();
					setTimeout("$('.error1').hide();", 5000);								
				}
				update_wallposts();
			}
		});
		return false;
	})
	
	$('#more').click(function() { 
		$('#ajax_loading_image').show();
		//var group = $('#group').val();
		var timestamp = $('ul#postlist li:last').attr('id'); //alert("hi");
		
		$.ajax({
			type: "GET",
			data: '',
			url: "ajax.php?do=updatewall&type=next_wallpost&timestamp="+timestamp,
			success: function(rep){
				response_array_t = rep.split('|::|'); //alert(rep);
				// add more div
				if(response_array_t[0] == 'Success'){
					$('ul#postlist').append(response_array_t[1]);
					$('.post_li').slideDown('slow');
					$('.post_li').removeClass("post_li");

					if(response_array_t[2] == 'g'){
						$('#more').show();
					}
					else{
						$('#more').hide();
					}

				}
				else{
				}
			}
		});
		$('#ajax_loading_image').hide();
		return false;
	})

});
 </script>
    <script type="text/javascript">
	var ajax_finish=1;
		// Walia code starts here
		function update_posts(){
			
			$('#ajax_loader1').show();
			var conversation_id = $('#m_conversation').val(); //alert(conversation);
			var timestamp = $('ul#postlist li:last').attr('id');
			var timestamp_more = $('ul#postlist li:first').attr('id');
			if(timestamp_more == undefined){timestamp_more= 0;}
			//alert(u);
	if(ajax_finish == 1){
		ajax_finish = 0;
			$.ajax({
				type: "GET",
				data: '',
				url: BASEURL+'ajax/my_conversation_d/'+conversation_id+'/'+timestamp+'/'+timestamp_more,
				//url: "ajax.php?do=update&type=message&conversation="+conversation+"&timestamp="+timestamp+"&timestamp_more="+timestamp_more,
				success: function(rep){
					response_array_t = rep.split('|::|');
					alert(rep);
					if(response_array_t[0] == 'Success'){
						$('ul#postlist').append(response_array_t[1]);
						$('.post_li').slideDown('slow');
						$('.post_li').removeClass("post_li");
					    $('#ajax_loader1').hide();
						if(response_array_t[2] == 'g'){
							$('#more').show();
							page++;
						}
						else{
							$('#more').hide();
						}
						
/*						var item = $(response_array_t[0]).hide().fadeIn(2000);
						$('ul#postlist li.start').after(item);
*/					}
					else{
						
					}
					
				}
				
			});
			
		ajax_finish = 1;
	}			return false;
			
		}

		$(document).ready(function () {

			$('#more').click(function() {
				$('#ajax_loader3').show();
				var conversation_id = $('#m_conversation').val();
				var timestamp = $('ul#postlist li:first').attr('id');
				$.ajax({
					type: "GET",
					data: '',
					url: BASEURL+'ajax/my_conversation_more/'+conversation_id+'/'+timestamp,
					success: function(rep){
							alert(rep);
						response_array_t = rep.split('|::|');
						// add more div
						if(response_array_t[0] == 'Success'){
							$('ul#postlist').prepend(response_array_t[1]);
							$('.post_li').slideDown('slow');
							$('.post_li').removeClass("post_li");

							if(response_array_t[2] == 'g'){
								$('#more').show();
							}
							else{
								$('#more').hide();
							}
	
	/*						var item = $(response_array_t[0]).hide().fadeIn(2000);
							$('ul#postlist li.start').after(item);
	*/					}
						else{
						}
					}
				});
				$('#ajax_loader3').hide();
				return false;
			})
		});

		$(document).ready(function(){	
		update_posts();								
			window.setInterval(function(){
				update_groupdata();
				update_posts();
			}, 15000);
			update_groupdata();
			update_posts();
		});
	</script>

