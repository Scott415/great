  <!-- Content Start -->
  <div class="content">
    <div class="container">
      <div class="create_folder_wrap">
          <?php echo $this->load->view($sidebar_content);?>
        <div class="cf_content" style="background:#fff;">
          <!--<div class="heading4"> Search New Friend </div>-->
          <!--<div class="notification">-->
            
			
				
				
				
				<div class="clf filler_container">
					
					<label>
						Show
					</label>
					
					<select id="entries">
						<option value="100">100</option>
						<option value="50">50</option>
						<option value="20">20</option>
						<option value="10">10</option>
					</select>
					
					<label>
						entries
					</label>					
					
					<div class="search_panel">
							<label>Search</label><input type="text" class="cf" placeholder="Search New Friend" id="search_ftxt"  />
					</div>	
           </div>
			  
            <div class="clear"></div>
				<div id="content" class="record">
					<?php echo $this->table->generate(); ?>
          </div>
			 <div class="clear"></div>
          <div class="pdf_wrap remove_mar">
          <div  id="dev_search_friend_result" >
        	 <div class="loading" style="display:none"></div>
          </div>
          </div>
        </div>
        <!-- cf content end -->
        <div class="clear"></div>
      </div>
	
    </div> 
  </div>
  <!-- Content End --> 
  
<script type="text/javascript" src="<?php echo base_url().'/js/jquery-1.10.2.min.js';?>"></script>
  <script type="text/javascript" src="<?php echo base_url().'js/jquery.tablesorter.min.js';?>"></script>
  <script type="text/javascript"> 
$(function(){
  $('#keywords').tablesorter(); 
});

$("#search_ftxt").keyup(function () {
    var value = this.value.toLowerCase().trim();

    $("#keywords tr").each(function (index) {
        if (!index) return;
        $(this).find("td").each(function () {
            var id = $(this).text().toLowerCase().trim();
            var not_found = (id.indexOf(value) == -1);
            $(this).closest('tr').toggle(!not_found);
            return not_found;
        });
    });
});

$("#entries").change(function(){
var limit= $(this).val();
var formData = {limit:limit}; //Array 
 $('.loading').show();
$.ajax({
    url : '<?php echo base_url()."user/find_friend";?>',
    type: "POST",
    data : formData,
    success: function(data, textStatus, jqXHR)
    {
	 $('.loading').hide();
       $("#content").html(data);//alert(data);		 //data - response from server
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
 //alert("error");
    }
});

});
</script>