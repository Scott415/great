<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<!----------------Placeholder--------------------->
<script>
    // placeholder polyfill
    $(document).ready(function(){
        function add() {
            if($(this).val() == ''){
                $(this).val($(this).attr('placeholder')).addClass('placeholder');
            }
        }

        function remove() {
            if($(this).val() == $(this).attr('placeholder')){
                $(this).val('').removeClass('placeholder');
            }
        }

        // Create a dummy element for feature detection
        if(!('placeholder' in $('<input>')[0])) {

            // Select the elements that have a placeholder attribute
            $('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

            // Remove the placeholder text before the form is submitted
            $('form').submit(function(){
                $(this).find('input[placeholder], textarea[placeholder]').each(remove);
            });
        }
    });
</script>
<!-- place holder end-->

  <!-- Content Start -->
  <div class="content">
    <div class="container">
      <div class="create_folder_wrap">
         <?php echo $this->load->view($sidebar_content);?>
        <div class="cf_content">
          <div class="heading4"> My Custom Folders </div>
          <div class="pdf_wrap remove_mar">
            <div class="notification">
              <div class="clf"> <img src="<?php echo base_url();?>images/my_costom_folder image1.png" />
                <input type="text" class="cf" placeholder="Search..."/>
              </div>
              <div class="noti_inner oth">
                <ul>
                  <li><a href="#"><img src="<?php echo base_url();?>images/my_costom_folder image2.png" /></a></li>
                  <li><a href="#"><img src="<?php echo base_url();?>images/my_costom_folder image3.png" /></a></li>
                  <li><a href="#"><img src="<?php echo base_url();?>images/my_costom_folder image4.png" /></a></li>
                </ul>
              </div>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="folder_name_wrap">
              <div class="fn_icon"> <img src="<?php echo base_url();?>images/create-folder image3.png" /> </div>
              <div class="fn_btn1"> Subject 01 </div>
              <div class="clear"></div>
            </div>
            <div class="pdf_name">
              <ul>
                <li><img src="<?php echo base_url();?>images/create-folder image6.png" /></li>
                <li><a href="#">New file name.pdf</a></li>
                <li>Files</li>
                <li class="ad_width">2-22/2014 | 10:00AM</li>
                <li><a href="#"><img src="<?php echo base_url();?>images/delete.png" /></a></li>
                <div class="clear"></div>
              </ul>
            </div>
            <div class="pdf_name">
              <ul>
                <li><img src="<?php echo base_url();?>images/create-folder image6.png" /></li>
                <li><a href="#">New file name.pdf</a></li>
                <li>Files</li>
                <li class="ad_width">2-22/2014 | 10:00AM</li>
                <li><a href="#"><img src="<?php echo base_url();?>images/delete.png" /></a></li>
                <div class="clear"></div>
              </ul>
            </div>
            <div class="folder_name_wrap">
              <div class="fn_icon"> <img src="<?php echo base_url();?>images/create-folder image3.png" /> </div>
              <div class="fn_btn1"> Subject 02 </div>
              <div class="clear"></div>
            </div>
            <div class="pdf_name">
              <ul>
                <li><img src="<?php echo base_url();?>images/create-folder image6.png" /></li>
                <li><a href="#">New file name.pdf</a></li>
                <li>Files</li>
                <li class="ad_width">2-22/2014 | 10:00AM</li>
                <li><a href="#"><img src="<?php echo base_url();?>images/delete.png" /></a></li>
                <div class="clear"></div>
              </ul>
            </div>
            <div class="pdf_name">
              <ul>
                <li><img src="<?php echo base_url();?>images/create-folder image6.png" /></li>
                <li><a href="#">New file name.pdf</a></li>
                <li>Files</li>
                <li class="ad_width">2-22/2014 | 10:00AM</li>
                <li><a href="#"><img src="<?php echo base_url();?>images/delete.png" /></a></li>
                <div class="clear"></div>
              </ul>
            </div>
            <div class="pdf_name">
              <ul>
                <li><img src="<?php echo base_url();?>images/create-folder image6.png" /></li>
                <li><a href="#">New file name.pdf</a></li>
                <li>Files</li>
                <li class="ad_width">2-22/2014 | 10:00AM</li>
                <li><a href="#"><img src="<?php echo base_url();?>images/delete.png" /></a></li>
                <div class="clear"></div>
              </ul>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <!-- cf content end -->
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <!-- Content End --> 
