<?php $user_id = $this->session->userdata("user_id"); //print_r($resultset);
		if($resultset['image'] <> ''){
			$profile_pic = base_url().'blogimages/'.$resultset['image'];
		}else{
			$profile_pic = base_url().'images/my-profile image3.png';
		}
 ?> 
  <!--Content Start-->
  <div class="content">
    <div class="container">
      <div class="create_folder_wrap">
         <?php echo $this->load->view($sidebar_content);?>
        <div class="cf_content">
          <div class="heading4"> Update Profile </div>
          <div class="pdf_wrap remove_mar">
            <div class="asd"></div>
            <div class="clear"></div>
            <!--<div class="my_profile">
              <div class="profile_pic"> <img src="<?php echo base_url(); ?>images/my-profile image3.png" /> </div>
              <div class="p_tags as">
                <div class="pro_1"> John Martin </div>
                <div class="pro_2"> Works at Solitaire Infosys </div>
              </div>
              <div class="clear"></div>
            </div>-->
            <div style=" margin-top:-20px; margin-left:20px; position:absolute;" id="message"> <font color='red'><?php echo $this->session->flashdata('errormsg'); ?></font> <font color='green'><?php echo $this->session->flashdata('successmsg'); ?></font> <br class="clear" />
          </div>
            <form method="post" action="<?php echo base_url(); ?>user/update_profile_db" enctype="multipart/form-data" >
            <div class="email_log_update">
              <div class="update_pro_field">First Name<span class="star">*</span> :</div>
              <input type="text" class="update_txtfield" name="first_name" value="<?php echo $resultset['first_name']; ?>" />
            </div>
            <div class="email_log_update">
              <div class="update_pro_field">Last Name :</div>
              <input type="text" class="update_txtfield" name="last_name" value="<?php echo $resultset['last_name']; ?>"/>
            </div>
            <!--<div class="email_log_update">
              <div class="update_pro_field">Email Id</div>
              <input type="text" class="update_txtfield" name="email"/>
            </div>-->
            <div class="email_log_update">
              <div class="update_pro_field">Education<!--<span class="star">*</span>--> :</div>
              <input type="text" class="update_txtfield" name="education" value="<?php echo $resultset['education']; ?>"/>
            </div>
            <div class="email_log_update">
              <div class="update_pro_field">Image :</div>
              <img src="<?php echo $profile_pic; ?>" id="blah" width="100" /> 
              <div class="prof_image_k"><input type="file" name="userfile" onchange="readURL(this);"  /></div>
              <input type="hidden" name="image" value="<?php echo $resultset['image']; ?>"  />
              <div class="clear"></div>
            </div>
            <div class="email_log_update">
              <div class="update_pro_field">Worked<!--<span class="star">*</span>--> :</div>
              <input type="text" class="update_txtfield" name="worked" value="<?php echo $resultset['worked']; ?>"/>
            </div>
            <div class="email_log_update">
              <div class="update_pro_field">About Me<!--<span class="star">*</span>--> :</div>
              <textarea type="text" class="textarea" name="about_me"><?php echo $resultset['about_me']; ?></textarea>
            </div><br class="clear" />
            <div class="update_btn">
              <input type="submit" class="send_msg save" value="Save">
              <a href="<?php echo base_url().'user/my_desk'; ?>" class="del_frd cancel">Cancel</a>
            </div>
            </form>
            <div class="clear"></div>
          </div>
        </div>
        <!--cf content end-->
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <!--Content End--> 
<script type="text/javascript">
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
					.attr('src', e.target.result)
					.width(100)
					.height(100);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
	
</script>
