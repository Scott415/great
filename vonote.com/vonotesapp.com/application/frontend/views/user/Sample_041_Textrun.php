<?php
/**
 * Header file
 */


$phpWord = new \PhpOffice\PhpWord\PhpWord();

// Ads styles


// New portrait section
$section = $phpWord->addSection();

// Add text run
$textrun = $section->addTextRun('pStyle');


$str = $content;
$str = str_replace("%2F","/",$str);
$str = str_replace("&nbsp;"," ",$str);
$str = str_replace("<div>","~",$str);
$str = str_replace("</div>","",$str);

$s = htmlentities($str);


echo $s;

//$str = '<a href="http://stackoverflow.com/"> Stack Overflow</a>';

$i=0;

$newstr ="";
$tagcheck = false;
$firstbrace = false;
$checktext = false;
$count = 0;
$face = "arial";
$size = "10";
$colour = "black";
while($i<strlen($str))
{
	if($str[$i]=="<" && $firstbrace == false)
	{
		if($str[$i+1] == "f")
		{
			$tagcheck = true;
			$firstbrace = true;
			$checktext = true;
			$newstr = str_replace("</font>","",$newstr);
			if(strpos($newstr,"~") != "")
			{
				$pos = strpos($newstr,"~");
				$str1 = substr($newstr,0,$pos);
				$textrun->addText($str1);
				$textrun->addTextBreak(1, 'fontStyle');
				$str2 = substr($newstr,$pos+1,strlen($newstr));
				$textrun->addText($str2);
			}
			else
			{
				$textrun->addText($newstr);
			}
			$newstr ="";
		}
		
		if($str[$i+1] == "/")
		{
			$tagcheck = true;
			$firstbrace = false;
			$checktext = true;
		}
		
	}
	error_reporting(0);
	if($str[$i] == "<" && $firstbrace = true)
	{
			
			$newstr = $newstr."<";			
			if(strpos($newstr,"color=") != "")
			{
				$s = explode('color="',$newstr);
				$t = explode('"',$s[1]);
				$colour = $t[0];
			}
			else
			{
				$colour = "black";
			}

			if(strpos($newstr,"size=") != "")
			{
				$s1 = explode('size="',$newstr);
				$t1 = explode('"',$s1[1]);
				$size = $t1[0]."<br>";
				if($t1[0]==1){$size=12;}
				if($t1[0]==2){$size=16;}
				if($t1[0]==3){$size=20;}
				if($t1[0]==4){$size=26;}
				if($t1[0]==5){$size=28;}
				if($t1[0]==6){$size=32;}
			}
			else
			{
				$size = "10";
			}
			
			if(strpos($newstr,"face=") != "")
			{
				$s2 = explode('face="',$newstr);
				$t2 = explode('"',$s2[1]);
				$face = $t2[0];
				
			}
			else
			{
						$face = "arial";
			}			
			
				$s3 = explode('>',$newstr);
				$t3 = explode('<',$s3[1]);
								
				$tagtext = htmlentities($t3[0]);
			

			$textrun->addText($tagtext,array('name'=>$face, 'color'=>$colour, 'size'=>$size));
			
			$newstr = "";//do after printing font tags

			$tagcheck = false;
			$firstbrace=false;
			$count++;
	}
	if($tagcheck == true && $firstbrace == true)
	{
		$newstr .= $str[$i];
	}
	
	if($tagcheck == false)
	{
		$checktext = false;
		$newstr .= $str[$i];	
	}
	if($i+1 == strlen($str))  // print in the end of doc
	{
		$newstr = str_replace("</font>","",$newstr);
		$newstr = str_replace("</font>","",$newstr);
			if(strpos($newstr,"~") != "")
			{
				$pos = strpos($newstr,"~");
				$str1 = substr($newstr,0,$pos);
				$textrun->addText($str1);
				$textrun->addTextBreak(1, 'fontStyle');
				$str2 = substr($newstr,$pos+1,strlen($newstr));
				$textrun->addText($str2);
			}
			else
			{
				$textrun->addText($newstr);
			}
			$newstr ="";
	}
	$i++;
}

// Save file
echo write($phpWord,$folder,$rel, $name, $writers);
if (!CLI) {
    include_once 'Sample_Footer.php';
}

use PhpOffice\PhpWord\Autoloader;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\IOFactory;

error_reporting(E_ALL);
define('CLI', (PHP_SAPI == 'cli') ? true : false);
define('EOL', CLI ? PHP_EOL : '<br />');
define('SCRIPT_FILENAME', basename($_SERVER['SCRIPT_FILENAME'], '.php'));
define('IS_INDEX', SCRIPT_FILENAME == 'index');

require_once './src/PhpWord/Autoloader.php';
Autoloader::register();
Settings::loadConfig();

// Set writers
$writers = array('Word2007' => 'docx');

// Set PDF renderer
if (Settings::getPdfRendererPath() === null) {
    $writers['PDF'] = null;
}

// Return to the caller script when runs by CLI
if (CLI) {
    return;
}

// Set titles and names
$pageHeading = str_replace('_', ' ', SCRIPT_FILENAME);
$pageTitle = IS_INDEX ? 'Welcome to ' : "{$pageHeading} - ";
$pageTitle .= 'PHPWord';
$pageHeading = IS_INDEX ? '' : "<h1>{$pageHeading}</h1>";

// Populate samples
$files = '';
if ($handle = opendir('.')) {
    while (false !== ($file = readdir($handle))) {
        if (preg_match('/^Sample_\d+_/', $file)) {
            $name = str_replace('_', ' ', preg_replace('/(Sample_|\.php)/', '', $file));
            $files .= "<li><a href='{$file}'>{$name}</a></li>";
        }
    }
    closedir($handle);
}

/**
 * Write documents
 *
 * @param \PhpOffice\PhpWord\PhpWord $phpWord
 * @param string $filename
 * @param array $writers
 */
function write($phpWord,$folder,$rel,$name, $filename, $writers)
{
    $result = '';

    // Write documents//$fp = fopen('./userdata/'.$arr["relation"].'/'.$arr["folder_name"].'/'.$arr["name"].".txt","wb");
    foreach ($writers as $writer => $extension) {
        $result .= date('H:i:s') . " Write to {$writer} format";
        if (!is_null($extension)) {
            $xmlWriter = IOFactory::createWriter($phpWord, $writer);
            $xmlWriter->save(__DIR__ . "/{$filename}.{$extension}");
            rename(base_url()."userdata/".$rel."/".$folder."/".$name."docx");
        } else {
            $result .= ' ... NOT DONE!';
        }
        $result .= EOL;
    }

    $result .= getEndingNotes($writers);

    return $result;
}

/**
 * Get ending notes
 *
 * @param array $writers
 */
function getEndingNotes($writers)
{
    $result = '';

    // Do not show execution time for index
    if (!IS_INDEX) {
        $result .= date('H:i:s') . " Done writing file(s)" . EOL;
        $result .= date('H:i:s') . " Peak memory usage: " . (memory_get_peak_usage(true) / 1024 / 1024) . " MB" . EOL;
    }

    // Return
    if (CLI) {
        $result .= 'The results are stored in the "results" subdirectory.' . EOL;
    } else {
        if (!IS_INDEX) {
            $types = array_values($writers);
            $result .= '<p>&nbsp;</p>';
            $result .= '<p>Results: ';
            foreach ($types as $type) {
                if (!is_null($type)) {
                    $resultFile = base_url()."userdata/".$rel."/".$folder."/".$name."docx";
                    echo $resultFile;exit;
                    if (file_exists($resultFile)) {
                        $result .= "<a href='{$resultFile}' class='btn btn-primary'>{$type}</a> ";
                    }
                }
            }
            $result .= '</p>';
        }
    }

    return $result;
}
