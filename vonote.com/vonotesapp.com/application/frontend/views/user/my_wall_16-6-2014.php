<!-- Content Start -->
<?php $user_id = $this->session->userdata("user_id");
		if($userdata['image'] <> ''){
			$profile_pic = base_url().'blogimages/'.$userdata['image'];
		}else{
			$profile_pic = base_url().'images/my-profile image3.png';
		}
		if($userdata['first_name'] <> ''){
			$uname = ucwords($userdata['first_name']).' '.ucwords($userdata['last_name']);
		}
		else{
			$uname = $userdata['email'];
		}

 ?>

<link href="css/responsive-accordion.css" rel="stylesheet" type="text/css" media="all" />
<div class="content">
  <div class="container">
    <div class="create_folder_wrap"> <?php echo $this->load->view($sidebar_content);?>
      <div class="cf_content">
        <div class="heading4"> My Bulletin Board</div>
        <div class="pdf_wrap remove_mar">
          <div class="my_profile frnd_profile_dev">
            <div class="profile_pic_dev"> <img src="<?php echo $profile_pic; ?>" width="100" /> </div>
            <div class="p_tags_dev as">
              <div class="pro_1"> <?php echo $uname; ?> </div>
              <div class="pro_2">
                <?php if($userdata['worked'] <> ''){ ?>
                Works at <?php echo $userdata['worked'].'<br />'; } ?> 
                 <?php if($userdata['education'] <> ''){ ?>
                Education:  <?php echo $userdata['education']; } ?>  </div>
            </div>
            <div class="btn_dev"> 
              <!--  <input type="button" value="Send Message" class="send_msg"/>--> 
              <!--<a href="" class="send_msg">Send Message</a> -->
              <!--<a href="" class="del_frd">Unfriend</a>-->
            <!--  <input type="button" value="Unfriend" class="del_frd"/>-->
            </div>
            <div class="clear"></div>
          </div>
          <div class="notification">
            <!--<div class="clf"> <img src="<?php echo base_url();?>images/my_costom_folder image1.png" />
              <input type="text" class="cf" placeholder="search..." id="search_txt" onkeyup="search_file()"/>
            </div>-->
            <!--<div class="noti_inner oth">
              <ul>
                <li><a href="#"><img src="<?php echo base_url();?>images/search image1.png" /></a></li>
                <li><a href="#"><img src="<?php echo base_url();?>images/create-folder image4.png" /></a></li>
                <li><a href="#"><img src="<?php echo base_url();?>images/icon_new.png" /></a></li>
              </ul>
            </div>-->
            <div class="clear"></div>
          </div>
          <div class="clear"></div>
          <div class="container">
            <ul class="responsive-accordion responsive-accordion-default bm-larger">
              <?php if(!empty($resultset)){ foreach($resultset as $key => $val){ 
                      $resultset1 = $this->user_model->my_desk_folder_under_files_from_db($val);
              ?>
              <li>
                <div class="responsive-accordion-head">
                  <div class="folder_name_wrap">
                    <div class="fn_icon_dev"> <img src="<?php echo base_url();?>images/create-folder image3.png" /> </div>
                    <div class="fn_btn1_dev"> <?php echo $val['name']; ?> </div>
                   <!-- <div class="share_folder"><a href="<?php echo base_url()."userdata/".$val['relation']."/".$val['name'] ?>" ><img src="<?php echo base_url();?>images/download.png"></a> </div>-->
                    <div class="clear"></div>
                  </div>
                </div>
                <div class="responsive-accordion-panel">
                  <div class="accord_content">
                    <?php  if(!empty($resultset1)){ foreach($resultset1 as $key1 => $val1){ 
							$file_ext = explode('.', $val1['userfile']); $file_ext1 = end($file_ext);
					 ?>
                    <div class="pdf_name folder">
                      <ul>
                        <li><img width="40" src="<?php echo base_url();?>file_logo/<?php echo file_logo($file_ext1); ?>" ></li>
                        <li><?php echo $val1['userfile']; ?></li>
                        <li><?php echo date("d-m-Y | h:i A", $val1['time']); ?></li>
                        <li>
                          <?php if($file_ext1 == "zip" ||  $file_ext1 == "rar"){ ?>
                          <?php }else{ ?>
                            <a href="<?php echo base_url();?>user/view_file_popup/<?php echo $val['relation'];?>/<?php echo $val['name'];?>/<?php echo $val1['userfile']; ?>" title="View Note" ><img src="<?php echo base_url();?>images/view.png" height="14" /></a>
                          <?php } ?>
                        	 &nbsp; &nbsp;  &nbsp; &nbsp; <a target="_blank" href="<?php echo base_url(); ?>user/download_file/<?php echo $val['name']; ?>/<?php echo $val1['userfile']; ?>"><img src="<?php echo base_url();?>images/download.png" /></a>
                          
                        </li>
                        <!-- <li>
                                    <input type="checkbox" />
                                  </li>-->
                        <div class="clear"></div>
                      </ul>
                    </div>
                    <?php }} else{ ?>
                    <div class="pdf_name folder">
                      <ul>
                        <li style="width:35%">No file under this folder</li>
                      </ul>
                    </div>
                    <?php } } }else{ ?>
                    <div class="folder_name_wrap">
                      <div class="fn_btn1">No folder found</div>
                    </div>
                    <?php } ?>
                    <div class="clear"></div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <!-- cf content end -->
      <div class="clear"></div>
    </div>
  </div>
</div>
<script src="<?php echo base_url();?>js/responsive-accordion.js" type="text/javascript"></script> 
