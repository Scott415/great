<!DOCTYPE html>
<html lang="en">
<head>
	<title>AJAX Test</title>
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('/jquery-ui-1.8.16.custom.css'); ?>">	
	<script src="<?php echo base_url('/jquery-1.6.3.min.js'); ?>"></script>
	<script src="<?php echo base_url('/jquery-ui-1.8.16.custom.min.js'); ?>"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$("#myform").dialog({
				autoOpen: false,
				modal: true,
				resizable: false,
				beforeClose: function() {
					var query = $.ajax({
						type: "POST",
						cache: false,
						url: "<?php echo site_url('/test/update'); ?>",
						data: { mytext: $("#mytext").val() },
						success: function(theresponse) {
							$("#response").html(theresponse);
						},
						error: function() {
							$("#response").html("AJAX request failed.");
						}
					});
					
					$("#response").show();
				}
			});
		});
		
		function showform() {
			$("#mytext").val("");
			$("#myform").dialog("open");
		}
	</script>

	<style type="text/css">
		div#page {
			width: 960px;
			margin: 0 auto;
			font-family: arial, helvetica, sans;
		}
		
		div#myform, p#response {
			display: none;
		}
	</style>
</head>
<body>

	<div id="page">
		<h1>Click the link and enter the data to be saved</h1>
		<p><a href="#" onclick="showform();">Click here</a></p>
		<p id="response"> </p>
	</div>
	
	<div id="myform" title="Enter some data">
		<form method="POST" action="<?php echo site_url('/test/update'); ?>">
			<label for="thetext">Data:&nbsp;</label><input type="text" name="mytext" id="mytext" value="">
		</form>
	</div>

</body>
</html>
                                        