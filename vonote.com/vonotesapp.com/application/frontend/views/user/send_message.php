<?php $user_id = $this->session->userdata("user_id");
	  $reciver_id = $this->uri->segment('3');
?>

<!--content start-->

<div class="content">
<div class="container">
  <div class="login_inner msg_inner">
    <div class="message"> 
      <div class="send_message">Send Message &lt; &lt; <a href="<?php echo base_url(); ?>user/inbox" title="Back My Message" > My Messages </a></div>
      <div class="message_main">
        <div class="login_content_lft">
          <div class="email_log">
          <div style=" margin-bottom:20px;margin-left: 20px; position:absolute;" id="message"> <font color='red'><?php echo $this->session->flashdata('errormsg'); ?></font> <font color='green'><?php echo $this->session->flashdata('successmsg'); ?></font> <br class="clear" />
          </div>
          <form method="post" action="<?php echo base_url(); ?>user/send_message_db" enctype="multipart/form-data" >
          	<input type="hidden" name="sender_id" value="<?php echo $user_id; ?>" >
            <input type="hidden" name="reciver_id" value="<?php echo $reciver_id; ?>" >
            
            <div class="email_log_pas msg_block">
              <div class="message_txt">Message</div>
              <textarea type="text" class="msg_textarea" name="message"> </textarea>
              <input class="attach_file" type="file" name="userfile" />
            </div>
             
            <div class="msg_btn">
              <input type="submit" value="Send" class="send_msg_bttn"/>
            </div>
           </form>
          </div>
          <div class="clear"></div>
        </div>
        <div class="login_content_rght">
          <div class="msg_rght_img"><img src="<?php echo base_url(); ?>images/message.png" alt="" /></div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>
<!--content ends--> 
