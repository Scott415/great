<!-- Content Start -->
<?php $user_id = $this->session->userdata("user_id"); ?>
<div class="content">
  <div class="container">
    <div class="create_folder_wrap">
       <?php echo $this->load->view($sidebar_content);?>
      <div class="cf_content">
          <div class="heading4"> Notifications </div>
          <div class="pdf_wrap remove_mar">
              <div class="clear"></div>
            <br class="clear" />
            <?php if(!empty($resultset)){ foreach($resultset as $k => $v){
				    $userdata = $this->user_model->get_user_data($v['sender_id']);
					if($userdata['image'] <> ''){
						$profile_pic = base_url().'blogimages/'.$userdata['image'];
					}else{
						$profile_pic = base_url().'images/my-profile image3.png';
					}
					if($userdata['first_name'] <> ''){
						$uname = ucwords($userdata['first_name']).' '.ucwords($userdata['last_name']);
					}
					else{
						$uname = $userdata['email'];
					}

			?>
            <div class="my_profile requests">
              <div class="profile_pic"><a href="<?php echo base_url(); ?>user/user_wall/<?php echo $v['sender_id']; ?>" >   <img src="<?php echo $profile_pic; ?>" width="80"> </a></div>
              <div class="p_tags">
                <div class="pro_1"> <?php  echo $uname; ?> </div>
                <div class="pro_2"> <?php echo $v['content']; ?> </div>
                <div class="pro_3"></div>
              </div>
              <div class="clear"></div>
            </div>
            <?php } } else{ ?>
            <div class="my_profile requests">
              <div class="p_tags">
                <div class="pro_2"> No activity found </div>
                <div class="pro_3"></div>
              </div>
              <div class="clear"></div>
            </div>
            <?php } ?>
            <div class="clear"></div>
          </div>
        </div>
      <!-- cf content end -->
      <div class="clear"></div>
    </div>
  </div>
</div>
<!-- Content End --> 
