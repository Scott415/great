 <?php $user_id = $this->session->userdata("user_id");  ?>
  <!-- Content Start -->
  <div class="content">
    <div class="container">
      <div class="create_folder_wrap">
         <?php echo $this->load->view($sidebar_content);?>
        <div class="cf_content">
          <div class="heading4"> My Message </div>
          <div class="pdf_wrap remove_mar">
          <?php foreach($resultset as $k => $v){ //echo $user_id. '<br>' . $v['reciver_id'];
					if($v['reciver_id'] == $user_id){
						$frnd['user_id'] = $v['sender_id'];
					}else if($v['sender_id'] == $user_id){
						$frnd['user_id'] = $v['reciver_id'];
					}
					// get friend info
					$resultset1 = $this->user_model->user_profile_data($frnd);
					if($resultset1['image'] <> ''){
						$profile_pic = base_url().'blogimages/'.$resultset1['image'];
					}else{
						$profile_pic = base_url().'images/my-profile image3.png';
					}
					if($resultset1['first_name'] <> ''){
						$uname = ucwords($resultset1['first_name']).' '.ucwords($resultset1['last_name']);
					}
					else{
						$uname = $resultset1['email'];
					}
					$latest_msg = $this->user_model->latest_message($v['conversation_id']); //print_r($latest_msg);
		  ?>
            <div class="my_friends_dev1">
              <div class="mf_left_inbox" style="width:9%"><a href="<?php echo base_url(); ?>user/my_messages/<?php echo $v['conversation_id']; ?>/<?php echo $frnd['user_id']; ?>" >  <img src="<?php echo $profile_pic; ?>" width="50" /></a> </div>
              <div class="mf_right">
           		<a href="<?php echo base_url(); ?>user/my_messages/<?php echo $v['conversation_id']; ?>/<?php echo $frnd['user_id']; ?>" > 
                    <div class="mf_hdg_1"> <?php echo $uname; ?> </div>
                    <div class="mf_hdg_2"><?php if($v['sender_id'] == $user_id){ ?><img height="11" src="<?php echo base_url();?>images/reply.png" ><?php } ?> <span class="sky"><?php echo strlen($latest_msg['message']) > 100 ? substr($latest_msg['message'], 0,100).'...' : $latest_msg['message']; ?></span> </div>
                </a>
              </div>
              <div class="clear"></div>
            </div>
          <?php } ?>
          </div>
        </div>
        <!-- cf content end -->
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <!-- Content End --> 