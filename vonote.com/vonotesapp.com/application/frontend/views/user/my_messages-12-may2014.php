 <?php $user_id = $this->session->userdata("user_id"); 
	   $conversation_id=$this->uri->segment('3'); 
	   $reciver_id=$this->uri->segment('4'); 
?>
  <!-- Content Start -->
  <div class="content">
    <div class="container">
      <div class="create_folder_wrap">
         <?php echo $this->load->view($sidebar_content);?>
        <div class="cf_content">
          <div class="heading4"> My Messages </div>
          <div class="pdf_wrap remove_mar">
            <div class="my_msg_txt">
            
            <form method="post" action="<?php echo base_url(); ?>user/reply_message" >
              <textarea rows="4" cols="50" placeholder="Write a reply..." id="message_id" name="message"></textarea>
              <div style="float:right"><input type="submit" value="Send" class="send"/>
              <img src="<?php echo base_url(); ?>images/ajax-loader.gif" id="ajax_loader"  style="display:none;" />
              </div>
              <input type="hidden" value="<?php echo $conversation_id; ?>" name="conversation_id" id="m_conversation"  />
              <input type="hidden" value="<?php echo $user_id; ?>" name="user_id" id="m_sender_id"  />
              <input type="hidden" value="<?php echo $reciver_id; ?>" name="reciver_id" id="m_reciver_id"  />
           </form>
            </div>
            <div style=" margin-bottom:20px; position:absolute;" id="message"> <font color='red'><?php echo $this->session->flashdata('errormsg'); ?></font> <font color='green'><?php echo $this->session->flashdata('successmsg'); ?></font> <br class="clear" />
          </div>
            <div id="devresults"></div>
          <img style="margin-left:250px" src="<?php echo base_url(); ?>images/ajax-loader.gif" id="ajax_loader1"  style="display:none;" />
          </div>
        </div>
        <!-- cf content end -->
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <!-- Content End --> 
<script type="text/javascript" >
//for conversation details
	function my_message(){
		$('#ajax_loader1').show();
		var conversation_id = $('#m_conversation').val(); //alert(conversation_id);
		$.ajax({
			type: "POST",
			url: BASEURL+'ajax/my_conversation/'+conversation_id,
			success: function(rep){
				//alert(rep);
				response_array_t = rep.split('|::|');
				if(response_array_t[0] == 'success'){
					$('#devresults').html(response_array_t[1]); //alert(response_array_t[1]);
					$('#devresults').show();
					$('#ajax_loader1').hide();
				}
				else{
					alert("Error connecting to Remote Server");
				}
			}
		});
		$('#loading').hide();
	}
	

//for sreply message
/*	function reply_message(){
		$('#ajax_loader').show();
		if(message != '' && conversation_id != ''){
			var conversation_id = $('#m_conversation').val(); //alert(conversation_id);
			var sender_id = $('#m_sender_id').val(); //alert(conversation_id);
			var reciver_id = $('#m_reciver_id').val(); //alert(conversation_id);
			var message = $('#message_id').val();
			$.ajax({
				type: "POST",
				url: BASEURL+'ajax/reply_message/'+conversation_id+'/'+sender_id+'/'+reciver_id+'/'+message,
				success: function(rep){
				//	alert(rep);
					response_array_t = rep.split('|::|');
					if(response_array_t[0] == 'success'){
						my_message();
					}
					else{
						alert("Error connecting to Remote Server");
					}
				}
			});
	  }
		$('#ajax_loader').hide();
	}
*/


	$(document).ready(function() {
		 my_message();
	});

</script>
