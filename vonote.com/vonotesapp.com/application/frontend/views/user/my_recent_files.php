  <!-- Content Start -->
  <div class="content">
    <div class="container">
      <div class="create_folder_wrap">
        <?php echo $this->load->view($sidebar_content);?>
        <div class="cf_content">
          <div class="heading4"> My Recent Files </div>
          <div class="pdf_wrap remove_mar">
          <?php if(!empty($resultset)){ foreach($resultset as $key => $val){
		 		 $file_ext = explode('.', $val['userfile']); $file_ext1 = end($file_ext); 
				 					  $val['userfile'] = $val['userfile'].".".$val['ext']; $file_ext1 =$val['ext'];

		  ?>
            <div class="pdf_name full_msg">
              <ul>
                <li><img width="40" src="<?php echo base_url();?>file_logo/<?php echo file_logo($file_ext1); ?>"  /></li>
                <li><a href="#"><?php echo $val['userfile']; ?></a></li>
              <!--  <li class="files">Files</li>-->
                <li class="files_width"><?php echo date("d-m-Y | h:i A", $val['time']); ?></li>
                <div class="clear"></div>
              </ul>
            </div>
            <?php } } else{ ?>
            <div class="pdf_name full_msg">
              <ul>
                <li>No recent activity found... </li>            
              </ul>
            </div>
		    <?php } ?>
            <div class="clear"></div>
          </div>
        </div>
        <!-- cf content end -->
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <!-- Content End --> 
