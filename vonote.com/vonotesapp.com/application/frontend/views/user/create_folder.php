<script type="text/javascript" src="<?php echo base_url();?>js/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").click(function() {
				$.fancybox.open({
					href : '<?php echo base_url();?>user/popup',
					type : 'iframe',
					padding : 5,
					width:500,
					height:800,
				});
			});
		});
</script>
</head>
<body>
<!-- Content Start -->
<div class="content">
  <div class="container">
    <div class="create_folder_wrap">
       <?php echo $this->load->view($sidebar_content);?>
      <div class="cf_content">
        <div class="heading4"> Create folder </div>
        <div class="notification">
          <div class="noti_inner">
            <ul>
              <li><a class="fancybox"  href="javascript:;"><img src="<?php echo base_url();?>images/create-folder image4.png" /></a></li>
              <li><a href="#"><img src="<?php echo base_url();?>images/create-folder image7.png" /></a></li>
              <li><a href="#"><img src="<?php echo base_url();?>images/create-folder image8.png" /></a></li>
            </ul>
          </div>
          <div class="clear"></div>
        </div>
        <div class="folder_name_wrap">
          <div class="fn_icon"> <img src="<?php echo base_url();?>images/create-folder image3.png" /> </div>
          <div class="fn_btn"> <a href="#">Folder Name</a> </div>
          <div class="clear"></div>
        </div>
        <div class="pdf_wrap">
          <div class="share_folder"> Share Folder </div>
          <div class="clear"></div>
          <div class="pdf_name">
            <ul>
              <li><img src="<?php echo base_url();?>images/create-folder image6.png" /></li>
              <li><a href="#">New file name.pdf</a></li>
              <li>Files</li>
              <li>2-22/2014 | 10:00AM</li>
              <li><a href="#"><img src="<?php echo base_url();?>images/delete.png" /></a></li>
              <li>
                <input type="checkbox" />
              </li>
              <div class="clear"></div>
            </ul>
          </div>
          <div class="folder_name_wrap second">
            <div class="fn_icon"> <img src="<?php echo base_url();?>images/create-folder image3.png" /> </div>
            <div class="fn_btn1"> Math </div>
            <div class="clear"></div>
          </div>
          <div class="pdf_name">
            <ul>
              <li><img src="<?php echo base_url();?>images/create-folder image6.png" /></li>
              <li><a href="#">New file name.pdf</a></li>
              <li>Files</li>
              <li class="ad_width">2-22/2014 | 10:00AM</li>
              <li><a href="#"><img src="<?php echo base_url();?>images/delete.png" /></a></li>
              <div class="clear"></div>
            </ul>
          </div>
          <div class="pdf_name">
            <ul>
              <li><img src="<?php echo base_url();?>images/create-folder image6.png" /></li>
              <li><a href="#">New file name.pdf</a></li>
              <li>Files</li>
              <li class="ad_width">2-22/2014 | 10:00AM</li>
              <li><a href="#"><img src="<?php echo base_url();?>images/delete.png" /></a></li>
              <div class="clear"></div>
            </ul>
          </div>
          <div class="pdf_name">
            <ul>
              <li><img src="<?php echo base_url();?>images/create-folder image6.png" /></li>
              <li><a href="#">New file name.pdf</a></li>
              <li>Files</li>
              <li class="ad_width">2-22/2014 | 10:00AM</li>
              <li><a href="#"><img src="<?php echo base_url();?>images/delete.png" /></a></li>
              <div class="clear"></div>
            </ul>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <!-- cf content end -->
      <div class="clear"></div>
    </div>
  </div>
</div>
<!-- Content End -->