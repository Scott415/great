<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/validationEngine_signup.jquery.css" />
<?php  //print_r($userdata); ?>
<!-- Content Start -->
<div class="content">
  <div class="container">
    <div class="create_folder_wrap">
       <?php echo $this->load->view($sidebar_content);?>
      <div class="cf_content">
        <div class="heading4"> Reset Password </div>
        
        <div class="pdf_wrap remove_mar">
      <form id="dev_pw" method="post" action="<?php echo base_url();?>user/update_password_to_db" >
        <input type="hidden" value="<?php echo $this->session->userdata("user_id"); ?>" name="login_user_id"  />
          <div class="reset_pass">
          	<div style="margin-top: -23px; margin-left:160px; position:absolute;" id="message"> 
                <font color='red'><?php echo $this->session->flashdata('errormsg'); ?></font> 
                <font color='green'><?php echo $this->session->flashdata('successmsg'); ?></font> 
            </div>

            <div class="reset_slogan"> Old Password<span class="star">*</span> : </div>
            <div class="reset_fld">
              <input type="password" name="old_password" class="validate[required]" value="<?php echo $userdata['old_password']; ?>" />
            </div>
            <div class="clear"></div>
          </div>
          <div class="reset_pass">
            <div class="reset_slogan"> New Password<span class="star">*</span> : </div>
            <div class="reset_fld">
              <input type="password" name="new_password" class="validate[required]" value="<?php echo $userdata['new_password']; ?>"/>
            </div>
            <div class="clear"></div>
          </div><div class="reset_pass">
            <div class="reset_slogan"> Confirm Password<span class="star">*</span> : </div>
            <div class="reset_fld">
              <input type="password" name="confirm_password" class="validate[required]" value="<?php echo $userdata['confirm_password']; ?>"/>
            </div>
            <div class="clear"></div>
          </div>
          
          <div class="reset_btn">
            <input type="submit" value="Submit" class="submit" />
          </div>
          </form>
        </div>
      </div>
      <!-- cf content end -->
      <div class="clear"></div>
    </div>
  </div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		// binds form submission and fields to the validation engine
		jQuery("#dev_pw").validationEngine();
	});
</script>

<!-- Content End --> 
