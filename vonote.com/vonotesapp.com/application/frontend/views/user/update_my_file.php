<!-- Content Start -->

<div class="content">
  <div class="container">
    <div class="create_folder_wrap"> <?php echo $this->load->view($sidebar_content);?>
      <div class="cf_content">
        <!--<div class="heading4"> My Recent Files </div>-->
        <div class="pdf_wrap remove_mar">
          <?php if($this->session->flashdata('successmsg')){ ?>
          <script type="text/javascript">
    setTimeout('ourRedirect()',3000)
    function ourRedirect(){
		parent.$.fancybox.close();  //can use jQuery instead of $
		window.parent.location ='<?php	echo base_url()."user/my_desk/"; ?>';
    }
</script>
          <?php	}  //print_r($userdata); ?>
          <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/notes_styles.css" />
          <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Courgette" />
          <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
          <form method="post" action="<?php echo base_url(); ?>user/edit_file_folder_to_database" >
            <input type="hidden" value="<?php echo $this->session->userdata("user_id"); ?>" name="user_id"  />
            <input type="hidden" value="<?php echo $this->uri->segment('4'); ?>" name="userfile"  />
            <input type="hidden" value="<?php echo $this->uri->segment('3'); ?>" name="sub_folder"  />
            <div id="pad" style="margin-left:140px;">
              <h2>Note</h2>
              <textarea id="note" name="file_content"><?php echo $filedata; ?></textarea>
              <div id="note2" >
                <div style=" position:absolute;" id="message"> <font color='red'>
				<?php if(empty($filedata)){ ?>There is error opening file. Please check filename is correct<?php } ?>
				<?php echo $this->session->flashdata('errormsg'); ?></font> <font color='green'><?php echo $this->session->flashdata('successmsg'); ?></font> <br class="clear" />
                </div>
              </div>
            </div>
            <div class="clear" style="margin-top:36px;" ></div>
            <!--<div class="pop_fol_wrap" >
              <div class="pop_label">Select Folder : </div>
              <div class="pop_field">
                <?php  $val = $this->session->userdata("user_id"); ?>
                <select name="folder_name" style="margin-left:8px; width:200px">
                  <?php $resultset1 = $this->user_model->list_of_folders_created_by_user($val);
		   foreach($resultset1 as $k => $v){
		?>
                  <option value="<?php echo $v['name']; ?>" <?php if($userdata["name"] == $v['folder_name']){echo 'selected="selected"';} ?>><?php echo $v['name']; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="clear"></div>
            </div>-->
            <!--<div class="pop_fol_wrap" >
              <div class="pop_label"> File Name : </div>
              <div class="pop_field">
                <input type="text" name="name" value="<?php echo $userdata["name"];?>"  />
                .txt </div>
              <div class="clear"></div>
            </div>-->
        <div class="pop_fol_wrap" style="margin-left:40px">
            <div class="pop_label"> Make it : </div>
            <div class="pop_field non">
              <input type="radio" name="privacy" value="1" <?php if($resultset["privacy"] == 1){echo 'checked="checked"';};?> />
              Private
              <input type="radio" name="privacy" value="0" <?php if($resultset["privacy"] == 0){echo 'checked="checked"';};?> />
              Public </div>
            <div class="clear"></div>
          </div>            
          <div class="pop_fol_wrap" style="margin-left:118px">
              <div class="pop_label"> </div>
              <div class="pop_field non">
                <input type="submit" value="Save" class="ok" name="create_folder" />
                <input type="reset" value="Clear" class="ok" name="create_folder" />
              </div>
              <div class="clear"></div>
            </div>
          </form>
          <!-- JavaScript includes. --> 
          <script src="<?php	echo base_url(); ?>assets/js/notes_script.js"></script>
          <div class="clear"></div>
        </div>
      </div>
      <!-- cf content end -->
      <div class="clear"></div>
    </div>
  </div>
</div>
<!-- Content End --> 
