<html>
<head>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
<title>Subscriber management</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8">

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/ui-lightness/jquery-ui.css" type="text/css" media="screen"/>	
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<style type="css/text">

/*Datatables style*/
#big_table_wrapper {background-color: #fff; }
#big_table_wrapper .dataTables_length {float:left;}
#big_table_wrapper .dataTables_filter {float:right;}
#big_Table_wrapper .ui-toolbar{padding:5px;}
#big_Table_wrapper .ui-toolbar{padding:5px;}
#big_table{width:730px; text-align: center;}
.dataTables_paginate .ui-button {    margin-right: -0.1em !important;}
.paging_full_numbers .ui-button {    color: #333333 !important;    cursor: pointer;    margin: 0;    padding: 2px 6px;}
.dataTables_info {    float: left;    width: 50%;}
.dataTables_info {    padding-top: 3px;}
.dataTables_paginate {    float: right;    text-align: right;}
.dataTables_paginate {    width: auto;}
.paging_full_numbers {    width: 350px !important;}
#big_table_processing > img {padding-left:20px;}


</style>
</head>
<body>
<div class="wrapper">
<script type="text/javascript">
        $(document).ready(function() {
	var oTable = $('#big_table').dataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": '<?php echo base_url(); ?>index.php/subscriber/datatable',
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "iDisplayStart ":20,
                "oLanguage": {
            "sProcessing": "<img src='<?php echo base_url(); ?>assets/images/ajax-loader_dark.gif'>"
        },  
        "fnInitComplete": function() {
                //oTable.fnAdjustColumnSizing();
         },
                'fnServerData': function(sSource, aoData, fnCallback)
            {
              $.ajax
              ({
                'dataType': 'json',
                'type'    : 'POST',
                'url'     : sSource,
                'data'    : aoData,
                'success' : fnCallback
              });
            }
	} );
} );
</script>
<h1>Subscriber management</h1>
<?php echo $this->table->generate(); ?>
    </div>
</body>
<footer>
    <label class="footer-label" >Tutorial created by Ahmed Samy <a href="http://www.ahmed-samy.com">blog </a></label>
</footer>
</html>