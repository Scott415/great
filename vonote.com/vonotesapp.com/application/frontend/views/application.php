<div class="slide-full">
      <div class="slide-inside">
        <h2 class="head">Welcome Aboard</h2>
        <div class="clear"></div>
        <div class="clear"></div>
        <div class="slide-main-txt">Our goal at SocialTyer is to embrace creation and innovation. Have a<br />
          look at a few pointers below before uploading your very first product</div>
        <div class="clear"></div>
        <div class="slide-product1" onclick="location.href('index.html')">
          <div class="slide-product-img1"><img src="images/erro.png" alt="Go to your product" title="Go to your product" /></div>
          <div class="slide-product-txt1">Payments</div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="content_main">
      <div class="content_sub_1">
         <div class="ques_ans">
           <div class="heading_ques">
           What we do
           </div>
           <div class="answer_para">
           Our platform is geared towards driving constant traction to some of the most innovative and creative products out there. 
           </div>
           <div class="underline_1">
           </div>
           
           <div class="heading_ques">
       How it works
           </div>
           <div class="answer_para">
   Every product you list will allow anyone in the entire world to share it on their social networks. Their friends clicking through those links will in turn reward your sharers with a 10% commission. Let's call these sales people "tyers."
           </div>
           <div class="underline_1">
           </div>
           
           <div class="heading_ques">
          Getting Started
           </div>
           <div class="answer_para">
           Listing your product is absolutely free, and our goal is to help you build successful stores around your product collections. Simply follow the next steps to start listing your first product.
           </div>
           <div class="underline_1">
           </div>
           
           <div class="heading_ques">
           What�s not allowed
           </div>
           <div class="answer_para">
            <ul class="list_notdo">
  <li> Providing misleading details through your product descriptions</li>
<li>No offensive or illegal material allowed</li>
<li>No tobacco, drugs, or drug paraphernalia</li>
</ul>

           </div>
          <div class="agree_terms">
                     <input type="checkbox" name="genre" id="check-1" value="action" />
		<label for="check-1">Before getting started with listing your very first product, I certify that I have read and acknowledged the <a href="#">terms and conditions.</a> </label>
            <input type="submit" class="submt_agree"  value="Get Started"/>
            
          
           <div class="clear"></div>
          </div>
         </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  