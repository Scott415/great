<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $master_title; ?></title>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>images/favicon.ico" />
<?php include("head.php");?>
<!-- incluse all the scripts and css here in this file -->
</head>
<?php
	foreach($this->_ci_view_paths as $key=>$val){
		$view_path=$key;	
	}
?>
<body>
<?php
	$controllername=$this->router->class;
?>
<div align="center">
  <div class="container">
    <?php
        include('top_area.php');   // Are above menu bar is included in this file
		include('menu_bar_admin.php');   // Menu bar is included in this file
    ?>
    <div class="content">
      <?php /*?><div style="margin-top: 20px;" id="message"> <font color='red'><?php echo $this->session->flashdata('errormsg'); ?></font> <font color='green'><?php echo $this->session->flashdata('successmsg'); ?></font> </div><?php */?>
      <div style="margin-top: 20px;" id="message_dev"> <font color='red'><?php echo $this->session->flashdata('errormsg'); ?></font> <font color='green'><?php echo $this->session->flashdata('successmsg'); ?></font> </div>
      <?php if(isset($master_body) && $master_body!=""){?>
      <?php include($view_path.$controllername."/".$master_body.".php"); ?>
      <?php } ?>
      <div class="image_0"> <a href="http://slinfy.com/" target="_blank">
        <?php if($this->config->item("footer_image")!=""){ ?>
        <img src="<?php echo $this->config->item("footer_image"); ?>" alt="images/logo_text" width="182" height="40" />
        <?php } ?>
        </a> </div>
    </div>
  </div>
  <div class="clear"></div>
</div>
</div>
</body>
</html>
<?php
	if($this->config->item("process")=="yes"){
	 $this->output->enable_profiler(TRUE);
	}
?>