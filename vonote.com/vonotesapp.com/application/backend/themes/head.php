<?php DEFINE("BASEURL",base_url()); ?>

<script type="text/javascript">
	var BASEURL='<?php echo BASEURL;?>';
</script>
<link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url();?>thickbox/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>scripts/jquery-latest.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>thickbox/thickbox-compressed.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/clock.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/validate_functions.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/common.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jsFunctions.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/dhtmlxcalendar.css"></link>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/dhtmlxcalendar_dhx_skyblue.css"></link>
<script src="<?php echo base_url();?>js/dhtmlxcalendar.js"></script>
<script>
  var myCalendar;
  function doOnLoad() {
	  myCalendar = new dhtmlXCalendarObject(["calendar","calendar2","calendar3"]);
  }
</script>

