<?php 
class validations extends CI_Model {
 
    function __construct(){
        parent::__construct();
    }
	
	public function validate_attributes($attfinalarray){
		$i=0;
		$err=0;
		foreach($attfinalarray as $key=>$val){
		    $data[$i]=$val;
			$i++;	
		}		
		$maxvalues=$this->get_maximum_value_count($data);
		if($err==0){
			if(!$this->getKeyValueMatch($data)){
				$this->session->set_flashdata("errormsg","Error : No two same variation exists");
				$err=1;
			}
		}
		if($err==0)	{
			foreach($data as $key=>$val){
				if(count($val)!=count($data[$maxvalues]) && $err==0){
					$this->session->set_flashdata("errormsg","Error : Number of attributes should be same");	
					$err=1;
				}
			}
		}
		if($err==0){
			foreach($data as $key=>$val){
				if(count(array_diff($val,$data[$maxvalues]))==0  && $err==0 && $key!=$maxvalues){
					$this->session->set_flashdata("errormsg","Error : Each variation must have same number of group attributes");
					$err=1;
				}	
			}
		}
		if($err==0){
			return true;
		}
		else{
			return false;	
		}
	}
	
	public function validate_experience($experiencearray){
		if($experiencearray["experience"]==""){
			$this->session->set_flashdata("errormsg","Error : Please enter experience");
			$err=1;		
		}	
		else if(!$this->common->check_store_experience_availability($experiencearray["experience"]) && $experiencearray["id"]==""){
			$this->session->set_flashdata("errormsg","Error : Experience already exists in our database");
			$err=1;	
		}
		else{
			$err=0;	
		}
		if($err==1){
			return false;	
		}	
		else{
			return true;	
		}
	}	
	
	public function validate_blog_comments($arr){
		$err=0;
		if($arr["comment"]==""){
			$this->session->set_flashdata("errormsg","Error : Please write your comment");
			$err=1;	
		}
		if($err==1){
			return false;	
		}	
		else{
			return true;	
		}
	}

/******************************************Store validation ends ******************************************/
	
	
	/******************************************Blogs validation starts ******************************************/

	public function validate_blog_category($categoryarray){
		if($categoryarray["category_name"]==""){
			$this->session->set_flashdata("errormsg","Error : Please enter category");
			$err=1;		
		}	
		else if(!$this->common->check_blog_categoryavailability($categoryarray["category_name"]) && $categoryarray["id"]==""){
			$this->session->set_flashdata("errormsg","Error : Category already exists in our database");
			$err=1;	
		}
		else{
			$err=0;	
		}
		if($err==1){
			return false;	
		}	
		else{
			return true;	
		}
	}
	
	public function validate_blog_data($blogarray){
		if($blogarray["blog_title"]==""){
			$this->session->set_flashdata("errormsg","Error : Please enter blog title");
			$err=1;		
		}
		/*else if($blogarray["blog_category"]==""){
			$this->session->set_flashdata("errormsg","Error : Please select blog category");
			$err=1;		
		}*/
		else if($blogarray["blog_images"]==""  && $blogarray["id"]==""){
			$this->session->set_flashdata("errormsg","Error : Please enter blog image");
			$err=1;		
		}
		else if(isset($blogarray["blog_images"]) && $blogarray["blog_images"]!="" && !in_array($this->common->get_extension($blogarray["blog_images"]),$this->config->item("allowedimages"))){		$this->session->set_flashdata("errormsg","Error : File type is not valid");
			$err=1;	
		}
		else if($blogarray["blog_content"]==""){
			$this->session->set_flashdata("errormsg","Error : Please enter blog content");
			$err=1;	
		}
		/*else if(!$this->common->check_blog_titleavailability($blogarray["category_name"],$blogarray["blog_category"]) && $blogarray["id"]==""){
			$this->session->set_flashdata("errormsg","Error : Category already exists in our database");
			$err=1;	
		}*/
		if($err==1){
			return false;	
		}	
		else{
			return true;	
		}
			
	}	
	
	/******************************************Blogs validation ends ******************************************/
	
	
	/******************************************Profile validation starts ****************************************/
	
	public function validatepasswords($arr){
		if($arr["oldpassword"]==""){
			$this->session->set_flashdata("errormsg","Error : Please enter old password");	
			$err=1;	
		}	
		else if($arr["newpassword"]==""){
			$this->session->set_flashdata("errormsg","Error : Please enter new password");	
			$err=1;	
		}
		else if(preg_match('/[#$@%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/',$arr["newpassword"])){
			$this->session->set_flashdata("errormsg","Error : Spacial characters are not allowed in new password");
			$err=1;	
		}
		else if(strlen($arr["password"]) < 8 || strlen($arr["password"]) > 15){
			$this->session->set_flashdata("errormsg","Error : Your password must be between 8 and 15 characters long ");
			$err=1;	
		}
		else if($arr["confirmnewpassword"]==""){
			$this->session->set_flashdata("errormsg","Error : Please confirm your password");	
			$err=1;	
		}
		else if($this->common->checkpasswordvalidity($arr)){
			$this->session->set_flashdata("errormsg","Error : Wrong old password");	
			$err=1;	
		}
		else if($arr["newpassword"]!=$arr["confirmnewpassword"]){
			$this->session->set_flashdata("errormsg","Error : Both functions do not matches");	
			$err=1;	
		}
		else{
			$err=0;	
		}
		if($err==0){
			return true;	
		}
		else{
			return false;	
		}
	}
	/******************************************Profile validation ends ******************************************/
//for user validations		
	public function validate_userdata($arr){
		
		if($arr["email"] == "" || $arr["email"] == "Email"){
			$err=1;
			$this->session->set_flashdata("errormsg","Error : Please enter Email Id");	
		}
		else if(!$this->common->validate_email($arr["email"])){
			$err=1;
			$this->session->set_flashdata("errormsg","Error : Email should be valid");	
		}
		else if($arr["password"] == "" || $arr["password"] == "Password"){
			$err=1;
			$this->session->set_flashdata("errormsg","Error : Please enter password");	
		}
		else if(strlen($arr["password"]) < 8 || strlen($arr["password"]) > 15){
			$this->session->set_flashdata("errormsg","Error : Your password must be between 8 and 15 characters long ");
			$err=1;	
		}
		else if(!$this->common->check_email_availabilty($arr["email"])){
			$err=1;
			$this->session->set_flashdata("errormsg","Error : User with this email id already exists");	
		}
		else{
			$err=0;
		}
		if($err == 1){
			return false;	
		}
		else{
			return true;	
		}
	}

//for update user profile
	public function validate_user_profile_data($arr){
		$err=0;
		if($arr["first_name"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter First Name");
			$err=1;	
		}
		/*else if($arr["education"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter Education");
			$err=1;		
		}
		else if($arr["worked"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter where you worked");
			$err=1;		
		}
		else if($arr["about_me"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter About Me");
			$err=1;		
		}*/
		if($err==0){
			return true;	
		}
		else{
			return false;	
		}
	}


	
	public function validate_contact_details($arr){
		if($arr["first_name"]==""){
			$err=1;
			$this->session->set_flashdata("errormsg","Error : Please enter your first name.");	
		}
		else if($arr["email"]==""){
			$err=1;
			$this->session->set_flashdata("errormsg","Error : Please enter your email.");	
		}
		else if(!$this->common->validate_email($arr["email"])){
			$err=1;
			$this->session->set_flashdata("errormsg","Error : Please enter correct email.");	
		}
		
		else if($arr["message"]==""){
			$err=1;
			$this->session->set_flashdata("errormsg","Error : Please enter your question.");	
		}
		
		if($err == 1){
			return false;	
		}
		else{
			return true;	
		}
	}
	
	public function get_maximum_value_count($data){
		$max=0;
		$maxvalue=0;
		foreach($data as $datakey=>$dataval){
			if(count($dataval)>$maxvalue){
				$max=$datakey;	
			}	
		}
		
		return $max;
	}
	
	public function getKeyValueMatch($data){
		$k=0;
		foreach($data as $key=>$val){
			sort($val);
			$val[count($val)]=implode(",",$val);
			$newval[$k++]=$val[count($val)-1];		
		}
		if(count(array_unique($newval))!=count($data)){
			return false;				
		}
		else{
			return true;	
		}
		
	}
	
	//for contact us page
	public function validate_contact_form($arr){
		$err=0;
		if($arr["subject"]==""){
			$err=1;
			$this->session->set_flashdata("errormsg","Error : Please enter subject");	
		}
		else if(trim($arr["message"])==""){
			$err=1;
			$this->session->set_flashdata("errormsg","Error : Please enter message");	
		}	
		
		if($err == 0){
			return true;
		}
		else{
			return false;
			
		}		
	}

	//for change password	
	public function update_password($arr){ 
		$err=0;// print_r($arr);
		if($arr["old_password"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter old password");
			$err=1;	
		}	
		else if($arr["new_password"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter new password");
			$err=1;	
		}	
		else if(strlen($arr["new_password"]) < 5 || strlen($arr["new_password"]) > 10){
			$this->session->set_flashdata("errormsg","Error : Your new password must be between 5 and 10 characters long ");
			$err=1;	
		}
		else if($arr["confirm_password"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter confirm password");
			$err=1;	
		}
		else if($arr["confirm_password"] <> $arr["new_password"]){
			$this->session->set_flashdata("errormsg","Error : New password and confirm password should match.");	
			$err=1;	
		}
		else if($arr["old_password"] <> ""){
			$this->db->select("password");
			$this->db->from("users");
			$this->db->where("id",$arr["id"]);
			$query = $this->db->get();
			//echo $this->db->last_query(); die;
			$result = $query->row_array();
			//echo  $result["password"]; die;
			if($arr["old_password"] <> $result["password"]){
				$this->session->set_flashdata("errormsg","Error : Wrong old password");	
			 	$err=1;	
			}else{
				$err=0;
			}
		}else{
			$err==0;
		}
		if($err==0){
			return true;	
		}
		else{
			return false;	
		}
	}
	
	//for change password	
	public function set_password_fb($arr){ 
		$err=0;// print_r($arr);
		if($arr["new_password"] == ""){
			$this->session->set_flashdata("errormsg","Error :  Please enter password");
			$err=1;	
		}	
		else if(strlen($arr["new_password"]) < 5 || strlen($arr["new_password"]) > 10){
			$this->session->set_flashdata("errormsg","Error : Your password must be between 5 and 10 characters long ");
			$err=1;	
		}
		else if($arr["confirm_password"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter confirm password");
			$err=1;	
		}
		else if($arr["confirm_password"] <> $arr["new_password"]){
			$this->session->set_flashdata("errormsg","Error : Password and confirm password should match.");	
			$err=1;	
		}
		if($err==0){
			return true;	
		}
		else{
			return false;	
		}
	}
	
//for news letter
	public function validate_newsletter($arr){
		$err=0;
		if($arr["user_email"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter your Newsletter Email Id");
			$err=1;	
		}
		else if(!$this->common->validate_email($arr['user_email'])){
			$this->session->set_flashdata("errormsg","Error : Email you entered is not valid");
			$err=1;		
		}
		if($err==0){
			return true;	
		}
		else{
			return false;	
		}
			
	}

//for create folder
	public function validate_create_folder_data($arr){
		$err=0;
		if(preg_match('/[^a-z0-9 ]+/i',$arr["name"])){
			$this->session->set_flashdata("errormsg","Error : Use of special character “@#$^&*()_!” is not allowed");
			$err=1;		
		}
		
		else if($arr["name"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter folder name");
			$err=1;	
		}
		
		else if(!$this->common->check_foldername_availabilty($arr)){
			$err=1;
			$this->session->set_flashdata("errormsg","Error : You already create folder with this name");	
		}
		if($err==0){
			return true;	
		}
		else{
			return false;	
		}
	}
//for upload file under folder
	public function validate_upload_file_folder_data($arr){
		$err=0;
		$total=$this->user_model->user_limit();
		$path=$_SERVER['DOCUMENT_ROOT']."/userdata/".$this->session->userdata("user_id");;
		$disk_used = (($this->common->foldersize($path))/1048576)+$arr["file_size"];
		if($disk_used > $total){
			$this->session->set_flashdata("errormsg","Error : You have no space, Please delete some files ");
			$err=1;	
		}
		elseif($arr["name"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please select folder name");
			$err=1;	
		}
		else if($arr["userfile"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please select file");
			$err=1;		
		}
		/*else if($arr["privacy"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please choose privacy typle");
			$err=1;		
		}*/
		/*else if(!$this->common->check_foldername_availabilty($arr)){
			$err=1;
			$this->session->set_flashdata("errormsg","Error : You already create folder with this name");	
		}*/
		if($err==0){
			return true;	
		}
		else{
			return false;	
		}
	}
//for create file under folder
	public function validate_create_file_folder_data($arr){
		$err=0;
		
		if($arr["name"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter file name");
			$err=1;	
		}
		else if(preg_match('/[^a-z0-9 ]+/i',$arr["name"])){
			$this->session->set_flashdata("errormsg","Error : Use of special character “@#$^&*()_!” is not allowed");
			$err=1;		
		}
		else if($arr["folder_name"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please select folder");
			$err=1;	
		}
		else if($arr["file_content"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter content in file");
			$err=1;	
		}
		else if(!$this->common->check_filename_availabilty($arr)){ 
			$err=1;
			$this->session->set_flashdata("errormsg","Error : File name already exists please choose another file name");	
		}
		if($err==0){
			return true;	
		}
		else{
			return false;	
		}
	}
	
//for create file under folder
	public function validate_update_file_folder_data($arr){
		$err=0;
		if($arr["userfile"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter file name");
			$err=1;	
		}
		/*else if($arr["sub_folder"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please select folder");
			$err=1;	
		}*/
		else if($arr["file_content"] == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter content in file");
			$err=1;	
		}
		else if($this->common->check_filename_exists($arr)){ 
			$err=1;
			$this->session->set_flashdata("errormsg","Error : File file not found");	
		}
		if($err==0){
			return true;	
		}
		else{
			return false;	
		}
	}

	 public function validate_message_reply($messagearray=array()){
		$err=0;
		 if($messagearray["message"]==""){
			$this->session->set_flashdata("errormsg","Error : Please enter your message");	
			$err=1;
		 }	
		 if($err==0){
		   return true; 
		 } 
		 else{
		   return false; 
		 }	 
	 }
	 
//for delete folder
	public function validate_delete_folder($arr){
		$err=0;
		$user_id = $this->session->userdata("user_id");
		$this->db->select("relation");
		$this->db->from("user_folders");
		$this->db->where("id",$arr["file_id"] );
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		$result = $query->row_array();
		if($user_id <> $result['relation']){ 
			$this->session->set_flashdata("errormsg","Error : You have no access to delete this Folder");	
			$err=1;	
		}else{
			$err=0;
		}
		if($err==0){
			return true;	
		}
		else{
			return false;	
		}
	}
	 
//for delete file
	public function validate_delete_file($arr){
		$err=0;
		$user_id = $this->session->userdata("user_id");
		$this->db->select("user_id");
		$this->db->from("user_files");
		$this->db->where("id",$arr["file_id"] );
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		$result = $query->row_array();
		if($user_id <> $result['user_id']){ 
			$this->session->set_flashdata("errormsg","Error : You have no access to delete this file");	
			$err=1;	
		}else{
			$err=0;
		}
		if($err==0){
			return true;	
		}
		else{
			return false;	
		}
	}

//for forget password
	public function validate_forgot_password($arr){
		$err=0;
		if($arr == ""){
			$this->session->set_flashdata("errormsg","Error : Please enter your email id");
			$err=1;	
		}
		else if(!$this->common->validate_email($arr)){
			$this->session->set_flashdata("errormsg","Error : Email you entered is not valid");
			$err=1;		
		}
		else if($this->common->check_email_availabilty($arr)){
				$this->session->set_flashdata("errormsg","Error : Email id not exist or your account not active, please enter correct email id ");
			    $err=1;		
		}
		else if(!$this->common->getuser_id_new($arr)){
				$this->session->set_flashdata("errormsg","Error : Your account is currently inactive. Please contact admin.");	
			    $err=1;		
		}
		
		if($err==0){
			return true;	
		}
		else{
			return false;	
		}
			
	}
//for send message
	public function validate_send_message($arr){ //echo "hello"; die;
		$where = array('id' => $arr["reciver_id"], 'status'=> 1, 'archive' => 0 );
		 $num = $this->common->chk_request('users', $where); 
		if(trim($arr['message']) == ""){ 
			$this->session->set_flashdata("errormsg","Error : Please enter message");
			$err=1;	
		}
		else if($num == 0){
				$this->session->set_flashdata("errormsg","Error : Access Decline");
			    $err=1;		

		}
		if($err==0){
			return true;	
		}
		else{
			return false;	
		}
    }

}



