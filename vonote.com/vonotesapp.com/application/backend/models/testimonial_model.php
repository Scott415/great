<?php 
class testimonial_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
	
	/********************************************************Testimonial function starts***************************************************/
	
	public function getTestimonialData($searchdata=array())
	{		
		if(!isset($searchdata["page"]) || $searchdata["page"]=="")
		{
			$searchdata["page"]=0;	
		}
	    if(!isset($searchdata["countdata"]))
		{	
			if(isset($searchdata["per_page"]) && $searchdata["per_page"]!="")
			{
				$recordperpage=$searchdata["per_page"];	
			}
			else
			{
				$recordperpage=1;
			}
			if(isset($searchdata["page"]) && $searchdata["page"]!="")
			{
				$startlimit=$searchdata["page"];	
			}
			else
			{
				$startlimit=0;
			}
		}
		
		$this->db->select("*,ugk_testimonials.id as testimonialid");
		$this->db->from("ugk_testimonials");
		if($searchdata["type"]=="activated")
		{
			$where=array("ugk_blogs.status"=>"1","ugk_blogs_category.category_status"=>"1");	
			$this->db->where($where);	
		}
		$where=array("ugk_testimonials.status <>"=>"4");
		$this->db->where($where);		
		if(isset($searchdata["per_page"]) && $searchdata["per_page"]!="")
		{
			if($recordperpage!="" && ($startlimit!="" || $startlimit==0))
			{
				$this->db->limit($recordperpage,$startlimit);
			}
		}
		
		$query = $this->db->get();
		$resultset=$query->result_array();
		return $resultset; 
	}
		
	public function add_edit_testimonial($testimonialarray)
	{
		if($testimonialarray["id"]=="")
		{
			$blogarray["date_posted"]=time();
			$blogarray["last_modified"]=time();
			return $this->db->insert("ugk_testimonials",$testimonialarray);	
		}	
		else
		{
			$testimonialarray["last_modified"]=time();
			$this->db->where("id",$blogarray["id"]);
			return $this->db->update("ugk_testimonials",$testimonialarray);	
		}	
	}
	
	public function enable_disable_testimonials($testimonialid,$status)
	{
		$this->db->where("id",$testimonialid);
		$array=array("status"=>$status);
		$this->db->update("ugk_testimonials",$array);		
	}
	
	public function archive_testimonials($testimonialid)
	{
		$where=array("id"=>$testimonialid);
		$array=array("status"=>4);
		$this->db->where($where);
		$this->db->update("ugk_testimonials",$array);		
	}	
	
	
}
?>