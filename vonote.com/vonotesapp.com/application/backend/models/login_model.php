<?php 
ob_start();
class login_model extends CI_Model { 
    function __construct(){
        parent::__construct();
    }
	
	/************************************************* Admin login functions starts **************************************************/	
	
	public function check_admin_login($arr){
		$checkarray=array("username"=>$arr["username"],"password"=>$arr["password"]);
		$this->db->select("username");
		$this->db->from("admin");
		$this->db->where($checkarray);
		$query=$this->db->get();
		//echo $this->db->last_query();die;
		$resultset=$query->row_array();
		if(isset($resultset["username"])){
			return $resultset["username"];
		}
		else{
			return "";	
		}
	}
		
	public function user_email_verify($lgId){		
		$arr = array("status"=>1);
		$this->db->where(array('id'=>$lgId));
        if($this->db->update("users",$arr)){
			return true;
		}
		else{
			return false;	
		}
	}
	
	function getUserData($userid){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where(array('users.id'=>$userid));
		$query = $this->db->get();
		$resultset = $query->row_array();
		return $resultset;
	}
	
	
	/*public function checkFaceBookLogin($facebookarray=array()){
		$this->db->select("*");
		$this->db->from("users");
		$this->db->where(array("fbid"=>$facebookarray["fbid"]));
		$result=$this->db->get();
		$count=$result->num_rows();
		if($count!=0){
			return $result->row_array();	
		}
		else{
			$this->db->insert("users",$facebookarray);
			$key=$this->db->insert_id();	
			$this->db->select("*");
			$this->db->from("users");
			$this->db->where(array("id"=>$key));
			$result=$this->db->get();
			return $result->row_array();
		}
	}*/
	
	/************************************************* Admin login functions ends **************************************************/
}