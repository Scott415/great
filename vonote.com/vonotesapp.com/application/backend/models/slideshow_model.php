<?php 
class slideshow_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
	
	/********************************** slideshow function starts*************************************/
	
	public function getSlideshowData($searchdata=array())
	{
		$searcharray=array("status"=>"status");
		
		if(!isset($searchdata["page"]) || $searchdata["page"]=="")
		{
			$searchdata["page"]=0;	
		}
	    if(!isset($searchdata["countdata"]))
		{	
			if(isset($searchdata["per_page"]) && $searchdata["per_page"]!="")
			{
				$recordperpage=$searchdata["per_page"];	
			}
			else
			{
				$recordperpage=1;
			}
			if(isset($searchdata["page"]) && $searchdata["page"]!="")
			{
				$startlimit=$searchdata["page"];	
			}
			else
			{
				$startlimit=0;
			}
		}
		
		$this->db->select("*,slideshow.id as sld_id");
		$this->db->from("slideshow");
		if(isset($searchdata["search"]) && $searchdata["search"]!="" && $searchdata["search"]!="search")
		{
			$this->db->like("slideshow.title",$searchdata["search"]);	
		}	
		foreach($searchdata as $key=>$val)
		{
			if(isset($searcharray[$key]) && $searchdata[$key]!="")
			{
				if(array_key_exists($key,$searcharray))
				{
					$where=array($searcharray[$key]=>$val);
					$this->db->where($where);
				}
			}
		}		
		
		$where=array("slideshow.status <>"=>"4");
		$this->db->where($where);		
		if(isset($searchdata["per_page"]) && $searchdata["per_page"]!="")
		{
			if($recordperpage!="" && ($startlimit!="" || $startlimit==0))
			{
				$this->db->limit($recordperpage,$startlimit);
			}
		}
		
		$query = $this->db->get();
		$resultset=$query->result_array();
		return $resultset; 
	}
	
	
	public function add_edit_slideshow($slideshowarray)
	{
		if($slideshowarray["id"]=="")
		{
			$slideshowarray["date_posted"]=time();
			$slideshowarray["last_modified"]=time();
			return $this->db->insert("slideshow",$slideshowarray);	
		}	
		else
		{
			if($_FILES["userfile"]["name"]!=''){
			$this->db->select("*");
			$this->db->from("slideshow");
			$this->db->where("id",$slideshowarray["id"]);
			$query = $this->db->get();
		    $Images=$query->row_array();
			$Images['sld_images'];
			unlink('../slideshowimages/'.$Images['sld_images']);
			}
			$slideshowarray["last_modified"]=time();
			$this->db->where("id",$slideshowarray["id"]);
			return $this->db->update("slideshow",$slideshowarray);	
		}	
	}
	
	public function getIndividualSlideshow($sld_id)
	{
		$this->db->select("*,slideshow.id as sld_id");	
		$this->db->from('slideshow');
		$where=array("slideshow.id"=>$sld_id,"slideshow.status <> "=>"0");
		$this->db->where($where);
		$query = $this->db->get();
		$resultset=$query->row_array();
		return $resultset;		
	}
	
	
	public function enable_disable_slideshow($sld_id,$status)
	{
		$this->db->where("id",$sld_id);
		$array=array("status"=>$status);
		$this->db->update("slideshow",$array);		
	}
	
	public function archive_slideshow($sld_id)
	{
		$where=array("id"=>$sld_id);
		$array=array("status"=>4);
		$this->db->where($where);
		$this->db->update("slideshow",$array);		
	}	
	
	/***************************************** slideshow function ends **********************************************/
	
	
}
?>