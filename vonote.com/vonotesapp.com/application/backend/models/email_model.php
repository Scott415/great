<?php 
class email_model extends CI_Model { 
    function __construct() {
        parent::__construct();
		$this->load->model("user_model");
    }

	/************************************************* Email functions starts **************************************************/	

	public function sendIndividualEmail($emailarr){
			$this->load->library('email', $config);
			$this->load->library('parser', $config);
			$this->email->set_newline("\r\n");
			$this->email->to($emailarr["to"]);// change it to yours
			$this->email->from($config['smtp_user'], 'VO Notes');
			$this->email->subject($emailarr["subject"]);
			$this->email->message($emailarr["message"]);
			$result = $this->email->send();
			/*$email_to = $emailarr["to"];
			$subject = $emailarr["subject"];
			$from = "vonotesmail@gmail.com";//$emailarr["email"];
			$message = $emailarr["message"];
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$headers .= 'From:'.$from. "\r\n";
			$result = mail($email_to,$subject,$message,$headers);*/
	  if($result){
			$err=0;
	  }
	  else{
		 //show_error($this->email->print_debugger());
		 $this->email->print_debugger();
		 return $err=1;
	  }
	}
	
	public function send_contact_email($emailarr){ 
			 $this->load->library('email', $config);
			 $this->load->library('parser', $config);
			 $this->email->set_newline("\r\n");
			 $this->email->to($emailarr["to"]);// change it to yours
			// $this->email->to('vonotesmail@gmail.com');
			 $this->email->from($config['smtp_user'], $this->config->item('sitename'));
			 //$this->email->from("testing.slinfy02@gmail.com", $this->config->item('sitename'));
			 $this->email->subject($emailarr["subject"]);
			 $this->email->message($emailarr["message1"]);
			 //$this->email->clear();
			 $result = $this->email->send(); 
			 if($result){
			  $err=0;
			  return true;
			 }
			 else{
				$this->email->print_debugger();
				$err=1;
				return false;
			  }
 }
	
	/*public function send_contact_email($emailarr){
		
	  $this->load->library('email', $config);
      $this->load->library('parser', $config);
	  $this->email->set_newline("\r\n");
	  $this->email->to($emailarr["to"]);// change it to yours
      $this->email->from($config['smtp_user'], 'VO Notes');
	  $this->email->subject($emailarr["subject"]);
	  $this->email->message($emailarr["message1"]);
	  $result = $this->email->send(); print_r($result); die;
	  if($result){ echo "asdfghjkl"; die;
			 return true;
	  }
	  else{
		 $this->email->print_debugger();
		  return false;echo "12345678910"; die;
		
	  }
		
   }*/
   
   //for forgot password	
	public function UserPasswordsendIndividualEmail($emailarr){ 
			$this->load->library('email', $config);
			$this->load->library('parser', $config);
			$this->email->set_newline("\r\n");
			
			$this->email->to($emailarr["to"]);// change it to yours
			$this->email->from($config['smtp_user'], $this->config->item('sitename'));
		
			$this->email->subject($emailarr["subject"]);
			$this->email->message($emailarr["message"]);
			$result = $this->email->send();
	if($result){ 
		$err=true;
	}
	else{//show_error($this->email->print_debugger());
		 $this->email->print_debugger();
		  $err=false;
		}
		 return $err;
	}
	
	/************************************************* Email function ends **************************************************/
}