<?php 
class newsletter_model extends CI_Model {
var $user_table = 'ugk_newsletters'; 
 
    function __construct()
    {
        parent::__construct();
		
    }
	
	function insertNewsletterData($arr)
	{
		$result = $this->db->insert('ugk_newsletters',$arr);
		return $result;
	}
	
	function getNewsletterdata()
	{
		$this->db->where('status',1);
		$this->db->select('*');
		$this->db->from('ugk_newsletters');
		$query = $this->db->get();
		$resultset=$query->row_array();
		return $resultset;
	}
	
	
}
?>