<?php 
class user_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }

	public function get_user(){
		$this->db->select("*");	
		$this->db->from('users');
		$where=array("status" =>1,"archive <> " => 1);
		$this->db->where($where);
		$this->db->order_by("name asc");
		$query = $this->db->get();
	    //echo $this->db->last_query();die;
		$resultset=$query->result_array();
		return $resultset;	
	}
	
	//for fetch user update profile data data
	public function user_profile_data($arr){// print_r($arr);
		$this->db->select("*");	
		$this->db->from('users');
		$where = array("id" =>$arr['user_id'], 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where);
		//$this->db->order_by('time'); // Order by
		$query = $this->db->get();
	   // echo $this->db->last_query(); die;
		$resultset=$query->row_array();
		//print_r($resultset);
		return $resultset;	
	}
	
	
	//for delete a file
	public function delete_a_friend($arr){
	print_r($arr);
	$this->db->where(array('friend_id'=>$arr['friend_id'],'user_id' => $arr['user_id']));
	$this->db->delete('friends'); 
	$this->db->where(array('friend_id'=>$arr['user_id'],'user_id' => $arr['friend_id']));
	$this->db->delete('friends');
		 //$this->db->last_query();die;
		return 1;
	}
	
	
	//for fetch user update profile data for admin panle  
	public function view_user_profile_data($arr){// print_r($arr);
		$this->db->select("*");	
		$this->db->from('users');
		$where = array("id" =>$arr['user_id'], 'archive <> ' => 1);
		$this->db->where($where);
		//$this->db->order_by('time'); // Order by
		$query = $this->db->get();
	  	//echo $this->db->last_query(); die;
		$resultset=$query->row_array();
		//print_r($resultset);
		return $resultset;	
	}

	//for update user profile data
	 public function add_edit_user($arr){
		if($_FILES["userfile"]["name"]!=''){
			$this->db->select("*");
			$this->db->from("users");
			$this->db->where("id",$arr["user_id"]);
			$query = $this->db->get();
		    $Images=$query->row_array();
			$Images['image'];
			unlink('blogimages/'.$Images['image']);
		}
			$id = $arr["user_id"];
			unset($arr["user_id"]);
			$this->db->where("id",$id);
			$result = $this->db->update("users",$arr);
			//echo $this->db->last_query(); die;
			return $result;
	 }
	//for update user profile data
	 public function add_user($arr){
	 $this->load->helper('date');

		 	if(trim($arr['fbid'])==''){
				$this->db->select("*");
				$this->db->from("users");
				$this->db->where(array('email' => $arr['email'], 'archive <>' => 1, 'status' => 1));
			//	$this->db->where('email' => $arr['email'], 'archive <>' => 1, 'status' => 1);
				$query = $this->db->get();
				//echo $this->db->last_query();
				$resultset=$query->num_rows();
				$res = $query->row_array();
				if($resultset <> 0){
					$arr["time"] = time();
					
				
			$arr["last_activity"] = now();
					$arr['status']=1;
					$arr["last_ip_address"] =  $this->input->ip_address();
					$this->db->where("email",$arr["email"]);
					$result = $this->db->update("users",$arr);

				}else{
					$arr["time"] = time();
					$arr['status']=1;
					$arr["last_activity"] = now();
					$arr["ip_address"] =  $this->input->ip_address();
					$arr["last_ip_address"] =  $this->input->ip_address();
					return $this->db->insert("users",$arr);	
				}
			}
			else{
				$arr['status']=1;
				$arr["time"] = time();
				$arr["last_activity"] = now();
				$arr["last_ip_address"] =  $this->input->ip_address();
				$arr["ip_address"] =  $this->input->ip_address();
				$result = $this->db->insert("users",$arr);
				//echo $this->db->last_query(); 
				return $result;
			}
	 }
	 
	public function get_user_data_fb($email){
		
		$this->db->select("id");
		$this->db->from("users");
		$this->db->where("email",$arr["email"]);
		$query = $this->db->get();
		$res = $query->row_array();
		$id=$res[id];
		return $id;
	}
	 
	 
	 public function edit_user($arr){
		//echo "vklnsdvbhsdbvjbsdjvbsdjbvsdbv"; 
		// debug($arr);die;
		if($arr["password"] <> ""){
			$arr["password"] = $this->common->$arr;//generate salted password 
		}
		$sql = "SELECT max(sort) from users";
		$query = $this->db->query($sql);
		//echo $this->db->last_query();
		$data = $query->result_array();
		if($data[0]['max(sort)'] == NULL){
			$arr["sort"] = 0;
		}
		else{
			$arr["sort"] = ($data[0]['max(sort)']+1);
		}
		$arr["status"]=1;
		$arr["time"]=time();
		$id= $arr["id"];
		unset($arr["id"]);
		$this->db->where(array("id"=>$id));
		return $this->db->update("users",$arr);
		//echo $this->db->last_query();die;
	 }
	 
	 public function verify_email($id)  
	{	
		$this->db->select('count(*) as count');
		$this->db->from('users');
		$this->db->where(array('id'=>$id,'status' => 0));
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		$verfied = $query->row_array();
		if($verfied["count"] > 0){
			$arr = array("status"=>1);
			$this->db->where(array('id'=>$id));
			$result = $this->db->update("users",$arr);
			$data='0';
		}
		else{
			$data='1';
		}
		return $data;
	}
	
	function get_user_data($user_id){
		
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where(array('id' => $user_id));
		$query = $this->db->get();
		//echo $this->db->last_query();
		$resultset = $query->row_array();
		return $resultset;
	}
	public function getuserData($searcharray=array()){
		$recordperpage="";
		$startlimit="";
		if(!isset($searcharray["page"]) || $searcharray["page"]==""){
			$searcharray["page"]=0;	
		}
		 if(!isset($searcharray["countdata"])){	
			if(isset($searcharray["per_page"]) && $searcharray["per_page"]!=""){
				$recordperpage=$searcharray["per_page"];	
			}
			else{
				$recordperpage=1;
			}
			if(isset($searcharray["page"]) && $searcharray["page"]!=""){
				$startlimit=$searcharray["page"];	
			}
			else{
				$startlimit=0;
			}
		}
		
		$sql = "SELECT * FROM users where 1=1 AND archive <>1";
		if(isset($searcharray["search"]) && $searcharray["search"]!=""){
			$sql.=" AND email like '%".$searcharray["search"]."%' OR first_name like '%".$searcharray["search"]."%' OR last_name like '%".$searcharray["search"]."%'";
		}
		
		if(isset($searcharray["page"]) && $searcharray["page"]!="")
		{
			if($recordperpage!="" && ($startlimit!="" || $startlimit==0))
			{
				$sql.=" limit  $startlimit,$recordperpage";
			}
		}
		$query = $this->db->query($sql);
		//echo $this->db->last_query();die;
		
		$resultset = $query->result_array();
		
	//	print_r($resultset);die;
	   return $resultset;
}

	public function getUserFolders($searcharray=array()){
		$recordperpage="";
		$startlimit="";
		if(!isset($searcharray["page"]) || $searcharray["page"]==""){
			$searcharray["page"]=0;	
		}
		 if(!isset($searcharray["countdata"])){	
			if(isset($searcharray["per_page"]) && $searcharray["per_page"]!=""){
				$recordperpage=$searcharray["per_page"];	
			}
			else{
				$recordperpage=1;
			}
			if(isset($searcharray["page"]) && $searcharray["page"]!=""){
				$startlimit=$searcharray["page"];	
			}
			else{
				$startlimit=0;
			}
		}
		
		$sql = "SELECT * FROM user_folders where 1=1 AND archive <>1";
		if(isset($searcharray["search"]) && $searcharray["search"]!=""){
			$sql.=" AND name like '%".$searcharray["search"]."%' ";
		}
		
		if(isset($searcharray["page"]) && $searcharray["page"]!=""){
			if($recordperpage!="" && ($startlimit!="" || $startlimit==0)){
				$sql.=" limit  $startlimit,$recordperpage";
			}
		}
		$query = $this->db->query($sql);
		//echo $this->db->last_query();die;
		$resultset = $query->result_array();
		//print_r($resultset);die;
	   return $resultset;
}

	public function enable_disable_folders($id,$status){
		//echo $id;
		//echo $status;
		$this->db->where("id",$id);
		$arr=array("status" => $status);
		return $this->db->update("user_folders",$arr);
		//return $this->db->last_query();
	}
	public function archive_folders($id){
		$this->db->where("id",$id);
		$arr=array("status" => 0, 'archive' => 1);
		return $this->db->update("user_folders",$arr);
		//return $this->db->last_query();
	}

	public function getUserfiles($searcharray=array()){
		$recordperpage="";
		$startlimit="";
		$sql1 = "select * from user_folders where archive=0 AND id='".$searcharray['floderid']."' AND relation='".$searcharray['rel_id']."'";
		$query1 = $this->db->query($sql1);
		//echo $this->db->last_query();die;
		$resultsetf = $query1->result_array();
		
		if(!isset($searcharray["page"]) || $searcharray["page"]==""){
			$searcharray["page"]=0;	
		}
		 if(!isset($searcharray["countdata"])){	
			if(isset($searcharray["per_page"]) && $searcharray["per_page"]!=""){
				$recordperpage=$searcharray["per_page"];	
			}
			else{
				$recordperpage=1;
			}
			if(isset($searcharray["page"]) && $searcharray["page"]!=""){
				$startlimit=$searcharray["page"];	
			}
			else{
				$startlimit=0;
			}
		}
		
		$sql = "SELECT * FROM user_files where 1=1 AND archive = 0 AND sub_folder='".$resultsetf[0]['name']."' AND folder_name='".$searcharray['rel_id']."'";
		if(isset($searcharray["search"]) && $searcharray["search"]!=""){
			$sql.=" AND name like '%".$searcharray["search"]."%' ";
		}
		
		if(isset($searcharray["page"]) && $searcharray["page"]!=""){
			if($recordperpage!="" && ($startlimit!="" || $startlimit==0)){
				$sql.=" limit  $startlimit,$recordperpage";
			}
		}
		$query = $this->db->query($sql);
		//echo $this->db->last_query();die;
		$resultset = $query->result_array();
		//print_r($resultset);die;
	   return $resultset;
}

	public function get_file_details($name) {
		$file=explode(".",$name);
		$name=$file[0];
		$sql = "SELECT * FROM user_files where  userfile = '$name'";
		$query = $this->db->query($sql);
		$resultset = $query->row_array();
		return $resultset;
		//print_r($resultset);
	}


	public function enable_disable_files($id,$status){
		//echo $id;
		//echo $status;
		$this->db->where("id",$id);
		$arr=array("status" => $status);
		return $this->db->update("user_files",$arr);
		//return $this->db->last_query();
	}
	public function archive_files($id){
		$this->db->where("id",$id);
		$arr=array("status" => 0, 'archive' => 1);
		return $this->db->update("user_files",$arr);
		//return $this->db->last_query();
	}
	
	public function getUserfile($searcharray=array()){
		$this->db->select("*");	
		
		$this->db->from('user_files');
		$where = array("id" => $searcharray["fid"], 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where);
		$this->db->order_by('time DESC'); // Order by
		$query = $this->db->get();
	   // echo $this->db->last_query();
		$resultset=$query->result_array();
	//	print_r($resultset1);
		return $resultset;	
		
		
		$recordperpage="";
		$startlimit="";
		$sql1 = "select * from user_folders where archive=0 AND id='".$searcharray['floderid']."' AND relation='".$searcharray['rel_id']."'";
		$query1 = $this->db->query($sql1);
		//echo $this->db->last_query();die;
		$resultsetf = $query1->result_array();
		
		if(!isset($searcharray["page"]) || $searcharray["page"]==""){
			$searcharray["page"]=0;	
		}
		 if(!isset($searcharray["countdata"])){	
			if(isset($searcharray["per_page"]) && $searcharray["per_page"]!=""){
				$recordperpage=$searcharray["per_page"];	
			}
			else{
				$recordperpage=1;
			}
			if(isset($searcharray["page"]) && $searcharray["page"]!=""){
				$startlimit=$searcharray["page"];	
			}
			else{
				$startlimit=0;
			}
		}
		
		$sql = "SELECT * FROM user_files where 1=1 AND archive = 0 AND sub_folder='".$resultsetf[0]['name']."' AND folder_name='".$searcharray['rel_id']."'";
		if(isset($searcharray["search"]) && $searcharray["search"]!=""){
			$sql.=" AND name like '%".$searcharray["search"]."%' ";
		}
		
		if(isset($searcharray["page"]) && $searcharray["page"]!=""){
			if($recordperpage!="" && ($startlimit!="" || $startlimit==0)){
				$sql.=" limit  $startlimit,$recordperpage";
			}
		}
		$query = $this->db->query($sql);
		$resultset = $query->row_array();
		//print_r($resultset);die;
	   return $resultset;
}
	

	public function view_user($id){
		$this->db->select("*");	
		$this->db->from('users');
		$where=array("id" =>$id,"archive <> " => 1);
		$this->db->where($where);
		$query = $this->db->get();
	    //echo $this->db->last_query();
		$resultset=$query->row_array();
		return $resultset;	
	}
	public function enable_disable_user($id,$status){
		//echo $id;
		//echo $status;
		$this->db->where("id",$id);
		$arr=array("status" => $status);
		return $this->db->update("users",$arr);
		//return $this->db->last_query();
	}
	public function archive_user($id){
		$this->db->where("id",$id);
		$arr=array("status" => 0, 'archive' => 1);
		return $this->db->update("users",$arr);
		//return $this->db->last_query();
	}
	public function delete_user($id){
		$this->db->where("id",$id);
		$arr=array("archive"=>1);
		return $this->db->update("users",$arr);
	//	echo $this->db->last_query();
	}
	
	
	// update password
	public function changeUserPassword($passarr){
		$id = $passarr['id'];
		$oldPass = $passarr['old_password'];
		$data['password'] = $passarr['new_password'];
		
		$this->db->where(array('id' =>$id));
		if($this->db->update("users",$data)){
		   return true; 
		}
		else{
		   return false; 
		}
	}
	
	// set new  password after login with facebook
	public function changeUserPassword_fb($passarr){
		$id = $passarr['id'];
		//$oldPass = $passarr['old_password'];
		$data['password'] = $passarr['new_password'];
		
		$this->db->where(array('id' =>$id));
		if($this->db->update("users",$data)){
		   return true; 
		}
		else{
		   return false; 
		}
	}
	
	//upload files to datadase
	public function upload_file_to_database($arr){
		$arr1['userfile'] = $arr['file_name'];
		$ext_n = end(explode('.', $arr['file_name']));
		$arr1['ext'] = $ext_n;
		$arr1['user_id'] = $arr['f_relation'];
		$arr1['foder_name'] = $arr['f_relation'];
		$arr1['sub_folder'] = $arr['f_name'];
		$sql = "SELECT max(sort) from user_files";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		if($data[0]['max(sort)'] == NULL){
			$arr1["sort"] = 0;
		}
		else{
			$arr1["sort"] = ($data[0]['max(sort)']+1);
		}
		$arr1["status"]=1;
		$arr1["time"]=time();
		 if($this->db->insert("user_files",$arr1)){
			 $mesg = "sucess";
		 }else{
			  $mesg = "error";
		 }
		// echo $this->db->last_query(); die;
		 //return
	}
	
	//create folder
	public function create_folder($arr){
		$sql = "SELECT max(sort) from user_folders";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		if($data[0]['max(sort)'] == NULL){
			$arr["sort"] = 0;
		}
		else{
			$arr["sort"] = ($data[0]['max(sort)']+1);
		}
		$arr["status"]=1;
		$arr["time"]=time();
		return $this->db->insert("user_folders",$arr);
	}
	//fetch reent files
	public function my_recent_files_from_db($arr){
		
		$this->db->select("*");	
		$this->db->from('user_files');
		$where=array("user_id" =>$arr["user_id"], "archive <> " => 1, 'status =' => 1);
		$this->db->where($where);
		$this->db->order_by('time DESC'); // Order by
   		$this->db->limit(20); // Limit, 20 entries		
		$query = $this->db->get();
	    //echo $this->db->last_query();
		$resultset=$query->result_array();
		return $resultset;	
	}
	
	//for my desk folder 
	public function my_desk_files_from_db($arr){
		$this->db->select("*");	
		$this->db->from('user_folders');
		$where = array("relation" =>$arr["user_id"], 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where);
		$this->db->order_by('time DESC'); // Order by
   		//$this->db->limit(20); // Limit, 20 entries		
		$query = $this->db->get();
	   // echo $this->db->last_query();
		$resultset=$query->result_array();
		return $resultset;	
	}
	//for my desk folder under list of  files
	public function my_desk_folder_under_files_from_db($arr){
		$this->db->select("*");	
		$this->db->from('user_files');
		$where = array("user_id" =>$arr["relation"], "sub_folder" =>$arr["name"], 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where);
		$this->db->order_by('time DESC'); // Order by
		$query = $this->db->get();
	   // echo $this->db->last_query();
		$resultset1=$query->result_array();
	//	print_r($resultset1);
		return $resultset1;	
	}
	//for my desk folder under list of  files
	public function my_desk_folder_under_files_from_db_wall($arr){
		$this->db->select("*");	
		$this->db->from('user_files');
		$where = array("user_id" =>$arr["relation"], "privacy" =>0, 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where);
		$this->db->order_by('time DESC'); // Order by
		$query = $this->db->get();
	   // echo $this->db->last_query();
		$resultset1=$query->result_array();
	//	print_r($resultset1);
		return $resultset1;	
	}
	
	//for my desk folder under list of  files
	public function my_uploaded_file_under_folder_from_db($arr){
		$this->db->select("*");	
		$this->db->from('user_files');
		$where1 = array("user_id" =>$arr["relation"], "sub_folder" =>$arr["name"], 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where1);
		$this->db->order_by('time DESC'); // Order by
		$query = $this->db->get();
	  //  echo $this->db->last_query(); die;
		$resultset=$query->result_array();
	//	print_r($resultset1);
		return $resultset;	
	}
	
/////////////////// my wall page ////////////
	//for my desk folder 
	public function my_wall_files_from_db($arr){
		$this->db->select("*");	
		$this->db->from('user_files');
		$where1 = array("user_id" =>$arr["user_id"], 'privacy' => '0', 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where1);
		$this->db->order_by('time DESC'); // Order by
   		//$this->db->limit(20); // Limit, 20 entries		
		$query = $this->db->get();
	  //echo $this->db->last_query();
		$resultset=$query->result_array();
		return $resultset;	
	}
	//for my desk folder under list of  files
	public function my_wall_folder_under_files_from_db($arr){
		$this->db->select("*");	
		$this->db->from('user_files');
		$where = array("user_id" =>$arr["relation"], "sub_folder" =>$arr["name"], 'privacy' => '0', 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where);
		$this->db->order_by('time DESC'); // Order by
		$query = $this->db->get();
	   // echo $this->db->last_query();
		$resultset1=$query->result_array();
	//	print_r($resultset1);
		return $resultset1;	
	}
		
	//for my desk upload file under folder
	public function upload_file_under_folder_db($arr1){
		$arr['user_id'] = $arr1['relation'];
		$arr['folder_name'] = $arr1['relation'];
		$arr['sub_folder'] = $arr1['name'];
		$arr['userfile'] = $arr1['userfile'];
		$arr['privacy'] = $arr1['privacy'];
		$ext_n = (explode('.', $arr1['userfile']));
		$arr['userfile']= $ext_n[0];
		$arr['ext'] = $ext_n[1];

		
		$this->db->select("*");	
		$sql = "SELECT max(sort) from user_files";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		if($data[0]['max(sort)'] == NULL){
			$arr["sort"] = 0;
		}
		else{
			$arr["sort"] = ($data[0]['max(sort)']+1);
		}
		$arr["status"]=1;
		$arr["time"]=time();
		return $this->db->insert("user_files",$arr);
	}
	
	//for copy file under folder
	
	public function copy_file_validation($arr1){
		$arr['user_id'] = $arr1['relation'];
		$arr['folder_name'] = $arr1['relation'];
		$arr['sub_folder'] = $arr1['name'];
		$arr['userfile'] = $arr1['copyfile'];
		//$this->db->select("*");	
		$sql = "SELECT * from user_files where user_id='".$arr['user_id']."' AND folder_name='".$arr['folder_name']."' AND sub_folder='".$arr['sub_folder']."' AND userfile='".$arr['userfile']."' AND archive=0";
		$query=$this->db->query($sql);
		$resultset=$query->result_array();
		return count($resultset);
		//$query=$this->db->get();
		//echo $this->db->last_query();
		//echo $data1 = $query->num_row(); 
		//return $data1;
	}
	public function copy_file_under_folder_db($arr1){
		$arr['user_id'] = $arr1['relation'];
		$arr['folder_name'] = $arr1['relation'];
		$arr['sub_folder'] = $arr1['name'];
		$arr['userfile'] = $arr1['copyfile'];
		$this->db->select("*");	
		$sql = "SELECT max(sort) from user_files";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		if($data[0]['max(sort)'] == NULL){
			$arr["sort"] = 0;
		}
		else{
			$arr["sort"] = ($data[0]['max(sort)']+1);
		}
		$arr["status"]=1;
		$arr["time"]=time();
		return $this->db->insert("user_files",$arr);
		//echo $this->db->last_query();
		//return $this->db->last_query();
	}
	
	//for my desk create file under folder
	public function create_file_under_folder_db($arr1){
		$arr['user_id'] = $arr1['relation'];
		$arr['folder_name'] = $arr1['relation'];
		$arr['sub_folder'] = $arr1['folder_name'];
		$arr['name'] = $arr1['name'];
		$arr['userfile'] = $arr1['userfile'];
		$file=explode(".",$arr["userfile"]);
		$arr["userfile"]=$file[0];
		$arr['privacy'] = $arr1['privacy'];
		$arr['ext'] = "doc";
		$this->db->select("*");	
		$sql = "SELECT max(sort) from user_files";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		if($data[0]['max(sort)'] == NULL){
			$arr["sort"] = 0;
		}
		else{
			$arr["sort"] = ($data[0]['max(sort)']+1);
		}
		$arr["status"]=1;
		$arr["time"]=time();
		return $this->db->insert("user_files",$arr);
	}
	
	//for my desk update file under folder
	public function update_file_under_folder_db($arr1){
		$arr['user_id'] = $arr1['relation'];
		$arr['folder_name'] = $arr1['relation'];
		$arr['sub_folder'] = $arr1['folder_name'];
		$arr['name'] = $arr1['friendly_name'];
		$data1['name'] = $arr1['friendly_name'];
		$data1['ext'] = "doc";
		$arr['userfile'] = $arr1['userfile'];
		$file=explode(".",$arr["userfile"]);
		$arr["userfile"]=$file[0];
		$data1['privacy'] = $arr1['privacy'];
		$data1["time"]=time();
		
		//$this->db->select("*");	
		$sql = "SELECT * from user_files where user_id='".$arr['user_id']."' AND sub_folder='".$arr['sub_folder']."' AND userfile='".$arr['userfile']."' ";
		$query = $this->db->query($sql);
		$data = $query->row_array();
		if($data == NULL || $data == '0' || $data == ''){
			$result = false;
		}
		else{
			$this->db->where(array("user_id"=>$arr['user_id'], "userfile"=>$arr['userfile'], "sub_folder"=>$arr['sub_folder']));
			$this->db->update("user_files",$data1);
		//	echo $this->db->last_query(); die;
			$result = true;
		}
		return $result;
	}
	//for my desk update file under folder
	public function get_file_data_db($arr1){
		$arr['user_id'] = $arr1['user_id'];
		$arr['folder_name'] = $arr1['user_id'];
		$arr['sub_folder'] = $arr1['sub_folder'];
		$arr['userfile'] = $arr1['userfile'];
		$data1['privacy'] = $arr1['privacy'];
		$this->db->select("*");	
		$this->db->from('user_files');
		$where = array("user_id" =>$arr['user_id'], 'sub_folder'=>$arr['sub_folder'], 'userfile' => $arr['userfile'],  'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		$resultset = $query->row_array();
		//print_r($resultset);
		return $resultset;	
	}
	

	//for list of folder created by user
	public function list_of_folders_created_by_user($id){
		$this->db->select("name");	
		$this->db->from('user_folders');
		$where = array("relation" =>$id,'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where);
		$this->db->order_by('time'); // Order by
		$query = $this->db->get();
	   // echo $this->db->last_query();
		$resultset1=$query->result_array();
	//	print_r($resultset1);
		return $resultset1;	
	}
	
	//for count total files uploaded by user
	public function no_of_files_uploaded_by_user($id){
		$this->db->select("count(*) as totalfile");	
		$this->db->from('user_files');
		$where = array("user_id" =>$id, 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where);
		//$this->db->order_by('time'); // Order by
		$query = $this->db->get();
	   //echo $this->db->count_all_results();
	    //$this->db->last_query();
		$resultset1=$query->row_array();
	   //$resultset=$query->num_row();
	    //print_r($resultset1);
		return $resultset1;	
	}
//for delete a folder
	public function delete_user_uploaded_folder($arr){
	 	//$a = base_url().'userdata/'.$arr["user_id"].'/'.$arr["folder_name"]; 
		$data["status"]=0;
		$data["archive"]=1;
		$id = $arr["file_id"];
		unset($arr["file_id"]);
		$this->db->where(array("name"=>$arr["folder_name"], "relation" => $arr["user_id"]));
		$result = $this->db->update("user_folders",$data);
		if($result){
			$data1["status"]=0;
			$data1["archive"]=1;
			$this->db->where(array("sub_folder"=>$arr['folder_name'], "user_id" => $arr["user_id"]));
			$this->db->update("user_files",$data1);

			//rmdir($a);
		}

		//echo $this->db->last_query();die;
		return $result;
		
	}
//for delete a file
	public function delete_user_uploaded_file_f($arr){
		$data["status"]=0;
		$data["archive"]=1;
		$id = $arr["file_id"];
		unset($arr["file_id"]);
		$this->db->where(array("sub_folder"=>$arr['folder_name'], "user_id" => $arr["user_id"]));
		$result = $this->db->update("user_files",$data);
		//echo $this->db->last_query();die;
		return $result;
	}
	
//for delete a file
	public function delete_user_uploaded_file($arr){
		$data["status"]=0;
		$data["archive"]=1;
		$id = $arr["file_id"];
		unset($arr["file_id"]);
		$this->db->where(array("id"=>$id, "user_id" => $arr["user_id"]));
		$result = $this->db->update("user_files",$data);
		//echo $this->db->last_query();die;
		return $result;
	}
	
	
//for my desk update file under folder
	public function file_data_db($arr1){
		$arr['user_id'] = $arr1['user_id'];
		$this->db->select("userfile");	
		$this->db->from('user_files');
		$where = array("user_id" =>$arr['user_id'], 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		$resultset = $query->result_array();
		//print_r($resultset);
		return $resultset;	
	}
	
//for search file
	public function getsearchpagedata($arr){
		$arr1 = mysql_real_escape_string($arr['search_txt']);
		$this->db->select("*");	
		$this->db->from('user_files');
		if(trim($arr1) == ''){
			$where =  "user_id = '".$arr['user_id']."' AND privacy='0'";
			//array("user_id" =>$arr['user_id'], "userfile like '%%'" =>$arr['user_id'], 'archive <> ' => 1, 'status =' => 1);
			$this->db->where($where);
			$this->db->order_by('time DESC'); // Order by
			$this->db->limit(20); // Limit, 20 entries		
		}
		else{
			//$where =  "user_id = '".$arr['user_id']."' AND userfile like '%".$arr1."%'"  AND "privacy = 0";
                        $where =  "privacy ='0' AND (userfile like '%".$arr1."%' OR name like '%".$arr1."%') " ;
			//array("user_id" =>$arr['user_id'], "userfile like '%%'" =>$arr['user_id'], 'archive <> ' => 1, 'status =' => 1);
			$this->db->where($where);
		}
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		$resultset = $query->result_array();
		 $data1 = ' ';
		  if(!empty($resultset)){
		  $data1 .= '<div class="pdf_name">
							<ul>
							  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
							  <li>File Name</li>
							  <li>Friendly Name</li>
							
							  <li>File Type</li>
							   <li>Created</li>
							  <div class="clear"></div>
							</ul>
						  </div><div class="clear"></div>';
			 foreach($resultset as $k => $v){ 
				$ext = explode('.', $v['userfile']); $ext1 = end($ext);// echo $ext1;
				$data1 .= '<div class="pdf_name">
							<ul>
							  <li><img width="40" src="'.base_url().'file_logo/'.file_logo($ext1).'"  /></li>
							  <li><a href="view_file_popup/'.$v['user_id'].'/'.$v['sub_folder'].'/'.$v['userfile'].'">'.$v['userfile'] .'</a></li>
							  <li><a href="view_file_popup/'.$v['user_id'].'/'.$v['sub_folder'].'/'.$v['name'].'">'.$v['name'] .'</a></li>
							  <li>.'.$ext1.'</li>
							  <li>'.date("d-m-Y | h:i A", $v['time']) .'</li>
							  <div class="clear"></div>
							</ul>
						  </div><div class="clear"></div>';
			   } 
		  }else{
				$data1 .= '<div class="pdf_name">
							<ul>
							  <li style="width:100%">No result found...</li>
							  <div class="clear"></div>
							</ul>
						  </div><div class="clear"></div>';
		  }
		
		//print_r($resultset);
		$message[0] = 'success';
		$message[1] = $data1;
		//$message[5] = $data2;
	
		$message_arr = implode('|::|', $message);
		echo $message_arr;
			
		return $message_arr;	
	}
	
//for search friend
	public function get_search_user_data($arr){
		$arr1 = mysql_real_escape_string($arr['search_txt']);
		$user_id = $this->session->userdata("user_id");
		$this->db->select("*");	
		$this->db->from('users');
		$where =  "status = '1' AND archive='0' AND (first_name like '%".$arr1."%' OR last_name like '%".$arr1."%' OR email like '%".$arr1."%'  OR education like '%".$arr1."%')";
		//array("user_id" =>$arr['user_id'], "userfile like '%%'" =>$arr['user_id'], 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		$resultset = $query->result_array();
		 $data1 = ' ';
		  if(!empty($resultset)){
			 foreach($resultset as $k => $v){ 
				if($v['image'] <> ''){$image = base_url().'blogimages/'.$v['image']; }else{ $image = base_url().'images/my-profile image3.png';}
				if($v['first_name'] <> ''){$name = ucwords($v['first_name']).' '.ucwords($v['last_name']); }else{ $name = $v['email'];}
				$data1 .= '<div class="my_friends_srch">
							  <div class="mf_left" style="width:8%"> <img src="'.$image.'" width="50" /> </div>
							  <div class="mf_right">
								<div class="mf_hdg_1"><span class="sky">'.$name.'</span></div><br class="clear">
								<div class="mf_hdg_2" id="dev_send_req_'.$v['id'].'"><a href="'.base_url().'user/user_wall/'.$v['id'].'" class="reqest_sent" >View Profile</a></div>
							 </div>
							  <div class="clear"></div>
							</div>';
			   } 
		  }else{
				$data1 .= '<div class="pdf_name">
							<ul>
							  <li style="width:100%">No result found...</li>
							  <div class="clear"></div>
							</ul>
						  </div><div class="clear"></div>';
		  }
		
		//print_r($resultset);
		$message[0] = 'success';
		$message[1] = $data1;
		//$message[5] = $data2;
	
		$message_arr = implode('|::|', $message);
		echo $message_arr;
			
		return $message_arr;	
	}
	
	//LIst of All users
	
	public function get_all_user_data(){
		$this->db->select("first_name,last_name,id,education");	
		$this->db->from('users');
		$query = $this->db->get();
		$resultset = $query->result_array();
		return $resultset;
		}

		
	public function get_all_user_data_limit($limit){
		$this->db->select("first_name,last_name,id,education");	
		$this->db->from('users');
		$this->db->limit($limit, 0);
		$query = $this->db->get();
		
		$resultset = $query->result_array();
		return $resultset;
		}
//add friend
	public function add_friend($arr){
		$sql = "SELECT max(sort) from users";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		if($data[0]['max(sort)'] == NULL){
			$arr["sort"] = 0;
		}
		else{
			$arr["sort"] = ($data[0]['max(sort)']+1);
		}
		$arr["status"]=0;
		$arr["time"]=time();
		return $this->db->insert("users",$arr);
	}
	
//for send friend request
	public function send_friend_request($arr){
		$arr['time'] = time();
		$where = array("reciver_id" => $arr['reciver_id'], "sender_id" => $arr["sender_id"],  "status" => '0', "archive" => '0' );
		//for chek friend request already sent or not
		$num_req = $this->common->chk_request('friend_requests', $where); 
		if($num_req == 0){
			$result = $this->db->insert("friend_requests",$arr);
			if($result){
				$arr1['reciver_id'] = $arr['reciver_id'];
				$arr1['sender_id'] = $arr['sender_id'];
				$arr1['content'] = 'sent you a friend request';
				$arr1['type'] =  '1';
				$arr1['status'] =  '0';
				$arr1['time'] = time();
				$this->db->insert("notifications",$arr1);

				$data1 = '<input type="button" value="Request Sent" class="reqest_sent" name="Add Friend" disabled="disabled" >';
			}else{
				$data1 = '<input type="button" value="Add Friend" class="ok" name="Add Friend" onclick="add_friend(\''. $arr['reciver_id'].'\')">';
			}
		}
		$message[0] = 'success';
		$message[1] = $data1;
		//$message[5] = $data2;
	
		$message_arr = implode('|::|', $message);
		echo $message_arr;
			
		return $message_arr;	

	}
	
//for total no of  notifications
	public function total_no_of_notifi($arr){
		$this->db->select("count(*) as totalnoti");	
		$this->db->from('notifications');
		$where = array("reciver_id" =>$arr['reciver_id'], 'archive <> ' => 1, 'status =' => 0);
		$this->db->where($where);
		$query = $this->db->get();
		//echo $this->db->last_query();
		$resultset1=$query->row_array();
		
		if($resultset1['totalnoti'] == 0){
			$data = '0';//'<img src="'.base_url().'images/create-folder image8.png" />';
			$data2 = 'No activity found'; 
		}else{
			$data = $resultset1['totalnoti'];//'<img onclick="notification_menu()" src="'.base_url().'images/notification.png" /><div class="t_notifi_dev"></div>';
		}
		$message[0] = 'success';
		$message[1] = $data;
		$message[2] = $data2;
	
		$message_arr = implode('|::|', $message);
		echo $message_arr;
			
		//return $message_arr;	
	}
	
//for notifications menu
	public function notification_menu_db($arr){
		$this->db->select("*");	
		$this->db->from('notifications');
		$where = array("reciver_id" =>$arr['reciver_id'], 'archive <> ' => 1, 'status =' => 0);
		$this->db->where($where);
		$this->db->order_by('time DESC'); // Order by
		$this->db->limit(6); // Limit, 20 entries		
		$query = $this->db->get();
		//echo $this->db->last_query();
		$resultset1=$query->result_array(); //print_r($resultset1);
		$data = '';
		if(!empty($resultset1)){
			foreach($resultset1 as $k => $v){
			  $userdata = $this->user_model->get_user_data($v['sender_id']);
			  if($userdata['image'] <> ''){$image = base_url().'blogimages/'.$userdata['image']; }else{ $image = base_url().'images/my-profile image3.png';}
			  if($userdata['first_name'] <> ''){$name = ucwords($userdata['first_name']).' '.ucwords($userdata['last_name']); }else{ $name = $userdata['email'];}
			if($v['type'] == 1){
				$data .= '<div class="noti_full_part" onclick="update_noti_status()>
                        <div class="noti_img"><a href="'.base_url().'user/user_wall/'.$v['sender_id'].'"> <img width="55px" src="'.$image .'" /></a> </div>
                        <div class="noti_cnt">
                          <div class="noti_ttl"><a href="'.base_url().'user/user_wall/'.$v['sender_id'].'">  '.$name.'</a>  </div>
                          <div class="confirm">
                            <input type="button" class="confirm" value="Confirm"  onclick="confirm_frnd_req(\''.$v['sender_id'].'\', 0)">
                          </div>
                          <div class="ignor">
                            <input type="button" class="ignore" value="Ignore"  onclick="confirm_frnd_req(\''.$v['sender_id'].'\', 1)">
                          </div>
                        </div>
                        <div class="clear"></div>
                      </div>';
			}
			else if($v['type'] == 2){
				$data .= '<a href="'.base_url().'user/user_wall/'.$v['sender_id'].'" onclick="update_noti_status(\''.$v['id'].'\')">
                      <div class="noti_full_part">
                        <div class="noti_img"> <img  width="55px" src="'.$image .'" /> </div>
                        <div class="noti_cnt">
                          <div class="noti_ttl"> '.$name.' </div>
                          <div class="noti_sub_ttl"> '.$v['content'].' </div>
                          <div class="noti_date"> '.date("d-M-y, h:i A", $v['time']).' </div>
                        </div>
                        <div class="clear"></div>
                      </div>
                      </a>';
			}
			else if($v['type'] == 3){
				$this->db->select("*");	
				$this->db->from('message');
				$wheres = array("reciver_id" =>$v['reciver_id'], "sender_id" =>$v['sender_id'], 'archive <> ' => 1, 'status =' => 0);
				$this->db->where($wheres);
				$this->db->order_by('time DESC'); // Order by
				$this->db->limit(1); // Limit, 20 entries		
				$query1 = $this->db->get();
				//echo $this->db->last_query();
				$resultset2=$query1->row_array(); //print_r($resultset1);
				$data .= '<a href="'.base_url().'user/my_messages/'.$resultset2['conversation_id'].'/'.$v['sender_id'].'">
                      <div class="noti_full_part">
                        <div class="noti_img"> <img width="55px" src="'.$image .'"/> </div>
                        <div class="noti_cnt">
                          <div class="noti_ttl">  '.$name.'  </div>
                          <div class="noti_sub_ttl"> '.$v['content'].' </div>
                          <div class="noti_date">  '.date("d-M-y, h:i A", $v['time']).'</div>
                        </div>
                        <div class="clear"></div>
                      </div>
                      </a>';
			}
		}
			$data .= ' <div class="lower_strip"><a href="'.base_url().'user/notification"> View All</a> </div>';

		}else{
			//$data = '<img src="'.base_url().'images/create-folder image8.png" />';
			$data .= '<div class="noti_full_part">
                        <div class="noti_cnt">
                          <div class="noti_ttl"> No activity found </div>
                        </div>
                        <div class="clear"></div>
                      </div>'; 
		}
		$message[0] = 'success';
		$message[1] = $data;
		$message[2] = $data2;
	
		$message_arr = implode('|::|', $message);
		echo $message_arr;
			
		return $message_arr;	
	}
	
//for notifications page
	public function notifications_page_db($arr){
		$this->db->select("*");	
		$this->db->from('notifications');
		$where = array("reciver_id" =>$arr['reciver_id'], 'archive <> ' => 1, 'status =' => 0);
		$this->db->where($where);
		$this->db->order_by('time DESC'); // Order by
		//$this->db->limit(6); // Limit, 20 entries		
		$query = $this->db->get();
		//echo $this->db->last_query();
		$resultset=$query->result_array(); //print_r($resultset1);
		return $resultset;	
	}
	
//for notifications status updation
	public function update_noti_status_db($id){
		$arr1['status'] = '1';
		$where = array("id" =>$id);
		$this->db->where($where);
		$resultset = $this->db->update("notifications",$arr1);
//	echo $this->db->last_query();
		return $resultset;	
	}

	
//for confirm friend request
	public function confirm_frnd_req_db($arr){
		$where = array("reciver_id" => $arr['reciver_id'], "sender_id" => $arr["sender_id"],  "status" => '0', "archive" => '0' );
		//for chek friend request already sent or not
		$num_req = $this->common->chk_request('friend_requests', $where); 
		
		//chk for already friend
		$where1 = array("friend_id" => $arr['reciver_id'], "user_id" => $arr["sender_id"],  "status" => '1', "archive" => '0' );
		$num_req1 = $this->common->chk_request('friends', $where1); 
		if($num_req1 == 0){
			if($num_req == 1){
				//for friend table entry
				$arr1['user_id'] =  $arr['sender_id'];
				$arr1['friend_id'] =  $arr['reciver_id'];
				$arr1['status'] =  '1';
				$arr1['time'] =  time();
				$result = $this->db->insert("friends",$arr1);
				if($result){
				//update friend request table
				$updata_f['status'] = 1;
				$updata_f['archive'] = 1;
				$this->db->where(array("reciver_id" => $arr['reciver_id'], "sender_id" => $arr["sender_id"]));
				$this->db->update("friend_requests",$updata_f);
					
				//update notifications status
				$updata['status'] = 1;
				$this->db->where(array("reciver_id" => $arr["reciver_id"]));
				$this->db->update("notifications",$updata);
				//echo $this->db->last_query();
					
					$arr2['reciver_id'] = $arr['sender_id'];
					$arr2['sender_id'] = $arr['reciver_id'];
					$arr2['content'] = ' has accepted your friend request';
					$arr2['type'] =  '2';
					$arr2['time'] = time();
					$this->db->insert("notifications",$arr2);
					$data1 = '<input type="button" value="Friend"  class="reqest_sent" disabled="disabled">';
				}
			}
		}
		//print_r($resultset);
		$message[0] = 'success';
		$message[1] = $data1;
		//$message[5] = $data2;
	
		$message_arr = implode('|::|', $message);
		//echo $message_arr;
			
		return $message_arr;	
		
	}
//for Ignore friend request
	public function ignore_frnd_req_db($arr){
		$where = array("reciver_id" => $arr['reciver_id'], "sender_id" => $arr["sender_id"],  "status" => '0', "archive" => '0' );
		//for chek friend request already sent or not
		$num_req = $this->common->chk_request('friend_requests', $where); 
		
		//chk for already friend
		$where1 = array("friend_id" => $arr['reciver_id'], "user_id" => $arr["sender_id"],  "status" => '1', "archive" => '0' );
		$num_req1 = $this->common->chk_request('friends', $where1); 
		//print_r($num_req1);
		if($num_req1 == 0){
			if($num_req == 1){
				//update friend request table
				$updata_f['status'] = 2;
				$updata_f['archive'] = 1;
				$this->db->where(array("reciver_id" => $arr['reciver_id'], "sender_id" => $arr["sender_id"]));
				$this->db->update("friend_requests",$updata_f);
				//update notifications status
				$updata['status'] = 1;
				$this->db->where(array("reciver_id" => $arr["reciver_id"]));
				$this->db->update("notifications",$updata);
					$data1 = '<input type="button" value="Declined" class="reqest_sent" disabled="disabled">';
				}
			}
		//print_r($resultset);
		$message[0] = 'success';
		$message[1] = $data1;
		//$message[5] = $data2;
	
		$message_arr = implode('|::|', $message);
	//	echo $message_arr;
			
		return $message_arr;	
		
	}
	//for list of friends
	public function my_friend_list($arr){
		$this->db->select("*")->from("friends")->where("(user_id='".$arr['user_id']."'  OR  friend_id='".$arr['user_id']."') AND status='1' AND blocked='0'");
		//$this->db->query("select count(*) from friends where (user_id='".$arr['user_id']."' AND friend_id='".$arr['friend_id']."') OR (user_id='".$arr['friend_id']."' AND friend_id='".$arr['user_id']."') AND status='1' AND blocked='0'");
		$result=$this->db->get();
		//echo $this->db->last_query();
		$resultset = $result->result_array();
		return $resultset;
		
	}
	//for send a message
	public function send_message_to_db($arr){ 
		$this->db->select("conversation_id")->from("message")->where("((sender_id='".$arr['sender_id']."' AND reciver_id='".$arr['reciver_id']."') OR (reciver_id='".$arr['sender_id']."' AND sender_id='".$arr['reciver_id']."'))  order by time DESC limit 0,1");
		$result=$this->db->get();
		//echo $this->db->last_query(); 
		$resultset = $result->row_array();
		if($resultset['conversation_id'] <> ''){
			$arr['conversation_id'] = $resultset['conversation_id'];
		}
		else{
			$arr['conversation_id'] = time().$this->common->generate_transaction_number();
		}
		$arr['time'] = time();
		$result = $this->db->insert("message",$arr);
		if($result){
		  $arr2['sender_id'] = $arr['sender_id'];
		  $arr2['reciver_id'] = $arr['reciver_id'];
		  $arr2['content'] = ' sent you a message ';
		  $arr2['type'] =  '3';
		  $arr2['status'] =  '0';
		  $arr2['time'] = time();
		  $this->db->insert("notifications",$arr2);
		}
		return $result;
		
	}

//for inbox
	public function my_inbox($user_id){
		$sql = "select * from message where (sender_id='".$user_id."'  OR  reciver_id='".$user_id."') AND archive='0' group by conversation_id order by id DESC  ";
				

		//$this->db->select("*")->from("message")->where("(sender_id='".$user_id."'  OR  reciver_id='".$user_id."') AND archive='0' group by conversation_id order by time DESC limit 0,1");
		//$this->db->where($where);
		//$this->db->order_by("name asc");
		$query = $this->db->query($sql);
	//	$query = $this->db->get();
	 //  echo $this->db->last_query();die;
		$resultset=$query->result_array();
		return $resultset;	
	}
	
//for inbox
	public function latest_message($id){
		$sql = "select * from message where conversation_id='".$id."' AND archive='0' order by time DESC limit 0,1";
		$query = $this->db->query($sql);
	//	$query = $this->db->get();
	   //echo $this->db->last_query();die;
		$resultse_l=$query->row_array();
		//print_r($resultse_l); die;
		return $resultse_l;	
	}
	
	
//for my conversation
	public function my_conversation($conversation){
		$this->db->select("*")->from("message")->where("(conversation_id='".$conversation."') AND archive='0' order by time DESC");
		//$this->db->where($where);
		//$this->db->order_by("name asc");
		$query = $this->db->get();
	//  echo $this->db->last_query();die;
		$resultset=$query->result_array();
		return $resultset;	
	}
	
	
	
//for conversation in details
	public function my_conversation_data($arr){  
		$this->db->select("*");	
		$this->db->from('message');
		$where =  "conversation_id = '".$arr['conversation_id']."' AND archive='0'";
		//array("user_id" =>$arr['user_id'], "userfile like '%%'" =>$arr['user_id'], 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where);
		$this->db->order_by('time DESC'); // Order by
		//$this->db->limit(10); // Limit, 20 entries		
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		$resultset = $query->result_array();
		 $data1 = ' ';
		  if(!empty($resultset)){ 
			 foreach($resultset as $k => $v){
				 if($v['sender_id'] == $arr["user_id"]){
					 $reply_img =  '<img height="11" src="'.base_url().'images/reply.png" >';
				 }else{
					  $reply_img = '';
				 }
				 
				//update message status
				//$updata['status'] = 1;
				//$this->db->where(array("reciver_id" => $v["reciver_id"], 'conversation_id' => $v["conversation_id"]));
				/*$this->db->where("conversation_id = '".$arr["user_id"]."' AND sender_id != '".$arr["user_id"]."' AND conversation_id='".$v["conversation_id"]."'");
				$result = $this->db->update("message",$updata);*/
				//update notifications status
				$updata['status'] = 1;
				//$this->db->where(array("reciver_id" => $v["reciver_id"]));
				$this->db->where("reciver_id = '".$arr["user_id"]."' AND sender_id != '".$arr["user_id"]."' AND type='3'");
				$result1 = $this->db->update("notifications",$updata);
				//echo $this->db->last_query(); die;

				$this->db->select("*");	
				$this->db->from('users');
				$where1 =  "id = '".$v['sender_id']."' ";
				//array("user_id" =>$arr['user_id'], "userfile like '%%'" =>$arr['user_id'], 'archive <> ' => 1, 'status =' => 1);
				$this->db->where($where1);
				$query1 = $this->db->get();
				$sender = $query1->row_array();
				$sent_on = $this->common->convert_time_days($v['time']);
					if($sender['image'] <> ''){
						$profile_pic = base_url().'blogimages/'.$sender['image'];
					}else{
						$profile_pic = base_url().'images/my-profile image3.png';
					}
					if($sender['first_name'] <> ''){
						$uname = ucwords($sender['first_name']).' '.ucwords($sender['last_name']);
					}
					else{
						$uname = $sender['email'];
					}
				//	print_r($sender);
				$data1 .= '<div class="message_wrap">
						   <div class="mf_left"><a href="'.base_url().'user/user_wall/'.$sender['id'].'" > <img src="'.$profile_pic.'" width="90" /></a> </div>
						  <div class="msg_hdg">
							<div class="msg_name">'.$uname.'</div>
							<div class="msg_date"><a href="'.base_url().'user/delete_a_message/'.$v['id'].'/'.$v['conversation_id'].'/'.$v['reciver_id'].'"><img src="'.base_url().'images/delete.png" /></a> &nbsp;&nbsp;'.$sent_on.' </div>
							<div class="msg_pgrph"> '.$reply_img.' '.nl2br($v['message']).' </div>
							<div class="clear"></div>
							
						  </div>
						</div>';
			   } 
		  }else{
			  $data1 .= '<div class="message_wrap">
						   <div class="msg_hdg">
							<div class="msg_name">No activity found..</div>
							<div class="clear"></div>
						  </div>
					  </div>';
		  }


		
		//print_r($resultset);
	//echo $data1;
		$message[0] = 'success';
		$message[1] = $data1;
		//$message[5] = $data2;
	
		$message_arr = implode('|::|', $message);
		echo $message_arr;
			
		return $message_arr;	
	}
	
//for reply message
	public function my_reply_data($arr){
		$this->db->select("conversation_id")->from("message")->where("(conversation_id='".$arr['conversation_id']."') order by time DESC limit 0,1");
		$result=$this->db->get();
		//echo $this->db->last_query(); 
		$resultset = $result->row_array();
		if($resultset['conversation_id'] <> ''){
			$arr['conversation_id'] = $resultset['conversation_id'];
		}
		else{
			$arr['conversation_id'] = time().$this->common->generate_transaction_number();
		}
		$arr['time'] = time();
		$arr['status'] = 0;
		$arr['message'] = urldecode($arr['message']);
		$result = $this->db->insert("message",$arr);
		if($result){
		  $arr2['sender_id'] = $arr['sender_id'];
		  $arr2['reciver_id'] = $arr['reciver_id'];
		  $arr2['content'] = ' sent a message ';
		  $arr2['type'] =  '3';
		  $arr2['status'] =  '0';
		  $arr2['time'] = time();
		  $this->db->insert("notifications",$arr2);
		// echo $this->db->last_query(); die;
		}
	
		
			
		return $result;	
		
	}
	
//for delete a message
	public function delete_user_message($arr){ 
		$data["status"]=0;
		$data["archive"]=1;
		$id = $arr["meesage"];
		unset($arr["meesage"]);
		$this->db->where(array("id"=>$id, "conversation_id" => $arr["conversation_id"]));
		
		$result = $this->db->update("message",$data);
		//echo $this->db->last_query();die;
		return $result;
		
	}
	
	//for my conv
// handle messages
 public function my_conversation_data_d($arr){ //echo "dsfsdfhsdj";
	$timestamp = $arr['timestamp'];
	//$timestamp_m = $arr['timestamp_m'];
	$page = $arr['page'];
	$perpage = 20;
	$offset = $page * $perpage;
	if($timestamp == '' || $timestamp == 'undefined'){$timestamp = 0;}
	$this->db->select("count(*) as tnum ");
	$this->db->from('message');
	$where2 =  "conversation_id = '".$arr['conversation_id']."' AND archive='0' AND time > '".$timestamp."' ";
	//array("user_id" =>$arr['user_id'], "userfile like '%%'" =>$arr['user_id'], 'archive <> ' => 1, 'status =' => 1);
	$this->db->where($where2);
	$this->db->order_by('time DESC '); // Order by
	//$this->db->limit(10); // Limit, 20 entries		
	$query = $this->db->get(); 
    //echo $this->db->last_query(); die;
	$resultset2 = $query->row_array();
	$num = $resultset2['tnum'];
	 
	$tabl = "message";
	$this->db->select("*");	
	$this->db->from('message');
	$where = array('conversation_id' => $arr['conversation_id'], 'time > ' => $timestamp, 'archive'=>0 );
	$this->db->where($where);
	$this->db->order_by("time DESC");
	$this->db->limit($perpage, $offset);
	$query = $this->db->get();
	//echo $this->db->last_query();
	$resultset=$query->result_array();
	 if(!empty($resultset)){ 
		foreach($resultset as $k => $fetch_msg){// print_r($fetch_msg);
		$attached = ''; 
		$this->db->select("*");	
		$this->db->from('users');
		$where1 =  "id = '".$fetch_msg['sender_id']."' ";
		//array("user_id" =>$arr['user_id'], "userfile like '%%'" =>$arr['user_id'], 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where1);
		$query1 = $this->db->get();
		$sender = $query1->row_array();
		$sent_on = $this->common->convert_time_days($fetch_msg['time']);
			if($sender['image'] <> ''){
				$profile_pic = base_url().'blogimages/'.$sender['image'];
			}else{
				$profile_pic = base_url().'images/my-profile image3.png';
			}
			if($sender['first_name'] <> ''){
				$uname = ucwords($sender['first_name']).' '.ucwords($sender['last_name']);
			}
			else{
				$uname = $sender['email'];
			}
		//get 1st time stamp for pagination purposes
		if($i == 0){ $timestamp_1st = $fetch_msg[time]; } 
		if(empty($fetch_msg['image'])){ $attached =  NULL; } else{ $attached = '<div class="msg_date"> <a target="_blank" href="'.base_url().'user/download_sent_file/'.$fetch_msg['image'].'"><img src="'.base_url().'images/download.png" /> </a></div>';}
		//echo $attached; die;

			$message[1] .=  '<li class="post_li" id="'.$fetch_msg[time].'">
								<div class="message_wrap">
								   <div class="mf_left" style="width:9%"><a href="'.base_url().'user/user_wall/'.$sender['id'].'" > <img src="'.$profile_pic.'" width="50" /></a> </div>
						 		   <div class="msg_hdg">
									<div class="msg_name">'.$uname.'</div>
									<div class="msg_date"><a href="'.base_url().'user/delete_a_message/'.$fetch_msg['id'].'/'.$fetch_msg['conversation_id'].'/'.$fetch_msg['reciver_id'].'"><img src="'.base_url().'images/delete.png" /></a> &nbsp;&nbsp;'.$sent_on.' </div>
									'.$attached.'
									<div class="msg_pgrph"> '.$reply_img.' '.nl2br($fetch_msg['message']).' </div>
									<div class="clear"></div>
								  </div>
								</div>
								<div class="clear"></div>
							</li>';
				$i++;
	}
	 }else{
		$message[1] .=  '<div class="message_wrap">
					 <div class="msg_hdg">
					  <div class="msg_name">No activity found..</div>
					  <div class="clear"></div>
					</div>
				</div>';
	 }


	if($num <= $offset+$perpage){
		$message[2] = 'r';
	}
	else{
		$message[2] = 'g';
	}
	$message[0] = 'Success';
	$updata['status'] = 1;
	//$this->db->where(array("reciver_id" => $v["reciver_id"]));
	$this->db->where("reciver_id = '".$arr["user_id"]."' AND sender_id != '".$arr["user_id"]."' AND type='3'");
	$result1 = $this->db->update("notifications",$updata);
	//echo $this->db->last_query(); die;

	
	ksort($message);

	$message_arr = implode('|::|', $message);
	echo $message_arr;
}

// MORE messages
	 public function my_conversation_data_more_d($arr){
		$tabl = "message";
		$timestamp = $arr['timestamp'];
		$conversation = mysql_real_escape_string($arr['conversation']);
	
		$page = $arr['page'];
		$perpage = 10;
		$offset = $page * $perpage;
		
		if($timestamp == ''){$timestamp = 0;}
		$this->db->select("count(*) as num ");
		$this->db->from('message');
		$where =  "conversation_id = '".$arr['conversation_id']."' AND archive='0' AND time < '$timestamp' ";
		//array("user_id" =>$arr['user_id'], "userfile like '%%'" =>$arr['user_id'], 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where);
		$this->db->order_by('time DESC'); // Order by
		//$this->db->limit(10); // Limit, 20 entries		
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		$resultset = $query->row_array();
		$num = $resultset['num'];
		
		$this->db->select("*");	
		$this->db->from('message');
		$where1 = array('conversation_id' => $arr['conversation_id'], 'time <' => $timestamp, 'archive'=>0 );
		$this->db->where($where1);
		//$this->db->order_by("time DESC");
		$this->db->limit($perpage, $offset);
		$query = $this->db->get();
		//echo $this->db->last_query();
		 $resultset1=$query->result_array();// print_r($resultset1);
		 $message[1] = '';
		 $message[2] = '';
		
		foreach($resultset1 as $k => $fetch_msg){ //print_r($fetch_msg); die;
			$this->db->select("*");	
			$this->db->from('users');
			$where1 =  "id = '".$fetch_msg['sender_id']."' ";
			//array("user_id" =>$arr['user_id'], "userfile like '%%'" =>$arr['user_id'], 'archive <> ' => 1, 'status =' => 1);
			$this->db->where($where1);
			$query1 = $this->db->get();
			$sender = $query1->row_array();
			$sent_on = $this->common->convert_time_days($fetch_msg['time']);
				if($sender['image'] <> ''){
					$profile_pic = base_url().'blogimages/'.$sender['image'];
				}else{
					$profile_pic = base_url().'images/my-profile image3.png';
				}
				if($sender['first_name'] <> ''){
					$uname = ucwords($sender['first_name']).' '.ucwords($sender['last_name']);
				}
				else{
					$uname = $sender['email'];
				}
	
			//get 1st time stamp for pagination purposes
			if($i == 0){$timestamp_1st = $fetch_msg[time];} 
					if(empty($fetch_msg['image'])){ $attached =  NULL; } else{ $attached = '<div class="msg_date"> <a target="_blank" href="'.base_url().'user/download_sent_file/'.$fetch_msg['image'].'"><img src="'.base_url().'images/download.png" /> </a></div>';}
				$message[1] .= '<li class="post_li" id="'.$fetch_msg[time].'">
									<div class="message_wrap">
									   <div class="mf_left" style="width:9%"><a href="'.base_url().'user/user_wall/'.$sender['id'].'" > <img src="'.$profile_pic.'" width="50" /></a> </div>
									   <div class="msg_hdg">
										<div class="msg_name">'.$uname.'</div>
										<div class="msg_date"><a href="'.base_url().'user/delete_a_message/'.$fetch_msg['id'].'/'.$fetch_msg['conversation_id'].'/'.$fetch_msg['reciver_id'].'"><img src="'.base_url().'images/delete.png" /></a> &nbsp;&nbsp;'.$sent_on.' </div>'.$attached.'
										<div class="msg_pgrph"> '.$reply_img.' '.nl2br($fetch_msg['message']).' </div>
										<div class="clear"></div>
									  </div>
									</div>
									<div class="clear"></div>
								</li>';
					$i++;
		}
		if($num <= $perpage){
			$message[2] = 'r';
		}
		else{
			$message[2] = 'g';
		}
		$message[0] = 'Success';
		ksort($message);
		$message_arr = implode('|::|', $message);
		echo $message_arr;
	}
	
	
//for get old folder name
	public function old_foldername($arr){
		$this->db->select("name");	
		$this->db->from('user_folders');
		$where1 =  "id = '".$arr['id']."' AND relation='".$arr['user_id']."' AND archive<>1" ;
		//array("user_id" =>$arr['user_id'], "userfile like '%%'" =>$arr['user_id'], 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where1);
		$query1 = $this->db->get();
		$resultset = $query1->row_array();
		return $resultset['name'];
	}
//for check name already exists
	public function rename_folder_chk_db($arr){
		$this->db->select('id');
		$this->db->from('user_folders');
		$this->db->where(array('name'=>$arr['new_name'], 'relation'=>$arr['user_id'], "archive <>"=>"1"));
		$query=$this->db->get();
	//echo $this->db->last_query();
		echo $resultset=$query->num_rows();
		
		/*if($resultset==0){
			return true;
		}
		else{
			return false;	
		}*/
		return 	$resultset;		
	}

//for update foldername in database
	public function rename_folder_db($arr){
		$arr1['name'] = urldecode($arr['new_name']);
		//$where1 = " id='".$arr["id"]."' AND relation='".$arr["user_id"]."'";
		$where1=array('id'=>$arr["id"],'relation'=>$arr["user_id"]);
		$this->db->where($where1);
		$result = $this->db->update("user_folders",$arr1);
		//echo $this->db->last_query();	
		if($result){
			return true;	
		}else{
			return false;
		}
	}
	
//for get old file name
	public function old_filename($arr){
		$this->db->select("userfile");	
		$this->db->from('user_files');
		$where1 = $this->db->where(array('user_id'=>$arr['user_id'], 'sub_folder'=>$arr['folder_name'], 'id'=>$arr['id'], "archive <>"=>"1"));

	//	$where1 =  "id = '".$arr['id']."' AND relation='".$arr['user_id']."' AND archive<>1" ;
		//array("user_id" =>$arr['user_id'], "userfile like '%%'" =>$arr['user_id'], 'archive <> ' => 1, 'status =' => 1);
		$this->db->where($where1);
		//$this->db->last_query();
		$query1 = $this->db->get();
		$resultset = $query1->row_array();
		return $resultset['userfile'];
	}
//for update foldername in database
	public function rename_file_db($arr){
		$arr1['userfile'] = urldecode($arr['new_name']);
		//$where1 = " id='".$arr["id"]."' AND relation='".$arr["user_id"]."'";
		$where1=array('id'=>$arr["id"],'user_id'=>$arr["user_id"], 'sub_folder'=>$arr['folder_name']);
		$this->db->where($where1);
		$result = $this->db->update("user_files",$arr1);
		//echo $this->db->last_query();	
		if($result){
			return true;	
		}else{
			return false;
		}
	}
	
	//create folder
	public function event_calendar($arr){
		
		return $this->db->insert("oc_calendar",$arr);
	}
	

	public function event_list($arr){
		$this->db->select("*");
		$this->db->from('oc_calendar');
		$this->db->where(array('username'=>$arr['user_id'],'start_date'=>$arr['date'].' 00:01:00'));
		$query1 = $this->db->get();
		$resultset = $query1->result_array();
		foreach($resultset as $row) {
		echo'<li id="'.$row['id'].'"><p>'.$row['event_task'].'<i class="deletes-icon"><a href="#" id="event_del"  onclick="event_del('.$row['id'].')">x</a></i></p> </li>';
		}
	}
	public function event_del($arr){
	$this->db->where('id', $arr['id']);
	$this->db->delete('oc_calendar'); 
	echo 1;
	}
	
	public function user_limit(){
		$this->db->select("value");	
		$this->db->from('users_files_limit');
		$query1 = $this->db->get();
		$resultset = $query1->row_array();
	return $resultset['value'];
	}
	
	function read_file_docx($input_file){
 
	$kv_strip_texts = ''; 
	$kv_texts = ''; 
	//if(!$input_file || !file_exists($input_file)) return false;
	//echo $input_file =  $_SERVER['DOCUMENT_ROOT']."/userdata/161/my files/1415711425.docx";
	//$input_file = $_SERVER['DOCUMENT_ROOT']."/user/file_pop/161/testing/1415712335.docx";
	$zip = new ZipArchive;

	 $zip = zip_open($input_file);
		
//	if (!$zip || is_numeric($zip)) return false;
	
	
	while ($zip_entry = zip_read($zip)) {
			
		if (zip_entry_open($zip, $zip_entry) == FALSE) continue;
			
		if (zip_entry_name($zip_entry) != "word/document.xml") continue;
 
		$kv_texts .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
			
		zip_entry_close($zip_entry);
	}
	
	zip_close($zip);
		
 
	$kv_texts = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $kv_texts);
	$kv_texts = str_replace('</w:r></w:p>', "\r\n", $kv_texts);
	$kv_strip_texts = strip_tags($kv_texts);
 
	return $kv_strip_texts;


}


function read_file_doc($filename){
if ( ($fh = fopen($filename, 'r')) !== false ) {

$headers = fread($fh, 0xA00);

# 1 = (ord(n)*1) ; Document has from 0 to 255 characters
$n1 = ( ord($headers[0x21C]) - 1 );

# 1 = ((ord(n)-8)*256) ; Document has from 256 to 63743 characters
$n2 = ( ( ord($headers[0x21D]) - 8 ) * 256 );

# 1 = ((ord(n)*256)*256) ; Document has from 63744 to 16775423 characters
$n3 = ( ( ord($headers[0x21E]) * 256 ) * 256 );

# (((ord(n)*256)*256)*256) ; Document has from 16775424 to 4294965504 characters
$n4 = ( ( ( ord($headers[0x21F]) * 256 ) * 256 ) * 256 );

# Total length of text in the document
$textLength = ($n1 + $n2 + $n3 + $n4);

$extracted_plaintext = fread($fh, $textLength);

# if you want the plain text with no formatting, do this
//echo $extracted_plaintext;

# if you want to see your paragraphs in a web page, do this
return $doc_data= nl2br($extracted_plaintext);

}
}

}
?>