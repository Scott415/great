<?php 
class store_model extends CI_Model {
var $user_table = 'ugk_stores'; 
 
    function __construct()
    {
        parent::__construct();
		
    }
	
	/************************************************* Store functions starts **************************************************/	
	
	function getStoreIdFromName($storename)
	{
		$result=$this->db->select("id")->from("ugk_stores")->where(array("store_name"=>$storename,"store_status <>"=>"4"))->get()->row_array();	
		return $result["id"];
	}
	function getStoreEmail($storeid)
	{
		$result=$this->db->select("useremail")->from("ugk_stores")->where(array("id"=>$storeid))->get()->row_array();	
		return $result["useremail"];
	}
	function getStoreNameFromId($storeid)
	{
		$result=$this->db->select("firstname,lastname")->from("ugk_stores")->where(array("id"=>$storeid))->get()->row_array();	
		return $result["firstname"]." ".$result["lastname"];
	}
	
	function getReferalCode($storeid)
	{
		$result=$this->db->select("store_referal_code")->from("ugk_stores")->where(array("id"=>$storeid))->get()->row_array();	
		return $result["store_referal_code"];
	}
			
	function getStoreDataAll()
	{
		$this->db->select('*,ugk_stores.id as store_id');
		$this->db->from('ugk_stores');
		
		$where=array('store_status <> '=>'4');
		$this->db->where($where);
		$query = $this->db->get();
		$resultset=$query->result_array();
		return $resultset;
	}
	
	
	function getStoreData($searchdata=array())
	{
		$recordperpage="";
		$startlimit="";
		if(!isset($searchdata["page"]) || $searchdata["page"]=="")
		{
			$searchdata["page"]=0;	
		}
	    if(!isset($searchdata["countdata"]))
		{	
			if(isset($searchdata["per_page"]) && $searchdata["per_page"]!="")
			{
				$recordperpage=$searchdata["per_page"];	
			}
			else
			{
				$recordperpage=1;
			}
			if(isset($searchdata["page"]) && $searchdata["page"]!="")
			{
				$startlimit=$searchdata["page"];	
			}
			else
			{
				$startlimit=0;
			}
		}
		
		$searcharray=array("status"=>"store_status");
		$this->db->select('*,ugk_stores.id as store_id');
		$this->db->from('ugk_stores');
		
		if(count($searchdata)!=0)
		{		
			foreach($searchdata as $key=>$val)
			{
				if(isset($searcharray[$key]) && $searchdata[$key]!="")
				{
					if(array_key_exists($key,$searcharray))
					{
						$where=array($searcharray[$key]=>$val);
						$this->db->where($where);
					}
				}
			}		
		}
		
		if(isset($searchdata["search"]) && $searchdata["search"]!="search" && $searchdata["search"]!="")
		{
			$this->db->like('ugk_stores.username', $searchdata["search"]);
		}
		$where=array('store_status <> '=>'4');
		$this->db->where($where);	
		if(isset($searchdata["per_page"]))
		{
			if($recordperpage!="" && ($startlimit!="" || $startlimit==0))
			{
				$this->db->limit($recordperpage,$startlimit);
			}
		}		
		$query = $this->db->get(); 
		$resultset=$query->result_array();
	//	print_r($resultset);die;
		return $resultset;
	}
	
	function getIndividualStoreData($storeid)
	{
		$this->db->select('*,ugk_stores.id as store_id');
		$this->db->from('ugk_stores');
		$where=array('store_status <> '=>'4',"ugk_stores.id"=>$storeid);
		$this->db->where($where);
		$query = $this->db->get();
		$resultset=$query->row_array();
		return $resultset;
	}
	
	function getStoreProductCount($storeid)
	{
		return $result=$this->db->select("*")->from("ugk_products")->where(array("user_uniq"=>$storeid,"product_status"=>"1"))->get()->num_rows();	
	}
	
	function archive_store($store_id)
	{
		//update seller table
		$where=array("id"=>$store_id);
		$array=array("store_status"=>4);
		$this->db->where($where);
		$this->db->update("ugk_stores",$array);	
		//update UGK Products
		$where=array("user_uniq"=>$store_id);
		$array=array("product_status"=>4);
		$this->db->where($where);
		$this->db->update("ugk_products",$array);		
	}
	
	
	
	
	
	public function enable_disable_store($storeid,$status)
	{
		$this->db->where("id",$storeid);
		$array=array("store_status"=>$status);
		$this->db->update("ugk_stores",$array);	
	}
	
	public function show_hide_store($storeid,$status)
	{
		$this->db->where("id",$storeid);
		$array=array("show_pdt"=>$status);
		$this->db->update("ugk_stores",$array);	
	}
	
	public function acept_condition($storeid)
	{
		$this->db->where("id",$storeid);
		$array=array("is_accept"=>1);
		return $this->db->update("ugk_stores",$array);	
	}
	
	
	public function process_withdrawlBonus($referal_code,$referenceid)
	{
		$store=$this->db->select("id,store_current_plan")->from("ugk_stores")->where(array("store_referal_code"=>$referal_code))->get()->row_array();	
		$store_id=$store["id"];
		$planid=$store["store_current_plan"];
		
		$referalbonus=$this->db->select("referal_commision")->from("ugk_store_plans")->where(array("id"=>$planid))->get()->row_array();
		$referalbonus=$referalbonus["referal_commision"];
		
		$getmainstore=$this->db->select("store_current_plan")->from("ugk_stores")->where(array("id"=>$store_id))->get()->row_array();	
		$mainplanid=$getmainstore["store_current_plan"];
		
		$mainstoreamount=$this->db->select("amount")->from("ugk_store_plans")->where(array("id"=>$mainplanid))->get()->row_array();
		$mainstoreamount=$mainstoreamount["amount"];		
		$referalamount=($referalbonus/100)*$mainstoreamount;
		
		$currentstore=$this->db->select("store_balance")->from("ugk_stores")->where(array("id"=>$store_id))->get()->row_array();
		$currentstorebalance=$currentstore["store_balance"];
		
		$newbalance=$currentstorebalance+$referalamount;		
		$storebalance=array("store_balance"=>$newbalance);
		$this->db->where("id",$store_id);
		$currentstorebalance=$this->db->update("ugk_stores",$storebalance);
		
	}
	
	public function add_edit_store($storearray)
	{
		$this->db->where("id",$storearray["id"]);
	    return $this->db->update("ugk_stores",$storearray);							
	}	
	
	/************************************************* Store function ends **************************************************/
	
	/************************************************* Category function starts **************************************************/
	public function enable_disable_category($categoryid,$status)
	{
		$this->db->where("id",$categoryid);
		$array=array("store_category_status"=>$status);
		$this->db->update("ugk_store_category",$array);		
	}
	
	function archive_category($category_id)
	{
		$where=array("id"=>$category_id);
		$array=array("store_category_status"=>4);
		$this->db->where($where);
		$this->db->update("ugk_store_category",$array);		
		$where=array("store_category"=>$category_id);
		$array=array("store_status"=>4);
		$this->db->where($where);
		$this->db->update("ugk_stores",$array);	
	}
		
	public function getStoreCategory($searcharray=array())
	{
		$recordperpage="";
		$startlimit="";
		if(!isset($searcharray["page"]) || $searcharray["page"]=="")
		{
			$searcharray["page"]=0;	
		}
	    if(!isset($searcharray["countdata"]))
		{	
			if(isset($searcharray["per_page"]) && $searcharray["per_page"]!="")
			{
				$recordperpage=$searcharray["per_page"];	
			}
			else
			{
				$recordperpage=1;
			}
			if(isset($searcharray["page"]) && $searcharray["page"]!="")
			{
				$startlimit=$searcharray["page"];	
			}
			else
			{
				$startlimit=0;
			}
		}
		$this->db->select('*');
		$this->db->from('ugk_store_category');
		if(isset($searcharray["type"]) && $searcharray["type"]=="activated")
		{
			$where=array("store_category_status"=>"1");
			$this->db->where($where);
		}
			
		if(isset($searcharray["search"]) && $searcharray["search"]!="" && $searcharray["search"]!="search")
		{
			$this->db->like("ugk_store_category.store_category_name",$searcharray["search"]);	
		}
		$where=array("store_category_status <> "=>"4");
		$this->db->where($where);
		if(isset($searcharray["per_page"]) && $searcharray["per_page"]!="")
		{
			if($recordperpage!="" && ($startlimit!="" || $startlimit==0))
			{
				$this->db->limit($recordperpage,$startlimit);
			}
		}
		
		 $query = $this->db->get();
		$resultset=$query->result_array();
		return $resultset;
			
	}
	
	public function getIndividualCategory($categoryid)
	{
		$this->db->select("*");	
		$this->db->from('ugk_store_category');
		$where=array("id"=>$categoryid,"store_category_status <> "=>"4");
		$this->db->where($where);
		$query = $this->db->get();
		$resultset=$query->row_array();
		return $resultset;
	}
	
	public function add_edit_category($categoryarray)
	{
		if($categoryarray["id"]=="")
		{
			return $this->db->insert("ugk_store_category",$categoryarray);	
		}	
		else
		{
			$this->db->where("id",$categoryarray["id"]);
			return $this->db->update("ugk_store_category",$categoryarray);	
		}
	}
	
	/************************************************* Category function ends **************************************************/
	
	/************************************************* Location function starts **************************************************/
	#public function getStoreCountries($searcharray=array(),$type="",$recordperpage="",$startlimit="")
	public function getStoreCountries($searchdata=array())
	{
		$recordperpage="";
		$startlimit="";
		if(!isset($searchdata["page"]) || $searchdata["page"]=="")
		{
			$searchdata["page"]=0;	
		}
	    if(!isset($searchdata["countdata"]))
		{	
			if(isset($searchdata["per_page"]) && $searchdata["per_page"]!="")
			{
				$recordperpage=$searchdata["per_page"];	
			}
			else
			{
				$recordperpage=1;
			}
			if(isset($searchdata["page"]) && $searchdata["page"]!="")
			{
				$startlimit=$searchdata["page"];	
			}
			else
			{
				$startlimit=0;
			}
		}
		
		
		$this->db->select('*');
		$this->db->from('ugk_countries');
		
		
		if(count($searchdata)!=0)
		{		
			foreach($searchdata as $key=>$val)
			{
				if(isset($searcharray[$key]) && $searchdata[$key]!="")
				{
					if(array_key_exists($key,$searcharray))
					{
						$where=array($searcharray[$key]=>$val);
						$this->db->where($where);
					}
				}
			}		
		}
		
		if(isset($searchdata["search"]) && $searchdata["search"]!="search" && $searchdata["search"]!="")
		{
			$this->db->like('ugk_countries.country_name', $searchdata["search"]);
		}
		
		if(isset($searchdata["type"]) && $searchdata["type"]=="activated")
		{
			$this->db->where('ugk_countries.status',"1");
		}
		
		
		$where=array('status <> '=>'4');
		$this->db->where($where);
		$this->db->where(array("id <> "=>"40"));
	     $this->db->where(array("id <> "=>"237"));
		if(isset($searchdata["per_page"]) && $searchdata["per_page"]!="")
		{
			if($recordperpage!="" && ($startlimit!="" || $startlimit==0))
			{
				$this->db->limit($recordperpage,$startlimit);
			}
		}
         
		$query = $this->db->get();
		$resultset=$query->result_array();
		return $resultset;
	}
	
	public function enable_disable_locations($locationid,$status)
	{
		$this->db->where("id",$locationid);
		$array=array("status"=>$status);
		$this->db->update("ugk_countries",$array);		
	}
	
	public function getCountryName($countryid)
	{
		$this->db->select("country_name");
		$this->db->from("ugk_countries");
		$this->db->where(array("id"=>$countryid));
		$result=$this->db->get();
		$result=$result->row_array();
		return $result["country_name"];
	}
	

	
	/************************************************* Location function ends **************************************************/
	
	/************************************************* Experience function starts **************************************************/
	public function enable_disable_experience($categoryid,$status)
	{
		$this->db->where("id",$categoryid);
		$array=array("status"=>$status);
		$this->db->update("ugk_store_experience",$array);		
	}
	
	function archive_experience($category_id)
	{
		$where=array("id"=>$category_id);
		$array=array("status"=>4);
		$this->db->where($where);
		$this->db->update("ugk_store_experience",$array);
		$where=array("store_experience"=>$category_id);
		$array=array("store_status"=>4);
		$this->db->where($where);
		$this->db->update("ugk_stores",$array);		
	}
	
	/*public function getStoreExperience($searcharray=array(),$type="",$recordperpage="",$startlimit="")*/
	
	public function getStoreExperience($searchdata=array())
	{
		
		$recordperpage="";
		$startlimit="";
		if(!isset($searchdata["page"]) || $searchdata["page"]=="")
		{
			$searchdata["page"]=0;	
		}
	    if(!isset($searchdata["countdata"]))
		{	
			if(isset($searchdata["per_page"]) && $searchdata["per_page"]!="")
			{
				$recordperpage=$searchdata["per_page"];	
			}
			else
			{
				$recordperpage=1;
			}
			if(isset($searchdata["page"]) && $searchdata["page"]!="")
			{
				$startlimit=$searchdata["page"];	
			}
			else
			{
				$startlimit=0;
			}
		}
		
		$this->db->select('*');
		$this->db->from('ugk_store_experience');
		
		
		if(count($searchdata)!=0)
		{		
			foreach($searchdata as $key=>$val)
			{
				if(isset($searcharray[$key]) && $searchdata[$key]!="")
				{
					if(array_key_exists($key,$searcharray))
					{
						$where=array($searcharray[$key]=>$val);
						$this->db->where($where);
					}
				}
			}		
		}
		
		if(isset($searchdata["search"]) && $searchdata["search"]!="search" && $searchdata["search"]!="")
		{
			$this->db->like('ugk_store_experience.experience', $searchdata["search"]);
		}
		
		if(isset($searchdata["type"]) && $searchdata["type"]=="activated")
		{
			$this->db->where("ugk_store_experience.status","1");
		}
		
		$where=array('ugk_store_experience.status <> '=>'4');
		$this->db->where($where);
		if($type=="activated")
		{
			$where=array("status"=>"1");
			$this->db->where($where);
		}
		if(isset($searcharray["per_page"]) && $searcharray["per_page"]!="")
		{
			if($recordperpage!="" && ($startlimit!="" || $startlimit==0))
			{
				$this->db->limit($recordperpage,$startlimit);
			}
		}
		
		$query = $this->db->get(); 
		$resultset=$query->result_array();
		return $resultset;
		
		
		
		
		/*if($type=="activated")
		{
			$where=array("status"=>"1");
			$this->db->where($where);
		}
			$where=array("status <> "=>"4");
			$this->db->where($where);
		if($recordperpage!="" || $startlimit!="")
		{
			$this->db->limit($recordperpage,$startlimit);
		}
		if(isset($searcharray["search"]) && $searcharray["search"]!="" && $searcharray["search"]!="search")
		{
			$this->db->like("ugk_store_experience.experience",$searcharray["search"]);	
		}*/
		$query = $this->db->get();
		$resultset=$query->result_array();
		return $resultset;
			
	}
	
	public function getIndividualexperience($experienceid)
	{
		$this->db->select("*");	
		$this->db->from('ugk_store_experience');
		$where=array("id"=>$experienceid,"status <> "=>"4");
		$this->db->where($where);
		$query = $this->db->get();
		$resultset=$query->row_array();
		return $resultset;
	}
	
	public function add_edit_experience($experiencearray)
	{
		if($experiencearray["id"]=="")
		{
			return $this->db->insert("ugk_store_experience",$experiencearray);	
		}	
		else
		{
			$this->db->where("id",$experiencearray["id"]);
			return $this->db->update("ugk_store_experience",$experiencearray);	
		}
	}
	
	/************************************************* Category function ends **************************************************/
	
	/************************************************* Plan function starts **************************************************/
	
	public function getStorePlans($searchdata=array())
	{
		
		$recordperpage="";
		$startlimit="";
		if(!isset($searchdata["page"]) || $searchdata["page"]=="")
		{
			$searchdata["page"]=0;	
		}
	    if(!isset($searchdata["countdata"]))
		{	
			if(isset($searchdata["per_page"]) && $searchdata["per_page"]!="")
			{
				$recordperpage=$searchdata["per_page"];	
			}
			else
			{
				$recordperpage=1;
			}
			if(isset($searchdata["page"]) && $searchdata["page"]!="")
			{
				$startlimit=$searchdata["page"];	
			}
			else
			{
				$startlimit=0;
			}
		}
		
		$this->db->select('*');
		$this->db->from('ugk_store_plans');
		
		if(count($searchdata)!=0)
		{		
			foreach($searchdata as $key=>$val)
			{
				if(isset($searcharray[$key]) && $searchdata[$key]!="")
				{
					if(array_key_exists($key,$searcharray))
					{
						$where=array($searcharray[$key]=>$val);
						$this->db->where($where);
					}
				}
			}		
		}
		
		if(isset($searchdata["type"]) && $searchdata["type"]=="activated")
		{
			$where=array("plan_status"=>"1");
			$this->db->where($where);
		}		
		if(isset($searchdata["search"]) && $searchdata["search"]!="search" && $searchdata["search"]!="")
		{
			$this->db->like('ugk_store_plans.plan_name', $searchdata["search"]);
		}
		$where=array('plan_status <> '=>'4');
		$this->db->where($where);
		if(isset($searcharray["per_page"]) && $searcharray["per_page"]!="")
		{
			if($recordperpage!="" && ($startlimit!="" || $startlimit==0))
			{
				$this->db->limit($recordperpage,$startlimit);
			}
		}
		$query = $this->db->get();
		$resultset=$query->result_array();
		return $resultset;
		
		/*if(isset($searcharray["type"]) && $searcharray["type"]=="activated")
		{
			$where=array("plan_status"=>"1");
			$this->db->where($where);
		}
		if(isset($searcharray["search"]) && $searcharray["search"]=="search" && $searcharray["search"]!="")
		{
			$where=array("plan_name"=>$searcharray["search"]);
			$this->db->like($where);	
		}
		$where=array('plan_status <> '=>'4');
		$this->db->where($where);
		$query = $this->db->get();
		$resultset=$query->result_array();
		return $resultset;*/
	}
	
	public function getIndividualPlandata($planid)
	{
		$this->db->select('*');
		$this->db->from('ugk_store_plans');
		$where=array('plan_status <> '=>'4','id'=>$planid);
		$this->db->where($where);
		$query = $this->db->get();
		$resultset=$query->row_array();
		return $resultset;	
	}
	
	public function enable_disable_plan($planid,$status)
	{
		$this->db->where("id",$planid);
		$array=array("plan_status"=>$status);
		$this->db->update("ugk_store_plans",$array);	
	}
	
	public function archive_plan($planid)
	{
		$where=array("id"=>$planid);
		$array=array("plan_status"=>4);
		$this->db->where($where);
		$this->db->update("ugk_store_plans",$array);	
	}
	
	public function add_edit_plan($arr)
	{
		if($arr["id"]=="")
		{
			$planbasicinfo["plan_name"]=$arr["plan_name"];
			$planbasicinfo["number_of_products"]=$arr["number_of_products"];
			$planbasicinfo["number_of_images"]=$arr["number_of_images"];
			$planbasicinfo["number_of_staffusers"]=$arr["number_of_staffusers"];
			$planbasicinfo["memory_usage"]=$arr["memory_usage"];
			$planbasicinfo["time_period"]=$arr["time_period"];
			$planbasicinfo["amount"]=$arr["amount"];
			$planbasicinfo["admin_commision"]=$arr["admin_commision"];
			$planbasicinfo["referal_commision"]=$arr["referal_commision"];
			if($this->db->insert("ugk_store_plans",$planbasicinfo))
			{
				$planid=$this->db->insert_id();		
				foreach($arr["chkpayment"] as $key=>$val)
				{
					$paymentinfo["plan_id"]=$planid;
					$paymentinfo["payment_id"]=$val;
					$this->db->insert("ugk_payment_plan",$paymentinfo);	
				}
				foreach($arr["chkshipping"] as $key=>$val)
				{
					$shippinginfo["plan_id"]=$planid;
					$shippinginfo["shipping_id"]=$val;
					$this->db->insert("ugk_shipping_plan",$shippinginfo);	
				}
				$err=0;
			}
			else
			{
				$err=1;	
			}
		}
		else
		{
			$planbasicinfo["id"]=$arr["id"];
			$planbasicinfo["plan_name"]=$arr["plan_name"];
			$planbasicinfo["number_of_products"]=$arr["number_of_products"];
			$planbasicinfo["number_of_images"]=$arr["number_of_images"];
			$planbasicinfo["number_of_staffusers"]=$arr["number_of_staffusers"];
			$planbasicinfo["memory_usage"]=$arr["memory_usage"];
			$planbasicinfo["time_period"]=$arr["time_period"];
			$planbasicinfo["amount"]=$arr["amount"];
			$planbasicinfo["admin_commision"]=$arr["admin_commision"];
			$planbasicinfo["referal_commision"]=$arr["referal_commision"];
			$this->db->where("id",$planbasicinfo["id"]);
			if($this->db->update("ugk_store_plans",$planbasicinfo))
			{
				$this->db->where(array("plan_id"=>$planbasicinfo["id"]));
				$this->db->delete("ugk_payment_plan");	
				$this->db->where(array("plan_id"=>$planbasicinfo["id"]));
				$this->db->delete("ugk_shipping_plan");
				foreach($arr["chkpayment"] as $key=>$val)
				{
					$paymentinfo["plan_id"]=$planbasicinfo["id"];
					$paymentinfo["payment_id"]=$val;
					$this->db->insert("ugk_payment_plan",$paymentinfo);	
				}
				foreach($arr["chkshipping"] as $key=>$val)
				{
					$shippinginfo["plan_id"]=$planbasicinfo["id"];
					$shippinginfo["shipping_id"]=$val;
					$this->db->insert("ugk_shipping_plan",$shippinginfo);	
				}	
				$err=0;
			}
			else
			{
				$err=1;	
			}
		}
		
		if($err==1)
		{
			return false;
		}
		else 
		{
			return true;	
		}
	}
	
	public function getPlanShippingMethods($planid)
	{
		$this->db->select("*");
		$this->db->from("ugk_store_plans");
		$this->db->join("ugk_shipping_plan","ugk_shipping_plan.plan_id=ugk_store_plans.id");
		$this->db->join("ugk_shipping_methods","ugk_shipping_plan.shipping_id=ugk_shipping_methods.id");
		$this->db->where(array("ugk_store_plans.id"=>$planid));
		$this->db->where(array("ugk_shipping_methods.method_status"=>"1"));
		$query=$this->db->get();
		$result=$query->result_array();
		return $result;	
	}
	
	public function getPlanPaymentMethods($planid)
	{
		$this->db->select("*");
		$this->db->from("ugk_store_plans");
		$this->db->join("ugk_payment_plan","ugk_payment_plan.plan_id=ugk_store_plans.id");
		$this->db->join("ugk_payment_methods","ugk_payment_methods.id=ugk_payment_plan.payment_id");
		$this->db->where(array("plan_status <> "=>4,"plan_id"=>$planid,"ugk_payment_methods.method_status"=>1));
		$this->db->where(array("ugk_store_plans.id"=>$planid));
		$this->db->where(array("ugk_payment_methods.method_status"=>"1"));
		$query=$this->db->get();
		$result=$query->result_array(array("plan_status <> "=>4,"plan_id"=>$planid));
		return $result;	
	}
	
	public function getStoreReferal($storeid)
	{
		$referalcode=$this->db->select("referal_code")->from("ugk_stores")->where("id",$storeid)->get()->row_array();	
		return $referalcode["referal_code"];
	}
	
	 public function setStoreSubscribedPlan($dataarr)
	 {
	  $result = $this->db->insert('ugk_store_subscribed_plans',$dataarr);
	  $storereferal = $this->getStoreReferal($dataarr["store_id"]);
	  if($storereferal!="")
	  {
			$this->process_withdrawlBonus($storereferal,$dataarr["store_id"]);  
	  }
	  if($result)
	  {
	    return true; 
	  }
	  else
	  {
	    return false;    
	  }
	 }
 
	 public function getStoreSubscribedPlan($store_id,$plan_id)
	 {
	  $this->db->select("*");
	  $this->db->from("ugk_store_subscribed_plans");
	  $this->db->where(array("store_id" => $store_id,"plan_id" => $plan_id));
	  $this->db->order_by("id", "desc");
	  $this->db->limit(0,1);
	  $query = $this->db->get();
	  $result = $query->result_array();
	  return $result;
	  
	 }
	 
	 public function setStoresUpdatedPlan($storearr)
	 {
	  $data["id"] = $storearr["id"];
	  $data["store_current_plan"] = $storearr["store_current_plan"];
	  $data["renewal_date"] = $storearr["renewal_date"];
	  $data["store_plan_status"] = $storearr["store_plan_status"];
	  $this->db->where(array("id"=>$data["id"]));
	  if($this->db->update("ugk_stores",$data))
	  {
	   	 return true; 
	  }
	  else
	  {
	  	 return false; 
	  }
	 }
 
 	public function changeStorePassword($passarr)
	{
		$store_id = $passarr['store_id'];
		$oldPass = $passarr['oldpassword'];
		$data['password'] = $passarr['newpassword'];
		
		$this->db->where(array('id' => $store_id, 'password' =>  $oldPass));
		if($this->db->update("ugk_stores",$data))
		{
		   return true; 
		}
		else
		{
		   return false; 
		}
	}
	public function changeuserPassword($passarr)
	{
		$store_id = $passarr['store_id'];
		$oldPass = $passarr['oldpassword'];
		$data['password'] = $passarr['newpassword'];
		
		$this->db->where(array('id' => $store_id, 'password' =>  $oldPass));
		if($this->db->update("ugk_users",$data))
		{
		   return true; 
		}
		else
		{
		   return false; 
		}
	}
 
 	public function getstorefeedbackdata($store_id)
	{
		$this->db->select('*');	
		$this->db->from('ugk_store_feedback');
		$this->db->where(array('status' => 1,'store_id'=>$store_id));
		$this->db->order_by("time desc");
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function getStorerating($store_id)
	{
		$result=$this->db->select("store_rating")->from("ugk_stores")->where(array("id"=>$store_id))->get()->row_array();
		return $result["store_rating"];
	}
	
	public function checkIfValidForRating($consumerid,$storeid)
	{
		$result=$this->db->select("count(id) as totalratings")->from("ugk_store_feedback")->where(array("consumer_id"=>$consumerid,"store_id"=>$storeid))->get()->row_array();
		$totalratings=$result["totalratings"];
		$query="select * from ugk_orders right join ugk_order_details on(ugk_orders.id=ugk_order_details.order_id) where ugk_orders.user_id='$consumerid' group by store_id,order_id";
		$result=$this->db->query($query);
		$result=$result->result_array();
		$totalorders=count($result);
		if($totalratings<$totalorders)
		{
			return "valid";	
		}
		else
		{
			return "invalid";	
		}
	}
	
	public function insertstorefeedback($data)
	{
		$result = $this->db->insert('ugk_store_feedback',$data);
		$ratingdata=$this->db->select("sum(rating) as sumrating,count(id) as totalrating")->from("ugk_store_feedback")->where(array("store_id"=>$data["store_id"],"status"=>"1"))->get()->row_array();
		$totalratings=($ratingdata["sumrating"]/$ratingdata["totalrating"]);
		$this->db->where("id",$data["store_id"]);
		$updatedata=array("store_rating"=>$totalratings);
		return $this->db->update("ugk_stores",$updatedata);
	}
	
	/************************************************* Plan function ends **************************************************/
	
	/***********************************************Store messaging starts **************************************************************/
	
	public function send_data_to_store($arr)
	{
		if($this->create_new_thread($arr))
		{
			$arr["thread_id"]=$this->db->insert_id();
			if($this->insert_message($arr))
			{
				return true;
			}	
			else
			{
				return false;	
			}
		}	
		else
		{
			return false;		
		}
	}
	
	public function create_new_thread($arr)
	{
		$thread["thread_subject"]=$arr["subject"];	
		$thread["store_id"]=$arr["store_id"];
		$thread["consumer_id"]=$arr["consumer_id"];
		$thread["date"]=time();
		return $this->db->insert("ugk_threads",$thread);		
	}
	
	public function insert_message($arr)
	{
		$message["sender_id"]=$arr["consumer_id"];
		$message["sender_type"]='user';
		$message["thread_id"]=$arr["thread_id"];
		$message["message"]=$arr["message"];
		$message["date"]=time();
		return $this->db->insert("ugk_thread_messages",$message);		
	}
	
	public function getLatestMessage($thread_id)
	{
		$this->db->select("message");	
		$this->db->from("ugk_thread_messages");
		$this->db->where("thread_id",$thread_id);
		$this->db->order_by("id desc");
		$result=$this->db->get();
		$result=$result->row_array();
		return $result["message"];
	}
	
	public function getLatestMessageTime($thread_id)
	{
		$this->db->select("date");	
		$this->db->from("ugk_thread_messages");
		$this->db->where("thread_id",$thread_id);
		$this->db->order_by("id desc");
		$result=$this->db->get();
		$result=$result->row_array();
		return $result["date"];
	}
	
	public function getConversationData($searcharray=array())
	{
		$query="select *,ugk_threads.id as super_thread_id from (select * from ugk_thread_messages where status='1' order by date desc) as tab1 inner join ugk_threads on(tab1.thread_id=ugk_threads.id) inner join ugk_stores on(ugk_threads.store_id=ugk_stores.id) where ugk_threads.status='1' and consumer_id='".$this->session->userdata('consumer_id')."'";
		if(isset($searcharray["store"]) && $searcharray["store"]!="")
		{
			//$this->db->like(array("ugk_stores.store_name"=>$searcharray["store"]));
			$query.=" and (ugk_stores.store_name like '%".$searcharray["store"]."%' or ugk_threads.thread_subject like '%".$searcharray["store"]."%')";	
		}
			$query.=" group by tab1.thread_id order by ugk_threads.date desc";
		$result=$this->db->query($query);
		return $result->result_array();	
	}
	
	public function getStoreConversationData($searcharray=array())
	{
		$query="select *,ugk_threads.id as super_thread_id from (select * from ugk_thread_messages where status='1' order by date desc) as tab1 inner join ugk_threads on(tab1.thread_id=ugk_threads.id) inner join ugk_users on(ugk_threads.consumer_id=ugk_users.id) where ugk_threads.status='1' and store_id='".$this->session->userdata("store_id")."'";
		if(isset($searcharray["user"]) && $searcharray["user"]!="")
		{
			//$this->db->like(array("ugk_stores.store_name"=>$searcharray["store"]));
			$query.=" and (ugk_users.username like '%".$searcharray["user"]."%' or ugk_threads.thread_subject like '%".$searcharray["user"]."%')";	
		}
			$query.=" group by tab1.thread_id order by ugk_threads.date desc";
		$result=$this->db->query($query);
		return $result->result_array();	
	}
	
	public function getThreadMessages($thread_id)
	{
		return $data["resultset"]=$this->db->select("*")->from("ugk_thread_messages")->where(array("thread_id"=>$thread_id,"status"=>"1"))->get()->result_array();	
	}
	
	public function getNameOfMessage($messageid)
	{
		$result=$this->db->select("sender_id,sender_type")->from("ugk_thread_messages")->where("id",$messageid)->get()->row_array();	
		if($result["sender_type"]=="user")
		{
			$sendername=$this->db->select("username")->from("ugk_users")->where("id",$result["sender_id"])->get()->row_array();	
			$sendername=$sendername["username"];
		}
		else if($result["sender_type"]=="store")
		{
			$sendername=$this->db->select("store_name")->from("ugk_stores")->where("id",$result["sender_id"])->get()->row_array();	
			$sendername=$sendername["store_name"];
		}
		
		return $sendername;
	}
	
	
	public function updateThreadToView($thread_id)
	{
		$array=array("is_viewed"=>"1");
		$this->db->where("id",$thread_id);
		return $this->db->update("ugk_threads",$array);			
	}
	
	public function updateStoreThreadToView($thread_id)
	{
		$array=array("is_store_viewed"=>"1");
		$this->db->where("id",$thread_id);
		return $this->db->update("ugk_threads",$array);			
	}
	
	public function addMessage($message)
	{
		$this->db->where("id",$message["thread_id"]);
		$this->db->update("ugk_threads",array("is_store_viewed"=>"0","date"=>time()));
		return $this->db->insert("ugk_thread_messages",$message);
	}
	
	public function addStoreMessage($message)
	{
		$this->db->where("id",$message["thread_id"]);
		$this->db->update("ugk_threads",array("is_viewed"=>"0","date"=>time()));
		return $this->db->insert("ugk_thread_messages",$message);
	}
	
	public function getUnReadConversationForUser($consumer_id)
	{
		$result=$this->db->select("id")->from("ugk_threads")->where(array("consumer_id"=>$consumer_id,"is_viewed"=>"0"))->get()->num_rows();
		return $result;
	}
	
	public function countStoreMessages($store_id)
	{
		$result=$this->db->select("id")->from("ugk_threads")->where(array("store_id"=>$store_id,"is_store_viewed"=>"0"))->get()->num_rows();
		return $result;
	}
	
	public function getRecipentNameFromThreadId($thread_id,$recipient)
	{
		$this->db->select("*")->from("ugk_threads");
		$this->db->join("ugk_stores","ugk_stores.id=ugk_threads.store_id");
		$this->db->join("ugk_users","ugk_users.id=ugk_threads.consumer_id");
		$this->db->where("ugk_threads.id",$thread_id);
		$result=$this->db->get();
		$result=$result->row_array();
		return $result[$recipient];
	}
	
	
	public function check_is_condition_accept($store_id)
	{
	    $result=$this->db->select("is_accept")->from("ugk_stores")->where(array("id"=>$store_id))->get()->row_array();
		return $result["is_accept"];
	}
	
	public function ugk_order_cancel_reasons()
	{
	   
		    $this->db->select("*");
			$this->db->from("ugk_order_cancel_reasons");
			$this->db->where(array("status"=>1));
			$result = $this->db->get();
		    return $result= $result->result_array();
			
		    
	}
	public function check_store_email_verification($storeid)
	{
	        $this->db->select("store_email_verified");
			$this->db->from("ugk_stores");
			$this->db->where(array("id"=>$storeid,"store_status"=>1));
			$result = $this->db->get();
		    return $result->result_array();
	}
	/***********************************************Store messaging ends **************************************************************/
	public function upload_store_image($userid,$img)

	{

		$where=array("id"=>$userid);

		$arr=array("store_image"=>$img);

		$this->db->where($where);

		return $this->db->update("ugk_stores",$arr);	

	}

	public function gettexstaes($id)
	{
	        $this->db->select("*");
			$this->db->from("ugk_states");
			if($id==2)
			{
			$this->db->where(array("is_provinces"=>1,'status'=>1));
			}
			else
			{
			$this->db->where(array("is_state"=>1,'status'=>1));	
			}
			$result = $this->db->get();
		    return $result= $result->result_array();
	}
	
	public function gettexcnt()
	{
	        $this->db->select("*");
			$this->db->from("ugk_states");
			$this->db->where(array("is_state"=>0,"is_provinces"=>0,'status'=>1));
			$result = $this->db->get();
		    return $result= $result->result_array();
	}
	
	
}