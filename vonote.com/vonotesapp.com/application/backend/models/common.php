<?php 
class common extends CI_Model {
    function __construct(){
        parent::__construct();
		$http="";
		if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on"){
			$http="http://";	
		}
		else{
			$http="https://";	
		}
		$prevurl=array("prevurl"=>base_url());
		$this->session->set_userdata($prevurl);
		$this->session->set_userdata("currenturl",base_url());
    }
	
	public function get_extension($file_name){
		$ext = explode('.', $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	public function checkplanexistence(){
		/*$this->db->select("id,renewal_date");
		$this->db->from("ugk_stores");	
		$result=$this->db->get();
		$result=$result->result_array();
		foreach($result as $key=>$val){
			if($val["renewal_date"]<time()+(24*60*60)){
				$this->db->where("id",$val["id"]);
				$planstatus=array("store_current_plan"=>"0");
				$this->db->update("ugk_stores",$planstatus);	
			}	
		}*/
	}
	
	/*public function check_previleage(){
		$controllername=$this->router->class;
		$methodname=$this->router->method;
		if($controllername!="login" && $controllername!="moderator_panel" && $controllername!="dashboard" && $controllername!="commonfunctions"){
			if($this->config->item("usertype")!="admin"){		
				$this->db->select("*");
				$this->db->from("ugk_moderator_functions");
				$this->db->where(array("controller_name"=>$controllername,"function_name"=>$methodname));
				$resultset=$this->db->get();
				$resultset=$resultset->row_array();
				$entity_type=$resultset["entity_type"];
				$previleage_type=$resultset["previleage_type"];
				if($this->check_moderator_access($entity_type,$previleage_type)){
					echo "You do not have previleage to access this page";	
					die;
				}
			}
		}
	}*/
//for twitter login		
		/*public function checktwitterlogin($tid){
			$this->db->select("*");
			$this->db->from("users");
			$this->db->where(array("tsid"=>$tid,"status <>"=>4));
			$result=$this->db->get();
		 // echo $this->db->last_query();
	    //  echo var_dump($this->db->queries);
	    	$countrows=$result->num_rows();
		   //echo $countrows;
		    if($countrows==0){
				$this->db->select("*");
				$this->db->from("ugk_stores");
				$this->db->where(array("tsid"=>$tid,"status <>"=>4));
				$results=$this->db->get();
				//echo $this->db->last_query();
				//echo var_dump($this->db->queries);
				$countrows=$results->num_rows();
				//echo $countrows;
				if($countrows==0){
					$this->session->set_flashdata("errormsg","Authentication failed. Invalid Credentials");
					return false;	
				}
				else{
					$results = $results->row_array();
					if($results["user_status"] <> 4){
					    $arr["id"]=$results["id"];
		                $arr["user_name"]=$results["firstname"]." ".$results["lastname"];
		                $arr["email"]=$results["useremail"];
						$arr['password'] = $results['password'];
						$arr["type"]='seller';
						return $arr;	
					}
					else{
						$this->session->set_flashdata("errormsg","Your account is currently inactive. Please contact admin.");	
						return false;
					}
				}
	    	}
		 else{
			$result=$result->row_array();
			if($result["user_status"]=="1"){
				 $arr["id"]=$result["id"];
		         $arr["user_name"]=$result["firstname"]." ".$result["lastname"];
		         $arr["email"]=$result["useremail"];
				 $arr['password'] = $result['password'];
				 $arr["type"]='buyer';
				 return $arr;	
			}
			else{
				$this->session->set_flashdata("errormsg","Your account is currently inactive. Please contact admin.");	
				return false;
			}
		}
	}*/

//for facebook login
public function checkUserFacebookLogin($email,$fbid){
	$this->db->select("*");
	$this->db->from("users");
	$this->db->where(array("email"=>$email,"fbid"=>$fbid,"status <>"=>4));
	$result=$this->db->get();
	//$this->db->last_query(); die;
	 $countrows=$result->num_rows();
	
	if($countrows==0){
		return $countrows;	
	}
	 else{
		$result=$result->row_array();
		if($result["status"]=="1"){
			$this->session->set_userdata('user_id',$result["id"]);
			$this->session->set_userdata('user_email',$result["email"]);
			$this->session->set_userdata('user_status',$result["user_status"]);
							
			return true;
		}
		else{
			//$this->session->set_flashdata("errormsg",lang('error_common_1'));	
			return false;
		}
	}		
}	
	/*public function checkFacebookLogin($email,$fbid){
		//echo $email;die;
		 $this->db->select("*");
		 $this->db->from("users");
		 $this->db->where(array("useremail"=>$email,"fbid"=>$fbid,"user_status <>"=>4));
	     $result=$this->db->get();
		// echo $this->db->last_query();
	   //  echo var_dump($this->db->queries);
	     $countrows=$result->num_rows();
		//echo $countrows;
		if($countrows==0){
			$this->db->select("*");
			$this->db->from("ugk_stores");
			$this->db->where(array("useremail"=>$email,"fbid"=>$fbid,"user_status <>"=>4));
			$results=$this->db->get();
			//echo $this->db->last_query();
			//echo var_dump($this->db->queries);
			$countrows=$results->num_rows();
			//echo $countrows;
			if($countrows==0){
					$this->session->set_flashdata("errormsg","Authentication failed. Invalid Email or password");
					return false;	
				}
		    else{
					$results = $results->row_array();
					if($results["store_status"] <> 4){
						$arr["id"]=$results["id"];
						$arr["user_name"]=$results["firstname"]." ".$results["lastname"];
						$arr["email"]=$results["useremail"];
						$arr["password"] = $results["password"];
						$arr["type"]='seller';
						return $arr;	
					}
					else{
						$this->session->set_flashdata("errormsg","Your account is currently inactive. Please contact admin.");	
						return false;
					}
				}
	    	}
		 else{
				$result=$result->row_array();
				if($result["user_status"]=="1"){
					 $arr["id"]=$result["id"];
					 $arr["user_name"]=$result["firstname"]." ".$result["lastname"];
					 $arr["email"]=$result["useremail"];
					 $arr["password"] = $result["password"];
					 $arr["type"]='buyer';
					 return $arr;	
				}
				else{
					 $this->session->set_flashdata("errormsg","Your account is currently inactive. Please contact admin.");	
					 return false;
				}
		}
	}*/
//for user login
	public function login_check_user_login($data){
		 $login["email"]=trim($data['email']);	
		 $login["password"]=trim($data['password']);
		 $this->session->set_flashdata("tempdata",$login);
		 if($this->common->authenticateUserLogin($login)){
				redirect(base_url()."user/my_desk");
		}
		else{	
			redirect(base_url()."home/");
		}
	}
	
	public function show_hide_previleages($previleagetype,$controllername="",$methodname=""){
		if($this->config->item("usertype")!="admin"){
			if($controllername==""){
				$controllername=$this->router->class;
			}
			if($methodname==""){
				$methodname=$this->router->method;	
			}
			$arr=array("manage"=>1,"enable/disable"=>2,"view"=>3,"add"=>4,"edit"=>5,"delete"=>6);	
			$previleage_type=$arr[$previleagetype];
			if($entityid=$this->getEntityId($controllername,$methodname)){
				if($this->check_moderator_access($entityid,$previleage_type)){
					return false;
				} 
				else{
					return true;	
				}
			}
			else{
				return false;	
			}
		}
		else{
			return true;	
		}
	}
	
	public function get_function_in_controller($controllername){
		/*$this->db->select("entity_type");
		$this->db->from("ugk_moderator_functions");
		$this->db->where("ugk_moderator_functions.controller_name",$controllername);
		$this->db->group_by("entity_type");
		$query=$this->db->get();
		$result=$query->result_array();	
		return $result;*/
	}
	
	
	public function show_hide_menu($controllername){
		if($this->config->item("usertype")!="admin"){
			$result=$this->get_function_in_controller($controllername);
			foreach($result as $key=>$val){
				$entityid=$val["entity_type"];	
				$resultarray=$this->check_get_method_exists($entityid);	

				if(isset($resultarray["id"]) && $resultarray["id"]!=""){
					$i=1;
					break;	
				}
				else{
					$i=0;	
				}
			}
			if($i==1){
				return true;	
			}
			else{
				return false;	
			}
		}
		else{
			return true;	
		}
	}
	
	public function checkActionTab(){
		$action=0;
		$arr=array("view"=>3,"edit"=>5,"delete"=>6);
		foreach($arr as $key=>$val){
			if($this->show_hide_previleages($key)){
				$action=1;
				break;	
			}
		}	
		if($action==1){
			return true;	
		}
		else{
			return false;	
		}
	}
	
	public function check_authentication(){
		$controllername=$this->router->class;
		$methodname=$this->router->method;
		if($controllername=="login" && $methodname!="logout"){
			$username=$this->session->userdata("username");
			if(isset($username) && $username!=""){
				redirect(base_url()."dashboard");	
			}
		}
		if($controllername!="login" && $controllername!="forget_password"){
			if($this->config->item("usertype")=="admin"){
				$this->db->select("username");
				$this->db->from("admin");
				$query=$this->db->get();
				$resultset=$query->row_array();
				if($resultset["username"]!=$this->session->userdata("username")){
					$this->session->set_flashdata("errormsg","Please login to access admin panel first");
					redirect(base_url()."login/");	
				}
			}
		}
	}
	
	function validate_email($email){
		return filter_var($email, FILTER_VALIDATE_EMAIL);	
	}
//for update password	
	public function update_profile($arr){
		$newarr["password"]=$arr["newpassword"];
		if(isset($arr["username"]) && $arr["username"]!=""){
			$newarr["username"]=$arr["username"];
		}
		return $this->db->update("admin",$newarr);
	}
	
/// for random genrator text	
	public function generate_transaction_number($digits=10){
		srand((double) microtime() * 10000000);
		$input = array("A", "B", "C", "D", "E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a", "b", "c", "d", "e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
		$random_generator="";
		for ($i=1; $i<=$digits; $i++){
			if(rand(1,2) == 1){
				$rand_index = array_rand($input);
				$random_generator .=$input[$rand_index];
			}
			else{
				$random_generator .=rand(1,9);
			}
		}
   		return $random_generator;	
	}

/////////////	
	public function removeUrl($parametername,$querystring){
		$newquerystring="";
		$querystring=explode("&",$querystring);
		foreach($querystring as $key=>$val){
			$newval=explode("=",$val);
			if($newval[0]!=""){
				if($newval[0]!=$parametername){
					$newquerystring.="&".$newval[0]."=".$newval[1];	
				}
			}
		}		
		$newquerystring=substr($newquerystring,1,strlen($newquerystring));
		return $newquerystring;
	}
	
////////////////	
	public function addUrl($parametername,$parametervalue,$querystring){
		//echo $parametername."=".$parametervalue;		
		$querystring=explode("&",$querystring);
		$newquerystring="";
		$i=0;
		if(count($querystring)!=0 && $querystring[0]!=""){
			foreach($querystring as $key=>$val){
					$valnew=explode("=",$val);
					if($valnew[0]!=$parametername){
						$newquerystring.="&".$valnew[0]."=".$valnew[1];
					}
					else{
						$newquerystring.="&".$parametername."=".$parametervalue;	
						$i=1;
					}
			}
		}
		if($i==0){
				$newquerystring.="&".$parametername."=".$parametervalue;
		}
		return $newquerystring=substr($newquerystring,1,strlen($newquerystring));
	}
	
//for check username already exists or not	
	public function check_username_availabilty($username){
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where(array('username'=>$username,"status <>"=>"0"));
		$query=$this->db->get();
		$resultset=$query->num_rows();
		if($resultset==0){
			return true;	
		}
		else{
			return false;	
		}			
	}
	
//for check email id already exists or not	
	public function check_email_availabilty($email){
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where(array('email'=>$email, "archive <>" => 1, 'status' => 1));
		$query=$this->db->get();
		$resultset=$query->num_rows(); 
		if($resultset == 0){
			return true;	
		}
		else{
			return false;	
		}			
	}
//for check folder name already exists or not	
	public function check_foldername_availabilty($arr){
		$this->db->select('id');
		$this->db->from('user_folders');
		$this->db->where(array('name'=>$arr['name'],'relation'=>$arr['relation'], "status <>"=>"0","archive <>"=>"1"));
		$query=$this->db->get();
	//	echo $this->db->lat_query();
		$resultset=$query->num_rows();
		if($resultset==0){
			return true;	
		}
		else{
			return false;	
		}			
	}
//for check file name already exists or not	
	public function check_filename_availabilty($arr){ 
		$this->db->select('id');
		$this->db->from('user_files');
		$this->db->where(array('name'=>$arr['name'],'user_id'=>$arr['relation'], "status <>"=>"0","archive <>"=>"1"));
		$query=$this->db->get();
		//echo $this->db->last_query();  
		$resultset=$query->num_rows();
		if($resultset==0){
			return true;	
		}
		else{
			return false;	
		}			
	}
	
//for check file name already exists or not	
	public function check_filename_exists($arr){ 
		$this->db->select('id');
		$this->db->from('user_files');
		$this->db->where(array('name'=>$arr['name'], 'user_id'=>$arr['user_id'], "sub_folder" => $arr['sub_folder'], "status <>"=>"0", "archive <>"=>"1"));
		$query=$this->db->get();
		//echo $this->db->last_query();  
		$resultset=$query->num_rows();
		if($resultset==0){
			return false;	
		}
		else{
			return true;	
		}			
	}

//	
	public function checkattributevalidations($attributes=array()){
		$i=0;
		$j=count($attributes[0]);
		$max=0;
		for($i=0;$i<count($attributes);$i++){
			if(count($attributes[$i])>$j){
				$max=$i;	
			}	
		}
		return $max;
	}
//	
	public function authenticateUserLogin($loginarray){
		$this->db->select("*");
		$this->db->from("users");
		$this->db->where(array("email"=>$loginarray["email"], "password" => $loginarray["password"], 'status' => 1, 'archive' => 0));
		$result1=$this->db->get();
		//echo $this->db->last_query(); //die;
		$countrows=$result1->num_rows();
		$result=$result1->row_array();  //print_r($result); die;
		/*
		$this->db->select("*");
		$this->db->from("users");
		$this->db->where(array("email"=>$loginarray["email"], "password" => $loginarray["password"]));
		$result2=$this->db->get();
		echo $this->db->last_query(); die;
		$result3=$result2->row_array();*/
		//print_r($result3);
		//echo $countrows;die; 
		//echo $result['status']; die;
		if($countrows<>0){
			if($result['status'] == 1){
				$this->session->set_userdata("user_id",$result["id"]);
				
				$this->session->set_userdata("user_email",$result["email"]);
				$this->session->set_userdata("user_status",$result["status"]);
				$arr["last_activity"] = now();
				$arr["last_ip_address"] =  $this->input->ip_address();
				$this->db->where("email",$result["email"]);
				$result = $this->db->update("users",$arr);
				return true;
			}
			else if($result['status']==0){
					$this->session->set_flashdata("errormsg","Your account is currently inactive. Please check you email and click on activation link.");	
					return false;
			}
			else if($result['status']==2){
					$this->session->set_flashdata("errormsg","Your account is currently suspended by admin.");	
					return false;
			}
		}
		else{
				$this->session->set_flashdata("errormsg","Wrong email or password.");	
				return false;
		}
	}
	
	//for check old password 
	public function checkpasswordvalidity($arr){
		$this->db->select("*");
		$this->db->from("users");
		$this->db->where("id",$this->session->userdata("user_id"));
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		$result = $query->row_array();
		
		if($this->validateHash($arr["old_password"],$result["new_password"])){
			return false;	
		}	
		else{
			return true;	
		}
	}
	
public function getuser_id_new($email){
		//echo $email;die;
	     $this->db->select("*");
		 $this->db->from("users");
		 $this->db->where(array("email"=>$email,"status =" => 1));
	     $query=$this->db->get();
		// echo $this->db->last_query();
	  //  echo var_dump($this->db->queries);
	 	 $countrows=$query->num_rows();
		//echo $countrows;
		if($countrows==0){
			return false;
		}else{
			return true;
		}
	}


//for get user id	
	public function getuser_id($email){
		//echo $email;die;
	     $this->db->select("*");
		 $this->db->from("users");
		 $this->db->where(array("email"=>$email,"status =" => 1));
	     $query=$this->db->get();
		// echo $this->db->last_query();
	  //  echo var_dump($this->db->queries);
	 	 $countrows=$query->num_rows();
		//echo $countrows;
		if($countrows==0){
			
			$results = $query->row_array();
			if($results["user_status"] == 0){
				$arr["id"]=$results["id"];
				$arr["user_name"]=$results["first_name"]." ".$results["last_name"];
				$arr["email"]=$results["email"];
				$arr["password"]=$results["password"];
				return $arr;	
			}
			else{
				$this->session->set_flashdata("errormsg","Your account is currently inactive. Please contact admin.");	
				return false;
			}
		}
		else{
			$result=$query->row_array();
			if($result["user_status"]=="1"){
				 $arr["id"]=$result["id"];
		        // $arr["user_name"]=$result["firstname"]." ".$result["lastname"];
		         $arr["email"]=$result["email"];
				 $arr["user_name"]=$result["first_name"]." ".$result["last_name"];
				 $arr["password"]=$result["password"];
				 return $arr;	
			}
			/*else{
				$this->session->set_flashdata("errormsg","Your account is currently inactive. Please contact admin.");	
				return false;
			}*/
		}
	}
	
	
	//for get user id	
	public function getadmin_id($email){
		//echo $email;die;
	     $this->db->select("*");
		 $this->db->from("admin");
		 $this->db->where(array("email"=>$email,"status =" => 1));
	     $query=$this->db->get();
		// echo $this->db->last_query();
	  //  echo var_dump($this->db->queries);
	 	 $countrows=$query->num_rows();
		//echo $countrows;
		if($countrows==0){
				$this->session->set_flashdata("errormsg","Email address is incorrect.");	
				return false;
		
		}
		else{
				$result=$query->row_array();
				 $arr["id"]=$result["id"];
		        // $arr["user_name"]=$result["firstname"]." ".$result["lastname"];
		       $arr["email"]=$result["email"];
				 $arr["username"]=$result["username"];
				 $arr["password"]=$result["password"];
				 return $arr;	
		
			/*else{
				$this->session->set_flashdata("errormsg","Your account is currently inactive. Please contact admin.");	
				return false;
			}*/
		}
	}
	
//for update password
	public function update_new_password($data){
		  $arr["password"]= $this->encrypt->encode($data["password"]);
		  $this->db->where(array("id"=>$data["user_id"]));	
		  return $this->db->update("users",$arr);
	}
	
//
	public function checkuserpasswordvalidity($arr){
		$password = $this->encrypt->encode($arr["oldpassword"]);
		$this->db->select("id");
		$this->db->from("users");
		$this->db->where(array("password"=>$password,"id" => $arr['store_id']));
		$query=$this->db->get();
		//echo $this->db->last_query();
		$result=$query->row_array();
		if($result["id"] != ""){
			return true;	
		}	
		else{
			return false;	
		}
	}
	
//for news letter
	public function newsletteremailvalidity($arr){
		$this->db->select("user_email");
		$this->db->from("newsletters");
		$this->db->where(array("user_email"=>$arr["user_email"]));
		$query=$this->db->get();
		$resultset=$query->num_rows();
		if($resultset==0){
			return true;	
		}
		else{
			return false;	
		}	
	}
	
//for content pages data like about us, privacy police, terme and conditions, career
	public function getContactInformation(){
		$contactdata=$this->db->select("*")->from("content_pages")->get();
		return $contactdata->row_array();	
	}
	
//for content pages data like login, signup, security
	public function getcontentpagedata($arr){
		//$resultset=$this->db->select("*")->from("contents")->get();
		$this->db->select("*");
		$this->db->from("contents");
		$this->db->where(array("page_name"=>$arr));
		$query=$this->db->get();
		//$this->db->last_query();
		return $resultset=$query->row_array();	
	}
//for Home content pages data 
	public function gethomepagedata(){
		$this->db->select("*")->from("contents");
		$this->db->where(array("id" => '7'));
		$query=$this->db->get();
		return $resultset=$query->row_array();	
	}
	
	
	
	//for check user login or not
 	public function check_user_login(){
		if(isset($_COOKIE['login'])){
			$this->db->select("*")->from("users");
			$this->db->where(array("email" => $_COOKIE['login']));
			
			$query=$this->db->get();
		//	echo $this->db->last_query(); die;
			$result = $query->row_array();
			if($result['status']==1){
				$this->session->set_userdata("user_id",$result["id"]);
				
				$this->session->set_userdata("user_email",$result["email"]);
				$this->session->set_userdata("user_status",$result["status"]);
				return true;
			}
			else{
				//$this->session->set_flashdata("errormsg","Your account is currently inactive. Please contact admin.");	
				return false;
			}
		}
		
		if($this->session->userdata("user_status")==1){		//echo $err;die;
			redirect(base_url()."user/my_desk");	
		}else{
			//redirect(base_url()."home");
		}
	}
	//for check user have access to page or not
 	public function check_user_acess(){
		if(isset($_COOKIE['login'])){
			$this->db->select("*")->from("users");
			$this->db->where(array("email" => $_COOKIE['login']));
			$query=$this->db->get();
			//echo $this->db->last_query(); die;
			$result = $query->row_array();
			if($result['status']==1){
				$this->session->set_userdata("user_id",$result["id"]);
				
				$this->session->set_userdata("user_email",$result["email"]);
				$this->session->set_userdata("user_status",$result["status"]);
				return true;
			}
			else{
				$this->session->set_flashdata("errormsg","Your account is currently inactive. Please contact admin.");	
				return false;
			}
		}
		if($this->session->userdata("user_status")<>1 && $_COOKIE['login'] == '' && $this->config->item("usertype")!='admin'){		//echo $err;die;
			redirect(base_url()."home/login");	
		}else{
			
			//redirect(base_url()."home");
		}
	}
	
	//for chk friend request, already friend etc
	public function chk_request($table, $where){
		$this->db->select("*")->from($table)->where($where);
		$result=$this->db->get();
		 $countrows = $result->num_rows();
		//echo $this->db->last_query();
		//$result=$CI->db->get()->row_array();
		//return $result->result_array->row();
		//return $result;
		return $countrows;
	}
	
	
	
	public function chk_already_friend($arr){
		//print_r($arr);
		$this->db->select("count(*) as tc")->from("friends")->where("((user_id='".$arr['user_id']."' AND friend_id='".$arr['reciver_id']."') OR (user_id='".$arr['reciver_id']."' AND friend_id='".$arr['user_id']."')) AND status='1' AND blocked='0'");
		//$this->db->query("select count(*) from friends where (user_id='".$arr['user_id']."' AND friend_id='".$arr['friend_id']."') OR (user_id='".$arr['friend_id']."' AND friend_id='".$arr['user_id']."') AND status='1' AND blocked='0'");
		$result=$this->db->get();
		$this->db->last_query();
		$countrows = $result->row_array(); //echo $countrows['tc']; die;
		return $countrows['tc'];
	}
	
  //show time in ago
 public function convert_time_days($time){
  
  $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
  $lengths = array("60","60","24","7","4.35","12","10");     
  $now = time();     
   $difference     = $now - $time;
   $tense         = "ago";     
  for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
   $difference /= $lengths[$j];
  }     
  $difference = round($difference);     
  if($difference != 1) {
   $periods[$j].= "s";
  }     
  return "$difference $periods[$j] ago ";   
 }
  
	public function foldersize($path) {
	error_reporting(E_ALL);
    $total_size = 0;
    $files = scandir($path);
    $cleanPath = rtrim($path, '/'). '/';

    foreach($files as $t) {
        if ($t<>"." && $t<>"..") {
            $currentFile = $cleanPath . $t;
            if (is_dir($currentFile)) {
                $size = $this->foldersize($currentFile);
                $total_size += $size;
            }
            else {
                $size = filesize($currentFile);
                $total_size += $size;
            }
        }   
    }
	
    return $total_size;
}

public function format_size($size) {
 $units = explode(' ', 'B KB MB GB TB PB');

    $mod = 1024;

    for ($i = 0; $size > $mod; $i++) {
        $size /= $mod;
    }

    $endIndex = strpos($size, ".")+3;

    return substr( $size, 0, $endIndex).' '.$units[$i];
}
	
	
}


 if(FRONTPATH != "frontend"){
	  $common= new common;	
	  $common->check_authentication();
	  //$common->check_previleage();
	  $common->show_hide_menu('stores');
	  $common->check_user_login();
	  $common->check_user_acess();
  }
