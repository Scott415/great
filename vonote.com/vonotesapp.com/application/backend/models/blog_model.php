<?php 
class blog_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }
	
	/********************************************************Blog function starts***************************************************/
	
	public function getBlogData($searchdata=array()){
		$searcharray=array("status"=>"status");
		
		if(!isset($searchdata["page"]) || $searchdata["page"]==""){
			$searchdata["page"]=0;	
		}
	    if(!isset($searchdata["countdata"])){	
			if(isset($searchdata["per_page"]) && $searchdata["per_page"]!=""){
				$recordperpage=$searchdata["per_page"];	
			}
			else{
				$recordperpage=1;
			}
			if(isset($searchdata["page"]) && $searchdata["page"]!=""){
				$startlimit=$searchdata["page"];	
			}
			else{
				$startlimit=0;
			}
		}
		
		$this->db->select("*,blogs.id as blogid");
		$this->db->from("blogs");
		if(isset($searchdata["search"]) && $searchdata["search"]!="" && $searchdata["search"]!="search"){
			$this->db->like("blogs.blog_title_english",$searchdata["search"]);	
		}	
		foreach($searchdata as $key=>$val){
			if(isset($searcharray[$key]) && $searchdata[$key]!=""){
				if(array_key_exists($key,$searcharray)){
					$where=array($searcharray[$key]=>$val);
					$this->db->where($where);
				}
			}
		}		
		$where=array("blogs.status <>"=>"4");
		$this->db->where($where);
		$this->db->group_by("blogs.id DESC");		
		if(isset($searchdata["per_page"]) && $searchdata["per_page"]!=""){
			if(isset($recordperpage) && $recordperpage!="" && ($startlimit!="" || $startlimit==0)){
				$this->db->limit($recordperpage,$startlimit);
			}
		}
		
		$query = $this->db->get();
		$resultset=$query->result_array();
		return $resultset; 
	}
	
	
	public function add_edit_blog($blogarray){
		if($blogarray["id"]==""){
			$blogarray["date_posted"]=time();
			$blogarray["last_modified"]=time();
			return $this->db->insert("blogs",$blogarray);	
		}	
		else{
			if($_FILES["userfile"]["name"]!=''){
			$this->db->select("*");
			$this->db->from("blogs");
			$this->db->where("id",$blogarray["id"]);
			$query = $this->db->get();
		    $Images=$query->row_array();
			$Images['blog_images'];
			unlink('../blogimages/'.$Images['blog_images']);
			}
			$blogarray["last_modified"]=time();
			$this->db->where("id",$blogarray["id"]);
			return $this->db->update("blogs",$blogarray);
		}	
	}
	
	public function getBlogData11($searchdata=array()){
		$searcharray=array("status"=>"status");
		
		if(!isset($searchdata["page"]) || $searchdata["page"]==""){
			$searchdata["page"]=0;	
		}
	    if(!isset($searchdata["countdata"])){	
			if(isset($searchdata["per_page"]) && $searchdata["per_page"]!=""){
				$recordperpage=$searchdata["per_page"];	
			}
			else{
				$recordperpage=1;
			}
			if(isset($searchdata["page"]) && $searchdata["page"]!=""){
				$startlimit=$searchdata["page"];	
			}
			else{
				$startlimit=0;
			}
		}
		
		$this->db->select("*,blogs.id as blogid");
		$this->db->from("blogs");
		if(isset($searchdata["search"]) && $searchdata["search"]!="" && $searchdata["search"]!="search"){
			$this->db->like("blogs.blog_title_english",$searchdata["search"]);	
		}	
		foreach($searchdata as $key=>$val){
			if(isset($searcharray[$key]) && $searchdata[$key]!=""){
				if(array_key_exists($key,$searcharray)){
					$where=array($searcharray[$key]=>$val);
					$this->db->where($where);
				}
			}
		}		
		$where=array("blogs.status <>"=>"4", "blogs.status"=>"1");
		$this->db->where($where);
		$this->db->group_by("blogs.id DESC");		
		if(isset($searchdata["per_page"]) && $searchdata["per_page"]!=""){
			if(isset($recordperpage) && $recordperpage!="" && ($startlimit!="" || $startlimit==0)){
				$this->db->limit($recordperpage,$startlimit);
			}
		}
		
		$query = $this->db->get();
		$resultset=$query->result_array();
		return $resultset; 
	}
	
	public function getIndividualBlog($blogid){
		$this->db->select("*");	
		$this->db->from('blogs');
		$where=array("id"=>$blogid,"status <> "=>"0", "archive <> "=>"1");
		$this->db->where($where);
		$query = $this->db->get();
		//echo $this->db->last_query(); 
		$resultset=$query->row_array();
		//print_r($resultset); die;
		return $resultset;		
	}
	
	public function getBlogComments($blogid){
		$this->db->select("users.user_name AS name,blog_comments.comment AS comment,blog_comments.date AS date");
		$this->db->from("blog_comments");
		$this->db->join("users","users.id=blog_comments.user_id");
		$this->db->where(array("blog_comments.status"=>"1","blog_comments.blog_id"=>$blogid));
		
		$query1=$this->db->get();
		$array1=$query1->result_array();
		
		$this->db->select("stores.store_name AS name,blog_comments.comment AS comment,blog_comments.date AS date");
		$this->db->from("blog_comments");
		$this->db->join("stores","stores.id=blog_comments.store_id");
		$this->db->where(array("blog_comments.status"=>"1","blog_comments.blog_id"=>$blogid));
		$query2=$this->db->get();
		$array2=$query2->result_array();
        
		$this->db->select("sub_stores.sub_store_name AS name,blog_comments.comment AS comment,blog_comments.date AS date");
		$this->db->from("blog_comments");
		$this->db->join("sub_stores","sub_stores.id=blog_comments.sub_store_id");
		$this->db->where(array("blog_comments.status"=>"1","blog_comments.blog_id"=>$blogid));
		$query3=$this->db->get();
		$array3=$query3->result_array();
		
		$resultset = array_reverse(array_merge($array1,$array2,$array3));
		return $resultset;
	}
	
	public function enable_disable_blog($blogid,$status){
		$this->db->where("id",$blogid);
		$array=array("status"=>$status);
		$this->db->update("blogs",$array);		
	}
	
	public function archive_blog($blogid){
		$where=array("id"=>$blogid);
		$array=array("status"=>4);
		$this->db->where($where);
		$this->db->update("blogs",$array);
	}	
	public function post_comment_database($arr){
		return $this->db->insert("blog_comments",$arr);
	}
//=================================================Blog Comment Function Start==================================================================//	
	public function getBlogCommentData($searchdata=array()){
		$searcharray=array("status"=>"status");
		
		if(!isset($searchdata["page"]) || $searchdata["page"]==""){
			$searchdata["page"]=0;	
		}
	    if(!isset($searchdata["countdata"])){	
			if(isset($searchdata["per_page"]) && $searchdata["per_page"]!=""){
				$recordperpage=$searchdata["per_page"];	
			}
			else{
				$recordperpage=1;
			}
			if(isset($searchdata["page"]) && $searchdata["page"]!=""){
				$startlimit=$searchdata["page"];	
			}
			else{
				$startlimit=0;
			}
		}
		
		$this->db->select("blog_comments.*,blogs.*,blog_comments.id as blog_comments_id,blog_comments.status as blog_comments_status");
		$this->db->from("blog_comments");
		$this->db->join("blogs","blogs.id=blog_comments.blog_id");
		if(isset($searchdata["search"]) && $searchdata["search"]!="" && $searchdata["search"]!="search"){
			$this->db->like("blogs.blog_title_english",$searchdata["search"]);	
		}	
		foreach($searchdata as $key=>$val){
			if(isset($searcharray[$key]) && $searchdata[$key]!=""){
				if(array_key_exists($key,$searcharray)){
					$where=array($searcharray[$key]=>$val);
					$this->db->where($where);
				}
			}
		}		
		$where=array("blog_comments.status <>"=>"4");
		$this->db->where($where);
		$this->db->group_by("blog_comments.id DESC");		
		if(isset($searchdata["per_page"]) && $searchdata["per_page"]!=""){
			if(isset($recordperpage) && $recordperpage!="" && ($startlimit!="" || $startlimit==0)){
				$this->db->limit($recordperpage,$startlimit);
			}
		}
		
		$query = $this->db->get();
		$resultset=$query->result_array();
		return $resultset; 
	}
	public function enable_disable_blog_comment($blogcommentid,$status){
		$this->db->where("id",$blogcommentid);
		$array=array("status"=>$status);
		$this->db->update("blog_comments",$array);		
	}
	
	public function archive_blog_comment($blogcommentid){
		$where=array("id"=>$blogcommentid);
		$array=array("status"=>4);
		$this->db->where($where);
		$this->db->update("blog_comments",$array);
	}
	public function getIndividualBlogComment($blogcommentid){
		$this->db->select("blog_comments.*,blogs.*,blog_comments.id as blog_comments_id");	
		$this->db->from('blog_comments');
		$this->db->join("blogs","blogs.id=blog_comments.blog_id");
		$where=array("blog_comments.id"=>$blogcommentid);
		$this->db->where($where);
		$query = $this->db->get();
		$resultset=$query->row_array();
		return $resultset;		
	}
	public function getUserCommentName($userid){
		//echo $userid;
		$this->db->select("user_name,user_type");	
		$this->db->from('users');
		$where=array("users.id"=>$userid);
		$this->db->where($where);
		$query = $this->db->get();
		$resultset=$query->row_array();
		
		return $resultset['user_name'];
				
	}
	public function getStoreCommentName($storeid){
		//echo $userid;
		$this->db->select("store_name,user_type");	
		$this->db->from('stores');
		$where=array("stores.id"=>$storeid);
		$this->db->where($where);
		$query = $this->db->get();
		$resultset=$query->row_array();
		
		return $resultset['store_name'];
				
	}
	public function getSubStoreCommentName($substoreid){
		$this->db->select("sub_store_name,user_type");	
		$this->db->from('sub_stores');
		$where=array("sub_stores.id"=>$substoreid);
		$this->db->where($where);
		$query = $this->db->get();
		$resultset=$query->row_array();
		
		return $resultset['sub_store_name'];
	}
	
	/*public function getIndividualBlogComment($blogcommentid)
	{
		$this->db->select("blog_comments.*,blogs.*,blog_comments.id as blog_comments_id,users.user_name as name,users.user_type as user_type,stores.store_name as name,stores.user_type as user_type");	
		$this->db->from('blog_comments');
		$this->db->join("blogs","blogs.id=blog_comments.blog_id");
		$this->db->join("users","users.id=blog_comments.user_id OR blog_comments.user_id=''");
		$this->db->join("stores","stores.id=blog_comments.store_id OR blog_comments.store_id=''");
		$where=array("blog_comments.id"=>$blogcommentid);
		$this->db->where($where);
		$query = $this->db->get();
		$resultset=$query->row_array();
		return $resultset;		
	}*/
//=================================================Blog Comment Function End==================================================================//	

	/*public function give_more_data($count)   // For show more comment
	{
		
	 	$this->db->select("*");
		$this->db->from("blog_comments");
		$this->db->join("users","users.id=blog_comments.user_id");
		$this->db->where(array("blog_comments.status"=>"1"));
		//$this->db->where('blog_id',$blog_id,'=');
		$this->db->limit($count);
		//return $resultset=$query->db->last_query();
		$query=$this->db->get();
		
		$resultset=$query->result_array();
		return $resultset;	
	}*/
	/********************************************************Blog function ends***************************************************/
	
	
	
	/*public function removeComment($commentid)
	{
		$this->db->where("id",$commentid);
		$arr["status"]="4";
		return $this->db->update("ugk_blog_comments",$arr);
	}*/

	
}
?>