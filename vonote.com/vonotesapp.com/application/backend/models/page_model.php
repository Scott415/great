<?php 

class page_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }

	/********************************************************Content function starts***************************************************/

	public function getPageData($pagename=""){
		$this->db->select("*");
		$this->db->from("content_pages");
		$this->db->where(array("page_name"=>$pagename,"page_status"=>"1"));
		$query=$this->db->get();
		//echo $this->db->last_query();
		$resultset=$query->row_array(); //print_r($resultset);
		return $resultset; 
	}

	public function getFaqData($pagename=""){
		$this->db->select("*");
		$this->db->from("faqs");
		$this->db->where(array("faq_status"=>"1"));
		$query=$this->db->get();
		//echo $this->db->last_query();
		$resultset=$query->result_array();
		return $resultset; 
	}
	public function updatepagedata($arr=array()){
		$this->db->where("id",$arr["id"]);
		return $this->db->update("content_pages",$arr);	
	}
	public function updatehomepagedata($arr=array()){
		$this->db->where("id",$arr["id"]);
		return $this->db->update("contents",$arr);	
	}

	public function update_contact_information($arr){
		return $this->db->update("contact_information",$arr);	
	}
	
	public function update_user_limit($arr){
		return $this->db->update("users_files_limit",$arr);	
	}
	

	/********************************************************Category function ends***************************************************/
}

?>