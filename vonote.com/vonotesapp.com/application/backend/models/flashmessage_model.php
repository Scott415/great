<?php 
class flashmessage_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
	
	/********************************************************Blog function starts***************************************************/
	
	public function getFlashMassageData($searchdata=array())
	{
		$searcharray=array("status"=>"status","category"=>"blog_category");
		
		if(!isset($searchdata["page"]) || $searchdata["page"]=="")
		{
			$searchdata["page"]=0;	
		}
	    if(!isset($searchdata["countdata"]))
		{	
			if(isset($searchdata["per_page"]) && $searchdata["per_page"]!="")
			{
				$recordperpage=$searchdata["per_page"];	
			}
			else
			{
				$recordperpage=1;
			}
			if(isset($searchdata["page"]) && $searchdata["page"]!="")
			{
				$startlimit=$searchdata["page"];	
			}
			else
			{
				$startlimit=0;
			}
		}
		
		$this->db->select("*,flash_message.id as flash_message_id");
		$this->db->from("flash_message");
		if(isset($searchdata["search"]) && $searchdata["search"]!="" && $searchdata["search"]!="search")
		{
			$this->db->like("flash_message.flash_message_title",$searchdata["search"]);	
		}	
		foreach($searchdata as $key=>$val)
		{
			if(isset($searcharray[$key]) && $searchdata[$key]!="")
			{
				if(array_key_exists($key,$searcharray))
				{
					$where=array($searcharray[$key]=>$val);
					$this->db->where($where);
				}
			}
		}		
		
		$where=array("flash_message.flash_message_status <>"=>"4");
		$this->db->where($where);		
		if(isset($searchdata["per_page"]) && $searchdata["per_page"]!="")
		{
			if(isset($recordperpage) && $recordperpage!="" && ($startlimit!="" || $startlimit==0))
			{
				$this->db->limit($recordperpage,$startlimit);
			}
		}
		
		$query = $this->db->get();
		$resultset=$query->result_array();
		return $resultset; 
	}
	
	
	public function add_edit_flashmessage($flashmessagearray)
	{
		if($flashmessagearray["id"]=="")
		{
			$flashmessagearray["flash_message_time"]=time();
			return $this->db->insert("flash_message",$flashmessagearray);	
		}	
		else
		{
			$flashmessagearray["flash_message_time"]=time();
			$this->db->where("id",$flashmessagearray["id"]);
			return $this->db->update("flash_message",$flashmessagearray);
		}	
	}
	
	public function getIndividualFlashMessage($blogid)
	{
		$this->db->select("*,flash_message.id as flash_message_id");	
		$this->db->from('flash_message');
		$where=array("flash_message.id"=>$blogid,"flash_message.flash_message_status <> "=>"0");
		$this->db->where($where);
		$query = $this->db->get();
		$resultset=$query->row_array();
		return $resultset;		
	}
	
	
	
	public function enable_disable_flashmessage($flashmessageid,$status)
	{
		$this->db->where("id",$flashmessageid);
		$array=array("flash_message_status"=>$status);
		$this->db->update("flash_message",$array);		
	}
	
	public function archive_flashmessage($flashmessageid)
	{
		$where=array("id"=>$flashmessageid);
		$array=array("flash_message_status"=>4);
		$this->db->where($where);
		$this->db->update("flash_message",$array);
	}	
	
}
?>