<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class slideshow extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('slideshow_model');
		$this->load->helper('url');
	}
	public function index()
	{
		$this->manage_slideshow();	
	}	
	public function manage_slideshow()
	{
		$page=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:""; //$this->input->get("page");
		
		if($page == '')
        {
            $page = '0';
        }else{
            if(!is_numeric($page)){
            redirect(BASEURL.'404');
            }else{
            $page = $page;
            }
        }
		
		$config["per_page"] = $this->config->item("perpageitem"); 
		$config['base_url']=base_url()."slideshow/manage_slideshow/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
		$countdata=array();
		$countdata=$_GET;
		$countdata["countdata"]="yes";	
		
		$config['total_rows']=count($this->slideshow_model->getSlideshowData($countdata));   
		$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
		$this->pagination->initialize($config);
		/*--------------------------Paging code ends---------------------------------------------------*/
		$searcharray=array();
		$searcharray=$_GET;
		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		$data["resultset"]=$this->slideshow_model->getSlideshowData($searcharray);
		$data["item"]="Slideshow";
		$data["master_title"]="Manage Slideshow";   
		$data["master_body"]="manage_slideshow"; 
		$this->load->theme('mainlayout',$data);	
	}
	
	public function add_slideshow()
	{
		$data["item"]="Slideshow";
		$data["do"]="add";
		$data["slideshowdata"]=$this->session->flashdata("tempdata");
		$data["master_title"]="Add Slideshow"; 
		$data["master_body"]="add_slideshow";  
		$this->load->theme('mainlayout',$data);	
		
		if($this->uri->segment(4)!=''&& $this->uri->segment(4)=='0')
		{
		header("Refresh:2;url=".base_url()."slideshow/manage_slideshow");
		}
	}
	
	public function edit_slideshow()
	{
		$data["item"]="Slideshow";
		$data["do"]="edit";
		$sld_id=$this->uri->segment(3);
		$data["slideshowdata"]=$this->slideshow_model->getIndividualSlideshow($sld_id);
		$data["master_title"]="Edit Slideshow";  
		$data["master_body"]="add_slideshow"; 
		$this->load->theme('mainlayout',$data);	
		
		if($this->uri->segment(4)!=''&& $this->uri->segment(4)=='2')
		{
		header("Refresh:2;url=".base_url()."slideshow/manage_slideshow");
		}
	}
	
	public function add_slideshow_to_database()
	{
		$arr["id"]=$this->input->post("id");
		$arr["title"]=$this->input->post("title");
		$arr["content"]=$this->input->post("content");
		$arr["links"]=$this->input->post("links");
		$arr["price"]=$this->input->post("price");
		$arr["sld_images"]=$_FILES["userfile"]["name"];
		//$image=$_FILES["userfile"]["name"];
		
		if($arr["sld_images"]!="")
		{
			$arr["sld_images"]=time().".".$this->common->get_extension($_FILES["userfile"]["name"]);
		}
		else
		{
			$arr["sld_images"]=$this->input->post("sld_images");	
		}
		$this->session->set_flashdata("tempdata",$arr);	
		if($this->validations->validate_slideshow_data($arr))
		{
			if($this->slideshow_model->add_edit_slideshow($arr))
			{
				$last_id = $this->db->insert_id();
				if($arr["sld_images"]!=$this->input->post("sld_images"))
				{
					$config['upload_path'] = '../slideshowimages/';
					$config['allowed_types'] = '*';
					$config['file_name']=$arr["sld_images"];
					
					$this->upload->initialize($config);
					if($this->upload->do_upload())
					{
						$err=0;
						
					}		
					else 
					{
						//echo $this->upload->display_errors();die;
						$this->session->set_flashdata("successmsg","There is some error uploading the files to server. Please contact server admin");		
					}		
				}
				if($arr["id"]=="")
				{
					$this->session->set_flashdata("successmsg","Slideshow added succesfully");
					$err=0; // for Slideshow added succesfully
				}
				else
				{
					$this->session->set_flashdata("successmsg","Slideshow updated succesfully");
					$err=2; // for Slideshow updated succesfully
						
				}
			}	
			else
			{
				$this->session->set_flashdata("errormsg","There is error adding Slideshow to data base . Please contact database admin");
				$err=1;
			}
		}
		else
		{
			$err=1;	
		}
		
		if($err==0)
		{
			redirect(base_url()."slideshow/add_slideshow/".$last_id."/".$err);
		}
		else if($err==2)
		{
			redirect(base_url()."slideshow/edit_slideshow/".$arr["id"]."/".$err);
		}
		else
		{
			redirect(base_url()."slideshow/add_slideshow");
			/*if($arr["id"]=="")
			{
				redirect(base_url()."slideshow/add_slideshow");
			}
			else
			{
				redirect(base_url()."slideshow/edit_slideshow/".$arr["id"]);	
			}*/
		}		
	}
	
	public function enable_disable_slideshow()
	{
		$sld_id=$this->uri->segment(3);
		$status=$this->uri->segment(4);
		if($status==0)
		{
			$show_status="deactivated";	
		}	
		else
		{
			$show_status="activated";	
		}
		
		$this->slideshow_model->enable_disable_slideshow($sld_id,$status);
		$this->session->set_flashdata("successmsg","Slideshow ".$show_status." successfully");	
		redirect(base_url()."slideshow/manage_slideshow");
	}
	
	public function archive_slideshow()
	{
		$delid=$this->uri->segment(3);
		if($delid!='')
		{	
			$this->slideshow_model->archive_slideshow($delid);
			$this->session->set_flashdata("successmsg","Slideshow archived successfully");	
			redirect(base_url()."slideshow/manage_slideshow");
		}
		else
		{
			$data=$this->input->post("chk");
			if(!isset($_REQUEST["chk"]) && count($_REQUEST["chk"])==0)
			{
				$this->session->set_flashdata("errormsg","No slideshow selected");	
				redirect(base_url()."slideshow/manage_slideshow");	
			}
			foreach($data as $key=>$val)
			{
				$this->slideshow_model->archive_slideshow($val);
			}
			
			$this->session->set_flashdata("successmsg","Selected slideshow archived successfully");	
			redirect(base_url()."slideshow/manage_slideshow");
		}	
	}
}
?>