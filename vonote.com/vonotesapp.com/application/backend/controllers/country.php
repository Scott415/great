<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class country extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('country_model');
		$this->load->helper('url');
	}
		
	public function index()
	{
		$this->manage_country();	
	}	
	public function manage_country()
	{
		
		$data["item"]="Country";
		$data["master_title"]="Manage country";   
		$data["master_body"]="manage_country";  
		
		$page=isset($_GET["per_page"])?$_GET["per_page"]:""; 
		if($page == ''){
            $page = '0';
        }
		else{
            if(!is_numeric($page)){
            redirect(BASEURL.'404');
            }else{
    	        $page = $page;
            }
        }
		$config["per_page"] = $this->config->item("perpageitem"); 
		$config['base_url']=base_url()."country/manage_country/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
		$countdata=array();
		$countdata=$_GET;
		$countdata["countdata"]="yes";	
		$config['total_rows']=count($this->country_model->getCountrydata($countdata));   
		$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
		$this->pagination->initialize($config);
		/*--------------------------Paging code ends---------------------------------------------------*/

		$searcharray=array();
		$searcharray=$_GET;
		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		 
		$data["resultset"]=$this->country_model->getCountrydata($searcharray);
		$this->load->theme('mainlayout',$data);	
	}
	public function enable_disable_country()
	{
		$countryid=$this->uri->segment(3);
		$status=$this->uri->segment(4);
		if($status==0){
			$show_status="deactivated";	
		}	
		else{
			$show_status="activated";	
		}
		$this->country_model->enable_disable_country($countryid,$status);
		$this->session->set_flashdata("successmsg","Country ".$show_status." successfully");	
		redirect(base_url()."country/manage_country");	
	}
	public function add_country()
	{
		$data["item"]="Country";
		$data["do"]="add";
		
		$data["countrydata"]=$this->session->flashdata("tempdata");
		$data["master_title"]="Add Country";  
		$data["master_body"]="add_country";  
		$this->load->theme('mainlayout',$data);	
		if($this->uri->segment(4)!=''&& $this->uri->segment(4)=='0')
		{
		header("Refresh:4;url=".base_url()."country/manage_country");
		}
	}
	public function edit_country()
	{
		$data["item"]="Country";
		$data["do"]="edit";
		$countryid=$this->uri->segment(3);
		$data["countrydata"]=$this->country_model->getIndividualCountry($countryid);
		$data["master_title"]="Edit Country";  
		$data["master_body"]="add_country";  
		$this->load->theme('mainlayout',$data);	
		if($this->uri->segment(4)!=''&& $this->uri->segment(4)=='2')
		{
		header("Refresh:4;url=".base_url()."country/manage_country");
		}
	}
	public function add_country_to_database()
	{
		$arr["CountryId"]=$this->input->post("CountryId");
		$arr["Country"]=ucfirst(trim($this->input->post("Country")));
		
		$this->session->set_flashdata("tempdata",$arr);
			
		if($this->validations->validate_country_data($arr))
		{
			if($this->country_model->add_edit_country($arr))
			{
				$last_id = $this->db->insert_id();
				if($arr["CountryId"]=="" && $last_id!='')
				{
					$this->session->set_flashdata("successmsg","Country added succesfully");
					$err=0;      // for country added succesfully
					redirect(base_url()."country/add_country/".$last_id."/".$err);
				}
				else
				{
					$this->session->set_flashdata("successmsg","Country updated succesfully");
					$err=2; // for country updated succesfully
					redirect(base_url()."country/edit_country/".$arr["CountryId"]."/".$err."");	
				}
			}
			/*else if($this->country_model->add_edit_country($arr)=='false')
			{
				$this->session->set_flashdata("errormsg","Country Already Exit");
				$err=1;
			}*/
			else
			{
				$this->session->set_flashdata("errormsg","There is error adding country to data base . Please contact database admin");
				$err=1;
			}
		}
		else
		{
			$err=1;
			redirect(base_url()."country/add_country");		
		}
	}
	public function archive_country()
	{
		$delid=$this->uri->segment(3);
		if($delid!='')
		{
			$this->country_model->archive_country($delid);
			$this->session->set_flashdata("successmsg","Blog archived successfully");
			redirect(base_url()."country/manage_country");
		}
		else
		{
			$data=$this->input->post("chk");
			if(!isset($_REQUEST["chk"]) && count($_REQUEST["chk"])==0)
			{
				$this->session->set_flashdata("errormsg","No Country selected");	
				redirect(base_url()."country/manage_country");	
			}
			foreach($data as $key=>$val)
			{
				$this->country_model->archive_country($val);
			}
			
			$this->session->set_flashdata("successmsg","Selected country archived successfully");	
			redirect(base_url()."country/manage_country");
		}
	}
	public function sort_country()
	{
		$data["item"]="Country";
		$data["master_title"]="Manage Country";  
		$data["master_body"]="sort_gen";  
       
		$page=isset($_GET["per_page"])?$_GET["per_page"]:""; 
		if($page == ''){
            $page = '0';
        }
		else{
            if(!is_numeric($page)){
            redirect(BASEURL.'404');
            }else{
    	        $page = $page;
            }
        }
		$config["per_page"] = $this->config->item("perpageitem"); 
		$config['base_url']=base_url()."country/manage_country/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
		$countdata=array();
		$countdata=$_GET;
		$countdata["countdata"]="yes";	
		$config['total_rows']=count($this->country_model->getCountrydata($countdata));   
		$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
		$this->pagination->initialize($config);
		/*--------------------------Paging code ends---------------------------------------------------*/

		$searcharray=array();
		$searcharray=$_GET;
		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		$data["resultset"]=$this->country_model->sort_country($searcharray);
		
		$this->load->view("country/sort_gen",$data);
		
		//$this->load->theme('mainlayout',$data);	
	}
	public function load_ajax()
	{
		$items = $this->input->post('item');	
		$total_items = count($this->input->post('item'));	
		if($this->country_model->sort_country_load($total_items, $items))
		{
			//echo $afftectedRows=$this->db->affected_rows();
			/*if($afftectedRows=$this->db->affected_rows()>'0')
			{
				echo 'Update Successfull';
			}*/
			/*else
			{
				echo 'Unsuccessfull';
			}*/
		}
	}
	
/**********************************************************For State Start**********************************************************************/
public function manage_state()
{
	$data["item"]="State";
	$data["master_title"]="Manage State";  
	$data["master_body"]="manage_state"; 
	
	$page=isset($_GET["per_page"])?$_GET["per_page"]:""; //$this->input->get("page");
	if($page == '')
	{
		$page = '0';
	}
	else
	{
		if(!is_numeric($page))
		{
			redirect(BASEURL.'404');
		}
		else
		{
			$page = $page;
		}
	}
	$config["per_page"] = $this->config->item("perpageitem"); 
	$config['base_url']=base_url()."country/manage_state/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
	$countdata=array();
	$countdata=$_GET;
	$countdata["countdata"]="yes";	
	$config['total_rows']=count($this->country_model->getStatedata($countdata));   
	$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
	$this->pagination->initialize($config);
	/*--------------------------Paging code ends---------------------------------------------------*/

	$searcharray=array();
	$searcharray=$_GET;
	$searcharray["per_page"]=$config["per_page"];
	$searcharray["page"]=$config["uri_segment"];
	$data["resultset"]=$this->country_model->getStatedata($searcharray);
	$this->load->theme('mainlayout',$data);
}
	public function enable_disable_state()
	{
		$stateid=$this->uri->segment(3);
		$status=$this->uri->segment(4);
		if($status==0){
			$show_status="deactivated";	
		}	
		else{
			$show_status="activated";	
		}
		$this->country_model->enable_disable_state($stateid,$status);
		$this->session->set_flashdata("successmsg","State ".$show_status." successfully");	
		redirect(base_url()."country/manage_state");	
	}
	public function add_state()
	{
		$data["item"]="State";
		$data["do"]="add";
		
		$data["statedata"]=$this->session->flashdata("tempdata");
		$data["master_title"]="Add State";  
		$data["master_body"]="add_state";  
		$this->load->theme('mainlayout',$data);	
		if($this->uri->segment(4)!=''&& $this->uri->segment(4)=='0')
		{
		header("Refresh:4;url=".base_url()."country/manage_state");
		}
	}
	public function edit_state()
	{
		$data["item"]="State";
		$data["do"]="edit";
		$stateid=$this->uri->segment(3);
		$data["statedata"]=$this->country_model->getIndividualState($stateid);
		$data["master_title"]="Edit State";   
		$data["master_body"]="add_state"; 
		$this->load->theme('mainlayout',$data);	
		if($this->uri->segment(4)!=''&& $this->uri->segment(4)=='2')
		{
		header("Refresh:4;url=".base_url()."country/manage_state");
		}
	}
	public function add_state_to_database()
	{
		$arr['CountryId']=$this->input->post("CountryId");
		$arr["StateId"]=$this->input->post("StateId");
		$arr["State"]=$this->input->post("State");
		
		$this->session->set_flashdata("tempdata",$arr);
		if($this->validations->validate_state_data($arr))
		{
			if($this->country_model->add_edit_state($arr))
			{
				$last_id = $this->db->insert_id();
				if($arr["StateId"]=="" && $last_id!='')
				{
					$this->session->set_flashdata("successmsg","State added succesfully");
					$err=0;      // for country added succesfully
					redirect(base_url()."country/add_state/".$last_id."/".$err);
				}
				else
				{
					$this->session->set_flashdata("successmsg","State updated succesfully");
					$err=2; // for country updated succesfully
					redirect(base_url()."country/edit_state/".$arr["StateId"]."/".$err."");	
				}
			}
			else
			{
				$this->session->set_flashdata("errormsg","There is error adding blog to data base . Please contact database admin");
				$err=1;
			}
		}
		else
		{
			$err=1;
			redirect(base_url()."country/add_state");		
		}
	}
	public function archive_state()
	{
		$delid=$this->uri->segment(3);
		if($delid!='')
		{
			$this->country_model->archive_state($delid);
			$this->session->set_flashdata("successmsg","State archived successfully");
			redirect(base_url()."country/manage_state");
		}
		else
		{
			$data=$this->input->post("chk");
			if(!isset($_REQUEST["chk"]) && count($_REQUEST["chk"])==0)
			{
				$this->session->set_flashdata("errormsg","No State selected");	
				redirect(base_url()."country/manage_state");	
			}
			foreach($data as $key=>$val)
			{
				$this->country_model->archive_state($val);
			}
			
			$this->session->set_flashdata("successmsg","Selected State archived successfully");	
			redirect(base_url()."country/manage_state");
		}
	}
		
/**********************************************************For State End**********************************************************************/	

/*******************************************For City Start********************************************************************/
 public function manage_city()
 {
	$data["item"]="City";
	$data["master_title"]="Manage City";   
	$data["master_body"]="manage_city";  
	
	$page=isset($_GET["per_page"])?$_GET["per_page"]:""; 
	if($page == '')
	{
		$page = '0';
	}
	else
	{
		if(!is_numeric($page))
		{
			redirect(BASEURL.'404');
		}
		else
		{
			$page = $page;
		}
	}
	$config["per_page"] = $this->config->item("perpageitem"); 
	$config['base_url']=base_url()."country/manage_city/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
	$countdata=array();
	$countdata=$_GET;
	$countdata["countdata"]="yes";	
	$config['total_rows']=count($this->country_model->getCitydata($countdata));   
	$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
	$this->pagination->initialize($config);
	/*--------------------------Paging code ends---------------------------------------------------*/

	$searcharray=array();
	$searcharray=$_GET;
	$searcharray["per_page"]=$config["per_page"];
	$searcharray["page"]=$config["uri_segment"];
	$data["resultset"]=$this->country_model->getCitydata($searcharray);
	$this->load->theme('mainlayout',$data);
 }
	public function enable_disable_city()
	{
		$cityid=$this->uri->segment(3);
		$status=$this->uri->segment(4);
		if($status==0){
			$show_status="deactivated";	
		}	
		else{
			$show_status="activated";	
		}
		$this->country_model->enable_disable_city($cityid,$status);
		$this->session->set_flashdata("successmsg","City ".$show_status." successfully");	
		redirect(base_url()."country/manage_city");	
	}
 public function add_city()
 {
	$data["item"]="City";
	$data["do"]="add";
	$data["citydata"]=$this->session->flashdata("tempdata");
	
	$data["master_title"]="Add City";   
	$data["master_body"]="add_city"; 
	$this->load->theme('mainlayout',$data);	
	if($this->uri->segment(4)!=''&& $this->uri->segment(4)=='0')
	{
	header("Refresh:4;url=".base_url()."country/manage_city");
	}

 }
	public function edit_city()
	{
		$data["item"]="City";
		$data["do"]="edit";
		$cityid=$this->uri->segment(3);
		$data["citydata"]=$this->country_model->getIndividualcity($cityid);
		$data["master_title"]="Edit City";   
		$data["master_body"]="add_city"; 
		$this->load->theme('mainlayout',$data);	
		if($this->uri->segment(4)!=''&& $this->uri->segment(4)=='2')
		{
		header("Refresh:4;url=".base_url()."country/manage_city");
		}
		
	}
 public function add_city_to_database()
	{
		$arr["cityId"]=$this->input->post("cityId");
		$arr["CountryId"]=$this->input->post("CountryId");
		$arr["StateId"]=$this->input->post("StateId");
		$arr["city"]=$this->input->post("city");
		
		$this->session->set_flashdata("tempdata",$arr);
		if($this->validations->validate_city_data($arr))
		{
			if($this->country_model->add_edit_city($arr))
			{
				$last_id = $this->db->insert_id();
				if($arr["cityId"]=="" && $last_id!='')
				{
					$this->session->set_flashdata("successmsg","City added succesfully");
					$err=0;      // for country added succesfully
					redirect(base_url()."country/add_city/".$last_id."/".$err);
				}
				else
				{
					$this->session->set_flashdata("successmsg","City updated succesfully");
					$err=2; // for country updated succesfully
					redirect(base_url()."country/edit_city/".$arr["cityId"]."/".$err."");	
				}
			}
			else
			{
				$this->session->set_flashdata("errormsg","There is error adding blog to data base . Please contact database admin");
				$err=1;
			}
		}
		else
		{
			$err=1;
			redirect(base_url()."country/add_city");		
		}
	}
	public function archive_city()
	{
		$delid=$this->uri->segment(3);
		if($delid!='')
		{
			$this->country_model->archive_city($delid);
			$this->session->set_flashdata("successmsg","City archived successfully");
			redirect(base_url()."country/manage_city");
		}
		else
		{
			$data=$this->input->post("chk");
			if(!isset($_REQUEST["chk"]) && count($_REQUEST["chk"])==0)
			{
				$this->session->set_flashdata("errormsg","No City selected");	
				redirect(base_url()."country/manage_city");	
			}
			foreach($data as $key=>$val)
			{
				$this->country_model->archive_city($val);
			}
			
			$this->session->set_flashdata("successmsg","Selected City archived successfully");	
			redirect(base_url()."country/manage_city");
		}
	}	
/*******************************************For City End**********************************************************************/	

}
	
?>