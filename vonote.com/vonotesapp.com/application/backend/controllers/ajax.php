<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ajax extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('ajax_model');
		$this->load->helper('url');
	}
		
	public function showState($CountryId)
	{
		$CountryId;
		$data["item"]="City";
		$data["master_title"]="Add City";  
		$data["master_body"]="add_city"; 

		$result = $this->ajax_model->showState($CountryId);
		$this->output->set_output(json_encode($result));
	}
}
?>