<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class blogs extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		
		$this->load->model('blog_model');
		$this->load->helper('url');
	}
	public function index(){
		$this->manage_blog();	
	}	
	public function manage_blog(){   
		$page=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:""; //$this->input->get("page");
		
		if($page == ''){
            $page = '0';
        }else{
            if(!is_numeric($page)){
            	redirect(BASEURL.'404');
            }else{
            	$page = $page;
            }
        }
		
		$config["per_page"] = $this->config->item("perpageitem"); 
		$config['base_url']=base_url()."blogs/manage_blog/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
		$countdata=array();
		$countdata=$_GET;
		$countdata["countdata"]="yes";	
		
		$config['total_rows']=count($this->blog_model->getBlogData($countdata));   
		$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
		$this->pagination->initialize($config);
/*--------------------------Paging code ends---------------------------------------------------*/
		$searcharray=array();
		$searcharray=$_GET;
		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		$data["resultset"]=$this->blog_model->getBlogData($searcharray);
		$data["item"]="Blogs";
		$data["master_title"]="Manage blogs";  
		$data["master_body"]="manage_blog";  
		$this->load->theme('mainlayout',$data);	
	}
	
	public function add_blog(){	
		
		$data["item"]="Blogs";
		$data["do"]="add";
		$data["blogdata"]=$this->session->flashdata("tempdata");
		$data["master_title"]="Add blogs";   
		$data["master_body"]="add_blog";  
		$this->load->theme('mainlayout',$data);
		if($this->uri->segment(4)!=''&& $this->uri->segment(4)=='0'){
			header("Refresh:4;url=".base_url()."blogs/manage_blog");
		}
	}
	public function edit_blog(){
		$data["item"]="Blogs";
		$data["do"]="edit";
		$blogid=$this->uri->segment(3);
		$data["blogdata"]=$this->blog_model->getIndividualBlog($blogid);
		$data["master_title"]="Edit blogs";   
		$data["master_body"]="add_blog"; 
		$this->load->theme('mainlayout',$data);	
		if($this->uri->segment(4)!=''&& $this->uri->segment(4)=='2'){
			header("Refresh:4;url=".base_url()."blogs/manage_blog");
		}
	}
	public function add_blog_to_database(){
		$arr["id"]=$this->input->post("id");
		$arr["blog_title"]=$this->input->post("blog_title");
		$arr["blog_content"]=$this->input->post("blog_content");
		$arr["blog_images"]=$_FILES["userfile"]["name"];
		if($arr["blog_images"] != ""){
			$arr["blog_images"]=time().".".$this->common->get_extension($_FILES["userfile"]["name"]);
		}
		else{
			$arr["blog_images"]=$this->input->post("blog_images");	
		}
		$this->session->set_flashdata("tempdata",$arr);	
		if($this->validations->validate_blog_data($arr)){
			if($this->blog_model->add_edit_blog($arr)){
				$last_id = $this->db->insert_id();
				
				if($arr["blog_images"]!=$this->input->post("blog_images")){
					$config['upload_path'] = '../blogimages/';
					$config['allowed_types'] = '*';
					$config['file_name']=$arr["blog_images"];
					$this->upload->initialize($config);
					if($this->upload->do_upload()){
						$err=0;
					}		
					else{
						//echo $this->upload->display_errors();die;
						$this->session->set_flashdata("successmsg","There is some error uploading the files to server. Please contact server admin");		
					}		
				}
				if($arr["id"]=="" && $last_id!=''){
					$this->session->set_flashdata("successmsg","Blog added succesfully");
					$err=0;      // for blog added succesfully
					redirect(base_url()."blogs/add_blog/".$last_id."/".$err);
				}
				else{
					$this->session->set_flashdata("successmsg","Blog updated succesfully");
					$err=2; // for blog updated succesfully
					redirect(base_url()."blogs/edit_blog/".$arr["id"]."/".$err."");	
				}
			}	
			else{
				$this->session->set_flashdata("errormsg","There is error adding blog to data base . Please contact database admin");
				$err=1;
			}
		}
		else{
			$err=1;
			redirect(base_url()."blogs/add_blog");		
		}
	}
	
	public function enable_disable_blog(){
		$blogid=$this->uri->segment(3);
		$status=$this->uri->segment(4);
		if($status==0){
			$show_status="deactivated";	
		}	
		else{
			$show_status="activated";	
		}
		
		$this->blog_model->enable_disable_blog($blogid,$status);
		$this->session->set_flashdata("successmsg","Blog ".$show_status." successfully");	
		redirect(base_url()."blogs/manage_blog");
	}
	
	public function archive_blog(){
		$delid=$this->uri->segment(3);
		if($delid!=''){	
			$this->blog_model->archive_blog($delid);
			$this->session->set_flashdata("successmsg","Blog archived successfully");	
			redirect(base_url()."blogs/manage_blog");
		}
		else{
			$data=$this->input->post("chk");
			if(!isset($_REQUEST["chk"]) && count($_REQUEST["chk"])==0){
				$this->session->set_flashdata("errormsg","No blog selected");	
				redirect(base_url()."blogs/manage_blog");	
			}
			foreach($data as $key=>$val){
				$this->blog_model->archive_blog($val);
			}
			
			$this->session->set_flashdata("successmsg","Selected blogs archived successfully");	
			redirect(base_url()."blogs/manage_blog");
		}	
	}
	
	public function view_blog(){
		$blogid=$this->uri->segment(3);
		if($blogid==""){
			redirect(base_url()."invalidpage");				
		}
		else{
			$data["resultset"]=$this->blog_model->getIndividualBlog($blogid);	
		}
		
		$data["master_title"]="View blog";  
		$data["master_body"]="view_blog";  
		$this->load->theme('mainlayout',$data);  
	}
//==================================================Blog Comments Section start=============================================================================//	
	public function manage_blog_comment(){   
		$page=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:""; //$this->input->get("page");
		
		if($page == ''){
            $page = '0';
        }else{
            if(!is_numeric($page)){
           	 redirect(BASEURL.'404');
            }else{
           	 $page = $page;
            }
        }
		
		$config["per_page"] = $this->config->item("perpageitem"); 
		$config['base_url']=base_url()."blogs/manage_blog_comment/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
		$countdata=array();
		$countdata=$_GET;
		$countdata["countdata"]="yes";	
		
		$config['total_rows']=count($this->blog_model->getBlogCommentData($countdata));   
		$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
		$this->pagination->initialize($config);
		/*--------------------------Paging code ends---------------------------------------------------*/
		$searcharray=array();
		$searcharray=$_GET;
		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		$data["resultset"]=$this->blog_model->getBlogCommentData($searcharray);
		$data["item"]="Blogs comments";
		$data["master_title"]="Manage blogs comment";   
		$data["master_body"]="manage_blog_comment";  
		$this->load->theme('mainlayout',$data);	
	}
	public function enable_disable_blog_comment(){
		$blogid=$this->uri->segment(3);
		$status=$this->uri->segment(4);
		if($status==0){
			$show_status="deactivated";	
		}	
		else{
			$show_status="activated";	
		}
		
		$this->blog_model->enable_disable_blog_comment($blogid,$status);
		$this->session->set_flashdata("successmsg","Blog comment ".$show_status." successfully");	
		redirect(base_url()."blogs/manage_blog_comment");
	}
	
	public function archive_blog_comment(){
		$delid=$this->uri->segment(3);
		if($delid != ''){	
			$this->blog_model->archive_blog_comment($delid);
			$this->session->set_flashdata("successmsg","Blog archived successfully");	
			redirect(base_url()."blogs/manage_blog_comment");
		}
		else{
			$data=$this->input->post("chk");
			if(!isset($_REQUEST["chk"]) && count($_REQUEST["chk"])==0){
				$this->session->set_flashdata("errormsg","No blog selected");	
				redirect(base_url()."blogs/manage_blog_comment");	
			}
			foreach($data as $key=>$val){
				$this->blog_model->archive_blog_comment($val);
			}
			
			$this->session->set_flashdata("successmsg","Selected blogs archived successfully");	
			redirect(base_url()."blogs/manage_blog_comment");
		}	
	}
	
	public function view_blog_comment(){
		$blogcommentid=$this->uri->segment(3);
		if($blogcommentid==""){
			redirect(base_url()."invalidpage");				
		}
		else{
			$data["resultset"]=$this->blog_model->getIndividualBlogComment($blogcommentid);	
		}
		
		$data["master_title"]="View blog";   
		$data["master_body"]="view_blog_comment";  
		$this->load->theme('mainlayout',$data); 
	}
//==================================================Blog Comments Section End=============================================================================//

	/**********************************************************Category functions ************************************************/
	
	/*public function manage_category()
	{
		$page=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:""; //$this->input->get("page");
		
		if($page == '')
        {
            $page = '0';
        }else{
            if(!is_numeric($page)){
            redirect(BASEURL.'404');
            }else{
            $page = $page;
            }
        }
		
		$config["per_page"] = $this->config->item("perpageitem"); 
		//$config['base_url'] = site_url("shippings/manage_shipping/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]));	
		$config['base_url']=base_url()."blogs/manage_category/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
		$countdata=array();
		$countdata=$_GET;
		$countdata["countdata"]="yes";	
		
		$config['total_rows']=count($this->blog_model->getCategoryData($countdata));   
		$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
		$this->pagination->initialize($config);
		/*--------------------------Paging code ends---------------------------------------------------*/
		/*$searcharray=array();
		$searcharray=$_GET;
		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		$data["resultset"]=$this->blog_model->getCategoryData($searcharray);
		$data["item"]="Category";
		$data["master_title"]="Manage category";   // Please enter the title of page......
		$data["master_body"]="manage_category";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);	
	}*/
	
	/*public function add_category()
	{
		$data["do"]="add";
		$data["item"]="Category";
		$data["categorydata"]=$this->session->flashdata("tempdata");
		$data["master_title"]="Add Category";   // Please enter the title of page......
		$data["master_body"]="add_category";  //  Please use view name in this field please do not include '.php' for including view name	
		$this->load->theme('mainlayout',$data);  // Loading theme
	}*/
	
	/*public function edit_category()
	{
		$data["do"]="edit";
		$categoryid=$this->uri->segment(3);
		$data["categorydata"]=$this->blog_model->getIndividualCategory($categoryid);	
		$data["master_title"]="Edit Category";   // Please enter the title of page......
		$data["master_body"]="add_category";  //  Please use view name in this field please do not include '.php' for including view name	
		$this->load->theme('mainlayout',$data);  // Loading theme
	}*/
	
	/*public function add_category_to_database()
	{
		$arr["id"]=$this->input->post("id");
		$arr["category_name"]=$this->input->post("category_name");	
		$this->session->set_flashdata("tempdata",$arr);	
		if($this->validations->validate_blog_category($arr))
		{
			if($this->blog_model->add_edit_category($arr))
			{
				$err=0;	
				$this->session->set_flashdata("successmsg","Category added succesfully");
			}	
			else
			{
				$this->session->set_flashdata("errormsg","There is error adding category to data base . Please contact database admin");
				$err=1;
			}
		}
		else
		{
			$err=1;	
		}
		
		if($err==0)
		{
			redirect(base_url()."blogs/manage_category");		
		}
		else
		{
			if($arr["id"]=="")
			{
				redirect(base_url()."blogs/add_category");
			}
			else
			{
				redirect(base_url()."blogs/edit_category/".$arr["id"]);	
			}
		}	
	}*/
	
	/*public function archive_category()
	{
		$delid=$this->uri->segment(3);
		if($delid!='')
		{	
			$this->blog_model->archive_category($delid);
			$this->session->set_flashdata("successmsg","Category archived successfully");	
			redirect(base_url()."blogs/manage_category");
		}
		else
		{
			$data=$this->input->post("chk");
			if(!isset($_REQUEST["chk"]) && count($_REQUEST["chk"])==0)
			{
				$this->session->set_flashdata("errormsg","No category selected");	
				redirect(base_url()."blogs/manage_category");	
			}
			foreach($data as $key=>$val)
			{
				$this->blog_model->archive_category($val);
			}
			
			$this->session->set_flashdata("successmsg","Selected categories archived successfully");	
			redirect(base_url()."blogs/manage_category");
		}	
	}*/
	
	/*public function enable_disable_category()
	{
		$categoryid=$this->uri->segment(3);
		$status=$this->uri->segment(4);
		if($status==0)
		{
			$show_status="deactivated";	
		}	
		else
		{
			$show_status="activated";	
		}
		
		$this->blog_model->enable_disable_category($categoryid,$status);
		$this->session->set_flashdata("successmsg","Category ".$show_status." successfully");	
		redirect(base_url()."blogs/manage_category");
	}*/
	
	/**********************************************************Category functions end ************************************************/
}
?>