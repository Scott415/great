<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class orders extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('order_model');
		$this->load->model('store_model');
		$this->load->model('product_model');
		$this->load->model('shipping_model');
		$this->load->model('payment_model');
	}
	
	public function index()
	{
	$this->manage_order();	
	}
	
	/***********************************************Order function starts **************************************************************/
	
	public function manage_order()
	{
		$data["item"]="Order";
		$page=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:""; //$this->input->get("page");
		
		if($page == '')
        {
            $page = '0';
        }else{
            if(!is_numeric($page)){
            redirect(BASEURL.'404');
            }else{
            $page = $page;
            }
        }
		
		$config["per_page"] = $this->config->item("perpageitem"); 
		//$config['base_url'] = site_url("shippings/manage_shipping/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]));	
		$config['base_url']=base_url()."orders/manage_order/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
		$countdata=array();
		$countdata=$_GET;
$countdata["countdata"]="yes";	
		
		$config['total_rows']=count($this->order_model->getOrderDataAdmin($countdata));   
		$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
		$this->pagination->initialize($config);
		/*--------------------------Paging code ends---------------------------------------------------*/
		$searcharray=array();
		$searcharray=$_GET;
		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		$data["resultset"]=$this->order_model->getOrderDataAdmin($searcharray);
		$data["master_title"]="Manage Orders";   // Please enter the title of page......
		$data["master_body"]="manage_order";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);  // Loading theme
	}
	
	public function view_order()
	{
		$orderid=$this->uri->segment(3);
		$store_id=$this->uri->segment(4);
		$data["item"]="Order";
		$data["resultset"]=$this->order_model->getIndividualOrderData($orderid,$store_id);
		$data["master_title"]="View Order";   // Please enter the title of page......
		$data["master_body"]="view_order";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);  // Loading theme	
	}
	
	public function manage_transaction()
	{
		$data["item"]="Transactions";
		$page=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:""; //$this->input->get("page");
		
		if($page == '')
        {
            $page = '0';
        }else{
            if(!is_numeric($page)){
            redirect(BASEURL.'404');
            }else{
            $page = $page;
            }
        }
		
		$config["per_page"] = $this->config->item("perpageitem"); 
		$config['base_url']=base_url()."orders/manage_transaction/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
		$countdata=array();
		$countdata=$_GET;
        $countdata["countdata"]="yes";	
		$config['total_rows']=count($this->order_model->getTransactionsData($countdata));   
		$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
		$this->pagination->initialize($config);
		/*--------------------------Paging code ends---------------------------------------------------*/
		$searcharray=array();
		$searcharray=$_GET;
		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		$data["resultset"]=$this->order_model->getTransactionsData($searcharray);
		$data["master_title"]="Manage Transaction";   // Please enter the title of page......
		$data["master_body"]="manage_transaction";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);  // Loading theme	
		
	}
	
	public function refund_transaction()
	{
		$data["item"]="Do refund";
		$data["do"]="add";
		$data["transaction_id"]=$this->uri->segment(3);
		$data["transaction_number"]=$this->order_model->getTransactionNumber($data["transaction_id"]);
		$data["master_title"]="Refund the transaction";   // Please enter the title of page......
		$data["master_body"]="refund_transaction";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);  // Loading theme	
			
	}
	
	public function cancel_transaction()
	{
		$arr["id"]=$this->input->post("id");
		$arr["cancellation_reason"]=$this->input->post("cancellation_reason");
		if($this->validations->validate_transactions($arr))
		{
			if($this->order_model->cancel_transaction($arr))
			{
				$err=0;
				$this->session->set_flashdata("successmsg","Transaction cancelled successfully");	
			}
			else
			{
				$err=1;
				$this->session->set_flashdata("errormsg","There was an error in cancelling this transaction.");
			}
		}	
		else
		{
			$err=1;	
		}
		
		if($err==1)
		{
			redirect(base_url()."orders/refund_transaction/".$arr["id"]);	
		}
		else
		{
			redirect(base_url()."orders/manage_transaction");
		}
	}
	
	public function show_cancellation()
	{
		$transanctionid=$this->uri->segment(3);
		$data["resultset"]=$this->order_model->getTransactionCancellationdetails($transanctionid);
		$data["master_title"]="Cancellation";   // Please enter the title of page......
		$data["master_body"]="show_cancellation";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);  // Loading theme
			
	}
	
	/***********************************************Order function ends **************************************************************/
	
}