<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class dashboard extends CI_Controller {
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		
		$data["master_title"]="Home page";   // Please enter the title of page......
		$data["master_body"]="dashboard";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);
	}
}

