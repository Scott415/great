<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class faqs extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('faq_model');
	}
		
	public function index()
	{
		$this->manage_blog();	
	}	
	
	public function manage_faq()
	{
		$page=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:""; //$this->input->get("page");
		
		if($page == '')
        {
            $page = '0';
        }else{
            if(!is_numeric($page)){
            redirect(BASEURL.'404');
            }else{
            $page = $page;
            }
        }
		
		$config["per_page"] = $this->config->item("perpageitem"); 
		//$config['base_url'] = site_url("shippings/manage_shipping/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]));	
		$config['base_url']=base_url()."faqs/manage_faq/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
		$countdata=array();
		$countdata=$_GET;
		$countdata["countdata"]="yes";	
		
		$config['total_rows']=count($this->faq_model->getFaqData($countdata));   
		$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
		$this->pagination->initialize($config);
		/*--------------------------Paging code ends---------------------------------------------------*/
		$searcharray=array();
		$searcharray=$_GET;
		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		$data["resultset"]=$this->faq_model->getFaqData($searcharray);
		$data["item"]="Faq";
		$data["master_title"]="Manage faq";   // Please enter the title of page......
		$data["master_body"]="manage_faq";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);	
	}
	
	public function archive_faq()
	{
		$faqid=$this->uri->segment(3);
		if($faqid!='')
		{	
			$this->faq_model->archive_faq($faqid);
			$this->session->set_flashdata("successmsg","Faq archived successfully");	
			redirect(base_url()."faqs/manage_faq");
		}
		else
		{
			$data=$this->input->post("chk");
			if(!isset($_REQUEST["chk"]) && count($_REQUEST["chk"])==0)
			{
				$this->session->set_flashdata("errormsg","No faqs selected");	
				redirect(base_url()."faqs/manage_faq");	
			}
			foreach($data as $key=>$val)
			{
				$this->faq_model->archive_faq($val);
			}
			
			$this->session->set_flashdata("successmsg","Selected faqs archived successfully");	
			redirect(base_url()."faqs/manage_faq");
		}	
	}
	
	public function enable_disable_faq()
	{
		$faqid=$this->uri->segment(3);
		$status=$this->uri->segment(4);
		if($status==0)
		{
			$show_status="deactivated";	
		}	
		else
		{
			$show_status="activated";	
		}
		
		$this->faq_model->enable_disable_faq($faqid,$status);
		$this->session->set_flashdata("successmsg","Faq ".$show_status." successfully");	
		redirect(base_url()."faqs/manage_faq");
	}
	
	public function view_faq()
	{
		$faqid=$this->uri->segment(3);
		if($faqid=="")
		{
			redirect(base_url().$this->config->item("error_page"));				
		}
		else
		{
			$data["resultset"]=$this->faq_model->getIndividualFaq($faqid);	
		}
		
		$data["master_title"]="View faq";   // Please enter the title of page......
		$data["master_body"]="view_faq";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);  // Loading theme
	}
	
	public function add_faq()
	{
		$data["item"]="Faq";
		$data["do"]="add";	
		$data["faqdata"]=$this->session->flashdata("tempdata");
		$data["master_title"]="Add faq";   // Please enter the title of page......
		$data["master_body"]="add_faq";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);  // Loading theme
	}
	
	public function edit_faq()
	{
		$data["item"]="Faq";
		$data["do"]="edit";	
		$faqid=$this->uri->segment(3);
		$data["faqdata"]=$this->faq_model->getIndividualFaq($faqid);
		$data["master_title"]="Add faq";   // Please enter the title of page......
		$data["master_body"]="add_faq";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);  // Loading theme
	
	}
	
	
	public function add_faq_to_database()
	{
		$arr["id"]=$this->input->post("id");
		$arr["faq_question"]=$this->input->post("faq_question");
		$arr["faq_answer"]=$this->input->post("faq_answer");
		$this->session->set_flashdata("tempdata",$arr);
		if($this->validations->validate_faqdata($arr))
		{
			if($this->faq_model->add_edit_faq($arr))
			{
				if($arr["id"]=="")
				{
					$this->session->set_flashdata("successmsg","Faq added successfully");	
					$err=0;
				}	
				else
				{
					$this->session->set_flashdata("successmsg","Faq updated successfully");
					$err=0;				}
			}	
			else 
			{
				if($arr["id"]=="")
				{
					$this->session->set_flashdata("errormsg","There was an error in adding the faq.");
					$err=1;	
				}	
				else
				{
					$this->session->set_flashdata("errormsg","There was an error in updating the faq.");	
					$err=1;
				}	
			}
		}
		
		else
		{
			$err=1;	
		}		
		
		if($err==1)
		{
			if($arr["id"]=="")
			{
				redirect(base_url()."faqs/add_faq");	
			}
			else 
			{
				redirect(base_url()."faqs/edit_faq/".$arr["id"]);
			}	
		}
		else
		{
			redirect(base_url()."faqs/manage_faq");
		}
	}
		
	/**********************************************************Category functions end ************************************************/
}
?>