<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
	}
/**********************************************Cron job function needs to added here **********************************************/
	function checkplanstatus()
	{
		$this->common->checkplanexistence();	
	}
/**********************************************Cron job function ends ***************************************************************/

/***********************************************Login function starts **************************************************************/
	public function index()
	{
		$captcha_result = '';
		$data["cap_img"] = $this -> _make_captcha();
		/*if ( $this -> input -> post( 'submit' ) ) {
		  if ( $this -> _check_capthca() ) {
			$captcha_result = 'GOOD';
		  }else {
			$captcha_result = 'BAD';
		  }
		}
		$data["cap_msg"] = $captcha_result;*/
		$data["master_title"]="Login";   // Please enter the title of page......
		$this->load->theme('login',$data);  // Loading theme		
	}
	function _make_captcha()
  {
    $this->load->helper('captcha');
    $vals = array(
      
      'img_path' => './captcha/', // PATH for captcha ( *Must mkdir (htdocs)/captcha )
      'img_url' => 'http://112.196.33.85/solitaire/demo/motorbike/admin/captcha/', // URL for captcha img
      'img_width' => 200, // width
      'img_height' => 60, // height
      'font_path'     => '../system/fonts/2.ttf',
      #'font_path' => BASEPATH . 'fonts/sazanami-gothic.ttf', // custom font ( not in Original CI )
      'expiration' => 7200 , 
      ); 
    // Create captcha
    $cap = create_captcha( $vals ); 
    // Write to DB
    if ( $cap ) {
      $data = array( 'captcha_id' => '',
        'captcha_time' => $cap['time'],
        'ip_address' => $this -> input -> ip_address(),
        'word' => $cap['word'] , 
        );
      $query = $this -> db -> insert_string( 'captcha', $data );
      $this -> db -> query( $query );
    }else {
      return "Umm captcha not work" ;
    }
    return $cap['image'] ;
  }

  function _check_capthca()
  { 
   // var_dump( $_POST );//debug

    // Delete old data ( 2hours)
    $expiration = time()-7200 ;

    $this -> db -> where( 'captcha_time < ', $expiration );
    $this -> db -> delete( 'captcha' );

   // var_dump( $this -> db -> last_query() ); 

    // Check input data
    $this -> db -> select( 'count(*) as count' );
    $this -> db -> where( 'word', $this -> input -> post( 'captcha' ) );
    $this -> db -> where( 'ip_address', $this -> input -> ip_address() );
    $this -> db -> where( 'captcha_time > ', $expiration );
    $query = $this -> db -> get( 'captcha' );
    $row = $query -> row();

   // var_dump( $this -> db -> last_query() );

    if ( $row -> count > 0 ) {
      return true;
    }
    return false;

  }
  
  // for captcha
  public function create_captcha1(){
$line_colors = preg_split("/,\s*?/", CODE_LINE_COLORS);
$char_colors = preg_split("/,\s*?/", CODE_CHAR_COLORS);
$fonts = collect_files(PATH_TTF, "ttf");

$img = imagecreatetruecolor(CODE_WIDTH, CODE_HEIGHT);
imagefilledrectangle($img, 0, 0, CODE_WIDTH - 1, CODE_HEIGHT - 1, gd_color(CODE_BG_COLOR));


// Draw lines

for ($i = 0; $i < CODE_LINES_COUNT; $i++)
    imageline($img,
        rand(0, CODE_WIDTH - 1),
        rand(0, CODE_HEIGHT - 1),
        rand(0, CODE_WIDTH - 1),
        rand(0, CODE_HEIGHT - 1),
        gd_color($line_colors[rand(0, count($line_colors) - 1)])
    );


// Draw code

$code = "";
$y = (CODE_HEIGHT / 2) + (CODE_FONT_SIZE / 2);
for ($i = 0; $i < CODE_CHARS_COUNT; $i++) {
    $color = gd_color($char_colors[rand(0, count($char_colors) - 1)]);
    $angle = rand(-30, 30);
    $char = substr(CODE_ALLOWED_CHARS, rand(0, strlen(CODE_ALLOWED_CHARS) - 1), 1);
    $font = PATH_TTF . "/" . $fonts[rand(0, count($fonts) - 1)];
    $x = (intval((CODE_WIDTH / CODE_CHARS_COUNT) * $i) + (CODE_FONT_SIZE / 2));
    $code .= $char;
    imagettftext($img, CODE_FONT_SIZE, $angle, $x, $y, $color, $font, $char);
}

$_SESSION['__img_code__'] = md5($code);

header("Content-Type: image/png");
imagepng($img);
imagedestroy($img);


function gd_color($html_color) {
    return preg_match('/^#?([\dA-F]{6})$/i', $html_color, $rgb)
      ? hexdec($rgb[1]) : false;
}


function collect_files($dir, $ext) {
    if (false !== ($dir = opendir($dir))) {
        $files = array();

        while (false !== ($file = readdir($dir)))
            if (preg_match("/\\.$ext\$/i", $file))
                $files[] = $file;

        return $files;

    } else
        return false;
}
  }
	public function check_login()
    {
		
		$arr["username"]=$this->input->post("username");
		$arr["password"]=$this->input->post("password");
		
		 $username=$this->login_model->check_admin_login($arr);

		if(isset($username) && $username!="")
		{
			$err=0;
			$this->session->set_userdata("username",$username);
		}	
		else
		{
			$err=1;	
			$this->session->set_flashdata("errormsg","Wrong user name and password");
		}
		if($err==1)
		{
			redirect(base_url()."login");	
		}
		else
		{
			redirect(base_url()."dashboard");	
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		$this->session->set_flashdata("successmsg","Log out successfully");	
		redirect(base_url()."login");
	}
	/***********************************************Login function ends **************************************************************/
}