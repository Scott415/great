<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('login_model');
	}

	/**********************************************Cron job function needs to added here **********************************************/

	/*function checkplanstatus(){
		$this->common->checkplanexistence();	
	}*/

	/**********************************************Cron job function ends ***************************************************************/

	/***********************************************Login function starts **************************************************************/
	
	public function index(){
		//$this->check_login();
		$data["master_title"] = "Login";   // Please enter the title of page......
		$this->load->theme('login',$data);  // Loading theme		
	}
	public function check_login(){
		$arr["username"]=$this->input->post("username");
		$arr["password"]=$this->input->post("password");
		
		
		$username=$this->login_model->check_admin_login($arr);
	
		if(isset($username) && $username!=""){
			$err=0;
			$this->session->set_userdata("username",$username);
		}	
		else{
			$err=1;	
			$this->session->set_flashdata("errormsg","Wrong user name and password");
		}

		if($err==1){
			redirect(base_url()."login");	
		}
		else{
			redirect(base_url()."dashboard");	
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		$this->session->set_flashdata("successmsg","Log out successfully");	
		redirect(base_url()."login");
	}

	/***********************************************Login function ends **************************************************************/
}