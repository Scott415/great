<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class flashmessage extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('flashmessage_model');
		$this->load->helper('url');
	}
		
	public function index()
	{
		$this->manage_flash_message();	
	}	
	public function manage_flashmessage()
	{   
		$page=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:""; //$this->input->get("page");
		
		if($page == '')
        {
            $page = '0';
        }else{
            if(!is_numeric($page)){
            redirect(BASEURL.'404');
            }else{
            $page = $page;
            }
        }
		
		$config["per_page"] = $this->config->item("perpageitem"); 
		$config['base_url']=base_url()."flashmessage/manage_flashmessage/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
		$countdata=array();
		$countdata=$_GET;
		$countdata["countdata"]="yes";	
		
		$config['total_rows']=count($this->flashmessage_model->getFlashMassageData($countdata));   
		$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
		$this->pagination->initialize($config);
		/*--------------------------Paging code ends---------------------------------------------------*/
		$searcharray=array();
		$searcharray=$_GET;
		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		$data["resultset"]=$this->flashmessage_model->getFlashMassageData($searcharray);
		$data["item"]="Flash Message";
		$data["master_title"]="Manage Flash Message";  
		$data["master_body"]="manage_flashmessage"; 
		$this->load->theme('mainlayout',$data);	
	}
	
	public function add_flashmessage()
	{	
		$data["item"]="Flash Massage";
		$data["do"]="add";
		$data["flashmessagedata"]=$this->session->flashdata("tempdata");
		$data["master_title"]="Add Flash Massage";  
		$data["master_body"]="add_flashmessage";  
		$this->load->theme('mainlayout',$data);
		if($this->uri->segment(4)!=''&& $this->uri->segment(4)=='0')
		{
		header("Refresh:4;url=".base_url()."flashmessage/manage_flashmessage");
		}
	}
	public function edit_flashmessage()
	{
		$data["item"]="Flash Massage";
		$data["do"]="edit";
		$flashmessageid=$this->uri->segment(3);
		$data["flashmessagedata"]=$this->flashmessage_model->getIndividualFlashMessage($flashmessageid);
		$data["master_title"]="Edit Flash Message";  
		$data["master_body"]="add_flashmessage"; 
		$this->load->theme('mainlayout',$data);	
		if($this->uri->segment(4)!=''&& $this->uri->segment(4)=='2')
		{
		header("Refresh:4;url=".base_url()."flashmessage/manage_flashmessage");
		}
	}
	public function add_flashmessage_to_database()
	{
		
		$arr["id"]=$this->input->post("id");
		$arr["flash_message_title"]=$this->input->post("flash_message_title");
		
		$this->session->set_flashdata("tempdata",$arr);	
		if($this->validations->validate_flash_message_data($arr))
		{
			if($this->flashmessage_model->add_edit_flashmessage($arr))
			{
				$last_id = $this->db->insert_id();
				
				if($arr["id"]=="" && $last_id!='')
				{
					$this->session->set_flashdata("successmsg","Flash Message added succesfully");
					$err=0;      // for Flash Message added succesfully
					redirect(base_url()."flashmessage/add_flashmessage/".$last_id."/".$err);
				}
				else
				{
					$this->session->set_flashdata("successmsg","Flash Message updated succesfully");
					$err=2; // for Flash Message updated succesfully
					redirect(base_url()."flashmessage/edit_flashmessage/".$arr["id"]."/".$err."");	
				}
			}	
			else
			{
				$this->session->set_flashdata("errormsg","There is error adding blog to data base . Please contact database admin");
				$err=1;
			}
		}
		else
		{
			$err=1;
			redirect(base_url()."flashmessage/add_flashmessage");		
		}
	}
	public function enable_disable_flashmessage()
	{
		$flashmessageid=$this->uri->segment(3);
		$status=$this->uri->segment(4);
		if($status==0)
		{
			$show_status="deactivated";	
		}	
		else
		{
			$show_status="activated";	
		}
		
		$this->flashmessage_model->enable_disable_flashmessage($flashmessageid,$status);
		$this->session->set_flashdata("successmsg","flash message ".$show_status." successfully");	
		redirect(base_url()."flashmessage/manage_flashmessage");
	}
	public function archive_flashmessage()
	{
		$delid=$this->uri->segment(3);
		if($delid!='')
		{	
			$this->flashmessage_model->archive_flashmessage($delid);
			$this->session->set_flashdata("successmsg","flash message archived successfully");	
			redirect(base_url()."flashmessage/manage_flashmessage");
		}
		else
		{
			$data=$this->input->post("chk");
			if(!isset($_REQUEST["chk"]) && count($_REQUEST["chk"])==0)
			{
				$this->session->set_flashdata("errormsg","No flash message selected");	
				redirect(base_url()."flashmessage/manage_flashmessage");	
			}
			foreach($data as $key=>$val)
			{
				$this->flashmessage_model->archive_flashmessage($val);
			}
			
			$this->session->set_flashdata("successmsg","Selected flash message archived successfully");	
			redirect(base_url()."flashmessage/manage_flashmessage");
		}	
	}
}
?>