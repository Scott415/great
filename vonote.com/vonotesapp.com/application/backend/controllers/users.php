<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class users extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
	}

	public function index(){
		$this->manage_user();	
	}

	/***********************************************User function starts **************************************************************/

	public function manage_user(){
		$data["item"]="User";
		$data["master_title"]="Manage user";   // Please enter the title of page......
		$data["master_body"]="manage_user";  //  Please use view name in this field please do not include '.php' for including view name

			/*--------------------------Paging code starts---------------------------------------------------*/

		$page=isset($_GET["per_page"])?$_GET["per_page"]:""; //$this->input->get("page");
		if($page == ''){
            $page = '0';
        }else{
            if(!is_numeric($page)){
	            redirect(BASEURL.'404');
            }else{
         	   $page = $page;
            }
        }

		$config["per_page"] = $this->config->item("perpageitem"); 
		//$config['base_url'] = site_url("users/manage_user/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]));	
		$config['base_url']=base_url()."users/manage_user/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
		$countdata=array();
		$countdata=$_GET;
		$countdata["countdata"]="yes";	
		$config['total_rows']=count($this->user_model->getUserdata($countdata));   
		$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
		$this->pagination->initialize($config);

		/*--------------------------Paging code ends---------------------------------------------------*/
		$searcharray=array();
		$searcharray=$_GET;
		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		$data["resultset"]=$this->user_model->getUserdata($searcharray);

		#$data["resultset"]=$this->user_model->getUserdata($_GET);
		$this->load->theme('mainlayout',$data);  // Loading theme

	}

	public function enable_disable_user(){
		$userid=$this->uri->segment(3);
		$status=$this->uri->segment(4);
		if($status==0){
			$show_status="deactivated";	
		}	
		else{
			$show_status="activated";	
		}

		$this->user_model->enable_disable_user($userid,$status);
		$this->session->set_flashdata("successmsg","User ".$show_status." successfully");	
		redirect(base_url()."users/manage_user");	

	}

	public function archive_user(){
		$delid=$this->uri->segment(3);
		if($delid != ''){	
			$this->user_model->archive_user($delid);
			$this->session->set_flashdata("successmsg","User removed successfully");	
			redirect(base_url()."users/manage_user");
		}
		else{
			$data=$this->input->post("chk");
			if(!isset($_REQUEST["chk"]) && count($_REQUEST["chk"])==0){
				$this->session->set_flashdata("errormsg","No user selected");	
				redirect(base_url()."users/manage_user");	
			}
			foreach($data as $key=>$val){
				$this->user_model->archive_user($val);
			}

			$this->session->set_flashdata("successmsg","Selected users removed successfully");	
			redirect(base_url()."users/manage_user");
		}
	}
	public function view_files(){
		$floderid=$this->uri->segment(4);
		$rel_id=$this->uri->segment(3);
		$data["item"]="Files";
		$data["master_title"]="Manage files";   // Please enter the title of page......
		$data["master_body"]="view_files";  //  Please use view name in this field please do not include '.php' for including view name

			/*--------------------------Paging code starts---------------------------------------------------*/

		$page=isset($_GET["per_page"])?$_GET["per_page"]:""; //$this->input->get("page");
		if($page == ''){
            $page = '0';
        }else{
            if(!is_numeric($page)){
	            redirect(BASEURL.'404');
            }else{
         	   $page = $page;
            }
        }

		$config["per_page"] = $this->config->item("perpageitem"); 
		//$config['base_url'] = site_url("users/manage_user/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]));	
		$config['base_url']=base_url()."users/view_files/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
		$countdata=array();
		$countdata=$_GET;
		$countdata["countdata"]="yes";
		$countdata["floderid"]=$floderid;	
		$countdata["rel_id"]=$rel_id;
		$config['total_rows']=count($this->user_model->getUserFiles($countdata));   
		$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
		$this->pagination->initialize($config);

		/*--------------------------Paging code ends---------------------------------------------------*/
		$searcharray=array();
		$searcharray=$_GET;
		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		$searcharray["floderid"] = $floderid;
		$searcharray["rel_id"] = $rel_id;
		$data["resultset"]=$this->user_model->getUserfiles($searcharray);
		//print_r($resultset);

		#$data["resultset"]=$this->user_model->getUserdata($_GET);
		$this->load->theme('mainlayout',$data);  // Loading theme

	}
	public function view_file(){
		$floderid=$this->uri->segment(4);
		$rel_id=$this->uri->segment(3);
		$fid=$this->uri->segment(5);
		$data["item"]="Files";
		$data["master_title"]="View files";   // Please enter the title of page......
		$data["master_body"]="view_file";  //  Please use view name in this field please do not include '.php' for including view name

		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		$searcharray["floderid"] = $floderid;
		$searcharray["rel_id"] = $rel_id;
		$searcharray["fid"] = $fid;
		$data["resultset"]=$this->user_model->getUserfile($searcharray);
		//print_r($resultset);

		#$data["resultset"]=$this->user_model->getUserdata($_GET);
		$this->load->theme('mainlayout',$data);  // Loading theme

	}
	public function enable_disable_files(){
		$userid=$this->uri->segment(3);
		$fid=$this->uri->segment(4);
		$status=$this->uri->segment(5);
		if($status==0){
			$show_status="deactivated";	
		}	
		else{
			$show_status="activated";	
		}

		$this->user_model->enable_disable_files($userid,$status);
		$this->session->set_flashdata("successmsg","Files ".$show_status." successfully");	
		redirect(base_url()."users/view_files/".$userid."/".$fid);	

	}

	public function archive_files(){
		$userid=$this->uri->segment(3);
		$fid=$this->uri->segment(4);
		$delid=$this->uri->segment(5);
		if($delid != ''){	
			$this->user_model->archive_files($delid);
			$this->session->set_flashdata("successmsg","files removed successfully");	
			redirect(base_url()."users/view_files/".$userid."/".$fid);
		}
		else{
			$data=$this->input->post("chk");
			if(!isset($_REQUEST["chk"]) && count($_REQUEST["chk"])==0){
				$this->session->set_flashdata("errormsg","No files selected");	
				redirect(base_url()."users/view_files/".$userid."/".$fid);	
			}
			foreach($data as $key=>$val){
				$this->user_model->archive_files($val);
			}

			$this->session->set_flashdata("successmsg","Selected files removed successfully");	
			redirect(base_url()."users/view_files/".$userid."/".$fid);
		}
	}

	public function view_user(){
		$arr['user_id'] = $this->uri->segment(3); 
		if($arr['user_id'] == "" || $arr['user_id' ]== 0){
			$data["master_title"]="Page not found";   // Please enter the title of page......
			$data["master_body"]=$this->config->item("error_page");  //  Please use view name in this field please do not include '.php' for including view name
		}
		else{
			$data["resultset"]=$this->user_model->view_user_profile_data($arr);
			//print_r($data["resultset"]['id']); die;	
		$id=$data['resultset']['id'];
		}
		$SIZE_LIMIT =  $this->user_model->user_limit();
	
		$SIZE_LIMIT = ($SIZE_LIMIT)*1024*1024; // 5 GB
		$path=$_SERVER['DOCUMENT_ROOT']."/userdata/".$id;
		$disk_used = $this->common->foldersize($path);
	
		$disk_remaining = $SIZE_LIMIT - $disk_used;

 
   // echo('diskspace used: ' . $this->common->format_size($disk_used) . '<br>');
   //echo( 'diskspace left: ' . $this->common->format_size($disk_remaining) . '<br><hr>');

		$data['used']= $this->common->format_size($disk_used);
		$data['disk_remaining']= $this->common->format_size($disk_remaining);
		
		$data["master_title"]="View user";   // Please enter the title of page......
		$data["master_body"]="view_user";  //  Please use view name in this field please do not include '.php' for including view name
		$this->load->theme('mainlayout',$data);  // Loading theme	
	}

//list of floders created by user
	public function view_folders(){
		$data["item"]="User's Folders";
		$data["master_title"]="Manage user Folders";   // Please enter the title of page......
		$data["master_body"]="view_folders";  //  Please use view name in this field please do not include '.php' for including view name

			/*--------------------------Paging code starts---------------------------------------------------*/

		$page=isset($_GET["per_page"])?$_GET["per_page"]:""; //$this->input->get("page");
		if($page == ''){
            $page = '0';
        }else{
            if(!is_numeric($page)){
	            redirect(BASEURL.'404');
            }else{
         	   $page = $page;
            }
        }

		$config["per_page"] = $this->config->item("perpageitem"); 
		$config['base_url']=base_url()."users/view_folders/?".$this->common->removeUrl("per_page",$_SERVER["QUERY_STRING"]);
		$countdata=array();
		$countdata=$_GET;
		$countdata["countdata"]="yes";	
		$config['total_rows']=count($this->user_model->getUserFolders($countdata));   
		$config["uri_segment"]=(isset($_GET["per_page"]) && $_GET["per_page"]!="")?$_GET["per_page"]:"0";
		$this->pagination->initialize($config);

		/*--------------------------Paging code ends---------------------------------------------------*/
		$searcharray=array();
		$searcharray=$_GET;
		$searcharray["per_page"]=$config["per_page"];
		$searcharray["page"]=$config["uri_segment"];
		$data["resultset"]=$this->user_model->getUserFolders($searcharray);
		//$data["userdata"]=$this->user_model->getUserdata($countdata);

		#$data["resultset"]=$this->user_model->getUserdata($_GET);
		$this->load->theme('mainlayout',$data);  // Loading theme

	}
	public function enable_disable_folders(){
		$userid=$this->uri->segment(3);
		$status=$this->uri->segment(4);
		if($status==0){
			$show_status="deactivated";	
		}	
		else{
			$show_status="activated";	
		}

		$this->user_model->enable_disable_folders($userid,$status);
		$this->session->set_flashdata("successmsg","Folders ".$show_status." successfully");	
		redirect(base_url()."users/view_folders");	

	}

	public function archive_folders(){
		$delid=$this->uri->segment(3);
		if($delid != ''){	
			$this->user_model->archive_user($delid);
			$this->session->set_flashdata("successmsg","Folder removed successfully");	
			redirect(base_url()."users/view_folders");
		}
		else{
			$data=$this->input->post("chk");
			if(!isset($_REQUEST["chk"]) && count($_REQUEST["chk"])==0){
				$this->session->set_flashdata("errormsg","No folders selected");	
				redirect(base_url()."users/view_folders");	
			}
			foreach($data as $key=>$val){
				$this->user_model->archive_folders($val);
			}

			$this->session->set_flashdata("successmsg","Selected folders removed successfully");	
			redirect(base_url()."users/view_folders");
		}
	}
	
	public function user_limit(){
		//$pagename=$this->uri->segment(3);

			//$data["do"]="edit";
			//$data["resultset"]=$this->common->gethomepagedata($pagename);
			$data["item"]="Homepage Content";
			$data["master_title"]="Manage Homepages";   // Please enter the title of page......
			$data["master_body"]="users_limit";  //  Please use view name in this field please do not include '.php' for including view name
			$this->load->theme('mainlayout',$data);	  // Loading theme for admin panel
		
	}
	

	/***********************************************User function ends **************************************************************/	
}