<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pages extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('page_model');
		$this->load->helper('url');
		$this->load->library('upload');	
	}

/**********************************************************Page functions starts ************************************************/
	public function index(){
		$this->manage_page();	
	}
		
//for content pages data like about us, privacy police, terme and conditions, career	
	public function manage_page(){
	$pagename=$this->uri->segment(3);
	
		if($pagename=="contact_info"){
			$data["do"]="edit";
			$data["resultset"]=$this->common->getContactInformation($pagename);
			$data["item"]="Contact info";
			$data["master_title"]="Contact info";   // Please enter the title of page......
			$data["master_body"]="contact_info";  //  Please use view name in this field please do not include '.php' for including view name
			$this->load->theme('mainlayout',$data);	  // Loading theme for admin panel
		}
		else{
			$data["do"]="edit";
			$data["resultset"]=$this->page_model->getPageData($pagename);
			$data["item"]="Content";
			$data["master_title"]="Manage pages";   // Please enter the title of page......
			$data["master_body"]="manage_page";  //  Please use view name in this field please do not include '.php' for including view name
			$this->load->theme('mainlayout',$data);	  // Loading theme for admin panel
		}
	}
//for content pages data like about us, privacy police, terme and conditions, career add to database
	public function update_page_to_database(){
		$arr["id"] = mysql_real_escape_string($this->input->post("id"));
		$arr["page_title"] = mysql_real_escape_string($this->input->post("page_title"));
		$arr["page_content"] = $this->input->post("page_content");
		$arr["page_name"] = mysql_real_escape_string($this->input->post("page_name"));
		if($this->page_model->updatepagedata($arr)){
			$this->session->set_flashdata("successmsg","Content updated successfully");
		}	
		else{
			$this->session->set_flashdata("errormsg","There is error updating content to database. Please contact database admin");	
		}
		redirect(base_url()."pages/manage_page/".$arr["page_name"]);
	}

	
//for home page content
	public function home_page(){
		$pagename=$this->uri->segment(3);
		if($pagename=="home_content"){
			$data["do"]="edit";
			$data["resultset"]=$this->common->gethomepagedata($pagename);
			$data["item"]="Homepage Content";
			$data["master_title"]="Manage Homepages";   // Please enter the title of page......
			$data["master_body"]="home_page";  //  Please use view name in this field please do not include '.php' for including view name

			$this->load->theme('mainlayout',$data);	  // Loading theme for admin panel
		}
	}
	
	//for define size content
	public function user_limit(){
	error_reporting(E_ALL);
		//$pagename=$this->uri->segment(3);
	$this->load->model('user_model');
			//$data["do"]="edit";
			//$data["resultset"]=$this->common->gethomepagedata($pagename);
			$data["item"]="Homepage Content";
			$data["master_title"]="Manage Homepages";   // Please enter the title of page......
			$data["master_body"]="users_limit";  //  Please use view name in this field please do not include '.php' for including view name
			$data["SIZE_LIMIT"] =  $this->user_model->user_limit();

			$this->load->theme('mainlayout',$data);	  // Loading theme for admin panel
		
	}
		public function update_user_limit(){
		$data['value'] = mysql_real_escape_string($this->input->post("user_limit"));
		$this->page_model->update_user_limit($data);
		$this->session->set_flashdata("successmsg","Updated successfully");	
		redirect(base_url()."pages/user_limit");		
		}
	
//for update homepage content
	public function update_homepage_to_database(){
		$arr["id"] = mysql_real_escape_string($this->input->post("id"));
		$arr["title"] = mysql_real_escape_string($this->input->post("title"));
		$arr["banner_content"] = mysql_real_escape_string($this->input->post("banner_content"));
		$arr["banner_content1"] = mysql_real_escape_string($this->input->post("banner_content1"));
		$arr["title1"] = mysql_real_escape_string($this->input->post("title1"));
		$arr["content1"] = mysql_real_escape_string($this->input->post("content1"));
		$arr["title2"] = mysql_real_escape_string($this->input->post("title2"));
		$arr["content2"] = mysql_real_escape_string($this->input->post("content2"));
		$arr["title3"] = mysql_real_escape_string($this->input->post("title3"));
		$arr["content3"] = mysql_real_escape_string($this->input->post("content3"));
		$arr["title4"] = mysql_real_escape_string($this->input->post("title4"));
		$arr["content4"] = mysql_real_escape_string($this->input->post("content4"));
		$arr["title5"] = mysql_real_escape_string($this->input->post("title5"));
		$arr["content5"] = mysql_real_escape_string($this->input->post("content5"));
		$arr["page_name"] = mysql_real_escape_string($this->input->post("page_name"));
		$arr["image0"] = mysql_real_escape_string($this->input->post("image0"));
		$arr["image1"] = mysql_real_escape_string($this->input->post("image1"));
		$arr["image2"] = mysql_real_escape_string($this->input->post("image2"));
		$arr["image3"] = mysql_real_escape_string($this->input->post("image3"));
		$arr["image4"] = mysql_real_escape_string($this->input->post("image4"));
		//print_r($_FILES["pics"]["name"]);
		$count = count($_FILES["pics"]["name"]);
		for($i=0;$i<=$count;$i++){
			$j=1;
			if($_FILES['pics']['name'][$i] <> ''){
				$_FILES['userfile']['name'] = $_FILES['pics']['name'][$i];
				$_FILES['userfile']['type'] = $_FILES['pics']['type'][$i];
				$_FILES['userfile']['tmp_name'] = $_FILES['pics']['tmp_name'][$i];
				$_FILES['userfile']['error'] = $_FILES['pics']['error'][$i];
				$_FILES['userfile']['size'] = $_FILES['pics']['size'][$i];
				$arr["image".$i]=$i.$this->common->generate_transaction_number().".".$this->common->get_extension($_FILES['pics']['name'][$i]);
			
				$config['upload_path'] = '../ckeditorimages/';
				$config['allowed_types'] = '*';
				$config['file_name']=$arr["image".$i];
				$this->upload->initialize($config);
			}else{
				$arr["image".$i]=$this->input->post("image".$i);
			}
			if($this->page_model->updatehomepagedata($arr)){
				$err=0;
				if(!empty($_FILES["pics"]["name"])){
					if($this->upload->do_upload()) {
						$this->session->set_flashdata("successmsg","Content updated successfully");
					}else{
						//$this->session->set_flashdata("errormsg","There is some error uploading the files to server. Please contact server admin");	
					}
				}
				$this->session->set_flashdata("successmsg","Content updated successfully");

			}		
			else{
				$err=1;
			    $this->session->set_flashdata("errormsg","There is error updating content to database. Please contact database admin");	

			}		
			$j++;
		}
		redirect(base_url()."pages/home_page/home_content");		
	}
	
//for security page content
	public function security_page(){ 
		$pagename=$this->uri->segment(3);
		if($pagename=="security_page"){
			$data["do"]="edit";
			$data["resultset"]=$this->common->getcontentpagedata($pagename);
			$data["item"]="Security Content";
			$data["master_title"]="Manage Security";   // Please enter the title of page......
			$data["master_body"]="security_page";  //  Please use view name in this field please do not include '.php' for including view name
			$this->load->theme('mainlayout',$data);	  // Loading theme for admin panel
		}
	}
//for security page content data add to database
	public function update_security_to_database(){
		$arr["id"] = mysql_real_escape_string($this->input->post("id"));
		$arr["title"] = mysql_real_escape_string($this->input->post("title"));
		$arr["banner_content"] = mysql_real_escape_string($this->input->post("banner_content"));
		$arr["banner_content1"] = mysql_real_escape_string($this->input->post("banner_content1"));
		$arr["title1"] = mysql_real_escape_string($this->input->post("title1"));
		$arr["content1"] = mysql_real_escape_string($this->input->post("content1"));
		$arr["title2"] = mysql_real_escape_string($this->input->post("title2"));
		$arr["content2"] = mysql_real_escape_string($this->input->post("content2"));
		$arr["title3"] = mysql_real_escape_string($this->input->post("title3"));
		$arr["content3"] = mysql_real_escape_string($this->input->post("content3"));
		$arr["title4"] = mysql_real_escape_string($this->input->post("title4"));
		$arr["content4"] = mysql_real_escape_string($this->input->post("content4"));
		$arr["title5"] = mysql_real_escape_string($this->input->post("title5"));
		$arr["content5"] = mysql_real_escape_string($this->input->post("content5"));
		$arr["page_name"] = mysql_real_escape_string($this->input->post("page_name"));
		$arr["image0"] = mysql_real_escape_string($this->input->post("image0"));
		$arr["image1"] = mysql_real_escape_string($this->input->post("image1"));
		$arr["image2"] = mysql_real_escape_string($this->input->post("image2"));
		$arr["image3"] = mysql_real_escape_string($this->input->post("image3"));
		$arr["image4"] = mysql_real_escape_string($this->input->post("image4"));
		
		$count = count($_FILES["pics"]["name"]);
		for($i=0;$i<=$count;$i++){$j=1;
			if($_FILES['pics']['name'][$i] <> ''){
				$_FILES['userfile']['name'] = $_FILES['pics']['name'][$i];
				$_FILES['userfile']['type'] = $_FILES['pics']['type'][$i];
				$_FILES['userfile']['tmp_name'] = $_FILES['pics']['tmp_name'][$i];
				$_FILES['userfile']['error'] = $_FILES['pics']['error'][$i];
				$_FILES['userfile']['size'] = $_FILES['pics']['size'][$i];
				$arr["image".$i] = $i.$this->common->generate_transaction_number().".".$this->common->get_extension($_FILES['pics']['name'][$i]);
				$config['upload_path'] = '../ckeditorimages/';
				$config['allowed_types'] = '*';
				$config['file_name']=$arr["image".$i];
				$this->upload->initialize($config);
			}else{
				$arr["image".$i]=$this->input->post("image".$i);
			}
			
			if($this->upload->do_upload()) {
				$err=0;
				
			}		
			else{
				$err=1;
			}		
		$j++;}
		$result=$this->page_model->updatehomepagedata($arr);
		if($result){
			$this->session->set_flashdata("successmsg","Content updated successfully");
		}
		else{
			$this->session->set_flashdata("successmsg","There is some error uploading the files to server. Please contact server admin");		
		}
		redirect(base_url()."pages/security_page/security_page");		
				
	}
//for Signup, login page content
	public function signup_page(){
		    $pagename=$this->uri->segment(3);
			$data["do"]="edit";
			$data["resultset"]=$this->common->getcontentpagedata($pagename);
			$data["item"]="Content Page";
			$data["master_title"]="Manage pages";   // Please enter the title of page......
			$data["master_body"]="manage_contentpage";  //  Please use view name in this field please do not include '.php' for including view name
			$this->load->theme('mainlayout',$data);	  // Loading theme for admin panel
	}
	
//for Signup, login page content data into database
	public function update_signuppage_to_database(){
		$arr["id"]=$this->input->post("id");
		$arr["banner_content"]=$this->input->post("banner_content");
		$arr["banner_content1"]=$this->input->post("banner_content1");
		$arr["title1"]=$this->input->post("title1");
		$arr["content1"]=$this->input->post("content1");
		$arr["title2"]=$this->input->post("title2");
		$arr["content2"]=$this->input->post("content2");
		$arr["title3"]=$this->input->post("title3");
		$arr["content3"]=$this->input->post("content3");
		$arr["title4"]=$this->input->post("title4");
		$arr["content4"]=$this->input->post("content4");
		$arr["page_name"]=$this->input->post("page_name");
		$arr["image0"]=$this->input->post("image0");
		$arr["image1"]=$this->input->post("image1");
		$arr["image2"]=$this->input->post("image2");
		$arr["image3"]=$this->input->post("image3");
		
		$count = count($_FILES["pics"]["name"]);
		for($i=0;$i<=$count;$i++){$j=1;
			if($_FILES['pics']['name'][$i] <> ''){
				$_FILES['userfile']['name'] = $_FILES['pics']['name'][$i];
				$_FILES['userfile']['type'] = $_FILES['pics']['type'][$i];
				$_FILES['userfile']['tmp_name'] = $_FILES['pics']['tmp_name'][$i];
				$_FILES['userfile']['error'] = $_FILES['pics']['error'][$i];
				$_FILES['userfile']['size'] = $_FILES['pics']['size'][$i];
				$arr["image".$i] = $i.$this->common->generate_transaction_number().".".$this->common->get_extension($_FILES['pics']['name'][$i]);
				$config['upload_path'] = '../ckeditorimages/';
				$config['allowed_types'] = '*';
				$config['file_name']=$arr["image".$i];
				$this->upload->initialize($config);
			}else{
				$arr["image".$i]=$this->input->post("image".$i);
			}
			if($this->upload->do_upload()) {
				$err=0;
				//$result=$this->page_model->updatehomepagedata($arr);
			}		
			else{
				$err=1;
			}		
			$j++;
		}
		$result=$this->page_model->updatehomepagedata($arr);
		if($result){
			$this->session->set_flashdata("successmsg","Content updated successfully");
		}
		else{
			$this->session->set_flashdata("successmsg","There is some error uploading the files to server. Please contact server admin");		
		}
		redirect(base_url()."pages/signup_page/".$arr["page_name"]);		
	}
	
/********************************************************************/
//for vision page content
	public function vision(){ 
		$pagename=$this->uri->segment(3);
		if($pagename=="vision"){
			$data["do"]="edit";
			$data["resultset"]=$this->common->getcontentpagedata($pagename);
			$data["item"]="Vision";
			$data["master_title"]="Manage Vision";   // Please enter the title of page......
			$data["master_body"]="vision";  //  Please use view name in this field please do not include '.php' for including view name
			$this->load->theme('mainlayout',$data);	  // Loading theme for admin panel
		}
	}
//for security page content data add to database
	public function update_vision_to_database(){
		$arr["id"] = mysql_real_escape_string($this->input->post("id"));
		$arr["title"] = mysql_real_escape_string($this->input->post("title"));
		$arr["content1"] = $this->input->post("content1");
		$arr["content2"] = $this->input->post("content2");
		$arr["content3"] = $this->input->post("content3");
		$arr["content4"] = $this->input->post("content4");
		$arr["image0"] = $this->input->post("image0");
		$arr["image1"] = $this->input->post("image1");
		$arr["image2"] = $this->input->post("image2");
		$arr["image3"] = $this->input->post("image3");
		
		$count = count($_FILES["pics"]["name"]);
		for($i=0;$i<=$count;$i++){$j=1;
			if($_FILES['pics']['name'][$i] <> ''){
				$_FILES['userfile']['name'] = $_FILES['pics']['name'][$i];
				$_FILES['userfile']['type'] = $_FILES['pics']['type'][$i];
				$_FILES['userfile']['tmp_name'] = $_FILES['pics']['tmp_name'][$i];
				$_FILES['userfile']['error'] = $_FILES['pics']['error'][$i];
				$_FILES['userfile']['size'] = $_FILES['pics']['size'][$i];
				$arr["image".$i] = $i.$this->common->generate_transaction_number().".".$this->common->get_extension($_FILES['pics']['name'][$i]);
				$config['upload_path'] = '../ckeditorimages/';
				$config['allowed_types'] = '*';
				$config['file_name']=$arr["image".$i];
				$this->upload->initialize($config);
			}else{
				$arr["image".$i]=$this->input->post("image".$i);
			}
			if($this->upload->do_upload()) {
				$err=0;
				//$result=$this->page_model->updatehomepagedata($arr);
			}		
			else{
				$err=1;
			}		
		$j++;}
		$result=$this->page_model->updatehomepagedata($arr);
		if($result){
			$this->session->set_flashdata("successmsg","Content updated successfully");
		}
		else{
			$this->session->set_flashdata("successmsg","There is some error uploading the files to server. Please contact server admin");		
		}
		redirect(base_url()."pages/vision/vision");		
				
	}
	
	

// function for get image extension
	public function get_image_Extension($imagename){
		$imagename=explode(".",$imagename);
		return $imagename[1];
	}
	
// function for upload imge
	public function upload_images(){
		$config['upload_path'] = '../ckeditorimages/';
		$config['allowed_types'] = '*';
		$config['file_name']=$_FILES['upload']['name'];
		$this->upload->initialize($config);
		$validimages=array("jpg","gif","png","jpeg","bmp");
		if(in_array($this->get_image_Extension($_FILES['upload']['name']),$validimages)){
			if($this->upload->do_upload('upload')){
				$arr=$this->upload->data();
				$url = $this->config->item("ckeditorimages").$arr['file_name'];
			}
			else{
				$message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";	
			}
		}
		else{
			$message = "Only images file with ".implode(",",$validimages)." extensions are allowed";	
		}
		 $funcNum = $_GET['CKEditorFuncNum'] ;
	     echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
	}
	/**********************************************************Page functions starts ************************************************/
}
?>