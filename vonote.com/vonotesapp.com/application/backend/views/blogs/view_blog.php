<div class="viewform">
<table width="850" border="0" cellpadding="5" style="margin-top:50px">
  <tr>
    <td width="150px" align="right" class="label_form"><div class="view1">Blog Title :</div></td>
    <td width="700px"><div class="view2"><?php echo $resultset['blog_title']; ?></div></td>
  </tr>
  <tr>
    <td align="right" class="label_form"><div class="view1">Date Posted :</div></td>
    <td><div class="view2"><?php echo date("d M,Y",$resultset['date_posted']); ?></div></td>
  </tr>
  <tr>
    <td align="right" class="label_form"><div class="view1">Last Modified :</div></td>
    <td><div class="view2"><?php echo date("d M,Y",$resultset['last_modified']); ?></div></td>
  </tr>
  <tr>
      <td align="right" class="label_form">Blog Image </td>
      <td><img src='<?php echo $this->config->item("blogimages");?><?php echo $resultset["blog_images"];?>' height="120px" width="120px"></td>
    </tr>
  <tr>
    <td align="right" class="label_form" valign="top"><div class="view1">Blog :</div></td>
    <td><div class="view2"><?php echo nl2br($resultset['blog_content']); ?></div></td>
  </tr>
</table>
<div style="width:100%;margin-left:225px"> <a class="a_button" href="<?php echo BASEURL;?>blogs/manage_blog">Close</a> <!--a class="a_button" href="<?php echo BASEURL;?>blogs/archive_blog/<?php echo $resultset['blogid'] ?>">Comment</a--> </div>
