<div class="form5">
<form id="add" name="add" method="post" action="<?php echo BASEURL;?>blogs/add_blog_to_database" enctype="multipart/form-data">
  <table width="950" border="0" cellpadding="5">
    <?php if($do=="edit"){ 	?>
    <input type="hidden" name="id" value="<?php echo $blogdata['blogid'];?>">
    <?php }	?>
    <tr>
      <td align="right" class="label_form">Blog Title: </td>
      <td><input name="blog_title" type="text" class="input_text" value="<?php echo $blogdata['blog_title'];?>"  />
        <br class="clear" /></td>
    </tr>
    <tr>
    <tr>
      <td align="right" class="label_form">Blog Image </td>
      <td><input name="userfile" type="file" /></td>
    </tr>
    <?php if($do=="edit"){ ?>
    <tr>
      <td align="right" class="label_form">Current Image </td>
      <td><img src='<?php echo $this->config->item("blogimages");?><?php echo $blogdata["blog_images"];?>' height="120px" width="120px"></td>
      <input type="hidden" name="blog_images" value="<?php echo $blogdata["blog_images"];?>">
    </tr>
    <?php } ?>
    <tr>
      <td align="right" valign="top" style="padding-top:25px;" class="label_form">Blog Content: </td>
      <td><textarea name="blog_content" class="input_textarea" rows="10" style="width:600px;"><?php echo $blogdata['blog_content'];?></textarea></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><div class="btn">
          <input class="button" name="submitbut" value="Save" type="submit" />
          <a class="a_button" href="<?php echo base_url(); ?>blogs/manage_blog">Cancel</a> </div></td>
    </tr>
  </table>
</form>
