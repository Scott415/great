<div class="down_category" style="margin-top:30px">
  <div class="head"> <span class="head_text">Country</span>
  <?php if($this->common->show_hide_previleages('add')){ ?>
    <div class="add"> <a href="<?php echo BASEURL;?>country/add_state"> <span class="add1"> <span class="head_text1" style="font-size:15px;"> <?php echo $item; ?></span> </span> </a> </div>
   <?php }?>
    <div class="resetallpage"><a href="<?php echo base_url(); ?>country/manage_state">Reset</a></div>
    <div class="search">
      <form method="GET" action="<?php echo BASEURL;?>country/manage_state/">
       <?php $search=$this->input->get("search"); ?>
        <input type="text" name="search" placeholder="search" value="<?php echo ($search!='' && $search!='search')?$search:''; ?>" onfocus="if(this.value=='search'){this.value=''}" onblur="if(this.value==''){this.value='search'}"/>
        <input type="submit" value="Search" name="Submitbut" class="side_search"  />
      </form>
    </div>
  </div>
  <br/>
  <form action="<?php echo BASEURL;?>country/archive_state" method="post" name="<?php echo $item; ?>" onsubmit="return archive_fun('<?php echo $item?>');">
    <input name="done" type="hidden" value="send" />
    <table width="100%" border="0" cellspacing="0" cellpadding="8px"  class="maintbl" >
      <tr class="fstrow"  >
        <td width="4%"><input class="check" type="checkbox" id="mainchbx" name="chk" /></td>
        <td align="left" width="24%" style="padding-left:10px">State Name</td>
         <td align="left" width="24%" style="padding-left:10px">Country Name</td>
        <td align="left" width="8%">Position</td>
        <td align="center" width="8%">Displayed</td>
        <td align="center" width="20%">Actions</td>
      </tr>
     <?php 
	 if(count($resultset)!=0){
	 $z=0;
	 foreach($resultset as $key=>$val){
		 //echo $val['Country'];?>
       <tr class="scndrow <?php echo $z%2 ? '' : 'alternate';?>">
        
          <td><input type='checkbox' name='chk[]'value="<?php echo $val['StateId'] ?>" id='checkme<?php echo $z; ?>'/></td >
          <td align="left"><?php echo $val['State'];?></td>
           <td align="left"><?php echo $val['Country'];?></td>
          <td align="left">
          <a href="#" class="thickbox">
          <img src="<?php echo BASEURL;?>images/sort-neither.png" title="Sort" width="13" height="16" />
          </a>
          </td>
          <td align="center"><?php if($val['state_status'] == '1'){ ?>
          <a href="<?php echo BASEURL?>country/enable_disable_state/<?php echo $val['StateId'];?>/0" onclick="return dis_fun('<?php echo $item?>');"><img src='<?php echo BASEURL;?>images/enabled.gif' title='Disable' width='16' height='16' /></a>
          <?php }else{ ?>
          <a href="<?php echo BASEURL?>country/enable_disable_state/<?php echo $val['StateId'];?>/1" onclick="return enb_fun('<?php echo $item?>');"><img src='<?php echo BASEURL;?>images/disabled.gif' title='Enable' width='16' height='16' /></a>
          <?php } ?></td>
          <td align="center">
              <!--a style="margin:0px 8px;" href="#" title="view"> <img src="<?php //echo BASEURL;?>images/view.png" /> </a-->
              <a style="margin:0px 8px;" href="<?php echo BASEURL?>country/edit_state/<?php echo $val['StateId'];?>" title="Edit"> <img src="<?php echo BASEURL;?>images/edit.png" /> </a> 
              <a style="margin:0px 8px;" href="<?php echo BASEURL?>country/archive_state/<?php echo $val['StateId'];?>" title="Delete" onclick="return del_fun('<?php echo $item?>');"> 
              <img src="<?php echo BASEURL;?>images/delete.png" /> 
              </a> 
        </td>
      </tr>
     <?php $z++;
	 }
	 }
	 else{
	?>
	 <tr class="scndrow">
	 <td colspan="5" align="center">No record found</td>
	 </tr>
	 <?php	} ?>
    </table>
     <div class="clear"></div>
    <div style="padding-top: 10px;">
    <?php echo $this->pagination->create_links();?>
    </div>
    <div class="clear"></div>
    <?php  if(count($resultset)!=0){?>
    <div class="slideshowdelend"> 
      <input type="submit" name="Delete" value="Delete Selected" class="formbutton" />
    </div>
    <?php }?>
  </form>
</div>

