<script type="text/javascript">
	$(document).ready(function() {
		$('#CountryId').change(function() {
			var CountryId = $('#CountryId').val();
			 console.log(CountryId);
			$.ajax({
					type: 'POST',
                    url: "<?php echo base_url();?>ajax/showState/"+CountryId,
                    data: '',
                    success: function (State) {
                       $.each(jQuery.parseJSON(State), function(StateId, State) //here we're doing a foeach loop round each city with id as the key and city as the value
                            {
                                var opt = $('<option />'); // here we're creating a new select option with for each city
                                opt.val(StateId);
                                opt.text(State);
                                $('#StateId').append(opt); //here we will append these new select options to a dropdown with the id 'cities'
                            });
                    }

                });
			});
    });
</script>
<div class="form5">
<form id="add" name="add" method="post" action="<?php echo BASEURL;?>country/add_city_to_database">
  <table width="950" border="0" cellpadding="5">
    <?php if($do=="edit"){?>
    <input type="hidden" name="cityId" value="<?php echo $citydata['cityId'];?>"/>
    <?php }?>
    <tr>
      <td align="right" class="label_form">Select Country: </td>
       <?php if($resultset=$this->country_model->SelectCountryData())
	  { ?>
      <td>
      	<select name="CountryId" id="CountryId" style="width:158px">
        	<option value="">Select Country</option>
            <?php foreach($resultset as $key=>$val){?>
            <option value="<?php echo $val['CountryId'];?>"<?php if($citydata['CountryId']==$val['CountryId']){echo "selected=selected";}?>><?php echo $val['Country']?></option>
            <?php }?>
        </select>
     </td>
     <?php }?>
    </tr>
    <tr>
      <td align="right" class="label_form">Select State: </td>
      <td>
      	<?php if($do=="edit"){
			$resultset=$this->country_model->SelectStateData();?>
            <select name="StateId" style="width:158px" id="StateId">
        	<option value="">Select State</option>
            <?php foreach($resultset as $key=>$val){?>
            <option value="<?php echo $val['StateId'];?>"<?php if($citydata['StateId']==$val['StateId']){echo "selected=selected";}?>><?php echo $val['State']?></option>
            <?php }?>
        </select>
        <?php }else{?>
      	<select name="StateId" style="width:158px" id="StateId">
        	<option value="">Select State</option>
        </select>
        <?php }?>
      </td>
    </tr>
    <tr>
      <td align="right" class="label_form">City Name: </td>
      <td><input name="city" type="text" class="input_text" value="<?php echo $citydata['city']?>"  />
        <br class="clear" /></td>
    </tr>
    <tr>
    
    <tr>
      <td>&nbsp;</td>
      <td><div class="btn">
          <input class="button" name="submitbut" value="Save" type="submit" />
          <a class="a_button" href="<?php echo base_url(); ?>country/manage_city">Cancel</a> </div></td>
    </tr>
  </table>
</form>
<div>
