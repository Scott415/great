<div style="margin-top: 20px;"></div>
<div class="down_category" style="margin-top:30px">
  <div class="head"> <span class="head_text"><?php echo $item; ?></span>
    <?php if($this->common->show_hide_previleages('add')){ ?>
    <div class="add"> <a href="<?php echo BASEURL;?>flashmessage/add_flashmessage"> <span class="add1"> <span class="head_text1" style="font-size:14px;"> <?php echo $item; ?></span> </span> </a></div>
    <?php	}  ?>
    <div class="resetallpage"><a href="<?php echo base_url(); ?>flashmessage/manage_flashmessage">Reset</a></div>
  </div>
  <div class="clear"></div>
  <form action="<?php echo BASEURL;?>flasmessage/archive_flasmessage" method="post" name="<?php echo $item; ?>" onsubmit="return archive_fun('<?php echo $item?>');">
    <input name="done" type="hidden" value="send" />
    <table width="100%" cellspacing="0" cellpadding="0"  class="maintbl" >
      <tr class="fstrow"  >
        <?php if($this->common->show_hide_previleages('delete')){?>
        <td width="5%"><input class="check" type="checkbox" id="mainchbx" name="chk" /></td>
        <?php	}?>
        <td align="left" width="55%">Flash Message</td>
        <?php if($this->common->show_hide_previleages('enable/disable')){  ?>
        <td align="center" width="15%">Displayed</td>
        <?php }	?>
        <td align="center" width="20%"><?php if($this->common->checkActionTab()){ 	?>
          Actions
          <?php }	?></td>
      </tr>
      <?php if(count($resultset)!=0){
			$z=0;
			foreach($resultset as $key=>$val){
			?>
      <tr class="scndrow <?php echo $z%2 ? '' : 'alternate';?>">
        <?php if($this->common->show_hide_previleages('delete')){ ?>
        <td><input type='checkbox' name='chk[]' value="<?php echo $val['flash_message_id'] ?>" id='checkme<?php echo $z; ?>' /></td >
        <?php }	?>
        <td align='left'><?php echo substr($val['flash_message_title'],0,100); ?></td>
        <?php
			if($this->common->show_hide_previleages('enable/disable')){ ?>
        <td align="center"><?php if($val['flash_message_status'] == '1'){ ?>
          <a href="<?php echo BASEURL?>flashmessage/enable_disable_flashmessage/<?php echo $val['flash_message_id'];?>/0" onclick="return dis_fun('<?php echo $item?>');"><img src='<?php echo BASEURL;?>images/enabled.gif' title='Disable' width='16' height='16' /></a>
          <?php }else{ ?>
          <a href="<?php echo BASEURL?>flashmessage/enable_disable_flashmessage/<?php echo $val['flash_message_id'];?>/1" onclick="return enb_fun('<?php echo $item?>');"><img src='<?php echo BASEURL;?>images/disabled.gif' title='Enable' width='16' height='16' /></a>
          <?php } ?></td>
        <?php }	?>
        <td><?php
			if($this->common->show_hide_previleages('edit'))
			{
	  	  	?>
          <a style="margin:0px 10px;" href="<?php echo BASEURL?>flashmessage/edit_flashmessage/<?php echo $val['flash_message_id'];?>" title="Edit"> <img src="<?php echo BASEURL ?>images/edit.png" /> </a>
          <?php
			}
			?>
          <?php if($this->common->show_hide_previleages('delete')){ ?>
          <a style="margin:0px 10px;" href="<?php echo BASEURL?>flashmessage/archive_flashmessage/<?php echo $val['flash_message_id'];?>" title="Delete" 
            onclick="return archive_fun('<?php echo $item?>');"> <img src="<?php echo BASEURL ?>images/delete.png" /> </a>
          <?php } ?></td>
      </tr>
      <?php	$z++;
			}
			}
			else{?>
      <tr class="scndrow">
        <td colspan="5" align="center">No record found</td>
      </tr>
      <?php	} ?>
    </table>
    <?php echo $this->pagination->create_links();?>
    <div class="clear"></div>
    <?php  if(count($resultset)!=0){  ?>
    <div class="slideshowdelend">
      <input type="submit" name="Delete" value="Delete Selected" class="formbutton" />
    </div>
    <?php } ?>
  </form>
</div>
