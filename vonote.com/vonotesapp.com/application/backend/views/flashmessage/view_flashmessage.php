<div class="viewform">
         <table width="850" border="0" cellpadding="5">
      
          <tr>
            <td width="150px" align="right" class="label_form"><div class="view1">Blog title :</div></td>
         	<td width="700px"><div class="view2"><?php echo $resultset['blog_title']; ?></div></td>
         </tr>
          <tr>
            <td align="right" class="label_form"><div class="view1">Blog Category :</div></td>
         	<td><div class="view2"><?php echo $resultset['category_name']; ?></div></td>
         </tr>
         <tr>
         	<td align="right" class="label_form"><div class="view1">Date Posted :</div></td>
            <td><div class="view2"><?php echo date("d M,Y",$resultset['date_posted']); ?></div></td>
         </tr>
          <tr>
         	<td align="right" class="label_form"><div class="view1">Last Modified :</div></td>
            <td><div class="view2"><?php echo date("d M,Y",$resultset['last_modified']); ?></div></td>
         </tr>
          <tr>
         	<td align="right" class="label_form"><div class="view1">Blog :</div></td>
            <td><div class="view2"><?php echo $resultset['blog_content']; ?></div></td>
         </tr>
         
         </table>
         <div style="width:100%;margin-left:225px">
         <a class="a_button" href="<?php echo BASEURL;?>blogs/manage_blog">Close</a>
           <a class="a_button" href="<?php echo BASEURL;?>blogs/archive_blog/<?php echo $resultset['blogid'] ?>">Archive</a>
           <?php
		   if($resultset['status']==1)
		   {
		   ?>
           <a class="a_button" href="<?php echo BASEURL;?>blogs/enable_disable_blog/<?php echo $resultset['blogid'] ?>/0">Deactivate</a>
           <?php
		   }
		   else
		   {
		   ?>
           <a class="a_button" href="<?php echo BASEURL;?>blogs/enable_disable_blog/<?php echo $resultset['blogid'] ?>/1">Activate</a>
           <?php
		   }
		   ?>
        </div>
     