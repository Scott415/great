  <?php $no_of_files = $this->user_model->no_of_files_uploaded_by_user($resultset['id']);  ?>

<div class="viewform" style="min-height:300px">
         <?php if(!is_array($resultset)){ ?>
         <table width="750" border="0" cellpadding="5">
              <tr>
                <td align="left" class="label_form"><div class="view1" style="color:#AD1111; font-weight:900"><img src="<?php echo base_url(); ?>images/error000.png"  /> No User Found</div></td>
                <td><div class="view2"></div></td>
             </tr>
         </table>
         <?php } else{ ?>
         <table width="1150" border="0" cellpadding="5">
          <tr>
            <td align="right" class="label_form"><div class="view1">First name :</div></td>
         	<td><div class="view2"><?php echo ucwords($resultset['first_name']); ?></div></td>
         </tr>
         
          <tr>
            <td align="right" class="label_form"><div class="view1">Last name :</div></td>
         	<td><div class="view2"><?php echo ucwords($resultset['last_name']); ?></div></td>
         </tr>
         <tr>
         	<td align="right" class="label_form"><div class="view1">Image :</div></td>
            <td><div class="view2" style="width:620px;"><div class="pic">
          		  <img src="<?php if($resultset['image'] <> ''){ echo $this->config->item("blogimages").$resultset['image']; } else{ echo base_url().'images/user_image.png';}?>" alt="" width="98" height="98" />           
          	    </div>
            </td>
         </tr>
         <tr>
         	<td align="right" class="label_form"><div class="view1">Email :</div></td>
            <td><div class="view2" style="width:620px;"><?php echo $resultset['email']  ?></div></td>
         </tr>
         <!-- <tr>
            <td align="right" class="label_form"><div class="view1">Password :</div></td>
         	<td><div class="view2"><?php echo $resultset['password']; ?></div></td>
         </tr> -->
          <tr>
         	<td align="right" class="label_form"><div class="view1">Total Files :</div></td>
            <td><div class="view2" style="width:620px;"><?php echo $no_of_files['totalfile']; ?></div></td>
         </tr> 
          <tr>
         	<td align="right" class="label_form"><div class="view1"> Status :</div></td>
            <td><div class="view2" style="width:620px;"><?php echo ($resultset['status']=="1")?"<font color='green'>Active</font>":"<font color='red'>Inactive</font>"; ?></div></td>
         </tr>
			
			<tr>
         	<td align="right" class="label_form"><div class="view1"> Used Space :</div></td>
            <td><div class="view2" style="width:620px;"><?php echo $used;?></div></td>
         </tr>
			<tr>
         	<td align="right" class="label_form"><div class="view1"> Remaining Space :</div></td>
            <td><div class="view2" style="width:620px;"><?php echo $disk_remaining; ?></div></td>
         </tr>
			
			<tr>
         	<td align="right" class="label_form"><div class="view1"> Date Signed Up :</div></td>
            <td><div class="view2" style="width:620px;"><?php echo $resultset['date_time']; ?></div></td>
         </tr>
			<tr>
         	<td align="right" class="label_form"><div class="view1"> IP Address(Last signIn) :</div></td>
            <td><div class="view2" style="width:620px;"><?php echo $resultset['last_ip_address']; ?></div></td>
         </tr>
			
         </table>
         <div style="width:100%;margin-left:100px"><a class="a_button" href="<?php echo BASEURL;?>users/manage_user">Close</a></div>
         <?php } ?>
  </div>
